<!-- BEGIN: Subheader -->
<style type="text/css">
	.box-wrapper{border:solid 1px #CCC;padding:10px;margin-bottom: 25px;}
	.box-wrapper.red-bg{background-color:#f44336;}
	.box-wrapper.yellow-bg{background-color:#ff9800;}
	.box-wrapper.purple-bg{background-color:#9c27b0;}
	.box-wrapper.blue-bg{background-color:#55acee;}
	.box-wrapper.green-bg{background-color:#5fd35f;}
	.box-wrapper.cyan-bg{background-color:#29b5bc;}
	.box-wrapper h4{color:#FFF;}
	.box-wrapper p{color:#FFF; font-size:24px; font-weight:bold;}
	.box-wrapper .fa{color:#FFF; font-size:50px; font-weight:bold;padding: 15px 5px;}
	.box-wrapper a{text-decoration: none;}
</style>
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Dashboard</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-md-3">
			<div class="box-wrapper red-bg">
				<a href="<?php echo base_url(ADMIN_BASE.'users/list')?>">
					<div class="pull-right">
						<i class="fa fa-user"></i>
					</div>
					<h4>Total User:</h4>
					<p><?php if(isset($total_user)){echo $total_user;}else{ echo '0';} ?></p>
				</a>
			</div>
		</div>
		<div class="col-md-3">
			<div class="box-wrapper blue-bg">
				<a href="<?php echo base_url(ADMIN_BASE.'subjects/list')?>">
					<div class="pull-right">
						<i class="fa fa-book"></i>
					</div>
					<h4>Total Subject:</h4>
					<p><?php if(isset($total_subject)){echo $total_subject;}else{ echo '0';} ?></p>
				</a>
			</div>
		</div>
		<div class="col-md-3">
			<div class="box-wrapper green-bg">
				<a href="<?php echo base_url(ADMIN_BASE.'standards/list')?>">
					<h4>Total Standard:</h4>
					<p><?php if(isset($total_standard)){echo $total_standard;}else{ echo '0';} ?></p>
				</a>
			</div>
		</div>
		<div class="col-md-3">
			<div class="box-wrapper cyan-bg">
				<a href="<?php echo base_url(ADMIN_BASE.'exams/list')?>">
					<h4>Total Upcoming Exam:</h4>
					<p><?php if(isset($total_upcoming_exam)){echo $total_upcoming_exam;}else{ echo '0';} ?></p>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="box-wrapper yellow-bg">
				<a href="<?php echo base_url(ADMIN_BASE.'exams/list')?>">
					<h4>Total Exam:</h4>
					<p><?php if(isset($total_exam)){echo $total_exam;}else{ echo '0';} ?></p>
				</a>	
			</div>
		</div>
		<div class="col-md-3">
			<div class="box-wrapper purple-bg">
				<a href="<?php echo base_url(ADMIN_BASE.'exams/list')?>">
					<h4>Total Pending Result:</h4>
					<p><?php if(isset($total_pending_result)){echo $total_pending_result;}else{ echo '0';} ?></p>
				</a>
			</div>
		</div>
	</div>		
</div>

<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Exam list</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-4">
							<div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="exam_datatableurl" value="<?php echo base_url(ADMIN_BASE.'dashboard/exam_dataTables');?>">
		<input type="hidden" id="edit_exam_url" value="<?php echo base_url(ADMIN_BASE.'exams/edit/');?>">
		<input type="hidden" id="delete_exam_url" value="<?php echo base_url(ADMIN_BASE.'exams/delete/');?>">
		<input type="hidden" id="questionselection_url" value="<?php echo base_url(ADMIN_BASE.'exams/question_selection/');?>">
		<input type="hidden" id="exam_result_url" value="<?php echo base_url(ADMIN_BASE.'exams/exam_result/');?>">
		<input type="hidden" id="exam_book_url" value="<?php echo base_url(ADMIN_BASE.'exams/exam_book/');?>">
		<div class="exam_table" id="ajax_data"></div>		
</div>


<!-- Delete popupModal -->
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="delModal">
    <div class="modal-dialog" role="document">
      	<div id="modal_datas">
	        <div class="modal-content">
	          	<div class="modal-header">
		            <h2 class="modal-title" id="delModal" style="text-align:center">Are You Sure you want to delete?</h2>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		              	<span aria-hidden="true">&times;</span>
		            </button>
	          	</div>
				<div class="modal-body">
					<input type="hidden" class="form-control" name="del_id" id="del_id">
					<div class="form-Action " style="text-align:center">
						<button class="btn btn-default" onclick="delete_record();">Delete</button>                
						<button class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>     
	      	</div>
    	</div>  
  	</div>
</div>
<!-- End popupModal -->


<!-- view popupModal -->
<div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModal">
	<div class="modal-dialog" role="document">
		  <div id="modal_datas">
			    <div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="dataModal" style="text-align:center">Info</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						
					</div>
			      	<div class="modal-body" id="Wrapper-notify">
				          
			      	</div>     
			  	</div>
		</div>  
	</div>
</div>
<!-- End popupModal -->