<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Edit Discount</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Discount Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="discount-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				
				<?php 
			      	$discountform_edit = array('id'=>'editform_discount','autocomplete'=>'off','name'=>'editform_discount','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'discounts/update',$discountform_edit);   
				?>
				<div class="m-portlet__body">
					<?php  
							$discount_id = !isset($discount_data->id)?'':$discount_data->id;
							$title_val = !isset($discount_data->title)?'':$discount_data->title;
							$discount_code_val = !isset($discount_data->discount_code)?'':$discount_data->discount_code;
							$discount_type_val = !isset($discount_data->discount_type)?'':$discount_data->discount_type;
							$amount_val = !isset($discount_data->amount)?'':$discount_data->amount;
							$per_user_use_count_val = !isset($discount_data->per_user_use_count)?'':$discount_data->per_user_use_count;
							$total_use_count_val = !isset($discount_data->total_use_count)?'':$discount_data->total_use_count;
							
					?>
					<input type="hidden" id="discount_id" name="discount_id" value="<?php echo $discount_id;?>">

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Title:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $title = array ("name" => "title", "id"=>"title","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Title","value"=>$title_val);
				                  echo form_input($title);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Discount code:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $discount_code = array ("name" => "discount_code", "id"=>"discount_code","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Discount Code","value"=>$discount_code_val);
				                  echo form_input($discount_code);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Discount type:
						</label>
						<div class="col-lg-6">
							<select name="discount_type" id="discount_type" class='form-control m-input'>
							   <option value="">Select discount type</option>
							   <option value="1" <?php if(isset($discount_data)){ if($discount_type_val == '1'){ echo 'Selected';}} ?>>Fixed</option>
							   <option value="2"<?php if(isset($discount_data)){ if($discount_type_val == '2'){ echo 'Selected';}} ?>>Percentage</option>
							</select>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Amount:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $amount = array ("name" => "amount", "id"=>"amount","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter amount","value"=>$amount_val,"maxlength"=>10);
				                  echo form_input($amount);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Per user use count:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $per_user_use_count = array ("name" => "per_user_use_count", "id"=>"per_user_use_count","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>$per_user_use_count_val);
				                  echo form_input($per_user_use_count);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Total use count:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $total_use_count = array ("name" => "total_use_count", "id"=>"total_use_count","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>$total_use_count_val);
				                  echo form_input($total_use_count);                                                    
				            ?>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'discounts/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



