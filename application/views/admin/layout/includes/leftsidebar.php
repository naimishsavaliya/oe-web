<!-- BEGIN: Aside Menu -->
<div id="m_ver_menu"
	class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
	m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
	<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
		<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a
			href="<?php echo base_url(ADMIN_BASE.'Dashboard/dashboard');?>" class="m-menu__link "> <i
				class="m-menu__link-icon flaticon-line-graph"></i> <span
				class="m-menu__link-title"> <span class="m-menu__link-wrap"> <span
						class="m-menu__link-text"> Dashboard </span> <!-- <span
						class="m-menu__link-badge"> <span class="m-badge m-badge--danger">
								2 </span>
					</span> -->
				</span>
			</span>
		</a></li>
		<li class="m-menu__section">
			<h4 class="m-menu__section-text">Components</h4> <i
			class="m-menu__section-icon flaticon-more-v3"></i>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='users')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-share"></i> <span
				class="m-menu__link-text"> User management </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='users' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'users/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add User </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='users' && ( $this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit' || $this->uri->segment(3)=='changepassword') )?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'users/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> User List</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='subjects')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-interface-9"></i> <span
				class="m-menu__link-text"> Subject management </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='subjects' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'subjects/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add Subject </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='subjects' && ($this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'subjects/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Subject List</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='standards')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-exclamation"></i> <span
				class="m-menu__link-text"> Standards management </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='standards' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'standards/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add Standards </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='standards' && ( $this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'standards/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Standards List</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='questions')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-interface-1"></i> <span
				class="m-menu__link-text"> Questions management </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='questions' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'questions/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add Questions </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='questions' && ($this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'questions/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Questions List</span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='questions' && $this->uri->segment(3)=='import')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'questions/import'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Questions Import</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='exams')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-interface-1"></i> <span
				class="m-menu__link-text"> Exam management </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='exams' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'exams/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add Exam </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='exams' && ($this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit' || $this->uri->segment(3)=='question_selection' || $this->uri->segment(3)=='exam_result' || $this->uri->segment(3)=='user_viewquestion' || $this->uri->segment(3)=='exam_book'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'exams/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Exam List</span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='exams' && ($this->uri->segment(3)=='transaction_list'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'exams/transaction_list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Transaction List</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='levels')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-interface-1"></i> <span
				class="m-menu__link-text"> Promotion </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='levels' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'levels/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add level </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='levels' && ($this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'levels/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Level List</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='discounts')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-interface-1"></i> <span
				class="m-menu__link-text"> Discount </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='discounts' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'discounts/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add discount </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='discounts' && ($this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'discounts/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Discount List</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='email_template')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-interface-1"></i> <span
				class="m-menu__link-text"> Email template </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='email_template' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'email_template/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add email template </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='email_template' && ($this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'email_template/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Email template List</span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='email_template' && ($this->uri->segment(3)=='setting' || $this->uri->segment(3)=='setting_edit'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'email_template/setting'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> General Setting</span>
					</a></li>
				</ul>
			</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu <?php echo ($this->uri->segment(2)=='notification')?'m-menu__item--expanded m-menu__item--open':'';?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
				class="m-menu__link m-menu__toggle"> <i
				class="m-menu__link-icon flaticon-interface-1"></i> <span
				class="m-menu__link-text"> Notification </span> <i
				class="m-menu__ver-arrow la la-angle-right"></i>
			</a>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='notification' && $this->uri->segment(3)=='add')?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'notification/add'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Add Notification </span>
					</a></li>
					<li class="m-menu__item <?php echo ($this->uri->segment(2)=='notification' && ($this->uri->segment(3)=='list' || $this->uri->segment(3)=='edit' || $this->uri->segment(3)=='users_list'))?'m-menu__item--active':'';?>" aria-haspopup="true"><a
						href="<?php echo base_url(ADMIN_BASE.'notification/list'); ?>" class="m-menu__link "> <i
							class="m-menu__link-bullet m-menu__link-bullet--dot"> <span></span>
						</i> <span class="m-menu__link-text"> Notification List</span>
					</a></li>
				</ul>
			</div>
		</li>
	</ul>
</div>
<!-- END: Aside Menu -->