<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Transaction List</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-12 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-3">
							<div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div>
						</div>
						<div class="col-md-3 form-inline">
							<label>Start date:&nbsp;</label>
					     	<?php                                             
				                  $start_date = array ("name" => "start_date", "id"=>"start_date","type"=>"date",
				                                 "class" => "form-control m-input");
				                  echo form_input($start_date);                                                    
				            ?>
						</div>
						<div class="col-md-3 form-inline">
							<label>End date:&nbsp;</label>
					     	<?php                                             
				                  $end_date = array ("name" => "end_date", "id"=>"end_date","type"=>"date",
				                                 "class" => "form-control m-input");
				                  echo form_input($end_date);                                                    
				            ?>
						</div>
						<div class="col-md-3">
							<input type="button" name="search" id="search" value="Submit" class="btn btn-info" />
						</div>
					</div>
				</div>
				<div class="col-xl-4 order-1 order-xl-2 m--align-right">
				<div class="m-separator m-separator--dashed d-xl-none"></div>
				</div>
			</div>
		</div>
		<input type="hidden" id="transaction_datatableurl" value="<?php echo base_url(ADMIN_BASE.'exams/transaction_dataTables');?>">
		<div class="transaction_table" id="ajax_data"></div>		
</div>



