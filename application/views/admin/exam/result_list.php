<!-- BEGIN: Subheader -->
<style type="text/css">
	.error{
		color: red;
	}
</style>
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto" style="width: 100%">
			<h3 class="m-subheader__title ">Exam Result List</h3>
			<a href="<?php echo base_url(ADMIN_BASE.'exams/list');?>" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill pull-right">
				<span><i class="la la-arrow-left"></i><span>Back</span></span>
			</a>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
		<div style="margin-top: 20px;">
			<div class="col-lg-8">
				<?php $is_error = false; ?>
				<?php if(validation_errors() != ''){ $is_error = true; ?>
				    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
				       <?php echo validation_errors(); ?>
				    </div>
				<?php } ?>
				<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
				    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
				       <?php echo $this->session->flashdata('success'); ?>
				    </div>
				<?php } ?>
				<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
				    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
				       <?php echo $this->session->flashdata('error'); ?>
				    </div>
				<?php } ?>
				<div class="alert" id="exam-register-error-msg" style="display:none"></div>
			</div>	
		</div>
		<div class="alert" id="exam-result-error-msg" style="display:none"></div>
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-4">
							<div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
				</div>
				<?php if($result_status==0){?>
				<div class="col-xl-4 order-1 order-xl-2 m--align-right">
					<a href="<?php echo base_url(ADMIN_BASE.'exams/generate_result/'.$exam_id);?>" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
					<span><i class="la la-cog"></i><span>Generate result</span></span>
					</a>
				<div class="m-separator m-separator--dashed d-xl-none"></div>
				</div>
				<?php } ?>
			</div>
		</div>
		<input type="hidden" id="list_exam_result" value="<?php echo base_url(ADMIN_BASE.'exams/result_dataTables/'.$exam_id);?>">
		<input type="hidden" id="user_viewquestion" value="<?php echo base_url(ADMIN_BASE.'exams/user_viewquestion/');?>">
		<div class="exam_result_table" id="ajax_data"></div>		
</div>

<!-- Delete popupModal -->
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="delModal">
    <div class="modal-dialog" role="document">
      	<div id="modal_datas">
	        <div class="modal-content">
	          	<div class="modal-header">
		            <h2 class="modal-title" id="delModal" style="text-align:center">Are You Sure you want to delete?</h2>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		              	<span aria-hidden="true">&times;</span>
		            </button>
	          	</div>
				<div class="modal-body">
					<input type="hidden" class="form-control" name="del_id" id="del_id">
					<div class="form-Action " style="text-align:center">
						<button class="btn btn-default" onclick="delete_record();">Delete</button>                
						<button class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>     
	      	</div>
    	</div>  
  	</div>
</div>
<!-- End popupModal -->


<!-- view popupModal -->
<div class="modal fade" id="rankModal" tabindex="-1" role="dialog" aria-labelledby="rankModal">
	<div class="modal-dialog" role="document">
		  <div id="modal_datas">
			    <div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="rankModal" style="text-align:center">Rank Form</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form id="RankFrm" name="RankFrm" method="post" action="">
				      	<div class="modal-body" id="Wrapper-notify">
				      	
				      	</div>
				      	<div class="modal-footer">
					      	<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
					      		Submit
					      	</button>
				      		<button type="button" class="btn btn-secondary" onclick="closeRankPopup();">Close</button>
				      	</div>
				    </form>     
			  	</div>
		</div>  
	</div>
</div>
<!-- End popupModal -->




