<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">User Question List</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-4">
							<div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 order-1 order-xl-2 m--align-right">
					<a href="<?php echo base_url(ADMIN_BASE.'exams/exam_result/'.$exam_id);?>" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
					<span><i class="la la-arrow-left"></i><span>
						Back
					</span>
					</span>
					</a>
				<div class="m-separator m-separator--dashed d-xl-none"></div>
				</div>
			</div>
		</div>
		<input type="hidden" id="list_question" value="<?php echo base_url(ADMIN_BASE.'exams/userquestion_dataTables/'.$exam_id.'/'.$user_id);?>">
		<div class="user_question_table" id="ajax_data"></div>		
</div>



