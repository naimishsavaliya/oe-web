<style type="text/css">
	.error{
		color: red;
	}
	.btn-container {
	    float: right;
	    margin-top: 15px;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto" style="width: 100%">
			<h3 class="m-subheader__title ">Question Selection</h3>
			<a href="<?php echo base_url(ADMIN_BASE.'exams/list');?>" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill pull-right">
				<span><i class="la la-arrow-left"></i><span>Back</span></span>
			</a>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title pull-left">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Question Selection Form
							</h3>							
						</div>
						<div class="btn-container">
							<button class="btn btn-info"><?php echo $subject_name;?></button>&nbsp;
							<button class="btn btn-primary"><?php echo $standard_name;?></button>
							<?php if(isset($selected_que_count) && $selected_que_count!='0'){ ?>
							<button class="btn btn-warning"><?php echo 'Selected Question Count: &nbsp;'.$selected_que_count;?></button>
							<?php } ?>

						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="exam-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				<?php 
			      	$question_selection = array('id'=>'question_selection','autocomplete'=>'off','name'=>'question_selection','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'exams/exam_question_save',$question_selection);   
				?>
				<div class="m-portlet__body">
					<input type="hidden" id="manual_questions_url" value="<?php echo base_url(ADMIN_BASE.'exams/manual_question/').$standard_id.'/'.$subject_id.'/'.$exam_id;?>">
					<input type="hidden" id="auto_questions_url" value="<?php echo base_url(ADMIN_BASE.'exams/auto_question/').$standard_id.'/'.$subject_id.'/'.$maximum_question;?>">
					<input type="hidden" id="examlist_url" value="<?php echo base_url(ADMIN_BASE.'exams/list');?>">
					<input type="hidden" id="exam_id" name="exam_id" value="<?php echo $exam_id;?>">
					<input type="hidden" id="subject_id" name="subject_id" value="<?php echo $subject_id;?>">
					<input type="hidden" id="standard_id" name="standard_id" value="<?php echo $standard_id;?>">
					<input type="hidden" id="maximum_question" name="maximum_question" value="<?php echo $maximum_question;?>">
					<input type="hidden" id="auto_question" name="auto_question" value="<?php echo $this->session->userdata('auto_question_cart');?>">
					<input type="hidden" id="chk_mode" name="chk_mode" value="<?php echo $mode;?>">

					<div class="m-form__group form-group">
						<label for="">
							Question selection mode :
						</label>
						<div class="pull-right">
							<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
								Submit
							</button>
		              		<a href="<?php echo base_url(ADMIN_BASE.'exams/list')?>" class="btn btn-secondary">Cancel</a>
						</div>
						<div class="m-radio-inline">
							<label class="m-radio" for="manual">
							<?php
								$manual_chk=false;
								$auto_chk = false;
								$manual = array('name'  => 'mode','id' => 'manual', 'value' => 'manual','class'   => 'form-control m-input');
								$auto = array('name'  => 'mode','id' => 'auto', 'value' => 'auto','class'   => 'form-control m-input');
								if(isset($mode) && $mode!=''){
					 	 			if($mode == 'manual'){
					 	 				$manual_chk = true;
								 	 	$auto_chk = false;
								 	 	$auto['disabled'] = true;
								 	}elseif ($mode == 'auto') {
					 	 				$manual_chk = false;
								 	 	$auto_chk = true;
								 	 	$manual['disabled'] = true;
								 	}
								 }
							?>
								<?php 
									echo form_radio($manual,'manual',$manual_chk);
								?>
								Manual
								<span></span>
							</label>
							<label class="m-radio" for="auto">
								<?php 
									echo form_radio($auto,'auto',$auto_chk);
								?>
								Auto
								<span></span>
							</label>
						</div>
					</div>
					<div class="m-content">
						<div class="manual_questions" id="manual_ajaxdata"></div>
						<div class="auto_questions" id="auto_ajaxdata"></div>			
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	var que_list = <?php echo json_encode($this->session->userdata("question_cart")) ?>;
</script>