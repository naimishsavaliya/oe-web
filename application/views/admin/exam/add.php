<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Add Exam</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Exam Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="exam-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				
				<?php 
			      	$examform = array('id'=>'exam_formadd','autocomplete'=>'off','name'=>'exam_formadd','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'exams/save',$examform);   
				?>
				<div class="m-portlet__body">
					<input type="hidden" id="examlist_url" value="<?php echo base_url(ADMIN_BASE.'exams/list');?>">

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Subject Name:
						</label>
						<div class="col-lg-10">
							<?php $subject_options = array(''=>'Select Subject Name');
							  	foreach($subject_data as $subject){
							      	$subject_options[$subject['id']] = $subject['name'];
							 	}
							  	echo form_dropdown('subject_id',$subject_options,set_value('subject_id'),'id="subject_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Standard Name:
						</label>
						<div class="col-lg-10">
							<?php $standard_options = array(''=>'Select Standard Name');
							  	foreach($standard_data as $standard){
							      	$standard_options[$standard['id']] = $standard['name'];
							 	}
							 	echo form_dropdown('standard_id',$standard_options,set_value('standard_id'),'id="standard_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Exam Title:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $exam_title = array ("name" => "exam_title", "id"=>"exam_title","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Exam Title","value"=>set_value('exam_title'));
				                  echo form_input($exam_title);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							description:
						</label>
						<div class="col-lg-10">
							<?php 
							  $description = array ("name" => "description", "id"=>"description","type"=>"text" , "class" => "form-control m-input ckeditor");
							  echo form_textarea($description);
							?> 
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Question Mark:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $que_mark = array ("name" => "que_mark", "id"=>"que_mark","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>set_value('que_mark'));
				                  echo form_input($que_mark);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Negative Mark(Question):
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $negative_mark = array ("name" => "negative_mark", "id"=>"negative_mark","type"=>"text",
				                                 "class" => "form-control m-input","min"=>"-10","value"=>set_value('negative_mark'));
				                  echo form_input($negative_mark);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Total Question:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $total_que = array ("name" => "total_que", "id"=>"total_que","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>set_value('total_que'));
				                  echo form_input($total_que);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Max Student:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $max_student = array ("name" => "max_student", "id"=>"max_student","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>set_value('max_student'));
				                  echo form_input($max_student);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Min Student:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $min_student = array ("name" => "min_student", "id"=>"min_student","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>set_value('min_student'));
				                  echo form_input($min_student);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Price:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $price = array ("name" => "price", "id"=>"price","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Price","value"=>set_value('price'),"maxlength"=>10);
				                  echo form_input($price);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Start Date:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $start_date = array ("name" => "start_date", "id"=>"start_date","type"=>"date",
				                                 "class" => "form-control m-input","value"=>set_value('start_date'));
				                  echo form_input($start_date);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Start Time:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $start_time = array ("name" => "start_time", "id"=>"start_time","type"=>"time",
				                                 "class" => "form-control m-input","value"=>set_value('start_time'));
				                  echo form_input($start_time);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								End Time:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $end_time = array ("name" => "end_time", "id"=>"end_time","type"=>"time",
				                                 "class" => "form-control m-input","value"=>set_value('end_time'));
				                  echo form_input($end_time);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Question Time:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $question_time = array ("name" => "question_time", "id"=>"question_time","type"=>"number","class" => "form-control m-input","min"=>"1","value"=>set_value('question_time'));
				                  echo form_input($question_time);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Exam Time:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $exam_time = array ("name" => "exam_time", "id"=>"exam_time","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>set_value('exam_time'));
				                  echo form_input($exam_time);                                                    
				            ?>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'exams/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



