<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Edit Question</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Question Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="question-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				<?php
				    $questionform = array('id'=>'questionformedit','autocomplete'=>'off','name'=>'questionformedit','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    			echo form_open(ADMIN_BASE.'questions/update',$questionform);   
				    	
				?>
				<div class="m-portlet__body">
					<input type="hidden" id="questionslist_url" value="<?php echo base_url(ADMIN_BASE.'questions/list');?>">

					<?php  
							$question_id = !isset($questionanswer_data->id)?'':$questionanswer_data->id;
							$question = !isset($questionanswer_data->question)?'':$questionanswer_data->question;
							$sub_id = !isset($questionanswer_data->subject_id)?'':$questionanswer_data->subject_id;
							$std_id = !isset($questionanswer_data->standard_id)?'':$questionanswer_data->standard_id;
							$answer_type = !isset($questionanswer_data->answer_type)?'':$questionanswer_data->answer_type;
							//var_dump($answer_type); die();
							$correct_answer = !isset($questionanswer_data->correct_answer)?'':$questionanswer_data->correct_answer;

					?>
					<input type="hidden" id="que_id" name="que_id" value="<?php echo $question_id;?>">

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Subject Name:
						</label>
						<div class="col-lg-10">
							<?php $subject_options = array(''=>'Select Subject Name');
							  	foreach($subject_data as $subject){
							      	$subject_options[$subject['id']] = $subject['name'];
							 	}
							  	echo form_dropdown('subject_id',$subject_options,set_value('subject_id',$sub_id),'id="subject_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Standard Name:
						</label>
						<div class="col-lg-10">
							<?php $standard_options = array(''=>'Select Standard Name');
							  	foreach($standard_data as $standard){
							      	$standard_options[$standard['id']] = $standard['name'];
							 	}
							 	echo form_dropdown('standard_id',$standard_options,set_value('standard_id',$std_id),'id="standard_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Question:
						</label>
						<div class="col-lg-10">
							<?php 
							  $question = array ("name" => "question", "id"=>"question","type"=>"text" , "class" => "form-control m-input ckeditor","value"=>$question);
							  echo form_textarea($question);
							?> 
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Answer Type:
						</label>
						<div class="col-lg-10">
							<select name="answer_type" id="answer_type" class='form-control m-input' disabled="disabled">
							   <option value="">Select Answer Type</option>
							   <option value="text2" <?php if(isset($questionanswer_data)){ if($answer_type == 'text2'){ echo 'Selected';}} ?>>Text-2</option>
							   <option value="text4"<?php if(isset($questionanswer_data)){ if($answer_type == 'text4'){ echo 'Selected';}} ?>>Text-4</option>
							   <option value="image2" <?php if(isset($questionanswer_data)){ if($answer_type == 'image2'){ echo 'Selected';}} ?>>Image-2</option>
							   <option value="image4" <?php if(isset($questionanswer_data)){ if($answer_type == 'image4'){ echo 'Selected';}} ?>>Image-4</option>
							</select>
						</div>
					</div>

					<div id="text2" style="<?php if($answer_type == 'text2'){ echo 'display: block';}else{ echo 'display: none'; }?>">
						<?php for ($w = 1; $w <= 2; $w++) { ?>
							<div class="form-group m-form__group row">
								<label class="col-lg-2 col-form-label">
									Answer <?php echo $w;?>
								</label>
								<div class="col-lg-8">
									<?php 
										$text2_answer='text2_answer'.$w;
										$text2 = array ("name" => $text2_answer, "id"=>$text2_answer,"type"=>"text" , "class" => "form-control m-input");

										$text2_fieldname='answer'.$w;
										if(isset($questionanswer_data->$text2_fieldname) && $answer_type == 'text2'){
											$text2["value"]=$questionanswer_data->$text2_fieldname;
										}else{
											$text2["value"]='';
										}
										echo form_textarea($text2);
									?> 
								</div>
							
								
								 <div class="col-lg-2">
							 	 	<?php 	$checked=false;
							 	 			if($answer_type == 'text2'){
										 	 	$correct_ids = explode('answer',$correct_answer);
										 	 	$correct_ids = $correct_ids['1'];	
										 	 	if($correct_ids==$w)
										 	 		$checked=true;
										 	 }
							 	 	$correct_ans = array("name"  => "correct_ans","id"  => "correct_ans","class" => "form-control m-input"
							 		 	);
							 	 	echo form_radio($correct_ans, $w, $checked); ?>
							 	 
								</div>
							</div>
						<?php } ?>
					</div>
					
					<div id="text4" style="<?php if($answer_type == 'text4'){ echo 'display: block';}else{ echo 'display: none'; }?>">
						<?php for ($x = 1; $x <= 4; $x++) { ?>
							<div class="form-group m-form__group row">
								<label class="col-lg-2 col-form-label">
									Answer <?php echo $x;?>
								</label>
								<div class="col-lg-8">
									<?php 
										$text4_answer='text4_answer'.$x;
										$text4 = array ("name" => $text4_answer, "id"=>$text4_answer,"type"=>"text" , "class" => "form-control m-input");

										$text4_fieldname='answer'.$x;
										if(isset($questionanswer_data->$text4_fieldname) && $answer_type == 'text4'){
											$text4["value"]=$questionanswer_data->$text4_fieldname;
										}else{
											$text4["value"]='';
										}
										echo form_textarea($text4);
									?>  
								</div>
								 <div class="col-lg-2">
							 	 	<?php $correct_ans = array("name"  => "correct_ans","id"  => "correct_ans","class" => "form-control m-input"
							 		 	);
					 	 		 	 	$checked=false;
							 	 			if($answer_type == 'text4'){
										 	 	$correct_ids = explode('answer',$correct_answer);
										 	 	$correct_ids = $correct_ids['1'];	
										 	 	if($correct_ids==$x)
										 	 		$checked=true;
										 	 }
							 	 	echo form_radio($correct_ans, $x, $checked); ?>
								</div>
							</div>
					<?php } ?>
					</div>

					<div id="image2" style="<?php if($answer_type == 'image2'){ echo 'display: block';}else{ echo 'display: none'; }?>">
						<?php for ($y = 1; $y <= 2; $y++) { ?>
							<div class="form-group m-form__group row">
								<label class="col-lg-2 col-form-label">
									Image:
								</label>
								<div class="col-lg-6">
								 	<?php 
								 	$image2_answer='image2_answer'.$y;                                            
							          $image2 = array ("name" => $image2_answer, "id"=>$image2_answer,"type"=>"file","class" => "form-control m-input");
							          echo form_input($image2);                                                    
							    	?>
								</div>
								<?php 
								    $src="";
								    $image2_field='answer'.$y;
								    if(isset($questionanswer_data->$image2_field) && $answer_type == 'image2'){
								      $style="width:150px; height:150px;";
								      $src=base_url('assets/uploads/question/').$question_id.'/'.$questionanswer_data->$image2_field;
								    }else{
								      $style="width:150px; height:150px;display:none";
								    }
								?>
								<div class="location_img">
								  <label for="image">
								    <img style="<?php echo $style;?>" id="preview_img" src="<?php echo $src;?>">
								  </label>
								</div>
					 			<div class="col-lg-2">
							 	 	<?php $correct_ans = array("name"  => "correct_ans","id"  => "correct_ans","class" => "form-control m-input"
							 		 	);
							 	 		 	$checked=false;
							 	 			if($answer_type == 'image2'){
										 	 	$correct_ids = explode('answer',$correct_answer);
										 	 	$correct_ids = $correct_ids['1'];	
										 	 	if($correct_ids==$y)
										 	 		$checked=true;
										 	 }
							 	 	echo form_radio($correct_ans, $y, $checked); ?>
								</div>
								
							</div>
						<?php } ?>
					</div>
					
					<div id="image4" style="<?php if($answer_type == 'image4'){ echo 'display: block';}else{ echo 'display: none'; }?>">
						<?php for ($z = 1; $z <= 4; $z++) { ?>
							<div class="form-group m-form__group row">
								<label class="col-lg-2 col-form-label">
									Image:
								</label>
								<div class="col-lg-6">
								 	<?php 
								 	$image4_answer='image4_answer'.$z;                                               
							          $image4 = array ("name" => $image4_answer, "id"=>$image4_answer,"type"=>"file","class" => "form-control m-input");
							          echo form_input($image4);                                                    
							    	?>
								</div>
								<?php 
								    $src="";
								    $image4_field='answer'.$z;
								    if(isset($questionanswer_data->$image4_field) && $answer_type == 'image4'){
								      $style="width:150px; height:150px;";
								      $src=base_url('assets/uploads/question/').$question_id.'/'.$questionanswer_data->$image4_field;
								    }else{
								      $style="width:150px; height:150px;display:none";
								    }
								?>
								<div class="location_img">
								  <label for="image">
								    <img style="<?php echo $style;?>" id="preview_img" src="<?php echo $src;?>">
								  </label>
								</div>
					 			<div class="col-lg-2">
							 	 	<?php $correct_ans = array("name"  => "correct_ans","id"  => "correct_ans","class" => "form-control m-input"
							 		 	);
					 	 		 	 	$checked=false;
							 	 			if($answer_type == 'image4'){
										 	 	$correct_ids = explode('answer',$correct_answer);
										 	 	$correct_ids = $correct_ids['1'];	
										 	 	if($correct_ids==$z)
										 	 		$checked=true;
										 	 }
							 	 	echo form_radio($correct_ans, $z, $checked); ?>
								</div>
							</div>
						<?php } ?>
					</div>
					
					
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'questions/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



