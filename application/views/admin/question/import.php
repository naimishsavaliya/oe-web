<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Question Import</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Import Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="question-import-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				
				<?php 
			      	$importform = array('id'=>'import_file','autocomplete'=>'off','name'=>'import_file','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'questions/unzip',$importform);   
				?>
				<div class="m-portlet__body">
					<input type="hidden" id="questionslist_url" value="<?php echo base_url(ADMIN_BASE.'questions/list');?>">

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Subject Name:
						</label>
						<div class="col-lg-10">
							<?php $subject_options = array(''=>'Select Subject Name');
							  	foreach($subject_data as $subject){
							      	$subject_options[$subject['id']] = $subject['name'];
							 	}
							  	echo form_dropdown('subject_id',$subject_options,set_value('subject_id'),'id="subject_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Standard Name:
						</label>
						<div class="col-lg-10">
							<?php $standard_options = array(''=>'Select Standard Name');
							  	foreach($standard_data as $standard){
							      	$standard_options[$standard['id']] = $standard['name'];
							 	}
							 	echo form_dropdown('standard_id',$standard_options,set_value('standard_id'),'id="standard_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							File:
						</label>
						<div class="col-lg-8">
						 	<?php                                            
					          $file = array ("name" => 'zip_file', "id"=>'zip_file',"type"=>"file","class" => "form-control m-input");
					          echo form_input($file);                                                    
					    	?>
						</div>
					</div>
					
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'questions/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



