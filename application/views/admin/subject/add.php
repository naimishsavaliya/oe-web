<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title "><?php echo $mode=='add'?'Add':'Edit'; ?> Subject</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Subject Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<div class="alert" id="subject-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				
				<?php 
				    $subject_id = isset($subject_data->id);
				    if($subject_id > 0){
				      	$subjectform = array('id'=>'subjectformedit','autocomplete'=>'off','name'=>'subjectformedit','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
				      	
				    }else{
				      	$subjectform = array('id'=>'subjectformadd','autocomplete'=>'off','name'=>'subjectformadd','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
				    }
				    	echo form_open('',$subjectform);   
				    	echo form_hidden('mode',$mode); 
					?>
				<div class="m-portlet__body">
					<input type="hidden" id="subjectlist_url" value="<?php echo base_url(ADMIN_BASE.'subjects/list');?>">
					<div class="form-group m-form__group row">
						<?php if($subject_id > 0) { $subjectname_val = !isset($subject_data->name)?'':$subject_data->name; } ?>
						<label class="col-lg-2 col-form-label">
							Subject Name:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $subject_name = array ("name" => "subject_name", "id"=>"subject_name","type"=>"text","class" => "form-control m-input","placeholder"=>"Enter Subject name");
										if($mode=='add'){
											$subject_name['value'] = set_value('subject_name');
										}elseif($mode=='edit'){
											$subject_name['value'] = $subjectname_val;
										}
				                  echo form_input($subject_name);                                                    
				            ?>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Image:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
					          $image = array ("name" => "image", "id"=>"image","type"=>"file","class" => "form-control m-input");
					          echo form_input($image);                                                    
					    	?>
						</div>
						<?php 
						    $src="";
						    if(isset($subject_data->image)){
						      $style="width:150px; height:150px;";
						      $src=base_url('assets/uploads/subject/').$subject_data->image;
						    }else{
						      $style="width:150px; height:150px;display:none";
						    }
						?>
						<div class="location_img">
						  <label for="image">
						    <img style="<?php echo $style;?>" id="preview_img" src="<?php echo $src;?>">
						  </label>
						</div>

					</div>
					
					
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'subjects/list')?>" class="btn btn-secondary">Cancel</a>
									<!-- <button type="reset" class="btn btn-secondary">
										Cancel
									</button> -->
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

