<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Add Level</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Level Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="level-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				
				<?php 
			      	$level_form = array('id'=>'level_formadd','autocomplete'=>'off','name'=>'level_formadd','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'levels/save',$level_form);   
				?>
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Level Name:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $level_name = array ("name" => "level_name", "id"=>"level_name","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Level Name","value"=>set_value('level_name'));
				                  echo form_input($level_name);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Amount:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $amount = array ("name" => "amount", "id"=>"amount","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Amount","value"=>set_value('amount'),"maxlength"=>10);
				                  echo form_input($amount);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
								Exam Count:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $exam_count = array ("name" => "exam_count", "id"=>"exam_count","type"=>"number",
				                                 "class" => "form-control m-input","min"=>"1","value"=>set_value('exam_count'));
				                  echo form_input($exam_count);                                                    
				            ?>
						</div>
					</div>
					<?php if(!empty($parent_data)){?>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Parent Level:
						</label>
						<div class="col-lg-6">
							<?php $level_options = array(''=>'Select parent level');
							  	foreach($parent_data as $level){
							      	$level_options[$level['level_id']] = $level['level_name'];
							 	}
							  	echo form_dropdown('parent_id',$level_options,set_value('parent_id'),'id="parent_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>
					<?php } ?>

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'levels/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



