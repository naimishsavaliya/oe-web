<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Setting List</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-4">
							<div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="setting_datatableurl" value="<?php echo base_url(ADMIN_BASE.'email_template/setting_dataTables');?>">
		<input type="hidden" id="edit_setting_url" value="<?php echo base_url(ADMIN_BASE.'email_template/setting_edit/');?>">
		<input type="hidden" id="delete_setting_url" value="<?php echo base_url(ADMIN_BASE.'email_template/setting_delete/');?>">
		<div class="setting_table" id="ajax_data"></div>		
</div>

<!-- Delete popupModal -->
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="delModal">
    <div class="modal-dialog" role="document">
      	<div id="modal_datas">
	        <div class="modal-content">
	          	<div class="modal-header">
		            <h2 class="modal-title" id="delModal" style="text-align:center">Are You Sure you want to delete?</h2>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		              	<span aria-hidden="true">&times;</span>
		            </button>
	          	</div>
				<div class="modal-body">
					<input type="hidden" class="form-control" name="del_id" id="del_id">
					<div class="form-Action " style="text-align:center">
						<button class="btn btn-default" onclick="delete_record();">Delete</button>                
						<button class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>     
	      	</div>
    	</div>  
  	</div>
</div>
<!-- End popupModal -->


