<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Add Email template</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Email template Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="template-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				
				<?php 
			      	$template_form = array('id'=>'template_formadd','autocomplete'=>'off','name'=>'template_formadd','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'email_template/save',$template_form);   
				?>
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Name:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $name = array ("name" => "name", "id"=>"name","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Name","value"=>set_value('name'));
				                  echo form_input($name);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Subject:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $subject = array ("name" => "subject", "id"=>"subject","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Subject","value"=>set_value('subject'));
				                  echo form_input($subject);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Description:
						</label>
						<div class="col-lg-10">
							<?php 
							  $description = array ("name" => "description", "id"=>"description","type"=>"text" , "class" => "form-control m-input");
							  echo form_textarea($description);
							?> 
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Email type:
						</label>
						<div class="col-lg-10">
							<select name="email_type" id="email_type" class='form-control m-input'>
							   <option value="">Select discount type</option>
							   <option value="1">For customer</option>
							   <option value="2">For admin</option>
							</select>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							From name:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $from_name = array ("name" => "from_name", "id"=>"from_name","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter From Name","value"=>set_value('from_name'));
				                  echo form_input($from_name);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							From email:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $from_email = array ("name" => "from_email", "id"=>"from_email","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter From Email","value"=>set_value('from_email'));
				                  echo form_input($from_email);                                                    
				            ?>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'email_template/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



