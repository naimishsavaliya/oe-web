<style>
	@media only screen and (max-width: 600px) {div.zm_-4151864388084014152_parse_-2075678471543463923 .x_1194979181inner-body { width: 100% !important } div.zm_-4151864388084014152_parse_-2075678471543463923 .x_1194979181footer { width: 100% !important } }
	@media only screen and (max-width: 500px) {div.zm_-4151864388084014152_parse_-2075678471543463923 .x_1194979181button { width: 100% !important } }
</style>
<?php 
	$name = !isset($fullname)?'':$fullname;
	$mo_no = !isset($mobile)?'':$mobile;
	$email_id = !isset($email)?'':$email;
	$udid = !isset($ud_id)?'':$ud_id;
	$mobile_type_val = !isset($mobile_type)?'':$mobile_type;
	$description_val = !isset($description)?'':$description;
?>
<table class="x_1194979181wrapper" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: rgb(245, 248, 250); margin: 0; padding: 0; width: 100%" width="100%" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box" align="center">
				<table class="x_1194979181content" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%" width="100%" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td class="x_1194979181body" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: rgb(255, 255, 255); border-bottom: 1px solid rgb(237, 239, 242); border-top: 1px solid rgb(237, 239, 242); margin: 0; padding: 0; width: 100%" width="100%">
								<table class="x_1194979181inner-body" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: rgb(255, 255, 255); margin: 0 auto; padding: 0; width: 570px" width="570" cellspacing="0" cellpadding="0" align="center">
									<tbody>
										<tr>
											<td class="x_1194979181content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px">
												<h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: rgb(47, 49, 51); font-size: 19px; font-weight: bold; margin-top: 0; text-align: left">User Detail</h1>
												<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: rgb(116, 120, 126); font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left">
												<br> Fullname: <strong style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box"> <?php echo $name;?> </strong>
												<br> Mobile No.: <strong style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box"> <?php echo $mo_no;?> </strong> 
												<br> Email: <strong style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box"> <?php echo $email_id;?> </strong>
												<br> Birthdate: <strong style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box"> <?php echo $birthdate;?> </strong>  
												<br> UDID: <strong style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box"> <?php echo $udid;?> </strong> 
												<br> Mobile type: <strong style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box"> <?php echo $mobile_type_val;?> </strong>
												<?php if($profile_pic!=''){?>
												<br> Profile: <img src="<?php echo base_url('assets/uploads/profile/'.$profile_pic)?>" width="250px" height="125px" style="margin-left:10px;">  
												<?php } ?>
												<br> Description: <strong style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box"> <?php echo $description_val;?>  </strong> <br>
												</p> 
												<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: rgb(116, 120, 126); font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left">Thanks,<br> Online Exam</p>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box">
								<table class="x_1194979181footer" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 570px" width="570" cellspacing="0" cellpadding="0" align="center">
									<tbody>
										<tr>
											<td class="x_1194979181content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px" align="center">
												<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: rgb(174, 174, 174); font-size: 12px; text-align: center">© 2018 Online Exam. All rights reserved.</p>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>