<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Edit General Setting</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								General Setting Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="setting-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				<?php 
			      	$generalsetting_form = array('id'=>'setting_form','autocomplete'=>'off','name'=>'setting_form','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'email_template/update_setting',$generalsetting_form);   
				?>
				<div class="m-portlet__body">
					<?php  
						$setting_id = !isset($setting_data->id)?'':$setting_data->id;
						$email_template_id = !isset($setting_data->email_template_id)?'':$setting_data->email_template_id;
						$setting_name_val = !isset($setting_data->setting_name)?'':$setting_data->setting_name;
						$setting_value_val = !isset($setting_data->setting_value)?'':$setting_data->setting_value;
						$setting_code_val = !isset($setting_data->setting_code)?'':$setting_data->setting_code;
					?>
					<input type="hidden" id="setting_id" name="setting_id" value="<?php echo $setting_id;?>">

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Name:
						</label>
						<div class="col-lg-6">
							<?php $template_options = array(''=>'Select Name');
							  	foreach($template_data as $value){
							      	$template_options[$value['id']] = $value['name'];
							 	}
							  	echo form_dropdown('email_template_id',$template_options,set_value('email_template_id',$email_template_id),'id="email_template_id" class="form-control m-input"'); 
							?>  
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Setting Name:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $setting_name = array ("name" => "setting_name", "id"=>"setting_name",
				                  	"type"=>"text","class" => "form-control m-input","placeholder"=>"Enter Setting Name","value"=>$setting_name_val);
				                  echo form_input($setting_name);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Setting Value:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $setting_value = array ("name" => "setting_value", "id"=>"setting_value","type"=>"text","class" => "form-control m-input","placeholder"=>"Enter Setting Value","value"=>$setting_value_val);
				                  echo form_input($setting_value);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Setting Code:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $setting_code = array ("name" => "setting_code", "id"=>"setting_code","type"=>"text","class" => "form-control m-input","placeholder"=>"Enter Setting Code","value"=>$setting_code_val,"disabled"=>true);
				                  echo form_input($setting_code);                                                    
				            ?>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'email_template/setting')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



