<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Edit Email template</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Email template Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="template-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				
				<?php 
			      	$templateform_edit = array('id'=>'editform_template','autocomplete'=>'off','name'=>'editform_template','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'email_template/update',$templateform_edit);   
				?>
				<div class="m-portlet__body">
					<?php  
							$template_id = !isset($template_data->id)?'':$template_data->id;
							$name_val = !isset($template_data->name)?'':$template_data->name;
							$subject_val = !isset($template_data->subject)?'':$template_data->subject;
							$description_val = !isset($template_data->description)?'':$template_data->description;
							$email_type_val = !isset($template_data->email_type)?'':$template_data->email_type;
							$from_name_val = !isset($template_data->from_name)?'':$template_data->from_name;
							$from_email_val = !isset($template_data->from_email)?'':$template_data->from_email;
							$variable_val = !isset($template_data->variable)?'':$template_data->variable;
							
					?>
					<input type="hidden" id="template_id" name="template_id" value="<?php echo $template_id;?>">

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Name:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $name = array ("name" => "name", "id"=>"name","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Name","value"=>$name_val);
				                  echo form_input($name);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Subject:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $subject = array ("name" => "subject", "id"=>"subject","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Subject","value"=>$subject_val);
				                  echo form_input($subject);                                                    
				            ?>
						</div>
					</div>
					<?php if($variable_val!=''){ ?>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Variable:
						</label>
						<div class="col-lg-6">
						 	<?php  echo $variable_val; ?>
						</div>
					</div>
					<?php } ?>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Description:
						</label>
						<div class="col-lg-10">
							<?php 
							  $description = array ("name" => "description", "id"=>"description","type"=>"text" , "class" => "form-control m-input","value"=>$description_val);
							  echo form_textarea($description);
							?> 
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Email type:
						</label>
						<div class="col-lg-10">
							<select name="email_type" id="email_type" class='form-control m-input'>
							   <option value="">Select email type</option>
							   <option value="1" <?php if(isset($template_data)){ if($email_type_val == '1'){ echo 'Selected';}} ?>>For customer</option>
							   <option value="2"<?php if(isset($template_data)){ if($email_type_val == '2'){ echo 'Selected';}} ?>>For admin</option>
							</select>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							From name:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $from_name = array ("name" => "from_name", "id"=>"from_name","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter From Name","value"=>$from_name_val);
				                  echo form_input($from_name);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							From email:
						</label>
						<div class="col-lg-10">
						 	<?php                                             
				                  $from_email = array ("name" => "from_email", "id"=>"from_email","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter From Email","value"=>$from_email_val);
				                  echo form_input($from_email);                                                    
				            ?>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'email_template/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



