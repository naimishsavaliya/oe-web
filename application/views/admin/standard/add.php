<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title "><?php echo $mode=='add'?'Add':'Edit'; ?> Standard</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Standard Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<div class="alert" id="standard-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				
				<?php 
				    $standard_id = isset($standard_data->id);
				    if($standard_id > 0){
				      	$standardform = array('id'=>'standardformedit','autocomplete'=>'off','name'=>'standardformedit','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed');
				      	
				    }else{
				      	$standardform = array('id'=>'standardformadd','autocomplete'=>'off','name'=>'standardformadd','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed');
				    }
				    	echo form_open('',$standardform);   
				    	echo form_hidden('mode',$mode); 
					?>
				<div class="m-portlet__body">
					<input type="hidden" id="standardlist_url" value="<?php echo base_url(ADMIN_BASE.'standards/list');?>">
					<div class="form-group m-form__group row">
						<?php if($standard_id > 0) { $standardname_val = !isset($standard_data->name)?'':$standard_data->name; } ?>
						<label class="col-lg-2 col-form-label">
							Standard Name:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $standard_name = array ("name" => "standard_name", "id"=>"standard_name","type"=>"text","class" => "form-control m-input","placeholder"=>"Enter Standard name");
										if($mode=='add'){
											$standard_name['value'] = set_value('standard_name');
										}elseif($mode=='edit'){
											$standard_name['value'] = $standardname_val;
										}
				                  echo form_input($standard_name);                                                    
				            ?>
						</div>
					</div>
					
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'standards/list')?>" class="btn btn-secondary">Cancel</a>
									<!-- <button type="reset" class="btn btn-secondary">
										Cancel
									</button> -->
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

