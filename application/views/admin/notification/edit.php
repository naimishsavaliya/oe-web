<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Edit Notification</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Notification Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<?php $is_error = false; ?>
						<?php if(validation_errors() != ''){ $is_error = true; ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo validation_errors(); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('success') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('success'); ?>
						    </div>
						<?php } ?>
						<?php if($this->session->flashdata('error') != '' && $is_error==false){ ?>
						    <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn">
						       <?php echo $this->session->flashdata('error'); ?>
						    </div>
						<?php } ?>
						<div class="alert" id="notification-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				<div class="clearfix"></div>
				
				<?php 
			      	$notificationform_edit = array('id'=>'notification_formedit','autocomplete'=>'off','name'=>'notification_formedit','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype'=>'multipart/form-data');
			    	echo form_open(ADMIN_BASE.'notification/update',$notificationform_edit);   
				?>
				<div class="m-portlet__body">
					<?php  
							$notification_id = !isset($notification_data->id)?'':$notification_data->id;
							$message_val = !isset($notification_data->message)?'':$notification_data->message;
							$subject_val = !isset($notification_data->subject)?'':$notification_data->subject;
							$notification_type_val = !isset($notification_data->notification_type)?'':$notification_data->notification_type;
							if($notification_data->notification_date==Null || $notification_data->notification_date=='0000-00-00T00:00:00'){
								$notification_date_val='';
							}else{
								$notification_date_val = date('Y-m-d\TH:i:s',strtotime($notification_data->notification_date));
							}
					?>
					<input type="hidden" id="notification_id" name="notification_id" value="<?php echo $notification_id;?>">

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Users:<p style=" font-size: 11px;">Ctrl + Click to Select</p>
						</label>
						<div class="col-lg-10">
							<select name="mul_user[]" id="mul_user" class='form-control m-input' multiple>
							<?php if(!empty($users_data)) { 
								$users_id = explode(',',$notification_data->users);
								foreach ($users_data as $value) { 
									$fullname=isset($value['fullname'])?$value['fullname']:'';
									$user_id=isset($value['id'])?$value['id']:'';
									$myselected="";
	                                if(in_array($user_id,$users_id)){
	                                    $myselected="selected";
	                                }
                            ?>
                    			<option value="<?php echo $user_id; ?>"<?php echo $myselected;?>><?php echo $fullname; ?></option>
                    		<?php } } ?>
							</select> 
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Subject:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $subject = array ("name" => "subject", "id"=>"subject","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Subject","value"=>$subject_val);
				                  echo form_input($subject);                                                    
				            ?>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Message:
						</label>
						<div class="col-lg-10">
							<?php 
							  $message = array ("name" => "message", "id"=>"message","type"=>"text" , "class" => "form-control m-input","value"=>$message_val);
							  echo form_textarea($message);
							?> 
						</div>
					</div>
					
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Notification type:
						</label>
						<div class="col-lg-10">
							<select name="notification_type" id="notification_type" class='form-control m-input'>
							   <option value="">Select type</option>
							   <option value="1" <?php if(isset($notification_data)){ if($notification_type_val == '1'){ echo 'Selected';}} ?>>Push notification</option>
							   <option value="2"<?php if(isset($notification_data)){ if($notification_type_val == '2'){ echo 'Selected';}} ?>>Email</option>
							</select>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Send Notification:
						</label>
						<div class="m-radio-inline">
							<?php
								$send_now = array('name'  => 'notification_mode',
													'id' => 'send_now', 
													'class'   => 'form-control m-input');
								$schedule = array('name'  => 'notification_mode',
													'id' => 'schedule', 
													'class'   => 'form-control m-input');
								if($notification_data->notification_date==Null || $notification_data->notification_date=='0000-00-00T00:00:00'){
									$send_now_chk = true;
									$schedule_chk = false;
									$schedule['disabled'] = true;
								}else{
									$send_now_chk = false;
									$schedule_chk = true;
									$send_now['disabled'] = true;
								}
							?>
							<label class="m-radio" for="send_now">
								<?php echo form_radio($send_now,'send_now',$send_now_chk);?>
								Send now
								<span></span>
							</label>
							<label class="m-radio" for="schedule">
								<?php echo form_radio($schedule,'schedule',$schedule_chk);?>
								Schedule
								<span></span>
							</label>
						</div>
					</div>

					<div id="n_date" style="display: none;">
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">
								Date:
							</label>
							<div class="col-lg-10">
							 	<?php                                             
					                  $notification_date = array ("name" => "notification_date", "id"=>"notification_date-datetime-local-input","type"=>"datetime-local",
					                                 "class" => "form-control m-input n_date-required","value"=>$notification_date_val);
					                  echo form_input($notification_date);                     
					            ?>
							</div>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'notification/list')?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>



