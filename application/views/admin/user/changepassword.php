<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Change Password</h3>
		</div>
		<div>
			<!-- <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
				<span class="m-subheader__daterange-label"> <span
					class="m-subheader__daterange-title"></span> <span
					class="m-subheader__daterange-date m--font-brand"></span>
			</span> <a href="#"
				class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
					<i class="la la-angle-down"></i>
			</a>
			</span> -->
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Change Password Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<div class="alert" id="password-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				
				<?php 
				    $changepasswordform = array('id'=>'changepasswordform','name'=>'changepasswordform','method'=>'post','autocomplete'=>'off','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed');
				    echo form_open('',$changepasswordform); 
					?>
				<div class="m-portlet__body">
					<input type="hidden" id="userlist_url" value="<?php echo base_url(ADMIN_BASE.'users/list');?>">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							New Password:
						</label>
						<div class="col-lg-6">
							<?php 
							    $new_password = array ("name" => "new_password", "id"=>"new_password","type"=>"password" , "class" => "form-control m-input","placeholder"=>"Enter New Password","value"=>set_value("new_password"));
							    echo form_input($new_password);
							?>
						</div>
					</div>


					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Confirm Password:
						</label>
						<div class="col-lg-6">
							<?php 
							    $r_password = array ("name" => "r_password", "id"=>"r_password","type"=>"password" , "class" => "form-control m-input","placeholder"=>"Enter Confirm Password","value"=>set_value("r_password"));
							    echo form_input($r_password);
							?>
						</div>
					</div>
					
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'users/list')?>" class="btn btn-secondary">Cancel</a>
									<!-- <button type="reset" class="btn btn-secondary">
										Cancel
									</button> -->
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

