<style type="text/css">
	.error{
		color: red;
	}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title "><?php echo $mode=='add'?'Add':'Edit'; ?> Users</h3>
		</div>
		
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								User Form
							</h3>
						</div>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<div class="col-lg-8">
						<div class="alert" id="user-register-error-msg" style="display:none"></div>
					</div>	
				</div>
				
				<?php 
				    $user_id = isset($users_data->id);
				    if($user_id > 0){
				      	$userform = array('id'=>'userformedit','autocomplete'=>'off','name'=>'userformedit','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed');
				      	
				    }else{
				      	$userform = array('id'=>'userformadd','autocomplete'=>'off','name'=>'userformadd','method'=>'post','class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed');
				    }
				    	echo form_open('',$userform);   
				    	echo form_hidden('mode',$mode); 
					?>
				<div class="m-portlet__body">
					<input type="hidden" id="userlist_url" value="<?php echo base_url(ADMIN_BASE.'users/list');?>">
					
					<div class="form-group m-form__group row">
						<?php if($user_id > 0) { $fullname_val = !isset($users_data->fullname)?'':$users_data->fullname; } ?>
						<label class="col-lg-2 col-form-label">
							Full Name:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $fullname = array ("name" => "fullname", "id"=>"fullname","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Full name");
										if($mode=='add'){
											$fullname['value'] = set_value('fullname');
										}elseif($mode=='edit'){
											$fullname['value'] = $fullname_val;
										}
				                  echo form_input($fullname);                                                    
				            ?>
							<!-- <span class="m-form__help">
								Please enter your First name
							</span> -->
						</div>
					</div>
					
					<div class="form-group m-form__group row">
						<?php if($user_id > 0) { $email_val = !isset($users_data->email)?'':$users_data->email; } ?>
						<label class="col-lg-2 col-form-label">
							Email address:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $email = array ("name" => "email", "id"=>"email","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter email");
					                  	if($mode=='add'){
					                  		$email['value'] = set_value('email');
					                  	}elseif($mode=='edit'){
					                  		$email['value'] = $email_val;
					                  	}
				                  echo form_input($email);                                                    
				            ?>
							<!-- <span class="m-form__help">
								We'll never share your email with anyone else
							</span> "onKeyPress"=>"return isNumberKey(event)"-->
						</div>
					</div>
					<div class="form-group m-form__group row">
						<?php if($user_id > 0) { $mobile_val = !isset($users_data->mobile)?'':$users_data->mobile; } ?>
						<label class="col-lg-2 col-form-label">
							Mobile No.:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $mobile = array ("name" => "mobile", "id"=>"mobile","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Mobile No.","maxlength"=>15,"minlength"=>10);
				                  			if($mode=='add'){
				                  				$mobile['value'] = set_value('mobile');
				                  			}elseif($mode=='edit'){
				                  				$mobile['value'] = $mobile_val;
				                  			}
				                  echo form_input($mobile);                                                    
				            ?>
							<!-- <span class="m-form__help">
								Please enter your Mobile No.
							</span> -->
						</div>
					</div>
					<div class="form-group m-form__group row">
						<?php if($user_id > 0) { $birthdate_val = ($users_data->birthdate=='0000-00-00')?'':date('Y-m-d',strtotime($users_data->birthdate)); } ?>
						<label class="col-lg-2 col-form-label">
							Birthdate:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $birthdate = array ("name" => "birthdate", "id"=>"birthdate","type"=>"date",
				                                 "class" => "form-control m-input");
				                  		if($mode=='edit'){
				                  			$birthdate['value'] = $birthdate_val;
				                  		}
				                  echo form_input($birthdate);                                                    
				            ?>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<?php if($user_id > 0) { $school_val = !isset($users_data->school)?'':$users_data->school; } ?>
						<label class="col-lg-2 col-form-label">
							School:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $school = array ("name" => "school", "id"=>"school","type"=>"text",
				                                 "class" => "form-control m-input","placeholder"=>"Enter School");
				                  			if($mode=='add'){
				                  				$school['value'] = set_value('school');
				                  			}elseif($mode=='edit'){
				                  				$school['value'] = $school_val;
				                  			}
				                  echo form_input($school);                                                    
				            ?>
							<!-- <span class="m-form__help">
								Please enter your School
							</span> -->
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Standard Name:
						</label>
						<div class="col-lg-6">
							<?php $standard_options = array(''=>'Select Standard Name');
							  	foreach($standard_data as $standard){
							      	$standard_options[$standard['id']] = $standard['name'];
							 	}
							 	echo form_dropdown('standard',$standard_options,set_value('standard',!isset($users_data->standard)?'':$users_data->standard),'id="standard" class="form-control m-input"'); 
							?>  
						</div>
					</div>
					<?php if($mode=='add'){?>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">
							Password:
						</label>
						<div class="col-lg-6">
						 	<?php                                             
				                  $password = array ("name" => "password", "id"=>"password","type"=>"password",
				                                 "class" => "form-control m-input","placeholder"=>"Enter Password");
				                  			if($mode=='add'){
				                  				$password['value'] = set_value('password');
				                  			}
				                  echo form_input($password);                                                    
				            ?>
							<!-- <span class="m-form__help">
								Please enter your Last name
							</span> -->
						</div>
					</div>
					<?php }else{ ?>
						<div class="form-group m-form__group row">
							<label class="col-lg-2 col-form-label">
								Password:
							</label>
							<div class="col-lg-6">
							 	<?php 	$CI =& get_instance();
						 				$CI->load->library('encryption');
									 	if($users_data->password!=''){
									 		$get_password = $CI->encryption->decrypt($users_data->password);
									 	}else{
									 		$get_password='';
									 	}
					                  $show_password = array ("name" => "show_password", "id"=>"show_password","type"=>"text", "class" => "form-control m-input","value"=>$get_password,"readonly"=>true);
					                  echo form_input($show_password);                                                    
					            ?>
								<!-- <span class="m-form__help">
									Please enter your Last name
								</span> -->
							</div>
						</div>
					<?php } ?>
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-brand" name="submit_btn" id="submit_btn">
										Submit
									</button>
				              		<a href="<?php echo base_url(ADMIN_BASE.'users/list')?>" class="btn btn-secondary">Cancel</a>
									<!-- <button type="reset" class="btn btn-secondary">
										Cancel
									</button> -->
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close(); ?>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

