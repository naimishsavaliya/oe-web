<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
CONST ADMIN_BASE = 'Admin/';
$route['default_controller'] = 'Dashboard/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route[ADMIN_BASE] = 'Admin/Dashboard/index';
$route[ADMIN_BASE . 'login'] = 'Admin/Dashboard/index';

$route[ADMIN_BASE . 'authentication/signup'] = 'Admin/Authentication/signup';
$route[ADMIN_BASE . 'authentication/login'] = 'Admin/Authentication/login';
$route[ADMIN_BASE.'logout'] = 'Admin/Authentication/logout';

$route[ADMIN_BASE . 'dashboard'] = 'Admin/Dashboard/dashboard';
$route[ADMIN_BASE . 'dashboard/exam_dataTables'] = 'Admin/Dashboard/exam_dataTables';

$route[ADMIN_BASE . 'profile/edit'] = 'Admin/Profile/edit';
$route[ADMIN_BASE . 'profile/update'] = 'Admin/Profile/update';
$route[ADMIN_BASE . 'profile/password_change'] = 'Admin/Profile/password_change';
$route[ADMIN_BASE . 'profile/pw_update'] = 'Admin/Profile/pw_update';

$route[ADMIN_BASE.'users/list'] = 'Admin/Users/index';
$route[ADMIN_BASE.'users/dataTables'] = 'Admin/Users/dataTables';
$route[ADMIN_BASE.'users/add'] = 'Admin/Users/edit';
$route[ADMIN_BASE.'users/edit/([^/]+)'] = 'Admin/Users/edit/$1';
$route[ADMIN_BASE.'users/user_delete/([^/]+)'] = 'Admin/Users/user_delete/$1';
$route[ADMIN_BASE.'users/user_changestatus/([^/]+)/([^/]+)'] = 'Admin/Users/user_changestatus/$1/$2';
$route[ADMIN_BASE.'users/changepassword/([^/]+)'] = 'Admin/Users/changepassword/$1';
$route[ADMIN_BASE.'users/view/([^/]+)'] = 'Admin/Users/view/$1';

$route[ADMIN_BASE.'subjects/list'] = 'Admin/Subjects/index';
$route[ADMIN_BASE.'subjects/dataTables'] = 'Admin/Subjects/dataTables';
$route[ADMIN_BASE.'subjects/subject_delete/([^/]+)'] = 'Admin/Subjects/subject_delete/$1';
$route[ADMIN_BASE.'subjects/subject_changestatus/([^/]+)/([^/]+)'] = 'Admin/Subjects/subject_changestatus/$1/$2';
$route[ADMIN_BASE.'subjects/add'] = 'Admin/Subjects/edit';
$route[ADMIN_BASE.'subjects/edit/([^/]+)'] = 'Admin/Subjects/edit/$1';

$route[ADMIN_BASE.'standards/list'] = 'Admin/Standards/index';
$route[ADMIN_BASE.'standards/dataTables'] = 'Admin/Standards/dataTables';
$route[ADMIN_BASE.'standards/standard_delete/([^/]+)'] = 'Admin/Standards/standard_delete/$1';
$route[ADMIN_BASE.'standards/standard_changestatus/([^/]+)/([^/]+)'] = 'Admin/Standards/standard_changestatus/$1/$2';
$route[ADMIN_BASE.'standards/add'] = 'Admin/Standards/edit';
$route[ADMIN_BASE.'standards/edit/([^/]+)'] = 'Admin/Standards/edit/$1';

$route[ADMIN_BASE.'questions/list'] = 'Admin/Questions/index';
$route[ADMIN_BASE.'questions/dataTables'] = 'Admin/Questions/dataTables';
$route[ADMIN_BASE.'questions/add'] = 'Admin/Questions/add';
$route[ADMIN_BASE.'questions/save'] = 'Admin/Questions/save';
$route[ADMIN_BASE.'questions/edit/([^/]+)'] = 'Admin/Questions/edit/$1';
$route[ADMIN_BASE.'questions/update'] = 'Admin/Questions/update';
$route[ADMIN_BASE.'questions/view/([^/]+)'] = 'Admin/Questions/view/$1';
$route[ADMIN_BASE.'questions/delete/([^/]+)'] = 'Admin/Questions/delete/$1';
$route[ADMIN_BASE.'questions/changestatus/([^/]+)/([^/]+)'] = 'Admin/Questions/changestatus/$1/$2';
$route[ADMIN_BASE.'questions/view/([^/]+)'] = 'Admin/Questions/view/$1';

$route[ADMIN_BASE.'questions/import'] = 'Admin/Questions/import';
$route[ADMIN_BASE.'questions/unzip'] = 'Admin/Questions/unzip';

$route[ADMIN_BASE.'exams/list'] = 'Admin/Exams/index';
$route[ADMIN_BASE.'exams/dataTables/(:num)'] = 'Admin/Exams/dataTables/$1';
$route[ADMIN_BASE.'exams/add'] = 'Admin/Exams/add';
$route[ADMIN_BASE.'exams/save'] = 'Admin/Exams/save';
$route[ADMIN_BASE.'exams/edit/([^/]+)'] = 'Admin/Exams/edit/$1';
$route[ADMIN_BASE.'exams/update'] = 'Admin/Exams/update';
$route[ADMIN_BASE.'exams/delete/([^/]+)'] = 'Admin/Exams/delete/$1';
$route[ADMIN_BASE.'exams/changestatus/([^/]+)/([^/]+)'] = 'Admin/Exams/changestatus/$1/$2';
$route[ADMIN_BASE.'exams/view/([^/]+)'] = 'Admin/Exams/view/$1';

$route[ADMIN_BASE.'exams/question_selection/([^/]+)'] = 'Admin/Exams/question_selection/$1';
$route[ADMIN_BASE.'exams/manual_question/([^/]+)/([^/]+)/([^/]+)'] = 'Admin/Exams/manual_question/$1/$2/$3';
$route[ADMIN_BASE.'exams/auto_question/([^/]+)/([^/]+)/([^/]+)'] = 'Admin/Exams/auto_question/$1/$2/$3';
$route[ADMIN_BASE.'exams/exam_question_save'] = 'Admin/Exams/exam_question_save';
$route[ADMIN_BASE.'exams/add_question'] = 'Admin/Exams/add_question';

$route[ADMIN_BASE.'exams/exam_result/([^/]+)'] = 'Admin/Exams/exam_result/$1';
$route[ADMIN_BASE.'exams/result_dataTables/(:num)'] = 'Admin/Exams/result_dataTables/$1';
$route[ADMIN_BASE.'exams/user_viewquestion/([^/]+)/([^/]+)'] = 'Admin/Exams/user_viewquestion/$1/$2';
$route[ADMIN_BASE.'exams/userquestion_dataTables/([^/]+)/([^/]+)'] = 'Admin/Exams/userquestion_dataTables/$1/$2';
$route[ADMIN_BASE.'exams/rank/([^/]+)/([^/]+)'] = 'Admin/Exams/rank/$1/$2';
$route[ADMIN_BASE.'exams/rank_update/([^/]+)/([^/]+)'] = 'Admin/Exams/rank_update/$1/$2';
$route[ADMIN_BASE.'exams/generate_result/([^/]+)'] = 'Admin/Exams/generate_result/$1';

$route[ADMIN_BASE.'exams/exam_book/([^/]+)'] = 'Admin/Exams/exam_book/$1';
$route[ADMIN_BASE.'exams/book_dataTables/(:num)'] = 'Admin/Exams/book_dataTables/$1';
$route[ADMIN_BASE.'exams/book_changestatus/([^/]+)/([^/]+)'] = 'Admin/Exams/book_changestatus/$1/$2';

$route[ADMIN_BASE.'exams/transaction_list'] = 'Admin/Exams/transaction_list';
$route[ADMIN_BASE.'exams/transaction_dataTables/([^/]+)/([^/]+)'] = 'Admin/Exams/transaction_dataTables/$1/$2';
$route[ADMIN_BASE.'exams/transaction_changestatus/([^/]+)/([^/]+)'] = 'Admin/Exams/transaction_changestatus/$1/$2';

$route[ADMIN_BASE.'levels/list'] = 'Admin/Levels/index';
$route[ADMIN_BASE.'levels/dataTables'] = 'Admin/Levels/dataTables';
$route[ADMIN_BASE.'levels/add'] = 'Admin/Levels/add';
$route[ADMIN_BASE.'levels/save'] = 'Admin/Levels/save';
$route[ADMIN_BASE.'levels/edit/([^/]+)'] = 'Admin/Levels/edit/$1';
$route[ADMIN_BASE.'levels/update'] = 'Admin/Levels/update';
$route[ADMIN_BASE.'levels/delete/([^/]+)'] = 'Admin/Levels/delete/$1';
$route[ADMIN_BASE.'levels/changestatus/([^/]+)/([^/]+)'] = 'Admin/Levels/changestatus/$1/$2';

$route[ADMIN_BASE.'discounts/list'] = 'Admin/Discounts/index';
$route[ADMIN_BASE.'discounts/dataTables'] = 'Admin/Discounts/dataTables';
$route[ADMIN_BASE.'discounts/add'] = 'Admin/Discounts/add';
$route[ADMIN_BASE.'discounts/save'] = 'Admin/Discounts/save';
$route[ADMIN_BASE.'discounts/edit/([^/]+)'] = 'Admin/Discounts/edit/$1';
$route[ADMIN_BASE.'discounts/update'] = 'Admin/Discounts/update';
$route[ADMIN_BASE.'discounts/delete/([^/]+)'] = 'Admin/Discounts/delete/$1';
$route[ADMIN_BASE.'discounts/changestatus/([^/]+)/([^/]+)'] = 'Admin/Discounts/changestatus/$1/$2';

$route[ADMIN_BASE.'email_template/list'] = 'Admin/Email_template/index';
$route[ADMIN_BASE.'email_template/dataTables'] = 'Admin/Email_template/dataTables';
$route[ADMIN_BASE.'email_template/add'] = 'Admin/Email_template/add';
$route[ADMIN_BASE.'email_template/save'] = 'Admin/Email_template/save';
$route[ADMIN_BASE.'email_template/edit/([^/]+)'] = 'Admin/Email_template/edit/$1';
$route[ADMIN_BASE.'email_template/update'] = 'Admin/Email_template/update';
$route[ADMIN_BASE.'email_template/delete/([^/]+)'] = 'Admin/Email_template/delete/$1';
$route[ADMIN_BASE.'email_template/changestatus/([^/]+)/([^/]+)'] = 'Admin/Email_template/changestatus/$1/$2';

$route[ADMIN_BASE.'email_template/setting'] = 'Admin/Email_template/setting';
$route[ADMIN_BASE.'email_template/setting_dataTables'] = 'Admin/Email_template/setting_dataTables';
$route[ADMIN_BASE.'email_template/setting_edit/([^/]+)'] = 'Admin/Email_template/setting_edit/$1';
$route[ADMIN_BASE.'email_template/update_setting'] = 'Admin/Email_template/update_setting';
$route[ADMIN_BASE.'email_template/setting_delete/([^/]+)'] = 'Admin/Email_template/setting_delete/$1';
$route[ADMIN_BASE.'email_template/setting_changestatus/([^/]+)/([^/]+)'] = 'Admin/Email_template/setting_changestatus/$1/$2';

$route[ADMIN_BASE.'cron/job'] = 'Admin/Cron/cron_job';
$route[ADMIN_BASE.'cron/send_now'] = 'Admin/Cron/send_now';
$route[ADMIN_BASE.'cron/schedule'] = 'Admin/Cron/schedule';

$route[ADMIN_BASE.'notification/list'] = 'Admin/Notification/index';
$route[ADMIN_BASE.'notification/dataTables'] = 'Admin/Notification/dataTables';
$route[ADMIN_BASE.'notification/add'] = 'Admin/Notification/add';
$route[ADMIN_BASE.'notification/save'] = 'Admin/Notification/save';
$route[ADMIN_BASE.'notification/edit/([^/]+)'] = 'Admin/Notification/edit/$1';
$route[ADMIN_BASE.'notification/update'] = 'Admin/Notification/update';
$route[ADMIN_BASE.'notification/delete/([^/]+)'] = 'Admin/Notification/delete/$1';
$route[ADMIN_BASE.'notification/changestatus/([^/]+)/([^/]+)'] = 'Admin/Notification/changestatus/$1/$2';

$route[ADMIN_BASE.'notification/users_list/([^/]+)'] = 'Admin/Notification/users_list/$1';
$route[ADMIN_BASE.'notification/users_dataTables/(:num)'] = 'Admin/Notification/users_dataTables/$1';

CONST API_BASE = 'api/v1/';

$route[API_BASE . 'auth'] 		= 'Api/User/generateToken';
$route[API_BASE . 'login'] 		= 'Api/User/login';
$route[API_BASE . 'register'] 	= 'Api/User/register';
$route[API_BASE . 'profile'] 			= 'Api/User/profile';
$route[API_BASE . 'profile_edit'] 		= 'Api/User/profile_edit';
$route[API_BASE . 'change_password'] 	= 'Api/User/change_password';
$route[API_BASE . 'forgot_password'] 	= 'Api/User/forgot_password';
$route[API_BASE . 'reset_password'] 	= 'Api/User/reset_password';
$route[API_BASE . 'user_reward'] 	= 'Api/User/user_reward';
$route[API_BASE . 'level_detail'] 	= 'Api/User/level_detail';
$route[API_BASE . 'register_device'] 	= 'Api/User/register_device';

$route[API_BASE . 'subject'] 	= 'Api/Subject/subject';
$route[API_BASE . 'standard'] 	= 'Api/Standard/standard';
$route[API_BASE . 'exam'] 		= 'Api/Exam/exam';
$route[API_BASE . 'question'] 	= 'Api/Exam/question';
$route[API_BASE . 'exam_submit']= 'Api/Exam/exam_submit';
$route[API_BASE . 'exam_book']	= 'Api/Exam/exam_book';
$route[API_BASE . 'upcoming_exam'] 		= 'Api/Exam/upcoming_exam';
$route[API_BASE . 'past_exam'] 		= 'Api/Exam/past_exam';
$route[API_BASE . 'exam_detail'] 		= 'Api/Exam/exam_detail';
$route[API_BASE . 'exam_start']	= 'Api/Exam/exam_start';
