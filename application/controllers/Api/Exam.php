<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Swagger\Annotations as SWG;

/**
 * @package
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *  apiVersion="1",
 *  swaggerVersion="1.2",
 *  resourcePath="/Exam",
 *  basePath="/api/v1/",
 *  produces="['application/json']"
 * )
 */

class Exam extends MY_Controller
{
    public function __construct(){
        
        parent::__construct();
        $this->load->library('encryption');
    }
    
    /**
     *
     * @SWG\Api(
     *   path="exam",
     *   description="Exam",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="Exam",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="subject_id",
     *           description="Subject id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="standard_id",
     *           description="Standard id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function exam(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $user_id = $this->input->get('user_id');
            $subject_id = $this->input->get('subject_id');
            $standard_id = $this->input->get('standard_id');
            $this->load->model('Exam_Model');
            $user = $this->Exam_Model->check_user($user_id);
            if($user==true){
                $exam_data =$this->Exam_Model->exam_list($subject_id,$standard_id,$user_id);
                if(!empty($exam_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $exam_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="question",
     *   description="Question",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="Question",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="exam_id",
     *           description="Exam id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function question(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $user_id = $this->input->get('user_id');
            $exam_id = $this->input->get('exam_id');
            $this->load->model('Exam_Model');
            $user = $this->Exam_Model->check_user($user_id);
            if($user==true){
                $question_data =$this->Exam_Model->question_list($exam_id);
                $records=[];
                if(!empty($question_data)){
                    foreach ($question_data as $key => $value) {
                        $data = [];
                        $data = [
                            'question_id' => $value['question_id'],
                            'question'=>strip_tags($value['question']),
                            'type' => $value['type']
                        ];
                        if($value['type']=='text2'){
                            $answer = [
                                'answer1' => $value['answer1'],
                                'answer2' => $value['answer2']
                            ];
                        }elseif ($value['type']=='text4') {
                            $answer = [
                               'answer1' => $value['answer1'],
                               'answer2' => $value['answer2'],
                               'answer3' => $value['answer3'],
                               'answer4' => $value['answer4']
                            ];
                        }elseif ($value['type']=='image2') {
                            $answer = [
                               'answer1' =>  base_url('assets/uploads/question/').$value['question_id'].'/'.$value['answer1'],
                               'answer2' =>  base_url('assets/uploads/question/').$value['question_id'].'/'.$value['answer2']
                            ];
                        }else{
                            $answer = [
                                'answer1' => base_url('assets/uploads/question/').$value['question_id'].'/'.$value['answer1'],
                                'answer2' => base_url('assets/uploads/question/').$value['question_id'].'/'.$value['answer2'],
                                'answer3' => base_url('assets/uploads/question/').$value['question_id'].'/'.$value['answer3'],
                                'answer4' => base_url('assets/uploads/question/').$value['question_id'].'/'.$value['answer4']
                            ];
                        }
                        $data['answer'] = $answer;
                        $records[] = $data;
                    }
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $records);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }else{
                 $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="exam_submit",
     *   description="Exam submit",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Exam submit",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="exam_id",
     *           description="Exam id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="question_data",
     *           description="Question data",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct exam id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id and exam id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, user id and exam id already exists message"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function exam_submit(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('Exam_Model');
            $user_id=$this->input->post('user_id');
            $exam_id=$this->input->post('exam_id');
            $question_data=$this->input->post('question_data');
            $user = $this->Exam_Model->check_user($user_id);
            $exam = $this->Exam_Model->check_exam($exam_id);
            if($user==true && $exam==true){
                $exam_submit_already = $this->Exam_Model->check_user_exam_submit('delete_status!="1" AND user_id="'.$user_id.'" AND exam_id="'.$exam_id.'"');
                if($exam_submit_already==true){
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'user id and exam id already exists message');
                }else{
                    $params = array();
                    $params ['user_id'] = $this->input->post('user_id');
                    $params ['exam_id'] = $this->input->post('exam_id');
                    $params ['question_data'] = $this->input->post('question_data');
                    $params ['created_at'] =  date("Y-m-d H:i:s");
                    $this->Exam_Model->insert($params);
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Result successfully');
                }
            }elseif($user==true && $exam==false){
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct exam id');
            }elseif ($user==false && $exam==true) {
                 $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id and exam id');
            }   
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="exam_book",
     *   description="Exam book",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Exam book",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="exam_id",
     *           description="Exam id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="amount",
     *           description="Amount",
     *           paramType="form",
     *           required=true,
     *           type="number",
     *           format="float"
     *         ),
     *         @SWG\Parameter(
     *           name="transaction_id",
     *           description="Transaction id",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="transaction_detail",
     *           description="Transaction detail",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct exam id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id and exam id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, user id and exam id already exists message"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Unsuccessfully"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function exam_book(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('Exam_Model');
            $this->load->library('email');
            $user_id=$this->input->post('user_id');
            $exam_id=$this->input->post('exam_id');
            $user = $this->Exam_Model->check_user($user_id);
            $exam = $this->Exam_Model->check_exam_id($exam_id);
            if($user==true && $exam==true){
                $exam_book_already = $this->Exam_Model->check_user_exam_book('delete_status!="1" AND user_id="'.$user_id.'" AND exam_id="'.$exam_id.'"');
                if($exam_book_already==true){
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'user id and exam id already exists message');
                }else{
                    $params = array();
                    $params ['user_id']             = $this->input->post('user_id');
                    $params ['exam_id']             = $this->input->post('exam_id');
                    $params ['amount']              = $this->input->post('amount');
                    $params ['transaction_id']      = $this->input->post('transaction_id');
                    $params ['transaction_detail']  = $this->input->post('transaction_detail');
                    $params ['created_at']          =  date("Y-m-d H:i:s");
                    $last_id=$this->Exam_Model->insert_exambook($params);
                    if($last_id){
                        $exambook_data = $this->Exam_Model->get_exambook_data('exam.delete_status!="1" AND exam_book.id="'.$last_id.'"');
                            $fullname='';
                            if(isset($exambook_data->fullname)){
                               $fullname=$exambook_data->fullname; 
                            }
                            $email='';
                            if(isset($exambook_data->email)){
                               $email=$exambook_data->email; 
                            }
                            $mobile='';
                            if(isset($exambook_data->mobile)){
                               $mobile=$exambook_data->mobile; 
                            }
                            $exam_title='';
                            if(isset($exambook_data->exam_title)){
                               $exam_title=$exambook_data->exam_title; 
                            }
                            $standard_name='';
                            if(isset($exambook_data->standard_name)){
                               $standard_name=$exambook_data->standard_name; 
                            }
                            $subject_name='';
                            if(isset($exambook_data->subject_name)){
                               $subject_name=$exambook_data->subject_name; 
                            }
                            $amount='';
                            if(isset($exambook_data->amount)){
                               $amount=$exambook_data->amount; 
                            }
                            $transaction_id='';
                            if(isset($exambook_data->transaction_id)){
                               $transaction_id=$exambook_data->transaction_id; 
                            }

                            $template_data = $this->Exam_Model->get_template_data('main.setting_code="examBook_customer" AND main.delete_status!="1" AND sub.delete_status!="1"');
                                if(!empty($template_data)){
                                    //subject
                                    $subject = !isset($template_data->subject)?'':$template_data->subject;
                                    $subject = str_replace("{{customer_name}}", $fullname, $subject);
                                    //description
                                    $description = !isset($template_data->description)?'':$template_data->description;
                                    $description = str_replace("{{customer_name}}", $fullname, $description);
                                    $description = str_replace("{{customer_mobile}}", $mobile, $description);
                                    $description = str_replace("{{customer_email}}", $email, $description);
                                    $description = str_replace("{{exam_title}}", $exam_title, $description);
                                    $description = str_replace("{{standard_name}}", $standard_name, $description);
                                    $description = str_replace("{{subject_name}}", $subject_name, $description);
                                    $description = str_replace("{{amount}}", $amount, $description);
                                    $description = str_replace("{{transaction_id}}", $transaction_id, $description);


                                    $from_name = !isset($template_data->from_name)?'':$template_data->from_name;
                                    $from_email = !isset($template_data->from_email)?'':$template_data->from_email;
                                    
                                    $config['protocol']     = 'smtp';
                                    $config['smtp_host']    = 'ssl://smtp.gmail.com';
                                    $config['smtp_port']    = '465';
                                    $config['smtp_timeout'] = '7';
                                    $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                                    $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                                    $config['charset']      = 'utf-8';
                                    $config['newline']      = "\r\n";
                                    $config['mailtype']     = 'html'; // or html
                                    $config['validation']   = TRUE; // bool whether to validate email or not      
                                    $this->email->initialize($config);
                                    $this->email->from($from_email, $from_name);
                                    $this->email->to($email); 
                                    $this->email->subject($subject);
                                    $this->email->message($description);
                                    $this->email->send();
                                }
                            
                            $admin_template_data = $this->Exam_Model->get_template_data('main.setting_code="examBook_admin" AND main.delete_status!="1" AND sub.delete_status!="1"');
                                if(!empty($admin_template_data)){
                                    //subject
                                    $admin_subject = !isset($admin_template_data->subject)?'':$admin_template_data->subject;
                                    $admin_subject = str_replace("{{customer_name}}", $fullname, $admin_subject);
                                    //description

                                    $admin_description = !isset($admin_template_data->description)?'':$admin_template_data->description;
                                    $admin_description = str_replace("{{customer_name}}", $fullname, $admin_description);
                                    $admin_description = str_replace("{{customer_mobile}}", $mobile, $admin_description);
                                    $admin_description = str_replace("{{customer_email}}", $email, $admin_description);
                                    $admin_description = str_replace("{{exam_title}}", $exam_title, $admin_description);
                                    $admin_description = str_replace("{{standard_name}}", $standard_name, $admin_description);
                                    $admin_description = str_replace("{{subject_name}}", $subject_name, $admin_description);
                                    $admin_description = str_replace("{{amount}}", $amount, $admin_description);
                                    $admin_description = str_replace("{{transaction_id}}", $transaction_id, $admin_description);

                                    $admin_from_name = !isset($admin_template_data->from_name)?'':$admin_template_data->from_name;
                                    $admin_from_email = !isset($admin_template_data->from_email)?'':$admin_template_data->from_email;

                                    $config['protocol']     = 'smtp';
                                    $config['smtp_host']    = 'ssl://smtp.gmail.com';
                                    $config['smtp_port']    = '465';
                                    $config['smtp_timeout'] = '7';
                                    $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                                    $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                                    $config['charset']      = 'utf-8';
                                    $config['newline']      = "\r\n";
                                    $config['mailtype']     = 'html'; // or html
                                    $config['validation']   = TRUE; // bool whether to validate email or not      
                                    $this->email->initialize($config);
                                    $this->email->from($email, $fullname);
                                    $this->email->to($admin_from_email); 
                                    $this->email->subject($admin_subject);
                                    $this->email->message($admin_description);
                                    $this->email->send();
                                }
                        $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Successfully'); 
                    }else{
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Unsuccessfully');
                    }
                }
            }elseif($user==true && $exam==false){
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct exam id');
            }elseif ($user==false && $exam==true) {
                 $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id and exam id');
            }
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
        /**
     *
     * @SWG\Api(
     *   path="upcoming_exam",
     *   description="Upcoming exam",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="Upcoming exam",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function upcoming_exam(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $user_id = $this->input->get('user_id');
            $this->load->model('Exam_Model');
            $user = $this->Exam_Model->check_user($user_id);
            if($user==true){
                $exam_data =$this->Exam_Model->upcoming_exam_list($user_id);
                if(!empty($exam_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $exam_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
       /**
    *
    * @SWG\Api(
    *   path="past_exam",
    *   description="Past exam",
    *   @SWG\Operations(
    *     @SWG\Operation(
    *       method="GET",
    *       summary="Past exam",
    *       notes="",
    *       nickname="Users",
    *       @SWG\Parameters(
    *         @SWG\Parameter(
    *           name="access_token",
    *           description="Username",
    *           paramType="query",
    *           required=true,
    *           type="string"
    *         ),
    *         @SWG\Parameter(
    *           name="user_id",
    *           description="User id",
    *           paramType="form",
    *           required=true,
    *           type="integer"
    *         )
    *       ),
    *       @SWG\ResponseMessages(
    *          @SWG\ResponseMessage(
    *            code=200,
    *            message="status = 0, No data available"
    *          ),
    *          @SWG\ResponseMessage(
    *            code=200,
    *            message="status = 1, Success"
    *          ),
    *          @SWG\ResponseMessage(
    *            code=200,
    *            message="status = 0, Please enter correct user id"
    *          ),
    *          @SWG\ResponseMessage(
    *            code=403,
    *            message="status = 0, Authentication failed !!!"
    *          ),
    *          @SWG\ResponseMessage(
    *            code=404,
    *            message="Not found"
    *          )
    *       )
    *     )
    *   )
    * )
    */
   public function past_exam(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $user_id = $this->input->get('user_id');
            $this->load->model('Exam_Model');
            $user = $this->Exam_Model->check_user($user_id);
            if($user==true){
                $exam_data =$this->Exam_Model->past_exam_list($user_id);
                if(!empty($exam_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $exam_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
       $this->response($response);
   }
     /**
   *
   * @SWG\Api(
   *   path="exam_detail",
   *   description="Exam detail",
   *   @SWG\Operations(
   *     @SWG\Operation(
   *       method="GET",
   *       summary="Exam detail",
   *       notes="",
   *       nickname="Users",
   *       @SWG\Parameters(
   *         @SWG\Parameter(
   *           name="access_token",
   *           description="Username",
   *           paramType="query",
   *           required=true,
   *           type="string"
   *         ),
   *         @SWG\Parameter(
   *           name="user_id",
   *           description="User id",
   *           paramType="form",
   *           required=true,
   *           type="integer"
   *         ),
   *         @SWG\Parameter(
   *           name="exam_id",
   *           description="Exam id",
   *           paramType="form",
   *           required=true,
   *           type="integer"
   *         )
   *       ),
   *       @SWG\ResponseMessages(
   *          @SWG\ResponseMessage(
   *            code=200,
   *            message="status = 0, No data available"
   *          ),
   *          @SWG\ResponseMessage(
   *            code=200,
   *            message="status = 0, Please enter correct user id"
   *          ),
   *          @SWG\ResponseMessage(
   *            code=200,
   *            message="status = 0, Please enter correct exam id"
   *          ),
   *          @SWG\ResponseMessage(
   *            code=200,
   *            message="status = 0, Please enter correct user id and exam id"
   *          ),
   *          @SWG\ResponseMessage(
   *            code=403,
   *            message="status = 0, Authentication failed !!!"
   *          ),
   *          @SWG\ResponseMessage(
   *            code=200,
   *            message="status = 1, Success"
   *          ),
   *          @SWG\ResponseMessage(
   *            code=404,
   *            message="Not found"
   *          )
   *       )
   *     )
   *   )
   * )
   */
    public function exam_detail(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('Exam_Model');
            $user_id = $this->input->get('user_id');
            $exam_id = $this->input->get('exam_id');
            $user = $this->Exam_Model->check_user($user_id);
            $exam = $this->Exam_Model->check_exam_id($exam_id);
            if($user==true && $exam==true){
                //$data_to_send=[];
                //$exam_data =$this->Exam_Model->get_exam_data('exam.delete_status!="1" AND exam.id="'.$exam_id.'"');
                $exam_data =$this->Exam_Model->get_exam_data('exam.delete_status!="1" AND exam.id="'.$exam_id.'" AND exam_book.user_id="'.$user_id.'"');
                // $exam_data =  (array) $exam_data;
                if(!empty($exam_data)){
                    $user_data =$this->Exam_Model->get_exam_userResultdata('exam.delete_status!="1" AND exam_result.delete_status!="1" AND exam_result.exam_id="'.$exam_id.'" AND (exam_result.winning_no BETWEEN 1 AND 10 )');
                        if(!empty($user_data)){
                            $exam_data->result         = $user_data;
                        }
                        //$data_to_send['exam_detail']    = $exam_data;
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $exam_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }elseif ($user==false && $exam==true) {
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }elseif ($user==true && $exam==false) {
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct exam id');
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id and exam id');
            }
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="exam_start",
     *   description="Exam start",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Exam start",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="exam_id",
     *           description="Exam id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="start_date",
     *           description="Start date (d-m-Y)",
     *           paramType="form",
     *           required=false,
     *           type="string",
     *           format="date"
     *         ),
    *         @SWG\Parameter(
     *           name="start_time",
     *           description="Start time (H:i:s)",
     *           paramType="form",
     *           required=false,
     *           type="string",
     *           format="time"
     *         ),
    *         @SWG\Parameter(
     *           name="end_time",
     *           description="End time (H:i:s)",
     *           paramType="form",
     *           required=false,
     *           type="string",
     *           format="time"
     *         ),
     *         @SWG\Parameter(
     *           name="status",
     *           description="Status",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct exam id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id and exam id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Unsuccessfully"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function exam_start(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('Exam_Model');
            $this->load->library('email');
            $user_id=$this->input->post('user_id');
            $exam_id=$this->input->post('exam_id');
            $user = $this->Exam_Model->check_user($user_id);
            $exam = $this->Exam_Model->check_exam_id($exam_id);
            if($user==true && $exam==true){
                $exam_start_already = $this->Exam_Model->check_user_exam_start('user_id="'.$user_id.'" AND exam_id="'.$exam_id.'"');
                if($exam_start_already==true){
                    $params = array();
                    if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$this->input->post('start_date'))) {
                        $params ['start_date']      = date('Y-m-d',strtotime($this->input->post('start_date')));
                    }

                    if(preg_match("/^(0?\d|1\d|2[0-3]):[0-5]\d:[0-5]\d$/",$this->input->post('start_time')) || preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/",$this->input->post('start_time'))
                        ){
                        $params ['start_time']      = date('H:i:s',strtotime($this->input->post('start_time')));
                    }

                    if(preg_match("/^(0?\d|1\d|2[0-3]):[0-5]\d:[0-5]\d$/",$this->input->post('end_time')) || preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/",$this->input->post('end_time'))
                        ){
                        $params ['end_time']      = date('H:i:s',strtotime($this->input->post('end_time')));
                    }

                    $params ['status']       = $this->input->post('status');
                    $params ['updated_at']   =  date("Y-m-d H:i:s");
                    $this->Exam_Model->update_exam_start($params,'user_id="'.$user_id.'" AND exam_id="'.$exam_id.'"');
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Successfully');
                }else{
                    
                    $params = array();
                    $params ['user_id']             = $this->input->post('user_id');
                    $params ['exam_id']             = $this->input->post('exam_id');
                    $params ['start_date']=NULL;
                    if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$this->input->post('start_date'))) {
                        $params ['start_date']      = date('Y-m-d',strtotime($this->input->post('start_date')));
                    }

                    $params ['start_time']=NULL;
                    if(preg_match("/^(0?\d|1\d|2[0-3]):[0-5]\d:[0-5]\d$/",$this->input->post('start_time')) || preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/",$this->input->post('start_time'))
                        ){
                        $params ['start_time']      = date('H:i:s',strtotime($this->input->post('start_time')));
                    }

                    $params ['status']=0;
                    if($this->input->post('status')!=''){
                         $params ['status']         = $this->input->post('status');
                    }
                    $params ['created_at']          =  date("Y-m-d H:i:s");
                    $last_id=$this->Exam_Model->insert_exam_start($params);
                    if($last_id){
                        $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Successfully'); 
                    }else{
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Unsuccessfully');
                    }
                }
            }elseif($user==true && $exam==false){
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct exam id');
            }elseif ($user==false && $exam==true) {
                 $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id and exam id');
            }
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
}