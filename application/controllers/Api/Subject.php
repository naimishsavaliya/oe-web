<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Swagger\Annotations as SWG;

/**
 * @package
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *  apiVersion="1",
 *  swaggerVersion="1.2",
 *  resourcePath="/Subject",
 *  basePath="/api/v1/",
 *  produces="['application/json']"
 * )
 */

class Subject extends MY_Controller
{
    public function __construct(){
        
        parent::__construct();
        $this->load->library('encryption');
    }
    
    /**
     *
     * @SWG\Api(
     *   path="subject",
     *   description="Subject",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="Subject",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function subject(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $user_id = $this->input->get('user_id');
            //var_dump($user_id); die();
            $this->load->model('user_model');
            $user = $this->user_model->check_user($user_id);
            if($user==true){
                $subject_data =$this->user_model->list_subject();
                if(!empty($subject_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $subject_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }

            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user Id');
            }   
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
        }
        $this->response($response);
    }
}