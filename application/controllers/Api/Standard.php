<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Swagger\Annotations as SWG;

/**
 * @package
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *  apiVersion="1",
 *  swaggerVersion="1.2",
 *  resourcePath="/Standard",
 *  basePath="/api/v1/",
 *  produces="['application/json']"
 * )
 */

class Standard extends MY_Controller
{
    public function __construct(){
        
        parent::__construct();
        $this->load->library('encryption');
    }

    /**
     *
     * @SWG\Api(
     *   path="standard",
     *   description="Standard",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="Standard",
     *       notes="",
     *       nickname="Standards",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="subject_id",
     *           description="Subject id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct subject id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id and subject id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function standard(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model');
            $user_id = $this->input->get('user_id');
            $subject_id = $this->input->get('subject_id');
            $user = $this->user_model->check_user($user_id);
            $subject = $this->user_model->check_subject($subject_id);
            if($user==true && $subject==true){
                $standard_data =$this->user_model->list_standard();
                if(!empty($standard_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $standard_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }

            }elseif($user==true && $subject==false){
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct subject id');
            }elseif ($user==false && $subject==true) {
                 $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id and subject id');
            }
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
        }
        $this->response($response);
    }
}