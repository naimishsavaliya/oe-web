<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Swagger\Annotations as SWG;

/**
 * @package
 * @category
 * @subpackage
 *
 * @SWG\Resource(
 *  apiVersion="1",
 *  swaggerVersion="1.2",
 *  resourcePath="/Users",
 *  basePath="/api/v1/",
 *  produces="['application/json']"
 * )
 */

class User extends MY_Controller
{
    public function __construct(){
        
        parent::__construct();
        $this->load->library('encryption');
    }
    
    /**
     *
     * @SWG\Api(
     *   path="auth",
     *   description="Authentication",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Authentication with client_id and client_secret",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="client_id",
     *           description="client id",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="client_secret",
     *           description="client secret",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="grant_type",
     *           description="grant type default client_credentials",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Invalid client_id or client_secret key"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),          
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function generateToken(){
        $response = $this->apibase->generateToken();
        if($response->getStatusCode() == 200){
            $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Token generated successfully', 'params' => $response->getParameters());
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Invalid client_id or client_secret key');
        }
        $this->response($response);
    }
    
    /**
     *
     * @SWG\Api(
     *   path="login",
     *   description="User login",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="User login with username and password",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="username",
     *           description="Username",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="ud_id",
     *           description="UDID",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="mobile_type",
     *           description="Mobile type (ANDROID,IPHONE)",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="password",
     *           description="password",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Invalid username or password"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Password incorrect"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function login(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model');
            $username    = $this->input->post('username',null);
            $password    = $this->input->post('password',null);
            $ud_id       = $this->input->post('ud_id');
            $mobile_type = $this->input->post('mobile_type');
            
            $username = ($username != '') ? $username : false;

            if(!$username && !$password){
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
            }        
            if($userdetail = $this->user_model->login($username)){
                if(!empty($userdetail)){
                    if($userdetail->profile_pic != ''){
                        $userdetail->profile_pic = base_url('assets/uploads/profile/').$userdetail->profile_pic;
                    }
                    //var_dump($userdetail[0]['password']); die();
                    $get_password = $this->encryption->decrypt($userdetail->password);
                    if($password == $get_password){
                        $records = array('user_id' => $userdetail->user_id,
                                        'fullname' => $userdetail->fullname,
                                        'mobile' => $userdetail->mobile ,
                                        'email' => $userdetail->email,
                                        'profile_pic' => $userdetail->profile_pic);
                        $response = array('status'=>1, 'message_type'=>'success', 'message' => 'User logged in successfully', 'params' => $records);
                    }else{
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Password incorrect');
                    }
                }else{
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Invalid username or password');
                }
            }else{
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Invalid username or password');
            }
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');   
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="register",
     *   description="User register",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="User register",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="fullname",
     *           description="Full name",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="mobile",
     *           description="Mobile no.",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="email",
     *           description="Email",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="birthdate",
     *           description="Birthdate (d-m-Y)",
     *           paramType="form",
     *           required=true,
     *           type="string",
     *           format="date"
     *         ),
     *         @SWG\Parameter(
     *           name="profile_pic",
     *           description="Profile Pic",
     *           paramType="form",
     *           required=false,
     *           type="file"
     *         ),
     *         @SWG\Parameter(
     *           name="ud_id",
     *           description="UDID",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="mobile_type",
     *           description="Mobile type (ANDROID,IPHONE)",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="password",
     *           description="Password",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, User already exists"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, User registred unsuccessfully"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function register(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model');
            $this->load->library('email');
            $result = $this->user_model->check_duplicate($this->input->post('mobile'),$this->input->post('email'));
            if($result>0){
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'User already exists');
            }else{
                $params = array();
                if(isset($_FILES['profile_pic']['name'])){
                    $path_to_upload = './assets/uploads/profile';
                    $name=$_FILES['profile_pic']['name'];
                    $explode=explode('.',$name);
                    $profile = time().".".$explode[1];
                        move_uploaded_file($_FILES['profile_pic']['tmp_name'],$path_to_upload.'/'.$profile);
                }else{
                    $profile='';
                }
                //var_dump($profile); die();
                $params ['fullname'] = $this->input->post('fullname');
                $params ['mobile'] = $this->input->post('mobile');
                $params ['email'] = $this->input->post('email');
                $params ['birthdate'] = date('Y-m-d',strtotime($this->input->post('birthdate')));
                $params ['profile_pic']= $profile;
                $params ['ud_id'] = $this->input->post('ud_id');
                $params ['mobile_type'] = $this->input->post('mobile_type');
                $params ['password'] = $this->encryption->encrypt($this->input->post('password'));
                $params ['created_at'] =  date("Y-m-d H:i:s");
                $last_id=$this->user_model->insert($params);
                if($last_id){
                    $template_data = $this->user_model->get_template_data('main.setting_code="welcome_user" AND main.delete_status!="1" AND sub.delete_status!="1"');
                        if(!empty($template_data)){
                            //subject
                            $subject = !isset($template_data->subject)?'':$template_data->subject;
                            $subject = str_replace("{{customer_name}}", $this->input->post("fullname"), $subject);
                            //description
                            $description = !isset($template_data->description)?'':$template_data->description;
                            $description = str_replace("{{customer_name}}", $this->input->post("fullname"), $description);

                            $from_name = !isset($template_data->from_name)?'':$template_data->from_name;
                            $from_email = !isset($template_data->from_email)?'':$template_data->from_email;
                            
                            $config['protocol']     = 'smtp';
                            $config['smtp_host']    = 'ssl://smtp.gmail.com';
                            $config['smtp_port']    = '465';
                            $config['smtp_timeout'] = '7';
                            $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                            $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                            $config['charset']      = 'utf-8';
                            $config['newline']      = "\r\n";
                            $config['mailtype']     = 'html'; // or html
                            $config['validation']   = TRUE; // bool whether to validate email or not      
                            $this->email->initialize($config);
                            $this->email->from($from_email, $from_name);
                            $this->email->to($this->input->post('email')); 
                            $this->email->subject($subject);
                            $this->email->message($description);
                            $this->email->send();
                        }
                    $admin_template_data = $this->user_model->get_template_data('main.setting_code="user_info" AND main.delete_status!="1" AND sub.delete_status!="1"');
                    if(!empty($admin_template_data)){
                            //subject
                            if($profile!=''){
                                $src=base_url('assets/uploads/profile/'.$profile);
                                $profile_image   = '<img src="'.$src.'" width="250px" height="125px" style="margin-left:10px;">';
                            }else{
                                $profile_image='No Image';
                            }
                            
                            $admin_subject = !isset($admin_template_data->subject)?'':$admin_template_data->subject;
                            $admin_subject = str_replace("{{customer_name}}", $this->input->post("fullname"), $admin_subject);

                            $admin_from_name = !isset($admin_template_data->from_name)?'':$admin_template_data->from_name;
                            $admin_from_email = !isset($admin_template_data->from_email)?'':$admin_template_data->from_email;

                            //description
                            $admin_description = !isset($admin_template_data->description)?'':$admin_template_data->description;
                            $admin_description = str_replace("{{customer_name}}", $this->input->post("fullname"), $admin_description);
                            $admin_description = str_replace("{{customer_mobile}}", $this->input->post("mobile"), $admin_description);
                            $admin_description = str_replace("{{customer_email}}", $this->input->post("email"), $admin_description);
                            $admin_description = str_replace("{{customer_birthdate}}", $this->input->post("birthdate"), $admin_description);
                            $admin_description = str_replace("{{customer_profile}}", $profile_image, $admin_description);
                            $admin_description = str_replace("{{customer_udid}}", $this->input->post("ud_id"), $admin_description);
                            $admin_description = str_replace("{{customer_mobile_type}}", $this->input->post("mobile_type"), $admin_description);

                            $config['protocol']     = 'smtp';
                            $config['smtp_host']    = 'ssl://smtp.gmail.com';
                            $config['smtp_port']    = '465';
                            $config['smtp_timeout'] = '7';
                            $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                            $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                            $config['charset']      = 'utf-8';
                            $config['newline']      = "\r\n";
                            $config['mailtype']     = 'html'; // or html
                            $config['validation']   = TRUE; // bool whether to validate email or not      
                            $this->email->initialize($config);
                            $this->email->from($this->input->post('email'), $this->input->post('fullname'));
                            $this->email->to($admin_from_email); 
                            $this->email->subject($admin_subject);
                            $this->email->message($admin_description);
                            $this->email->send();
                        }
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'User registred successfully'); 
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'User registred unsuccessfully');
                } 
            }
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
        }
        $this->response($response);
    }
    
    /**
     *
     * @SWG\Api(
     *   path="profile",
     *   description="Profile",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="Profile",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function profile(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model');
            $user_id = $this->input->get('user_id');
            $user = $this->user_model->check_user($user_id);
            if($user==true){
                $profile_data =$this->user_model->get_profile_data('user.id="'.$user_id.'" AND user.status="1"');
                if(!empty($profile_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $profile_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user Id');
            }
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="profile_edit",
     *   description="Profile edit",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Profile edit",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="fullname",
     *           description="Full name",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="mobile",
     *           description="Mobile no.",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="email",
     *           description="Email",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="birthdate",
     *           description="Birthdate (d-m-Y)",
     *           paramType="form",
     *           required=false,
     *           type="string",
     *           format="date"
     *         ),
     *         @SWG\Parameter(
     *           name="profile_pic",
     *           description="Profile Pic",
     *           paramType="form",
     *           required=false,
     *           type="file"
     *         ),
     *         @SWG\Parameter(
     *           name="ud_id",
     *           description="UDID",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="mobile_type",
     *           description="Mobile type (ANDROID,IPHONE)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, User already exists"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter user Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function profile_edit(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model');
            $user_id = $this->input->post('user_id');
            if($user_id!=''){
                $user = $this->user_model->check_user($user_id);
                if($user==true){
                    $result = $this->user_model->check_email_mobile('(email="'.$this->input->post('email').'" OR mobile="'.$this->input->post('mobile').'") AND id!="'.$user_id.'"');
                    if($result>0){
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'User already exists');
                    }else{
                        $params = array();
                        if($this->input->post('fullname')!=''){
                             $params ['fullname'] = $this->input->post('fullname');
                        }
                        if($this->input->post('mobile')!=''){
                             $params ['mobile'] = $this->input->post('mobile');
                        }
                        if($this->input->post('email')!=''){
                             $params ['email'] = $this->input->post('email');
                        }
                        if($this->input->post('birthdate')!='' && $this->input->post('birthdate')!='00-00-0000'){
                             $params ['birthdate'] = date('Y-m-d',strtotime($this->input->post('birthdate')));
                        }
                        if(isset($_FILES['profile_pic']['name'])){
                         $path_to_upload = './assets/uploads/profile';
                         $name=$_FILES['profile_pic']['name'];
                         $explode=explode('.',$name);
                         $profile = time().".".$explode[1];
                            move_uploaded_file($_FILES['profile_pic']['tmp_name'],$path_to_upload.'/'.$profile);
                            $params ['profile_pic']= $profile;
                        }
                        if($this->input->post('ud_id')!=''){
                             $params ['ud_id'] = $this->input->post('ud_id');
                        }
                        if($this->input->post('mobile_type')!=''){
                             $params ['mobile_type'] = $this->input->post('mobile_type');
                        }
                        $params ['updated_at'] =  date("Y-m-d H:i:s");
                        $this->user_model->update_profile($params,$user_id);
                        $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Profile updated successfully');
                    }
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter user Id');
            } 
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="change_password",
     *   description="Change password",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Change password",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="password",
     *           description="Password",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter user Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function change_password(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model');
            $this->load->library('encryption');
            $user_id = $this->input->post('user_id');
            if($user_id!=''){
                $user = $this->user_model->check_user($user_id);
                if($user==true){
                        $params = array();
                        if($this->input->post('password')!=''){
                             $params ['password'] = $this->encryption->encrypt($this->input->post('password'));
                        }
                        $params ['updated_at'] =  date("Y-m-d H:i:s");
                        $this->user_model->update_pw($params,'id="'.$user_id.'"');
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Password changed successfully');
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter user Id');
            } 
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="forgot_password",
     *   description="Forgot password",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Forgot password",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="email",
     *           description="Email",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct email"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Could not send email, please try again later"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function forgot_password(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->library('email');
            $this->load->helper('string');
            $this->load->model('user_model'); 
            $email = $this->input->post('email');
            if($email!=''){
                $result = $this->user_model->check_email($email);
                if($result==true){
                    $random_value=random_string('alnum', 6);
                    $user_data = $this->user_model->get_user_data('email="'.$email.'"');
                    $template_data = $this->user_model->get_template_data('main.setting_code="forgot_password" AND main.delete_status!="1" AND sub.delete_status!="1"');

                    //subject
                    $subject = !isset($template_data->subject)?'':$template_data->subject;
                    $subject = str_replace("{{customer_name}}", "$user_data->fullname", $subject);
                    //description
                    $description = !isset($template_data->description)?'':$template_data->description;
                    $description = str_replace("{{customer_name}}", "$user_data->fullname", $description);
                    $description = str_replace("{{token}}", "$random_value", $description);
                    $description = str_replace("{{customer_email}}", "$email", $description);

                    $from_name = !isset($template_data->from_name)?'':$template_data->from_name;
                    $from_email = !isset($template_data->from_email)?'':$template_data->from_email;
                    
                    $config['protocol']    = 'smtp';
                    $config['smtp_host']    = 'ssl://smtp.gmail.com';
                    $config['smtp_port']    = '465';
                    $config['smtp_timeout'] = '7';
                    $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                    $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                    $config['charset']    = 'utf-8';
                    $config['newline']    = "\r\n";
                    $config['mailtype'] = 'html'; // or html
                    $config['validation'] = TRUE; // bool whether to validate email or not      
                    $this->email->initialize($config);
                    $this->email->from($from_email, $from_name);
                    $this->email->to($email); 
                    $this->email->subject($subject);
                    $this->email->message($description); 
                    if($this->email->send()){ 
                        $params ['forgot_token']        =  $random_value;
                        $params ['forgot_token_date']   =  date("Y-m-d H:i:s");
                        $this->user_model->insert_forgot_token($params,$email);
                        $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Send email'); 
                    }else{ 
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Could not send email, please try again later');
                    }
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct email');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
            } 
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="reset_password",
     *   description="Reset password",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Reset password",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="email",
     *           description="Email",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="password",
     *           description="Password",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="forgot_token",
     *           description="Forgot token",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct email"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Forgot token does not match"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function reset_password(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model'); 
            $this->load->library('encryption');
            $email = $this->input->post('email');
            if($email!=''){
                $result = $this->user_model->check_email($email);
                $forgot_token = $this->input->post('forgot_token');
                if($result==true){
                    $data=$this->user_model->verify_token('email="'.$email.'" AND forgot_token="'.$forgot_token.'" AND status="1"');
                    if($data==true){
                        $params = array();
                        $params ['password'] = $this->encryption->encrypt($this->input->post('password'));
                        $params ['updated_at'] =  date("Y-m-d H:i:s");
                        $this->user_model->update_pw($params,'email="'.$email.'"');
                        $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Reset password successfully');
                    }else{
                        $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Forgot token does not match');
                    }
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct email');
                }
            }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
            }   
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="user_reward",
     *   description="User reward",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="User reward",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function user_reward(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $user_id = $this->input->get('user_id');
            $this->load->model('user_model');
            $user = $this->user_model->check_user($user_id);
            if($user==true){
                $reward_data =$this->user_model->get_user_reward('user_id="'.$user_id.'" AND status="1" AND delete_status="0"');
                //echo '<pre>'; var_dump($reward_data); die();
                if(!empty($reward_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $reward_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }

            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user Id');
            }
        }else{
           $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!'); 
        }
        $this->response($response);
    }
    /**
     *
     * @SWG\Api(
     *   path="level_detail",
     *   description="Level detail",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="GET",
     *       summary="Level detail",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="level_id",
     *           description="Level id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct level Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user Id and level Id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function level_detail(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $user_id = $this->input->get('user_id');
            $level_id = $this->input->get('level_id');
            $this->load->model('user_model');
            $user = $this->user_model->check_user($user_id);
            $level = $this->user_model->check_level($level_id);
            if($user==true && $level==true){
                $level_data =$this->user_model->get_level_data('main.level_id="'.$level_id.'" AND main.status="1" AND main.delete_status="0"');
                if(!empty($level_data)){
                    $response = array('status'=>1, 'message_type'=>'success', 'message' => 'Success', 'params' => $level_data);
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'No data available');
                }
            }elseif ($user==true && $level==false) {
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct level Id');
            }elseif ($user==false && $level==true) {
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user Id');
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user Id and level Id');
            }
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');  
        }
        $this->response($response);
    }
        /**
     *
     * @SWG\Api(
     *   path="register_device",
     *   description="Register Device",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Register Device",
     *       notes="",
     *       nickname="Users",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="access_token",
     *           description="Username",
     *           paramType="query",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="user_id",
     *           description="User id",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *         ),
     *         @SWG\Parameter(
     *           name="ud_id",
     *           description="UDID",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="token",
     *           description="Token",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="device_name",
     *           description="Device name",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         ),
     *         @SWG\Parameter(
     *           name="mobile_type",
     *           description="Mobile type (ANDROID,IPHONE)",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *         )
     *       ),
     *       @SWG\ResponseMessages(
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, No data available"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, User device registred unsuccessfully"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter user Id and ud_id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 0, Please enter correct user id"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=403,
     *            message="status = 0, Authentication failed !!!"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=200,
     *            message="status = 1, Success"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Not found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function register_device(){
        $response = $this->apibase->verifyToken();
        if($response==true){
            $this->load->model('user_model');
            $user_id = $this->input->post('user_id');
            $ud_id = $this->input->post('ud_id');
            if($user_id!='' && $ud_id!=''){
                $user = $this->user_model->check_user($user_id);
                if($user==true){
                    $device = $this->user_model->check_user_device('ud_id="'.$ud_id.'" AND user_id!="'.$user_id.'" AND status="1" AND delete_status="0"');
                    if($device==true){
                        $this->user_model->user_device_delete('ud_id="'.$ud_id.'" AND user_id!="'.$user_id.'" AND status="1" AND delete_status="0"');
                        $params = array();
                        $params ['user_id']      = $user_id;
                        $params ['ud_id']        = $ud_id;
                        $params ['token']        = $this->input->post('token');
                        $params ['device_name']  = $this->input->post('device_name');
                        $params ['mobile_type']  = $this->input->post('mobile_type');
                        $params ['created_at']   =  date("Y-m-d H:i:s");
                        $last_id=$this->user_model->insert_user_device($params);
                        if($last_id){
                            $response = array('status'=>1, 'message_type'=>'success', 'message' => 'User device registred successfully');
                        }else{
                            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'User device registred unsuccessfully');
                        } 
                    }else{
                        $user_device_alredy = $this->user_model->check_user_device('ud_id="'.$ud_id.'" AND user_id="'.$user_id.'" AND status="1" AND delete_status="0"');
                        if($user_device_alredy==true){
                            $params = array();
                            $params ['token']        =  $this->input->post('token');
                            //$params ['device_name']  = $this->input->post('device_name');
                            //$params ['mobile_type']  = $this->input->post('mobile_type');
                            $params ['updated_at']   =  date("Y-m-d H:i:s");
                            $this->user_model->update_device_token($params,'ud_id="'.$ud_id.'" AND user_id="'.$user_id.'" AND status="1" AND delete_status="0"');
                            $response = array('status'=>1, 'message_type'=>'success', 'message' => 'User device registred successfully');
                            //$response = array('status'=>0, 'message_type'=>'error', 'message' => 'User_id And Ud_id already exists');
                        }else{
                            $params = array();
                            $params ['user_id']      = $user_id;
                            $params ['ud_id']        = $ud_id;
                            $params ['token']        = $this->input->post('token');
                            $params ['device_name']  = $this->input->post('device_name');
                            $params ['mobile_type']  = $this->input->post('mobile_type');
                            $params ['created_at']   =  date("Y-m-d H:i:s");
                            $last_id=$this->user_model->insert_user_device($params);
                            if($last_id){
                                $response = array('status'=>1, 'message_type'=>'success', 'message' => 'User device registred successfully');
                            }else{
                                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'User device registred unsuccessfully');
                            } 
                        }
                    }
                }else{
                    $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter correct user id');
                }
            }else{
                $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Please enter user Id and ud_id');
            }
        }else{
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Authentication failed !!!');  
        }
        $this->response($response); 
    }
}