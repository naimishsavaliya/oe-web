<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_template extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Email_template_model');
	}

	public function index()
	{	

		$jsfilearray = array(
			base_url().'assets/backend/js/template_datatable.js'
		);
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Email template list';
		$this->viewParams['content'] = 'email_template/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Email_template_model->datatable_data($generalSearch);
		$total_rec = $this->Email_template_model->total_record("delete_status!='1'",$generalSearch);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}

    public function add()
	{
		$this->viewParams['page_title'] = 'Add Email template';
		$jsfilearray = array(
			base_url().'assets/backend/css/ckeditor/ckeditor.js',
			base_url().'assets/backend/css/ckeditor/config.js',
			base_url().'assets/backend/css/ckeditor/build-config.js',
			base_url().'assets/backend/js/jquery.validate.min.js',
			base_url().'assets/backend/js/additional-methods.js',
			base_url().'assets/backend/js/email_template.js'
       	);
		$this->viewParams['jsfilearray'] = $jsfilearray;	
		$this->viewParams['content'] = 'email_template/add';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}
	public function save(){
		if($this->input->post()){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('subject', 'Subject', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('email_type', 'Email type', 'required');
			$this->form_validation->set_rules('from_name', 'From name', 'required');
			$this->form_validation->set_rules('from_email', 'From email', 'required|valid_email');
			if ($this->form_validation->run() === FALSE)
			{
				$this->viewParams['page_title'] = 'Add Email template';
				$jsfilearray = array(
					base_url().'assets/backend/css/ckeditor/ckeditor.js',
					base_url().'assets/backend/css/ckeditor/config.js',
					base_url().'assets/backend/css/ckeditor/build-config.js',
					base_url().'assets/backend/js/jquery.validate.min.js',
					base_url().'assets/backend/js/additional-methods.js',
					base_url().'assets/backend/js/email_template.js'
               	);
				$this->viewParams['jsfilearray'] = $jsfilearray;
				$this->viewParams['content'] = 'email_template/add';
				$this->load->view('admin/layout/2colmn-left',$this->viewParams);
			}else{
				$records['name'] 				= $this->input->post('name');
				$records['subject'] 			= $this->input->post('subject');
				$records['description'] 		= $this->input->post('description');
				$records['email_type'] 			= $this->input->post('email_type');
				$records['from_name'] 			= $this->input->post('from_name');
				$records['from_email'] 			= $this->input->post('from_email');
				$records['created_at'] 			= date("Y-m-d H:i:s");
				//echo '<pre>'; var_dump($records); exit();
				$template_id=$this->Email_template_model->insert($records);
				if(!empty($template_id)){
					$this->session->set_flashdata('success', 'Email template added successfully.');
					redirect(ADMIN_BASE.'email_template/list', 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Email template added unsuccessfully.');
					redirect(ADMIN_BASE.'email_template/add', 'refresh');
				}
			}
							
		}
	}
    public function edit($id='')
	{
		$template_data=$this->Email_template_model->get_template_data('id="'.$id.'" AND delete_status!="1"');
		if(empty($template_data)){
			show_404();
		}else{
			$this->viewParams['template_data'] = $template_data;
			$this->viewParams['page_title'] = 'Edit Email template';
			$jsfilearray = array(
				base_url().'assets/backend/css/ckeditor/ckeditor.js',
				base_url().'assets/backend/css/ckeditor/config.js',
				base_url().'assets/backend/css/ckeditor/build-config.js',
				base_url().'assets/backend/js/jquery.validate.min.js',
				base_url().'assets/backend/js/additional-methods.js',
				base_url().'assets/backend/js/email_template.js'
       		);
			$this->viewParams['jsfilearray'] = $jsfilearray;		
			$this->viewParams['content'] = 'email_template/edit';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		
	}
	public function update(){
		if($this->input->post('template_id')){
			$template_id=$this->input->post('template_id');
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('subject', 'Subject', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('email_type', 'Email type', 'required');
			$this->form_validation->set_rules('from_name', 'From name', 'required');
			$this->form_validation->set_rules('from_email', 'From email', 'required|valid_email');
			if ($this->form_validation->run() === FALSE){
				$template_data=$this->Email_template_model->get_template_data('id="'.$template_id.'" AND delete_status!="1"');
				if(empty($template_data)){
					show_404();
				}else{
					$this->viewParams['template_data'] = $template_data;
					$this->viewParams['page_title'] = 'Edit Email template';
					$jsfilearray = array(
						base_url().'assets/backend/css/ckeditor/ckeditor.js',
						base_url().'assets/backend/css/ckeditor/config.js',
						base_url().'assets/backend/css/ckeditor/build-config.js',
						base_url().'assets/backend/js/jquery.validate.min.js',
						base_url().'assets/backend/js/additional-methods.js',
						base_url().'assets/backend/js/email_template.js'
					);
					$this->viewParams['jsfilearray'] = $jsfilearray;		
					$this->viewParams['content'] = 'email_template/edit';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$records['name'] 				= $this->input->post('name');
				$records['subject'] 			= $this->input->post('subject');
				$records['description'] 		= $this->input->post('description');
				$records['email_type'] 			= $this->input->post('email_type');
				$records['from_name'] 			= $this->input->post('from_name');
				$records['from_email'] 			= $this->input->post('from_email');
				$records['updated_at'] 			= date("Y-m-d H:i:s");
				$update=$this->Email_template_model->update($records,$template_id);
				if($update==true){
					$this->session->set_flashdata('success', 'Email template Updated successfully.');
					redirect(ADMIN_BASE.'email_template/edit/'.$template_id, 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Email template Updated unsuccessfully.');
					redirect(ADMIN_BASE.'email_template/edit/'.$template_id, 'refresh');
				}
			}				
		}	
	}
	public function delete($id)
	{
		$deleted_at = date("Y-m-d H:i:s");
		$delete_status = '1';
		$update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
		$this->db->where("id",$id);
		$this->db->update("tbl_email_template",$update);		
		return TRUE;
	}

	public function changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_email_template",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}
	public function setting()
	{	

		$jsfilearray = array(
			base_url().'assets/backend/js/setting_datatable.js'
		);
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Setting list';
		$this->viewParams['content'] = 'email_template/setting_list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function setting_dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Email_template_model->setting_dataTable_data($generalSearch);
		$total_rec = $this->Email_template_model->setting_total_record("setting.delete_status!='1'",$generalSearch);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}
    public function setting_edit($id='')
	{
		$setting_data=$this->Email_template_model->get_setting_data('id="'.$id.'" AND delete_status!="1"');
		if(empty($setting_data)){
			show_404();
		}else{
			$this->viewParams['template_data'] = $this->Email_template_model->fetch_all_template();
			$this->viewParams['setting_data'] = $setting_data;
			$this->viewParams['page_title'] = 'Edit General Setting';
			$jsfilearray = array(
				base_url().'assets/backend/js/jquery.validate.min.js',
				base_url().'assets/backend/js/additional-methods.js',
				base_url().'assets/backend/js/setting.js'
			    		);
			$this->viewParams['jsfilearray'] = $jsfilearray;		
			$this->viewParams['content'] = 'email_template/setting_edit';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		
	}
	public function update_setting(){
		if($this->input->post('setting_id')){
			$setting_id=$this->input->post('setting_id');
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('email_template_id', 'Email template name', 'required');
			$this->form_validation->set_rules('setting_name', 'Setting name', 'required');
			$this->form_validation->set_rules('setting_value', 'Setting value', 'required');
			//$this->form_validation->set_rules('setting_code', 'Setting code', 'required');
			if ($this->form_validation->run() === FALSE){
				$setting_data=$this->Email_template_model->get_setting_data('id="'.$setting_id.'" AND delete_status!="1"');
				if(empty($setting_data)){
					show_404();
				}else{
					$this->viewParams['template_data'] = $this->Email_template_model->fetch_all_template();
					$this->viewParams['setting_data'] = $setting_data;
					$this->viewParams['page_title'] = 'Edit General Setting';
					$jsfilearray = array(
						base_url().'assets/backend/js/jquery.validate.min.js',
						base_url().'assets/backend/js/additional-methods.js',
						base_url().'assets/backend/js/setting.js'
					);
					$this->viewParams['jsfilearray'] = $jsfilearray;		
					$this->viewParams['content'] = 'email_template/setting_edit';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$records['email_template_id'] 	= $this->input->post('email_template_id');
				$records['setting_name'] 		= $this->input->post('setting_name');
				$records['setting_value'] 		= $this->input->post('setting_value');
				//$records['setting_code'] 		= $this->input->post('setting_code');
				$records['updated_at'] 			= date("Y-m-d H:i:s");
				$update=$this->Email_template_model->update_setting($records,$setting_id);
				if($update==true){
					$this->session->set_flashdata('success', 'General Setting Updated successfully.');
					redirect(ADMIN_BASE.'email_template/setting_edit/'.$setting_id, 'refresh');
				}else{
					$this->session->set_flashdata('error', 'General Setting Updated unsuccessfully.');
					redirect(ADMIN_BASE.'email_template/setting_edit/'.$setting_id, 'refresh');
				}
			}				
		}	
	}
	public function setting_delete($id)
	{
		$deleted_at = date("Y-m-d H:i:s");
		$delete_status = '1';
		$update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
		$this->db->where("id",$id);
		$this->db->update("tbl_setting",$update);		
		return TRUE;
	}

	public function setting_changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_setting",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}
	
}