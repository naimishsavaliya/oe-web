<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Subject_model');
	}

	public function index()
	{	
		$jsfilearray = array(
								base_url().'assets/backend/js/subject_datatable.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Subject list';
		$this->viewParams['content'] = 'subject/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Subject_model->list_subject($generalSearch);
		$total_rec = $this->Subject_model->total_record($generalSearch);
		$pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
		//echo json_encode($this->Subject_model->list_subject($generalSearch));
		//exit();
	}

    public function edit($id=0)
	{
		if($id==0){
			//var_dump('ADD'); die();
				$this->viewParams['mode']='add';
		}else{
			//var_dump('EDIT'); die();
				$this->viewParams['mode']='edit';
					$this->viewParams['subject_data'] = array();
					if($id!=0){	
						$this->viewParams['subject_data'] = $this->Subject_model->get_subject_detail(array('id'),array($id));
						//echo '<pre>'; var_dump($this->viewParams['subject_data']->image); die();
						}
		}

			if($this->input->post('mode')){
					$this->load->helper('form');
					$this->load->library('Form_validation');
					if($this->input->post('mode') == 'add'){
						$this->form_validation->set_rules('subject_name', 'Subject Name', 'required');
						//$this->form_validation->set_rules('image', 'Image', 'required');
					}elseif($this->input->post('mode') == 'edit'){
						$this->form_validation->set_rules('subject_name', 'Subject Name', 'required');	
					}

					if ($this->form_validation->run() === FALSE)
					{
						$errors = validation_errors();
						echo json_encode(['error'=>$errors]);
						exit();
					}else{					
						if($this->input->post('mode') == 'edit')
						{
							$path_to_upload = './assets/uploads/subject/';

							if(!file_exists($path_to_upload)){
								$create = mkdir($path_to_upload, 0777, true);
								if ( ! $create)
								return;
							}

							$file_name=$this->viewParams['subject_data']->image;
							if($_FILES['image']['tmp_name']!=''){
								if($this->viewParams['subject_data']->image!=''){
									@unlink('./assets/uploads/subject/'.$this->viewParams['subject_data']->image);
								}
								$name=$_FILES['image']['name'];
								$explode=explode('.',$name);
								$file_name = time().".".$explode[1];

								$config['upload_path']          = $path_to_upload;
							    $config['allowed_types']        = 'gif|jpg|png|jpeg';
							    $config['max_size'] 			= '2048000';
							    $config['file_name'] = $file_name;
								$this->load->library('Upload', $config);
								$this->upload->initialize($config);
								$img = 'image';
								if(!$this->upload->do_upload('image')){
									$errors = array('error' => $this->upload->display_errors());
									echo json_encode(['error'=>$errors]);
									exit();
								}
								else{
									$file = $this->upload->data();
									$name=$file['file_name'];
									$explode=explode('.',$name);
									$file_name = time().".".$explode[1];
									//var_dump($file_name); die();
									//$file_name = time().$file['file_name'];
									$data = ['file' => $path_to_upload.$file['file_name']];
									$this->load->library('Image_lib');
									$config['image_library'] 	= 'gd2';
									$config['source_image'] 	= $path_to_upload.$file['file_name'];
									$config['create_thumb'] 	= false;
									$config['maintain_ratio'] 	= TRUE;
									$config['max_size'] 		= '2048000';
									$config['new_image'] 		= $path_to_upload."thumbs/".$file_name;
									$this->image_lib->clear();
									$this->image_lib->initialize($config);
									$this->image_lib->resize();

								}
							}
							$records = array(
								'name'       			=>	$this->input->post('subject_name'),
								'image'					=>	$file_name,
								'updated_at'  			=>  date("Y-m-d H:i:s")
							);	

							$update=$this->Subject_model->update($records,array('id'=>$id));
							if($update==true){
								echo json_encode(array('success'=>"Subject updated successfully."));
					 			exit();
							}else{
								echo json_encode(['error'=>'Error']);
								exit();
							}

						}elseif($this->input->post('mode') == 'add'){

							$path_to_upload = './assets/uploads/subject/';
							if ( ! file_exists($path_to_upload) ){
								$create = mkdir($path_to_upload, 0777, true);
								if ( ! $create)
								return;
							}
							if($_FILES['image']['tmp_name']!=''){
								$name=$_FILES['image']['name'];
								$explode=explode('.',$name);
								$file_name = time().".".$explode[1];

								$config['upload_path']          = $path_to_upload;
							    $config['allowed_types']        = 'gif|jpg|png|jpeg';
							    $config['max_size'] 			= '2048000';
							    $config['file_name'] = $file_name;
								$this->load->library('Upload', $config);
								$this->upload->initialize($config);
								$img = 'image';
								if ( ! $this->upload->do_upload('image')){
									$errors = array('error' => $this->upload->display_errors());
									echo json_encode(['error'=>$errors]);
									exit();
								}
								else{
									$file = $this->upload->data();
									$name=$file['file_name'];
									$explode=explode('.',$name);
									$file_name = time().".".$explode[1];
									$data = ['file' => $path_to_upload.$file['file_name']];
									$this->load->library('Image_lib');
									$config['image_library'] 	= 'gd2';
									$config['source_image'] 	= $path_to_upload.$file['file_name'];
									$config['create_thumb'] 	= false;
									$config['maintain_ratio'] 	= TRUE;
									$config['max_size'] 		= '2048000';
									$config['new_image'] 		= $path_to_upload."thumbs/".$file_name;
									$this->image_lib->clear();
									$this->image_lib->initialize($config);
									$this->image_lib->resize();
								}
							}
							$records = array(
								'name'       			=>	$this->input->post('subject_name'),
								'image'					=>	$file_name,
								'created_at'  			=>  date("Y-m-d H:i:s")
							);	
							$insert=$this->Subject_model->insert($records);
							if($insert==true){
								echo json_encode(array('success'=>"Subject added successfully."));
					 			exit();
							}else{
								echo json_encode(['error'=>'Error']);
								exit();
							}
						}
					}
								
				}
				if($id==0)
				{
					$this->viewParams['page_title'] = 'Add Subject';
					$jsfilearray = array(
											base_url().'assets/backend/js/jquery.validate.min.js',
											base_url().'assets/backend/js/additional-methods.js',
											base_url().'assets/backend/js/subject.js'
				 	                       );
					$this->viewParams['jsfilearray'] = $jsfilearray;
				}
				else
				{
					$this->viewParams['page_title'] = 'Edit Subject';
					$jsfilearray = array(
											base_url().'assets/backend/js/jquery.validate.min.js',
											base_url().'assets/backend/js/additional-methods.js',
											base_url().'assets/backend/js/subject.js'
				 	                       );
					$this->viewParams['jsfilearray'] = $jsfilearray;
				}
			$this->viewParams['content'] = 'subject/add';
        	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		public function subject_delete($id)
		{
			$this->Subject_model->delete($id);		
			return TRUE;
		}

		public function subject_changestatus($id,$status)
		{ 
			$update=array("status"=>$status);
			$this->db->where("id",$id);
			$this->db->update("tbl_subject",$update);
			echo json_encode(['responce'=>'success']);
			exit();
		
		}


	
}