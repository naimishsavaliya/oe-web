<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Notification_model');
	}

	public function index(){	
		$jsfilearray = array(
			base_url().'assets/backend/js/notification_datatable.js'
		);
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Notification list';
		$this->viewParams['content'] = 'notification/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables(){
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Notification_model->datatable_data($generalSearch);
		$total_rec = $this->Notification_model->total_record("delete_status!='1'",$generalSearch);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}

    public function add()
	{
		$this->viewParams['users_data'] = $this->Notification_model->get_users_data();
		$this->viewParams['page_title'] = 'Add Notification';
		$jsfilearray = array(
			base_url().'assets/backend/js/jquery.validate.min.js',
			base_url().'assets/backend/js/additional-methods.js',
			base_url().'assets/backend/js/notification.js'
       	);
		$this->viewParams['jsfilearray'] = $jsfilearray;	
		$this->viewParams['content'] = 'notification/add';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}
	public function save(){
		if($this->input->post()){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('mul_user[]','Users','required');
			$this->form_validation->set_rules('message', 'Message', 'required');
			$this->form_validation->set_rules('notification_type', 'Notification type', 'required');
			if ($this->form_validation->run() === FALSE){
				$this->viewParams['users_data'] = $this->Notification_model->get_users_data();
				$this->viewParams['page_title'] = 'Add Notification';
				$jsfilearray = array(
					base_url().'assets/backend/js/jquery.validate.min.js',
					base_url().'assets/backend/js/additional-methods.js',
					base_url().'assets/backend/js/notification.js'
				);
				$this->viewParams['jsfilearray'] = $jsfilearray;	
				$this->viewParams['content'] = 'notification/add';
				$this->load->view('admin/layout/2colmn-left',$this->viewParams);
			}else{
				$notification_mode=$this->input->post('notification_mode');
				if($notification_mode=='send_now'){
					$notification_date=Null;
				}else{
					$notification_date = date('Y-m-d\TH:i:s',strtotime($this->input->post('notification_date')));
				}
				$mul_user = implode(',',$this->input->post('mul_user'));
				$records['users'] 				= $mul_user;
				$records['message'] 			= $this->input->post('message');
				$records['subject'] 			= $this->input->post('subject');
				$records['notification_type'] 	= $this->input->post('notification_type');
				$records['notification_date'] 	= $notification_date;
				$records['created_at'] 			= date("Y-m-d H:i:s");
				// echo '<pre>'; var_dump($records); exit();
				$notification_id=$this->Notification_model->insert($records);
				if(!empty($notification_id)){
					$this->session->set_flashdata('success', 'Notification added successfully.');
					redirect(ADMIN_BASE.'notification/list', 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Notification added unsuccessfully.');
					redirect(ADMIN_BASE.'notification/add', 'refresh');
				}
			}					
		}
	}
    public function edit($id=''){
		$notification_data=$this->Notification_model->get_notification_data('id="'.$id.'" AND delete_status!="1"');
		if(empty($notification_data)){
			show_404();
		}else{
			$this->viewParams['users_data'] = $this->Notification_model->get_users_data();
			$this->viewParams['notification_data'] = $notification_data;
			$this->viewParams['page_title'] = 'Edit Notification';
			$jsfilearray = array(
				base_url().'assets/backend/js/jquery.validate.min.js',
				base_url().'assets/backend/js/additional-methods.js',
				base_url().'assets/backend/js/notification.js'
       		);
			$this->viewParams['jsfilearray'] = $jsfilearray;		
			$this->viewParams['content'] = 'notification/edit';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}	
	}
	public function update(){
		if($this->input->post('notification_id')){
			$notification_id=$this->input->post('notification_id');
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('mul_user[]','Users','required');
			$this->form_validation->set_rules('message', 'Message', 'required');
			$this->form_validation->set_rules('notification_type', 'Notification type', 'required');
			if ($this->form_validation->run() === FALSE){
				$notification_data=$this->Notification_model->get_notification_data('id="'.$notification_id.'" AND delete_status!="1"');
				if(empty($notification_data)){
					show_404();
				}else{
					$this->viewParams['users_data'] = $this->Notification_model->get_users_data();
					$this->viewParams['notification_data'] = $notification_data;
					$this->viewParams['page_title'] = 'Edit Notification';
					$jsfilearray = array(
						base_url().'assets/backend/js/jquery.validate.min.js',
						base_url().'assets/backend/js/additional-methods.js',
						base_url().'assets/backend/js/notification.js'
					);
					$this->viewParams['jsfilearray'] = $jsfilearray;		
					$this->viewParams['content'] = 'notification/edit';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$notification_mode=$this->input->post('notification_mode');
				if($notification_mode=='send_now'){
					$notification_date=Null;
				}else{
					$notification_date = date('Y-m-d\TH:i:s',strtotime($this->input->post('notification_date')));
				}
				$mul_user = implode(',',$this->input->post('mul_user'));
				$records['users'] 				= $mul_user;
				$records['message'] 			= $this->input->post('message');
				$records['subject'] 			= $this->input->post('subject');
				$records['notification_type'] 	= $this->input->post('notification_type');
				$records['notification_date'] 	= $notification_date;
				$records['updated_at'] 			= date("Y-m-d H:i:s");
				$update=$this->Notification_model->update($records,$notification_id);
				if($update==true){
					$this->session->set_flashdata('success', 'Notification Updated successfully.');
					redirect(ADMIN_BASE.'notification/edit/'.$notification_id, 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Notification Updated unsuccessfully.');
					redirect(ADMIN_BASE.'notification/edit/'.$notification_id, 'refresh');
				}
			}				
		}	
	}
	public function delete($id){
		$deleted_at = date("Y-m-d H:i:s");
		$delete_status = '1';
		$update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
		$this->db->where("id",$id);
		$this->db->update("tbl_notification",$update);		
		return TRUE;
	}

	public function changestatus($id,$status){ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_notification",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}
	public function users_list($notification_id=''){
		$notification_data= $this->Notification_model->check_notification("id='".$notification_id."' AND delete_status!='1'");
		if(empty($notification_data)){
			show_404();
		}else{
			$this->viewParams['notification_id'] 	= $notification_data->id;
			$this->viewParams['page_title'] 		= 'Notification user list';
			$jsfilearray = array(
				base_url('assets/backend/js/notification_user_datatable.js')
			);
			$this->viewParams['jsfilearray'] 		= $jsfilearray;	
			$this->viewParams['content'] 			= 'notification/users_list';
			$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
	}
	public function users_dataTables($notification_id=''){
		$notification_data= $this->Notification_model->check_notification("id='".$notification_id."' AND delete_status!='1'");
		$users='';
		if(isset($notification_data->users)){
			$users=$notification_data->users;
		}
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Notification_model->users_datatable_data($generalSearch,$users);
		$total_rec = $this->Notification_model->users_total_record($generalSearch,$users);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}
	
}