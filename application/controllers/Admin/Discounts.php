<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discounts extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Discount_model');
	}

	public function index()
	{	

		$jsfilearray = array(
			base_url().'assets/backend/js/discount_datatable.js'
		);
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Discount list';
		$this->viewParams['content'] = 'discount/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Discount_model->datatable_data($generalSearch);
		$total_rec = $this->Discount_model->total_record("delete_status!='1'",$generalSearch);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}

    public function add()
	{
		$this->viewParams['page_title'] = 'Add Discount';
		$jsfilearray = array(
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/discount.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;	
		$this->viewParams['content'] = 'discount/add';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}
	public function save(){
		if($this->input->post()){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('discount_code', 'Discount code', 'required');
			$this->form_validation->set_rules('discount_type', 'Discount type', 'required');
			$this->form_validation->set_rules('amount', 'Amount', 'required');
			$this->form_validation->set_rules('per_user_use_count', 'Per user use count', 'required');
			$this->form_validation->set_rules('total_use_count', 'Total use count', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->viewParams['page_title'] = 'Add Discount';
				$jsfilearray = array(
					base_url().'assets/backend/js/jquery.validate.min.js',
					base_url().'assets/backend/js/additional-methods.js',
					base_url().'assets/backend/js/discount.js'
				);
				$this->viewParams['jsfilearray'] = $jsfilearray;	
				$this->viewParams['content'] = 'discount/add';
				$this->load->view('admin/layout/2colmn-left',$this->viewParams);
			}else{
				$records['title'] 				= $this->input->post('title');
				$records['discount_code'] 		= $this->input->post('discount_code');
				$records['discount_type'] 		= $this->input->post('discount_type');
				$records['amount'] 				= $this->input->post('amount');
				$records['per_user_use_count'] 	= $this->input->post('per_user_use_count');
				$records['total_use_count'] 	= $this->input->post('total_use_count');
				$records['created_at'] 			= date("Y-m-d H:i:s");
				//echo '<pre>'; var_dump($records); exit();
				$discount_id=$this->Discount_model->insert($records);
				if(!empty($discount_id)){
					$this->session->set_flashdata('success', 'Discount added successfully.');
					redirect(ADMIN_BASE.'discounts/list', 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Discount added unsuccessfully.');
					redirect(ADMIN_BASE.'discounts/add', 'refresh');
				}
			}
							
		}
	}
    public function edit($id='')
	{
		$discount_data=$this->Discount_model->get_discount_data('id="'.$id.'"');
		if(empty($discount_data)){
			show_404();
		}else{
			$this->viewParams['discount_data'] = $discount_data;
			$this->viewParams['page_title'] = 'Edit Discount';
			$jsfilearray = array(
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/discount.js'
           	);
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['content'] = 'discount/edit';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		
	}
	public function update(){
		if($this->input->post('discount_id')){
			$discount_id=$this->input->post('discount_id');
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('discount_code', 'Discount code', 'required');
			$this->form_validation->set_rules('discount_type', 'Discount type', 'required');
			$this->form_validation->set_rules('amount', 'Amount', 'required');
			$this->form_validation->set_rules('per_user_use_count', 'Per user use count', 'required');
			$this->form_validation->set_rules('total_use_count', 'Total use count', 'required');
			if ($this->form_validation->run() === FALSE){
				$discount_data=$this->Discount_model->get_discount_data('id="'.$discount_id.'"');
				if(empty($discount_data)){
					show_404();
				}else{
					$this->viewParams['discount_data'] = $discount_data;
					$this->viewParams['page_title'] = 'Edit Discount';
					$jsfilearray = array(
									base_url().'assets/backend/js/jquery.validate.min.js',
									base_url().'assets/backend/js/additional-methods.js',
									base_url().'assets/backend/js/discount.js'
				);
					$this->viewParams['jsfilearray'] = $jsfilearray;	
					$this->viewParams['content'] = 'discount/edit';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$records['title'] 				= $this->input->post('title');
				$records['discount_code'] 		= $this->input->post('discount_code');
				$records['discount_type'] 		= $this->input->post('discount_type');
				$records['amount'] 				= $this->input->post('amount');
				$records['per_user_use_count'] 	= $this->input->post('per_user_use_count');
				$records['total_use_count'] 	= $this->input->post('total_use_count');
				$records['updated_at'] 		= date("Y-m-d H:i:s");
				$update=$this->Discount_model->update($records,$discount_id);
				if($update==true){
					$this->session->set_flashdata('success', 'Discount Updated successfully.');
					redirect(ADMIN_BASE.'discounts/edit/'.$discount_id, 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Discount Updated unsuccessfully.');
					redirect(ADMIN_BASE.'discounts/edit/'.$discount_id, 'refresh');
				}
			}				
		}	
	}
	public function delete($id)
	{
		$deleted_at = date("Y-m-d H:i:s");
		$delete_status = '1';
		$update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
		$this->db->where("id",$id);
		$this->db->update("tbl_discount",$update);		
		return TRUE;
	}

	public function changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_discount",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}	
}