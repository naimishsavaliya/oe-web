<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class AdminController extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        
        $this->load->library('apibase',array('dbobj'=>$this->db));
        
        $method = $this->router->fetch_method();
        if(!in_array($method, array('generateToken')) && !$this->apibase->verifyToken()){
            
            $response = array('status'=>0, 'message_type'=>'error', 'message' => 'Invalid token!!!');
            $this->response($response);
        }
    }
    
    public function response($response){
        $this->output
        ->set_content_type('application/json');
        if(is_array($response)){
            $this->output->set_output(json_encode($response));
        }else if(is_object($response)){
            $this->output->set_output(json_encode($response));
        }else{
            $this->output->set_output($response);
        }
    }
}