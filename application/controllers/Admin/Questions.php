<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Question_model');
	}

	public function index()
	{	
		$jsfilearray = array(
								base_url().'assets/backend/js/question_datatable.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Question list';
		$this->viewParams['content'] = 'question/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];	
				$generalSearch='( que.id LIKE "%'.$generalSearch.'%" OR que.question LIKE "%'.$generalSearch.'%" OR que.created_at LIKE "%'.$generalSearch.'%" OR ans.answer_type LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" AND que.delete_status!="1" )';	
		}
		$datatable_data=$this->Question_model->datatable_data($generalSearch);
		$total_rec = $this->Question_model->total_record($generalSearch);
        $pagination = $cur_page = $limit = '';
        if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
        }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}

    public function add()
	{
		$this->viewParams['standard_data'] = $this->Question_model->get_standard_data(array('status'),array('1'));
		$this->viewParams['subject_data'] = $this->Question_model->get_subject_data(array('status'),array('1'));
		$this->viewParams['page_title'] = 'Add Question';
		$jsfilearray = array(
								base_url().'assets/backend/css/ckeditor/ckeditor.js',
								base_url().'assets/backend/css/ckeditor/config.js',
								base_url().'assets/backend/css/ckeditor/build-config.js',
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/question.js'
								//base_url().'assets/backend/js/que_validate.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;	
		$this->viewParams['content'] = 'question/add';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}
	public function save(){
		if($this->input->post()){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('standard_id', 'Standard Name', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject Name', 'required');
			$this->form_validation->set_rules('question', 'Question', 'required');
			$this->form_validation->set_rules('answer_type', 'Answer Type', 'required');
			$this->form_validation->set_rules('correct_ans', 'Correct Answer', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->viewParams['standard_data'] = $this->Question_model->get_standard_data(array('status'),array('1'));
				$this->viewParams['subject_data'] = $this->Question_model->get_subject_data(array('status'),array('1'));
				$this->viewParams['page_title'] = 'Add Question';
				$jsfilearray = array(
									base_url().'assets/backend/css/ckeditor/ckeditor.js',
									base_url().'assets/backend/css/ckeditor/config.js',
									base_url().'assets/backend/css/ckeditor/build-config.js',
									base_url().'assets/backend/js/jquery.validate.min.js',
									base_url().'assets/backend/js/additional-methods.js',
									base_url().'assets/backend/js/question.js'
									//base_url().'assets/backend/js/que_validate.js'
				                       );
				$this->viewParams['jsfilearray'] = $jsfilearray;	
				$this->viewParams['content'] = 'question/add';
				$this->load->view('admin/layout/2colmn-left',$this->viewParams);
			}else{
				//var_dump(date("Y-m-d H:i"));die();
				$question_records['standard_id'] = $this->input->post('standard_id');
				$question_records['subject_id'] = $this->input->post('subject_id');
				$question_records['question'] = $this->input->post('question');
				$question_records['created_at'] = date("Y-m-d H:i:s");
				$question_id=$this->Question_model->insert($question_records);
					if(!empty($question_id)){
						$answer_records=[];
						$que_records=[];
						$file_name='';
						if($this->input->post('answer_type')=='text2'){
							for ($i = 1; $i <= 2; $i++) {
							 	$answer_records['answer'.$i] = $this->input->post('text2_answer'.$i);
							 	//var_dump($answer_records['answer'.$i]); die();
							}
						}elseif($this->input->post('answer_type')=='text4'){
							for ($i = 1; $i <= 4; $i++) {
							 	$answer_records['answer'.$i] = $this->input->post('text4_answer'.$i);
							}
						}elseif($this->input->post('answer_type')=='image2'){

							$path_to_upload = './assets/uploads/question/'.$question_id.'/';
							if ( ! file_exists($path_to_upload) ){
								$create = mkdir($path_to_upload, 0777, true);
								if ( ! $create)
								return;
							}
							for ($i = 1; $i <= 2; $i++) {
							 	if($_FILES['image2_answer'.$i]['tmp_name']!=''){
							 		$name=$_FILES['image2_answer'.$i]['name'];
							 		$explode=explode('.',$name);
							 		$image_name=md5($question_id.$explode[0]);
							 		$file_name = $image_name.".".$explode[1];

							 		$config['upload_path']          = $path_to_upload;
							 	    $config['allowed_types']        = 'gif|jpg|png|jpeg';
							 	    $config['max_size'] 			= '2048000';
							 	    $config['file_name'] 			= $file_name;
							 		$this->load->library('Upload', $config);
							 		$this->upload->initialize($config);
							 		$img = 'image2_answer'.$i;
							 		if ( ! $this->upload->do_upload('image2_answer'.$i)){
							 			$errors = array('error' => $this->upload->display_errors());
							 			echo json_encode($errors);
							 			exit();
							 		}
							 		else{
							 		
								// var_dump("expression");
								// exit();
							 			$file = $this->upload->data();
										$name=$file['file_name'];
										$file_name = $name;
							 			$data = ['file' => $path_to_upload.$file['file_name']];
							 			$this->load->library('Image_lib');
							 			$config['image_library'] 	= 'gd2';
							 			$config['source_image'] 	= $path_to_upload.$file['file_name'];
							 			$config['create_thumb'] 	= false;
							 			$config['maintain_ratio'] 	= TRUE;
							 			$config['max_size'] 		= '2048000';
							 			$config['new_image'] 		= $path_to_upload."thumbs/".$file_name;
							 			$this->image_lib->clear();
							 			$this->image_lib->initialize($config);
							 			$this->image_lib->resize();
							 		}
							 	}
							 	$answer_records['answer'.$i] = $file_name;
							}
							
						}else{
							$path_to_upload = './assets/uploads/question/'.$question_id.'/';
							if ( ! file_exists($path_to_upload) ){
								$create = mkdir($path_to_upload, 0777, true);
								if ( ! $create)
								return;
							}
							for ($i = 1; $i <= 4; $i++) {
							 	if($_FILES['image4_answer'.$i]['tmp_name']!=''){
							 		$name=$_FILES['image4_answer'.$i]['name'];
							 		$explode=explode('.',$name);
							 		$image_name=md5($question_id.$explode[0]);
							 		$file_name = $image_name.".".$explode[1];

							 		$config['upload_path']          = $path_to_upload;
							 	    $config['allowed_types']        = 'gif|jpg|png|jpeg';
							 	    $config['max_size'] 			= '2048000';
							 	    $config['file_name'] = $file_name;
							 		$this->load->library('Upload', $config);
							 		$this->upload->initialize($config);
							 		$img = 'image4_answer'.$i;
							 		if ( ! $this->upload->do_upload('image4_answer'.$i)){
							 			$errors = array('error' => $this->upload->display_errors());
							 			echo $errors;
							 			exit();
							 		}
							 		else{
							 			$file = $this->upload->data();
							 			$name=$file['file_name'];
										$file_name = $name;
							 			$data = ['file' => $path_to_upload.$file['file_name']];
							 			$this->load->library('Image_lib');
							 			$config['image_library'] 	= 'gd2';
							 			$config['source_image'] 	= $path_to_upload.$file['file_name'];
							 			$config['create_thumb'] 	= false;
							 			$config['maintain_ratio'] 	= TRUE;
							 			$config['max_size'] 		= '2048000';
							 			$config['new_image'] 		= $path_to_upload."thumbs/".$file_name;
							 			$this->image_lib->clear();
							 			$this->image_lib->initialize($config);
							 			$this->image_lib->resize();
							 		}
							 	}
							 	$answer_records['answer'.$i] = $file_name;
							}
						}
						$answer_records['question_id'] = $question_id;
						$answer_records['answer_type'] = $this->input->post('answer_type');
						if(!empty($this->input->post('correct_ans'))){
							$correct_ans=$this->input->post('correct_ans');
							$que_records['correct_answer'] = 'answer'.$correct_ans;
						}else{
							$que_records['correct_answer']=[];
						}
						$answer_records['created_at'] = date("Y-m-d H:i:s");
						$answer_id=$this->Question_model->answer_insert($answer_records);
						$q_id=$this->Question_model->update($que_records,$question_id);
						$this->session->set_flashdata('success', 'Question added successfully.');
						redirect(ADMIN_BASE.'questions/list', 'refresh');
					}else{
						$this->session->set_flashdata('error', 'Question added unsuccessfully.');
						redirect(ADMIN_BASE.'questions/add', 'refresh');
					}	
			}
							
		}
	}
    public function edit($id)
	{
		$this->viewParams['standard_data'] = $this->Question_model->get_standard_data(array('status'),array('1'));
		$this->viewParams['subject_data'] = $this->Question_model->get_subject_data(array('status'),array('1'));

		$questionanswer_data=$this->Question_model->get_questionanswer_data($id);
		if(empty($questionanswer_data)){
			show_404();
		}else{
			$this->viewParams['questionanswer_data'] = $questionanswer_data;
			//echo '<pre>'; var_dump($this->viewParams['questionanswer_data']); die();
			$this->viewParams['page_title'] = 'Edit Question';
			$jsfilearray = array(
									base_url().'assets/backend/css/ckeditor/ckeditor.js',
									base_url().'assets/backend/css/ckeditor/config.js',
									base_url().'assets/backend/css/ckeditor/build-config.js',
									base_url().'assets/backend/js/jquery.validate.min.js',
									base_url().'assets/backend/js/additional-methods.js',
									base_url().'assets/backend/js/que_validate.js'
									//base_url().'assets/backend/js/question.js'
		 	                       );
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['content'] = 'question/edit';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		
	}
	public function update(){
		if($this->input->post('que_id')){
			$question_id=$this->input->post('que_id');
			$questionanswer_data=$this->Question_model->get_questionanswer_data($question_id);
			//var_dump($questionanswer_data); die();
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('standard_id', 'Standard Name', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject Name', 'required');
			$this->form_validation->set_rules('question', 'Question', 'required');
			$this->form_validation->set_rules('correct_ans', 'Correct Answer', 'required');
			
			if ($this->form_validation->run() === FALSE){
				$this->viewParams['standard_data'] = $this->Question_model->get_standard_data(array('status'),array('1'));
				$this->viewParams['subject_data'] = $this->Question_model->get_subject_data(array('status'),array('1'));
				$questionanswer_data=$this->Question_model->get_questionanswer_data($question_id);
				if(empty($questionanswer_data)){
					show_404();
				}else{
					$this->viewParams['questionanswer_data'] = $questionanswer_data;
					$this->viewParams['page_title'] = 'Edit Question';
					$jsfilearray = array(
										base_url().'assets/backend/css/ckeditor/ckeditor.js',
										base_url().'assets/backend/css/ckeditor/config.js',
										base_url().'assets/backend/css/ckeditor/build-config.js',
										base_url().'assets/backend/js/jquery.validate.min.js',
										base_url().'assets/backend/js/additional-methods.js',
										base_url().'assets/backend/js/que_validate.js'
					                       );
					$this->viewParams['jsfilearray'] = $jsfilearray;	
					$this->viewParams['content'] = 'question/edit';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$que_records['standard_id'] = $this->input->post('standard_id');
				$que_records['subject_id'] = $this->input->post('subject_id');
				$que_records['question'] = $this->input->post('question');
				$que_records['updated_at'] = date("Y-m-d H:i:s");
				if(!empty($this->input->post('correct_ans'))){
					$correct_ans=$this->input->post('correct_ans');
					$que_records['correct_answer'] = 'answer'.$correct_ans;
				}else{
					$que_records['correct_answer']=[];
				}
				$q_id=$this->Question_model->update($que_records,$question_id);
					if($questionanswer_data->answer_type=='text2'){
						for ($i = 1; $i <= 2; $i++) {
						 	$answer_records['answer'.$i] = $this->input->post('text2_answer'.$i);
						}
					}elseif($questionanswer_data->answer_type=='text4'){
						for ($i = 1; $i <= 4; $i++) {
						 	$answer_records['answer'.$i] = $this->input->post('text4_answer'.$i);
						}
					}elseif($questionanswer_data->answer_type=='image2'){

						$path_to_upload = './assets/uploads/question/'.$question_id.'/';
						if ( ! file_exists($path_to_upload) ){
							$create = mkdir($path_to_upload, 0777, true);
							if ( ! $create)
							return;
						}
						for ($i = 1; $i <= 2; $i++) {
							$file_name='';
							$image2_fieldname='answer'.$i;
						 	if($_FILES['image2_answer'.$i]['tmp_name']!=''){
								if(isset($questionanswer_data->$image2_fieldname) && $questionanswer_data->answer_type=='image2'){
									$file_name=$questionanswer_data->$image2_fieldname;
									$im=$questionanswer_data->$image2_fieldname;
									if($im!=''){
										@unlink(FCPATH.'assets/uploads/question/'.$question_id.'/'.$im);
									}
								}
						 		$name=$_FILES['image2_answer'.$i]['name'];
						 		$explode=explode('.',$name);
						 		$image_name=md5($question_id.$explode[0]);
						 		$file_name = $image_name.".".$explode[1];

						 		$config['upload_path']          = $path_to_upload;
						 	    $config['allowed_types']        = 'gif|jpg|png|jpeg';
						 	    $config['max_size'] 			= '2048000';
						 	    $config['file_name'] 			= $file_name;
						 		$this->load->library('Upload', $config);
						 		$this->upload->initialize($config);
						 		$img = 'image2_answer'.$i;
						 		if ( ! $this->upload->do_upload('image2_answer'.$i)){
						 			$errors = array('error' => $this->upload->display_errors());
						 			echo json_encode($errors);
						 			exit();
						 		}
						 		else{
						 			$file = $this->upload->data();
									$name=$file['file_name'];
									$file_name = $name;
						 			$data = ['file' => $path_to_upload.$file['file_name']];
						 			$this->load->library('Image_lib');
						 			$config['image_library'] 	= 'gd2';
						 			$config['source_image'] 	= $path_to_upload.$file['file_name'];
						 			$config['create_thumb'] 	= false;
						 			$config['maintain_ratio'] 	= TRUE;
						 			$config['max_size'] 		= '2048000';
						 			$config['new_image'] 		= $path_to_upload."thumbs/".$file_name;
						 			$this->image_lib->clear();
						 			$this->image_lib->initialize($config);
						 			$this->image_lib->resize();
						 		}
							 	$answer_records['answer'.$i] = $file_name;
						 	}
						}
						
					}else{
						$path_to_upload = './assets/uploads/question/'.$question_id.'/';
						if ( ! file_exists($path_to_upload) ){
							$create = mkdir($path_to_upload, 0777, true);
							if ( ! $create)
							return;
						}
						for ($i = 1; $i <= 4; $i++) {
							$file_name='';
							$image4_fieldname='answer'.$i;
						 	if($_FILES['image4_answer'.$i]['tmp_name']!=''){
						 		if(isset($questionanswer_data->$image4_fieldname) && $questionanswer_data->answer_type=='image4'){
						 			$file_name=$questionanswer_data->$image4_fieldname;
						 			$im4=$questionanswer_data->$image4_fieldname;
									if($im4!=''){
										@unlink(FCPATH.'assets/uploads/question/'.$question_id.'/'.$im4);
									}
						 		}
						 		$name=$_FILES['image4_answer'.$i]['name'];
						 		$explode=explode('.',$name);
						 		$image_name=md5($question_id.$explode[0]);
						 		$file_name = $image_name.".".$explode[1];

						 		$config['upload_path']          = $path_to_upload;
						 	    $config['allowed_types']        = 'gif|jpg|png|jpeg';
						 	    $config['max_size'] 			= '2048000';
						 	    $config['file_name'] = $file_name;
						 		$this->load->library('Upload', $config);
						 		$this->upload->initialize($config);
						 		$img = 'image4_answer'.$i;
						 		if ( ! $this->upload->do_upload('image4_answer'.$i)){
						 			$errors = array('error' => $this->upload->display_errors());
						 			echo $errors;
						 			exit();
						 		}
						 		else{
						 			$file = $this->upload->data();
						 			$name=$file['file_name'];
									$file_name = $name;
						 			$data = ['file' => $path_to_upload.$file['file_name']];
						 			$this->load->library('Image_lib');
						 			$config['image_library'] 	= 'gd2';
						 			$config['source_image'] 	= $path_to_upload.$file['file_name'];
						 			$config['create_thumb'] 	= false;
						 			$config['maintain_ratio'] 	= TRUE;
						 			$config['max_size'] 		= '2048000';
						 			$config['new_image'] 		= $path_to_upload."thumbs/".$file_name;
						 			$this->image_lib->clear();
						 			$this->image_lib->initialize($config);
						 			$this->image_lib->resize();
						 		}
						 		$answer_records['answer'.$i] = $file_name;
						 	}
						 	
						}
					}
					$answer_records['updated_at'] = date("Y-m-d H:i:s");
					$update=$this->Question_model->answer_update($answer_records,$question_id);
				
				$this->session->set_flashdata('success', 'Question Updated successfully.');
				redirect(ADMIN_BASE.'questions/edit/'.$question_id, 'refresh');
					
			}
							
		}
		
	}
	public function delete($id)
	{
		$deleted_at = date("Y-m-d H:i:s");
		$delete_status = '1';
		$update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
		$this->db->where("id",$id);
		$this->db->update("tbl_question",$update);		
		return TRUE;
	}

	public function changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_question",$update);
		$this->db->where("question_id",$id);
		$this->db->update("tbl_answer",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	
	}
	public function view($id){
		$this->viewParams['questionanswer_data']=$this->Question_model->get_viewdata($id);
		    //echo '<pre>'; var_dump($this->viewParams['questionanswer_data']); die();
		    $ajax_payload = '';
		    if(!empty($this->viewParams['questionanswer_data'])){
		    	$sub_name='';
		    	$std_name='';
		    	$que_title='';
		    	$type='';
		    	if($this->viewParams['questionanswer_data']->subject_name){
		    		$sub_name=$this->viewParams['questionanswer_data']->subject_name;
		    	}
		    	
		    	if($this->viewParams['questionanswer_data']->standard_name){
		    		$std_name=$this->viewParams['questionanswer_data']->standard_name;
		    	}
		    	
		    	if($this->viewParams['questionanswer_data']->question){
		    		$que_title=strip_tags($this->viewParams['questionanswer_data']->question);
		    	}
		    	
		    	if($this->viewParams['questionanswer_data']->answer_type){
		    		$type=$this->viewParams['questionanswer_data']->answer_type;
		    	}

		    	$ajax_payload = '<table id="questation_data" class="table display responsive nowrap"><tr><th>Subject Name</th><td>'.$sub_name.'</td></tr><tr><th>Standard Name</th><td>'.$std_name.'</td></tr><tr><th>Question</th><td>'.$que_title.'</td></tr><tr><th>Answer Type</th><td>'.$type.'</td></tr>';

	    		if($type=='text2'){
					for ($w = 1; $w <= 2; $w++) { 
						$text2_fieldname='answer'.$w;
						$t2_fieldname='Answer'.' '.$w;
						if(isset($this->viewParams['questionanswer_data']->$text2_fieldname)){
							$answer_records=$this->viewParams['questionanswer_data']->$text2_fieldname;
								$correct='';
						 	 	$correct_ids = explode('answer',$this->viewParams['questionanswer_data']->correct_answer);
						 	 	$correct_ids = $correct_ids['1'];	
						 	 	if($correct_ids==$w){
						 	 		$correct='<br/><span>(Correct Answer)</span>';
						 	 	}
							 	 
						}else{
							$answer_records='';
						}
						$ajax_payload.='<tr><th>'.$t2_fieldname.' '.$correct.'</th><td>'.$answer_records.'</td></tr>';
					}
	    		}elseif ($type=='text4') {
	    			for ($x = 1; $x <= 4; $x++) { 
	    				$text4_fieldname='answer'.$x;
	    				$t4_fieldname='Answer'.' '.$x;
	    				if(isset($this->viewParams['questionanswer_data']->$text4_fieldname)){
	    					$answer_records=$this->viewParams['questionanswer_data']->$text4_fieldname;
								$correct='';
						 	 	$correct_ids = explode('answer',$this->viewParams['questionanswer_data']->correct_answer);
						 	 	$correct_ids = $correct_ids['1'];	
						 	 	if($correct_ids==$x){
						 	 		$correct='<br/><span>(Correct Answer)</span>';
						 	 	}
	    				}else{
	    					$answer_records='';
	    				}
	    				$ajax_payload.='<tr><th>'.$t4_fieldname.' '.$correct.'</th><td>'.$answer_records.'</td></tr>';
	    			}
	    			
	    		}elseif ($type=='image2') {
	    			for ($y = 1; $y <= 2; $y++) { 
	    				$image2_fieldname='answer'.$y;
	    				$i2_fieldname='Answer'.' '.$y;
	    				if(isset($this->viewParams['questionanswer_data']->$image2_fieldname)){
	    					$answer_records=$this->viewParams['questionanswer_data']->$image2_fieldname;
    							$correct='';
    					 	 	$correct_ids = explode('answer',$this->viewParams['questionanswer_data']->correct_answer);
    					 	 	$correct_ids = $correct_ids['1'];	
    					 	 	if($correct_ids==$y){
    					 	 		$correct='<br/><span>(Correct Answer)</span>';
    					 	 	}
	    				}else{
	    					$answer_records='';
	    				}
	    				$ajax_payload.='<tr><th>'.$i2_fieldname.' '.$correct.'</th><td><img src="'.base_url("assets/uploads/question/".$id."/".$answer_records).'" style="width:50px;height:50px;"></td></tr>';
	    			}
	    		}else{
	    			for ($z = 1; $z <= 4; $z++) { 
	    				$image4_fieldname='answer'.$z;
	    				$i4_fieldname='Answer'.' '.$z;
	    				if(isset($this->viewParams['questionanswer_data']->$image4_fieldname)){
	    					$answer_records=$this->viewParams['questionanswer_data']->$image4_fieldname;
    							$correct='';
    					 	 	$correct_ids = explode('answer',$this->viewParams['questionanswer_data']->correct_answer);
    					 	 	$correct_ids = $correct_ids['1'];	
    					 	 	if($correct_ids==$z){
    					 	 		$correct='<br/><span>(Correct Answer)</span>';
    					 	 	}
	    				}else{
	    					$answer_records='';
	    				}
	    				$ajax_payload.='<tr><th>'.$i4_fieldname.' '.$correct.'</th><td><img src="'.base_url("assets/uploads/question/".$id."/".$answer_records).'" style="width:50px;height:50px;"></td></tr>';
	    			}
	    		}

		    	$created_at='';
		    	if($this->viewParams['questionanswer_data']->que_created_at!='0000-00-00 00:00:00'){
		    		$created_at = date('Y-m-d',strtotime($this->viewParams['questionanswer_data']->que_created_at));
		    		$ajax_payload.='<tr><th>Created Date.</th><td>'.$created_at.'</td></tr>';
		    	}
		    	$updated_at='';
		    	if($this->viewParams['questionanswer_data']->que_updated_at!='0000-00-00 00:00:00'){
		    		$updated_at = date('Y-m-d',strtotime($this->viewParams['questionanswer_data']->que_updated_at));
		    		$ajax_payload.='<tr><th>Updated Date.</th><td>'.$updated_at.'</td></tr>';
		    	}

		    	$ajax_payload.='</table>'; 
		        echo json_encode($ajax_payload);
		        exit();

		    }else{
		        echo 'Error';
		        exit();
		    }
	}

    public function import()
	{
		$this->viewParams['standard_data'] = $this->Question_model->get_standard_data(array('status'),array('1'));
		$this->viewParams['subject_data'] = $this->Question_model->get_subject_data(array('status'),array('1'));
		$this->viewParams['page_title'] = 'Question Import';
		$jsfilearray = array(
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/import.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;	
		$this->viewParams['content'] = 'question/import';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}
    public function unzip()
	{	
		if($this->input->post()){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('standard_id', 'Standard Name', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject Name', 'required');
			if(empty($_FILES['zip_file']['name'])){
				$this->form_validation->set_rules('zip_file','zip file','required');
			}
			
			if ($this->form_validation->run() === FALSE)
			{
					$this->viewParams['standard_data'] = $this->Question_model->get_standard_data(array('status'),array('1'));
					$this->viewParams['subject_data'] = $this->Question_model->get_subject_data(array('status'),array('1'));
					$this->viewParams['page_title'] = 'Question Import';
					$jsfilearray = array(
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/import.js'
					               );
					$this->viewParams['jsfilearray'] = $jsfilearray;	
					$this->viewParams['content'] = 'question/import';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
			}else{
				$file_name='';
				//var_dump($subject_id); die();
				
				$path_to_upload = './assets/uploads/question/';
				if ( ! file_exists($path_to_upload) ){
					$create = mkdir($path_to_upload, 0777, true);
					if ( ! $create)
					return;
				}
				$name=$_FILES['zip_file']['name'];
				$ext = explode('.', $name);
				$file_name = md5(time()).'.'.$ext[1];
				$config['upload_path']          = $path_to_upload;
				$config['allowed_types']        = 'zip';
				$config['file_name'] 			= $file_name;
				$this->load->library('Upload', $config);
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('zip_file')){
					$this->session->set_flashdata('error', $this->upload->display_errors());
					redirect(ADMIN_BASE.'questions/import', 'refresh');
				}
				else{
					$standard_id = $this->input->post('standard_id');
					$subject_id = $this->input->post('subject_id');
					$folder_name = $ext['0'];
					$data = $path_to_upload.$file_name;
					$extract_folder = $path_to_upload.'/'.$folder_name;
					$zip = new ZipArchive;
					if ($zip->open($data) === TRUE) 
					{
						if ( ! file_exists($extract_folder) ){
							$create = mkdir($extract_folder, 0777, true);
							if ( ! $create)
							return;
						}
						$zip->extractTo($extract_folder);
						$zip->close();
						$dirs = array_filter(glob($extract_folder.'/*'), 'is_dir');
						// var_dump($dirs); exit();
						$option_arr = [
							'1'=>'answer1',
							'2'=>'answer2',
							'3'=>'answer3',
							'4'=>'answer4'
						];
						foreach ($dirs as $key => $dir) {
							$row_count = 0;
							foreach(glob($dir.'/*.csv') as $filename){
								$fp = fopen($filename,'r') or die("can't open file");
								while($csv_line = fgetcsv($fp,1024))
								{
									$question = $answer = [];
									if($row_count > 0){
										$question['subject_id'] 		= $subject_id;
										$question['standard_id']		= $standard_id;
										$question['question']			= $csv_line[0];
										$question['correct_answer']		= $option_arr[array_search($csv_line[5],$csv_line)];
										$question['created_at']			= date('y-m-d H:i:s');
										$this->db->insert('tbl_question', $question);
										$question['id'] 				= $this->db->insert_id();

										$answer['question_id'] 			= $question['id'];
										$answer['answer_type']			= strtolower(str_replace('-','',($csv_line[6])));
										$answer['answer1'] 				= $csv_line[1];
										$answer['answer2'] 				= $csv_line[2];
										$answer['answer3'] 				= $csv_line[3];
										$answer['answer4'] 				= $csv_line[4];
										$answer['created_at']			= date('y-m-d H:i:s');
										
										$answer['answer_type'] = ($answer['answer_type'] != '') ? $answer['answer_type'] : 'text4';
										$this->db->insert('tbl_answer', $answer);
										$answer['id'] 					= $this->db->insert_id();

										$upload_path = './assets/uploads/question/'.$question['id'];
										

										if($answer['answer_type']=='image2'){
											if ( ! file_exists($upload_path) ){
												$create = mkdir($upload_path, 0777, true);
												if ( ! $create)
												return;
											}
											$file_name1=$file_name2='';
												if($answer['answer1']!=''){
													$explode=explode('.',$answer['answer1']);
													$image1_name=md5($question['id'].$explode[0]);
													$file_name1 = $image1_name.".".$explode[1];
													$update_answer['answer1'] = $file_name1;
													copy($dir.'/'.$answer['answer1'], $upload_path.'/'.$file_name1);
												}

												if($answer['answer2']!=''){
													$explode=explode('.',$answer['answer2']);
													$image2_name=md5($question['id'].$explode[0]);
													$file_name2 = $image2_name.".".$explode[1];
													$update_answer['answer2'] = $file_name2;
													copy($dir.'/'.$answer['answer2'], $upload_path.'/'.$file_name2);
												}
											$this->db->where('id', $answer['id']);
											$this->db->update('tbl_answer', $update_answer);
										}elseif ($answer['answer_type']=='image4') {
											if ( ! file_exists($upload_path) ){
												$create = mkdir($upload_path, 0777, true);
												if ( ! $create)
												return;
											}
											$file_name1=$file_name2=$file_name3=$file_name4='';
												if($answer['answer1']!=''){
													$explode=explode('.',$answer['answer1']);
													$image1_name=md5($question['id'].$explode[0]);
													$file_name1 = $image1_name.".".$explode[1];
													$update_answer['answer1'] = $file_name1;
													copy($dir.'/'.$answer['answer1'], $upload_path.'/'.$file_name1);
												}

												if($answer['answer2']!=''){
													$explode=explode('.',$answer['answer2']);
													$image2_name=md5($question['id'].$explode[0]);
													$file_name2 = $image2_name.".".$explode[1];
													$update_answer['answer2'] = $file_name2;
													copy($dir.'/'.$answer['answer2'], $upload_path.'/'.$file_name2);
												}

												if($answer['answer3']!=''){
													$explode=explode('.',$answer['answer3']);
													$image3_name=md5($question['id'].$explode[0]);
													$file_name3 = $image3_name.".".$explode[1];
													$update_answer['answer3'] = $file_name3;
													copy($dir.'/'.$answer['answer3'], $upload_path.'/'.$file_name3);
												}

												if($answer['answer4']!=''){
													$explode=explode('.',$answer['answer4']);
													$image4_name=md5($question['id'].$explode[0]);
													$file_name4 = $image4_name.".".$explode[1];
													$update_answer['answer4'] = $file_name4;
													copy($dir.'/'.$answer['answer4'], $upload_path.'/'.$file_name4);
												}

											$this->db->where('id', $answer['id']);
											$this->db->update('tbl_answer', $update_answer);
										}
									}
									$row_count++;
								}
								fclose($fp) or die("can't close file");
							}
						}
						if($file_name!=''){
							@unlink(FCPATH.'assets/uploads/question/'.$file_name);
							$this->deleteDirectory($extract_folder);
						}

						// call to below function.
						$this->session->set_flashdata('success', 'Question Import successfully.');
						redirect(ADMIN_BASE.'questions/import', 'refresh');
					}else{
						$this->session->set_flashdata('error', 'Question Import unsuccessfully.');
						redirect(ADMIN_BASE.'questions/import', 'refresh');
					}
				}
			}
							
		}
	}
	//end unzip function

	// to remove junk files (extracted folder) starts
	public function deleteDirectory($dir) {
	    if (!file_exists($dir)) {
	        return true;
	    }

	    if (!is_dir($dir)) {
	        return unlink($dir);
	    }

	    foreach (scandir($dir) as $item) {
	        if ($item == '.' || $item == '..') {
	            continue;
	        }

	        if (!$this->deleteDirectory($dir . '/' . $item)) {
	            return false;
	        }

	    }

	    return rmdir($dir);
	}
	// to remove junk files (extracted folder) ends

			
}