<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standards extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Standard_model');
	}

	public function index()
	{	
		$jsfilearray = array(
								base_url().'assets/backend/js/standard_datatable.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Standard list';
		$this->viewParams['content'] = 'standard/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Standard_model->list_standard($generalSearch);
		$total_rec = $this->Standard_model->total_record($generalSearch);
    	$pagination = $cur_page = $limit = '';
        if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
        }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
		// echo json_encode($this->Standard_model->list_standard($generalSearch));
		// exit();
	}

    public function edit($id=0)
	{
		if($id==0){
			//var_dump('ADD'); die();
				$this->viewParams['mode']='add';
		}else{
			//var_dump('EDIT'); die();
				$this->viewParams['mode']='edit';
					$this->viewParams['standard_data'] = array();
					if($id!=0){	
						$this->viewParams['standard_data'] = $this->Standard_model->get_standard_detail(array('id'),array($id));
						//echo '<pre>'; var_dump($this->viewParams['standard_data']); die();
						}
		}

			if($this->input->post('mode')){
					$this->load->helper('form');
					$this->load->library('Form_validation');
					if($this->input->post('mode') == 'add'){
						$this->form_validation->set_rules('standard_name', 'Standard', 'required');
					}elseif($this->input->post('mode') == 'edit'){
						$this->form_validation->set_rules('standard_name', 'Standard', 'required');	
					}

					if ($this->form_validation->run() === FALSE)
					{
						$errors = validation_errors();
						echo json_encode(['error'=>$errors]);
						exit();
					}else{					
						if($this->input->post('mode') == 'edit')
						{
							$records = array(
								'name'       			=>	$this->input->post('standard_name'),
								'updated_at'  			=>  date("Y-m-d H:i:s")
							);	

							$update=$this->Standard_model->update($records,array('id'=>$id));
							if($update==true){
								echo json_encode(array('success'=>"Standard updated successfully."));
					 			exit();
							}else{
								echo json_encode(['error'=>'Error']);
								exit();
							}

						}elseif($this->input->post('mode') == 'add'){
							
							$records = array(
								'name'       			=>	$this->input->post('standard_name'),
								'created_at'  			=>  date("Y-m-d H:i:s")
							);	
							$insert=$this->Standard_model->insert($records);
							if($insert==true){
								echo json_encode(array('success'=>"Standard added successfully."));
					 			exit();
							}else{
								echo json_encode(['error'=>'Error']);
								exit();
							}
						}
					}
								
				}
				if($id==0)
				{
					$this->viewParams['page_title'] = 'Add Standard';
					$jsfilearray = array(
											base_url().'assets/backend/js/jquery.validate.min.js',
											base_url().'assets/backend/js/additional-methods.js',
											base_url().'assets/backend/js/standard.js'
				 	                       );
					$this->viewParams['jsfilearray'] = $jsfilearray;
				}
				else
				{
					$this->viewParams['page_title'] = 'Edit Standard';
					$jsfilearray = array(
											base_url().'assets/backend/js/jquery.validate.min.js',
											base_url().'assets/backend/js/additional-methods.js',
											base_url().'assets/backend/js/standard.js'
				 	                       );
					$this->viewParams['jsfilearray'] = $jsfilearray;
				}
			$this->viewParams['content'] = 'standard/add';
        	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		public function standard_delete($id)
		{
			$this->Standard_model->delete($id);		
			return TRUE;
		}

		public function standard_changestatus($id,$status)
		{ 
			$update=array("status"=>$status);
			$this->db->where("id",$id);
			$this->db->update("tbl_standard",$update);
			echo json_encode(['responce'=>'success']);
			exit();
		
		}


	
}