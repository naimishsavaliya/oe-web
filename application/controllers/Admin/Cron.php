<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cron extends CI_Controller {
   	public function __construct(){
       parent::__construct();
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->model('Admin/Cron_model');	
   	}
   	public function cron_job(){
   		$this->load->library('email');
   		$day=strtotime("+1 days");
   		$exam_id=[];
   		$exam_data=$this->Cron_model->get_exam_data($day);
   		foreach ($exam_data as $key => $value) {
   			$exam_id[] = $value['id'];
   		}
   		if(!empty($exam_id)){
   			$ExamUser_data=$this->Cron_model->get_ExamUser_data($exam_id);
   			if(!empty($ExamUser_data)){
               $i=1;
               $template_data = $this->Cron_model->get_template_data('main.setting_code="exam_duedate" AND main.delete_status!="1" AND sub.delete_status!="1"');
   				foreach ($ExamUser_data as $key2 => $row) {
                  if(!empty($template_data)){
                        $subject = !isset($template_data->subject)?'':$template_data->subject;
                        $subject = str_replace("{{customer_name}}", $row['fullname'], $subject);
                        //description
                        $description = !isset($template_data->description)?'':$template_data->description;
                        $description = str_replace("{{customer_name}}", $row['fullname'], $description);
                        $description = str_replace("{{customer_mobile}}", $row['mobile'], $description);
                        $description = str_replace("{{customer_email}}", $row['email'], $description);
                        $description = str_replace("{{exam_startdate}}", date('d-m-Y',strtotime($row['start_date'])), $description);
                        $description = str_replace("{{exam_title}}", $row['exam_title'], $description);
                        $description = str_replace("{{standard_name}}", $row['standard_name'], $description);
                        $description = str_replace("{{subject_name}}", $row['subject_name'], $description);

                        $from_name = !isset($template_data->from_name)?'':$template_data->from_name;
                        $from_email = !isset($template_data->from_email)?'':$template_data->from_email;
                  }
                  $config['protocol']     = 'smtp';
                  $config['smtp_host']    = 'ssl://smtp.gmail.com';
                  $config['smtp_port']    = '465';
                  $config['smtp_timeout'] = '7';
                  $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                  $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                  $config['charset']      = 'utf-8';
                  $config['newline']      = "\r\n";
                  $config['mailtype']     = 'html'; // or html
                  $config['validation']   = TRUE; // bool whether to validate email or not      
                  $this->email->initialize($config);
                  $this->email->from($from_email, $from_name);
                  $this->email->to($row['email']); 
                  $this->email->subject($subject);
                  $this->email->message($description); 
                  if($this->email->send()){ 
                     echo $i++.'&nbsp;Send email.<br/>'; 
                  }else{ 
                     echo $i++.'&nbsp;Could not send email, please try again later<br/>';
                  }
   				}
   			}else{
   				echo 'No record found';
   			}
   		}else{
   			echo 'No record found';
   		}
   		exit();
   	}
   public function send_now(){
      $this->load->library('email');
      $notification_data=$this->Cron_model->get_notification_data("status='0' AND delete_status='0' AND notification_date IS NULL");
      if(!empty($notification_data)){
         foreach ($notification_data as $key => $value) {
            $notification_id     =  isset($value['id'])?$value['id']:'';
            $users               =  isset($value['users'])?$value['users']:'';
            $message             =  isset($value['message'])?$value['message']:'';
            $subject             =  isset($value['subject'])?$value['subject']:'';
            $notification_type   =  isset($value['notification_type'])?$value['notification_type']:'';
            $notification_userdata=$this->Cron_model->notification_users($users);
            if(!empty($notification_userdata)){
               foreach ($notification_userdata as $key1 => $row) {
                  $user_id                =  isset($row['id'])?$row['id']:'';
                  $fullname               =  isset($row['fullname'])?$row['fullname']:'';
                  $email                  =  isset($row['email'])?$row['email']:'';
                  $mobile                 =  isset($row['mobile'])?$row['mobile']:'';
                  $config['protocol']     = 'smtp';
                  $config['smtp_host']    = 'ssl://smtp.gmail.com';
                  $config['smtp_port']    = '465';
                  $config['smtp_timeout'] = '7';
                  $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                  $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                  $config['charset']      = 'utf-8';
                  $config['newline']      = "\r\n";
                  $config['mailtype']     = 'html';   // or html
                  $config['validation']   = TRUE;     // bool whether to validate email or not      
                  $this->email->initialize($config);
                  $this->email->from('onlineexam@gmail.com', 'Online Exam');
                  $this->email->to($email); 
                  $this->email->subject($subject);
                  $this->email->message($message); 
                  if($this->email->send()){
                     $info_message='Send email.'; 
                  }else{
                     $info_message='Could not send email, please try again later.';  
                  }
               }
            }else{
               echo 'No record found';
            }
            $records['status']   = 1;
            $records['updated_at']= date("Y-m-d H:i:s");
            $update=$this->Cron_model->update($records,$notification_id);
         }
         echo $info_message;
      }else{
         echo 'No record found';
      }
      exit();
   }
   public function schedule(){
      $this->load->library('email');
      $date = date("Y-m-d H:i:s");
      $notification_data=$this->Cron_model->get_notification_data("status='0' AND delete_status='0' AND '".$date."' < notification_date AND notification_date IS NOT NULL");
      if(!empty($notification_data)){
         foreach ($notification_data as $key => $value) {
            $notification_id     =  isset($value['id'])?$value['id']:'';
            $users               =  isset($value['users'])?$value['users']:'';
            $message             =  isset($value['message'])?$value['message']:'';
            $subject             =  isset($value['subject'])?$value['subject']:'';
            $notification_type   =  isset($value['notification_type'])?$value['notification_type']:'';
            $notification_userdata=$this->Cron_model->notification_users($users);
            if(!empty($notification_userdata)){
               foreach ($notification_userdata as $key1 => $row) {
                  $user_id                =  isset($row['id'])?$row['id']:'';
                  $fullname               =  isset($row['fullname'])?$row['fullname']:'';
                  $email                  =  isset($row['email'])?$row['email']:'';
                  $mobile                 =  isset($row['mobile'])?$row['mobile']:'';
                  $config['protocol']     = 'smtp';
                  $config['smtp_host']    = 'ssl://smtp.gmail.com';
                  $config['smtp_port']    = '465';
                  $config['smtp_timeout'] = '7';
                  $config['smtp_user']    = 'naimish.empyreal@gmail.com';
                  $config['smtp_pass']    = 'emrpkuumnvgihbsk';
                  $config['charset']      = 'utf-8';
                  $config['newline']      = "\r\n";
                  $config['mailtype']     = 'html';   // or html
                  $config['validation']   = TRUE;     // bool whether to validate email or not      
                  $this->email->initialize($config);
                  $this->email->from('onlineexam@gmail.com', 'Online Exam');
                  $this->email->to($email); 
                  $this->email->subject($subject);
                  $this->email->message($message); 
                  if($this->email->send()){
                     $info_message='Send email.'; 
                  }else{
                     $info_message='Could not send email, please try again later.';  
                  }
               }
            }else{
               echo 'No record found';
            }
            $records['status']   = 1;
            $records['updated_at']= date("Y-m-d H:i:s");
            $update=$this->Cron_model->update($records,$notification_id);
         }
         echo $info_message;
      }else{
         echo 'No record found';
      }
      exit();
   }
}
