<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Levels extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Level_model');
	}

	public function index()
	{	

		$jsfilearray = array(
			base_url().'assets/backend/js/levels_datatable.js'
		);
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Level list';
		$this->viewParams['content'] = 'level/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Level_model->datatable_data($generalSearch);
		$total_rec = $this->Level_model->total_record("main.delete_status!='1'",$generalSearch);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}

    public function add()
	{
		$this->viewParams['parent_data'] = $this->Level_model->get_parent_data('status="1" AND delete_status!="1"');
		$this->viewParams['page_title'] = 'Add Level';
		$jsfilearray = array(
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/level.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;	
		$this->viewParams['content'] = 'level/add';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}
	public function save(){
		if($this->input->post()){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('level_name', 'Level Name', 'required');
			$this->form_validation->set_rules('amount', 'Amount', 'required');
			$this->form_validation->set_rules('exam_count', 'Exam count', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->viewParams['parent_data'] = $this->Level_model->get_parent_data('status="1" AND delete_status!="1"');
				$this->viewParams['page_title'] = 'Add Level';
				$jsfilearray = array(
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/level.js'
				                   );
				$this->viewParams['jsfilearray'] = $jsfilearray;	
				$this->viewParams['content'] = 'level/add';
				$this->load->view('admin/layout/2colmn-left',$this->viewParams);
			}else{
				$records['level_name'] 		= $this->input->post('level_name');
				$records['amount'] 			= $this->input->post('amount');
				$records['exam_count'] 		= $this->input->post('exam_count');
				$records['parent_id'] 		= $this->input->post('parent_id');
				$records['created_at'] 		= date("Y-m-d H:i:s");
				$level_id=$this->Level_model->insert($records);
				if(!empty($level_id)){
					$this->session->set_flashdata('success', 'Level added successfully.');
					redirect(ADMIN_BASE.'levels/list', 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Level added unsuccessfully.');
					redirect(ADMIN_BASE.'levels/add', 'refresh');
				}
			}
							
		}
	}
    public function edit($id='')
	{
		$level_data=$this->Level_model->get_level_data('level_id="'.$id.'"');
		if(empty($level_data)){
			show_404();
		}else{
			$this->viewParams['parent_data'] = $this->Level_model->get_parent_data('status="1" AND delete_status!="1" AND level_id!="'.$id.'"');
			$this->viewParams['level_data'] = $level_data;
			$this->viewParams['page_title'] = 'Edit Level';
			$jsfilearray = array(
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/level.js'
           	);
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['content'] = 'level/edit';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		
	}
	public function update(){
		if($this->input->post('level_id')){
			$level_id=$this->input->post('level_id');
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('level_name', 'Level Name', 'required');
			$this->form_validation->set_rules('amount', 'Amount', 'required');
			$this->form_validation->set_rules('exam_count', 'Exam count', 'required');
			if ($this->form_validation->run() === FALSE){
				$level_data=$this->Level_model->get_level_data('level_id="'.$level_id.'"');
				if(empty($level_data)){
					show_404();
				}else{
					$this->viewParams['parent_data'] = $this->Level_model->get_parent_data('status="1" AND delete_status!="1" AND level_id!="'.$level_id.'"');
					$this->viewParams['level_data'] = $level_data;
					$this->viewParams['page_title'] = 'Edit Level';
					$jsfilearray = array(
							base_url().'assets/backend/js/jquery.validate.min.js',
							base_url().'assets/backend/js/additional-methods.js',
							base_url().'assets/backend/js/level.js'
					);
					$this->viewParams['jsfilearray'] = $jsfilearray;	
					$this->viewParams['content'] = 'level/edit';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$records['level_name'] 		= $this->input->post('level_name');
				$records['amount'] 			= $this->input->post('amount');
				$records['exam_count'] 		= $this->input->post('exam_count');
				$records['parent_id'] 		= $this->input->post('parent_id');
				$records['updated_at'] 		= date("Y-m-d H:i:s");
				$update=$this->Level_model->update($records,$level_id);
				if($update==true){
					$this->session->set_flashdata('success', 'Level Updated successfully.');
					redirect(ADMIN_BASE.'levels/edit/'.$level_id, 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Level Updated unsuccessfully.');
					redirect(ADMIN_BASE.'levels/edit/'.$level_id, 'refresh');
				}
			}
							
		}	
	}
	public function delete($id)
	{
		$deleted_at = date("Y-m-d H:i:s");
		$delete_status = '1';
		$update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
		$this->db->where("level_id",$id);
		$this->db->update("tbl_level_master",$update);		
		return TRUE;
	}

	public function changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("level_id",$id);
		$this->db->update("tbl_level_master",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}	
}