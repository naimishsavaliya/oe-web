<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exams extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/Exam_model');
	}

	public function index()
	{	

		$jsfilearray = array(
			base_url().'assets/backend/js/exams_datatable.js'
		);
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Exam list';
		$this->viewParams['content'] = 'exam/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables($type='')
	{
		$generalSearch='';
		$date=date("Y-m-d");
		$filter_where='';
		if($type=='1'){
			$filter_where='exam.start_date >"'.$date.'"';
		}elseif ($type=='2') {
			$filter_where='exam.start_date <"'.$date.'"';
		}
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Exam_model->datatable_data($generalSearch,$filter_where);
		//$total_rec = $this->Exam_model->total_record("exam.delete_status!='1'",$filter_where);
		$total_rec = $this->Exam_model->total_record($generalSearch,$filter_where);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
		// echo json_encode($this->Exam_model->datatable_data($generalSearch));
		// exit();
	}

    public function add()
	{
		$this->viewParams['standard_data'] = $this->Exam_model->get_standard_data(array('status'),array('1'));
		$this->viewParams['subject_data'] = $this->Exam_model->get_subject_data(array('status'),array('1'));
		$this->viewParams['page_title'] = 'Add Exam';
		$jsfilearray = array(
								base_url().'assets/backend/css/ckeditor/ckeditor.js',
								base_url().'assets/backend/css/ckeditor/config.js',
								base_url().'assets/backend/css/ckeditor/build-config.js',
								base_url().'assets/backend/js/jquery.validate.min.js',
								base_url().'assets/backend/js/additional-methods.js',
								base_url().'assets/backend/js/exam.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;	
		$this->viewParams['content'] = 'exam/add';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}
	public function save(){
		if($this->input->post()){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('standard_id', 'Standard Name', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject Name', 'required');
			$this->form_validation->set_rules('exam_title', 'Exam Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('total_que', 'Total Question', 'required');
			$this->form_validation->set_rules('que_mark', ' Question Mark', 'required');
			$this->form_validation->set_rules('negative_mark', 'Negative Mark', 'required');
			$this->form_validation->set_rules('max_student', 'Max Student', 'required');
			$this->form_validation->set_rules('min_student', 'Min Student', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('start_date', 'Start Date', 'required');
			$this->form_validation->set_rules('start_time', 'Start Time', 'required');
			$this->form_validation->set_rules('end_time', 'End Time', 'required');
			$this->form_validation->set_rules('question_time', 'Question Time', 'required');
			$this->form_validation->set_rules('exam_time', 'Exam Time', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->viewParams['standard_data'] = $this->Exam_model->get_standard_data(array('status'),array('1'));
				$this->viewParams['subject_data'] = $this->Exam_model->get_subject_data(array('status'),array('1'));
				$this->viewParams['page_title'] = 'Add Exam';
				$jsfilearray = array(
					base_url().'assets/backend/css/ckeditor/ckeditor.js',
					base_url().'assets/backend/css/ckeditor/config.js',
					base_url().'assets/backend/css/ckeditor/build-config.js',
					base_url().'assets/backend/js/jquery.validate.min.js',
					base_url().'assets/backend/js/additional-methods.js',
					base_url().'assets/backend/js/exam.js'
				);
				$this->viewParams['jsfilearray'] = $jsfilearray;	
				$this->viewParams['content'] = 'exam/add';
				$this->load->view('admin/layout/2colmn-left',$this->viewParams);
			}else{
				$start_date='';
				if(!empty($this->input->post('start_date'))){
					$start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
				}
				$start_time='';
				if(!empty($this->input->post('start_time'))){
					$start_time = date('H:i:A',strtotime($this->input->post('start_time')));
				}
				$end_time='';
				if(!empty($this->input->post('end_time'))){
					$end_time = date('H:i:A',strtotime($this->input->post('end_time')));
				}
				$records['subject_id'] 		= $this->input->post('subject_id');
				$records['standard_id'] 	= $this->input->post('standard_id');
				$records['exam_title'] 		= $this->input->post('exam_title');
				$records['discription'] 	= $this->input->post('description');
				$records['total_que'] 		= $this->input->post('total_que');
				$records['max_student'] 	= $this->input->post('max_student');
				$records['min_student'] 	= $this->input->post('min_student');
				$records['price'] 			= $this->input->post('price');
				$records['que_mark'] 		= $this->input->post('que_mark');
				$records['negative_mark'] 	= $this->input->post('negative_mark');
				$records['start_date'] 		= $start_date;
				$records['start_time'] 		= $start_time;
				$records['end_time'] 		= $end_time;
				$records['question_time'] 	= $this->input->post('question_time');
				$records['exam_time'] 		= $this->input->post('exam_time');
				$records['created_at'] 		= date("Y-m-d H:i:s");
				$exam_id=$this->Exam_model->insert($records);
				if(!empty($exam_id)){
					$this->session->set_flashdata('success', 'Exam added successfully.');
					redirect(ADMIN_BASE.'exams/list', 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Exam added unsuccessfully.');
					redirect(ADMIN_BASE.'exams/add', 'refresh');
				}
			}
							
		}
	}
    public function edit($id='')
	{
		$this->viewParams['standard_data'] = $this->Exam_model->get_standard_data(array('status'),array('1'));
		$this->viewParams['subject_data'] = $this->Exam_model->get_subject_data(array('status'),array('1'));
		$exam_data=$this->Exam_model->get_exam_data($id);
		if(empty($exam_data)){
			show_404();
		}else{
			$this->viewParams['exam_data'] = $exam_data;
			$this->viewParams['page_title'] = 'Edit Exam';
			$jsfilearray = array(
				base_url().'assets/backend/css/ckeditor/ckeditor.js',
				base_url().'assets/backend/css/ckeditor/config.js',
				base_url().'assets/backend/css/ckeditor/build-config.js',
				base_url().'assets/backend/js/jquery.validate.min.js',
				base_url().'assets/backend/js/additional-methods.js',
				base_url().'assets/backend/js/edit_exam.js'
			);
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['content'] = 'exam/edit';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
		
	}
	public function update(){
		if($this->input->post('exam_id')){
			$exam_id=$this->input->post('exam_id');
			$exam_data=$this->Exam_model->get_exam_data($exam_id);
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('standard_id', 'Standard Name', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject Name', 'required');
			$this->form_validation->set_rules('exam_title', 'Exam Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('que_mark', ' Question Mark', 'required');
			$this->form_validation->set_rules('negative_mark', 'Negative Mark', 'required');
			$this->form_validation->set_rules('total_que', 'Total Question', 'required');
			$this->form_validation->set_rules('max_student', 'Max Student', 'required');
			$this->form_validation->set_rules('min_student', 'Min Student', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('start_date', 'Start Date', 'required');
			$this->form_validation->set_rules('start_time', 'Start Time', 'required');
			$this->form_validation->set_rules('end_time', 'End Time', 'required');
			$this->form_validation->set_rules('question_time', 'Question Time', 'required');
			$this->form_validation->set_rules('exam_time', 'Exam Time', 'required');
			if ($this->form_validation->run() === FALSE){
				$this->viewParams['standard_data'] = $this->Exam_model->get_standard_data(array('status'),array('1'));
				$this->viewParams['subject_data'] = $this->Exam_model->get_subject_data(array('status'),array('1'));
				$exam_data=$this->Exam_model->get_exam_data($exam_id);
				if(empty($exam_data)){
					show_404();
				}else{
					$this->viewParams['exam_data'] = $exam_data;
					$this->viewParams['page_title'] = 'Edit Exam';
					$jsfilearray = array(
						base_url().'assets/backend/css/ckeditor/ckeditor.js',
						base_url().'assets/backend/css/ckeditor/config.js',
						base_url().'assets/backend/css/ckeditor/build-config.js',
						base_url().'assets/backend/js/jquery.validate.min.js',
						base_url().'assets/backend/js/additional-methods.js',
						base_url().'assets/backend/js/edit_exam.js'
					);
					$this->viewParams['jsfilearray'] = $jsfilearray;	
					$this->viewParams['content'] = 'exam/edit';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$start_date='';
				if(!empty($this->input->post('start_date'))){
					$start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
				}
				$start_time='';
				if(!empty($this->input->post('start_time'))){
					$start_time = date('H:i:A',strtotime($this->input->post('start_time')));
				}
				$end_time='';
				if(!empty($this->input->post('end_time'))){
					$end_time = date('H:i:A',strtotime($this->input->post('end_time')));
				}
				$records['subject_id'] 		= $this->input->post('subject_id');
				$records['standard_id'] 	= $this->input->post('standard_id');
				$records['exam_title'] 		= $this->input->post('exam_title');
				$records['discription'] 	= $this->input->post('description');
				$records['total_que'] 		= $this->input->post('total_que');
				$records['max_student'] 	= $this->input->post('max_student');
				$records['min_student'] 	= $this->input->post('min_student');
				$records['price'] 			= $this->input->post('price');
				$records['que_mark'] 		= $this->input->post('que_mark');
				$records['negative_mark'] 	= $this->input->post('negative_mark');
				$records['start_date'] 		= $start_date;
				$records['start_time'] 		= $start_time;
				$records['end_time'] 		= $end_time;
				$records['question_time'] 	= $this->input->post('question_time');
				$records['exam_time'] 		= $this->input->post('exam_time');
				$records['updated_at'] 		= date("Y-m-d H:i:s");
				$update=$this->Exam_model->update($records,$exam_id);
				if($update==true){
					$this->session->set_flashdata('success', 'Exam Updated successfully.');
					redirect(ADMIN_BASE.'exams/edit/'.$exam_id, 'refresh');
				}else{
					$this->session->set_flashdata('error', 'Exam Updated unsuccessfully.');
					redirect(ADMIN_BASE.'exams/edit/'.$exam_id, 'refresh');
				}
			}
							
		}	
	}
	public function delete($id)
	{
		$deleted_at = date("Y-m-d H:i:s");
		$delete_status = '1';
		$update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
		$this->db->where("id",$id);
		$this->db->update("tbl_exam",$update);		
		return TRUE;
	}

	public function changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_exam",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}
	public function view($id){
		$this->viewParams['exam_data']=$this->Exam_model->get_viewdata($id);
		    $ajax_payload = '';
		    if(!empty($this->viewParams['exam_data'])){
		    	$sub_name=$std_name=$exam_title=$discription=$que_mark=$negative_mark=$total_que=$max_student=$min_student=$price=$question_time=$exam_time='';
		    	if($this->viewParams['exam_data']->subject_name){
		    		$sub_name=$this->viewParams['exam_data']->subject_name;
		    	}
		    	
		    	if($this->viewParams['exam_data']->standard_name){
		    		$std_name=$this->viewParams['exam_data']->standard_name;
		    	}

		    	if($this->viewParams['exam_data']->exam_title){
		    		$exam_title=$this->viewParams['exam_data']->exam_title;
		    	}
		    	
		    	if($this->viewParams['exam_data']->discription){
		    		$discription=strip_tags($this->viewParams['exam_data']->discription);
		    	}

		    	if($this->viewParams['exam_data']->que_mark){
		    		$que_mark=$this->viewParams['exam_data']->que_mark;
		    	}

		    	if($this->viewParams['exam_data']->negative_mark!=''){
		    		$negative_mark=$this->viewParams['exam_data']->negative_mark;
		    	}
		    	
		    	if($this->viewParams['exam_data']->total_que){
		    		$total_que=$this->viewParams['exam_data']->total_que;
		    	}
		    	
		    	if($this->viewParams['exam_data']->max_student){
		    		$max_student=$this->viewParams['exam_data']->max_student;
		    	}

		    	if($this->viewParams['exam_data']->min_student){
		    		$min_student=$this->viewParams['exam_data']->min_student;
		    	}
		    	if($this->viewParams['exam_data']->price){
		    		$price=$this->viewParams['exam_data']->price;
		    	}
		    	if($this->viewParams['exam_data']->question_time){
		    		$question_time=$this->viewParams['exam_data']->question_time;
		    	}
		    	if($this->viewParams['exam_data']->exam_time){
		    		$exam_time=$this->viewParams['exam_data']->exam_time;
		    	}
		    	
		    	$ajax_payload = '<table id="exam_table" class="table display responsive nowrap"><tr><th>Subject Name</th><td>'.$sub_name.'</td></tr><tr><th>Standard Name</th><td>'.$std_name.'</td></tr><tr><th>Exam Title</th><td>'.$exam_title.'</td></tr><tr><th>Discription</th><td>'.$discription.'</td></tr><tr><th>Question Mark</th><td>'.$que_mark.'</td></tr><tr><th>Negative Mark</th><td>'.$negative_mark.'</td></tr><tr><th>Total Question</th><td>'.$total_que.'</td></tr><tr><th>Max. Student</th><td>'.$max_student.'</td></tr><tr><th>Min. Student</th><td>'.$min_student.'</td></tr><tr><th>Price</th><td>'.$price.'</td></tr><tr><th>Question Time</th><td>'.$question_time.'</td></tr><tr><th>Exam Time</th><td>'.$exam_time.'</td></tr>';

		    	$start_date='';
		    	if($this->viewParams['exam_data']->start_date!='0000-00-00'){
		    		$start_date = date('Y-m-d',strtotime($this->viewParams['exam_data']->start_date));
		    		$ajax_payload.='<tr><th>Start Date.</th><td>'.$start_date.'</td></tr>';
		    	}

		    	$start_time='';
		    	if($this->viewParams['exam_data']->start_time!='00:00:00'){
		    		$start_time = date('H:i:A',strtotime($this->viewParams['exam_data']->start_time));
		    		$ajax_payload.='<tr><th>Start Time.</th><td>'.$start_time.'</td></tr>';
		    	}

		    	$end_time='';
		    	if($this->viewParams['exam_data']->end_time!='00:00:00'){
		    		$end_time = date('H:i:A',strtotime($this->viewParams['exam_data']->end_time));
		    		$ajax_payload.='<tr><th>End Time.</th><td>'.$end_time.'</td></tr>';
		    	}

		    	$created_at='';
		    	if($this->viewParams['exam_data']->created_at!='0000-00-00 00:00:00'){
		    		$created_at = date('Y-m-d',strtotime($this->viewParams['exam_data']->created_at));
		    		$ajax_payload.='<tr><th>Created Date.</th><td>'.$created_at.'</td></tr>';
		    	}
		    	$updated_at='';
		    	if($this->viewParams['exam_data']->updated_at!='0000-00-00 00:00:00'){
		    		$updated_at = date('Y-m-d',strtotime($this->viewParams['exam_data']->updated_at));
		    		$ajax_payload.='<tr><th>Updated Date.</th><td>'.$updated_at.'</td></tr>';
		    	}

		    	$ajax_payload.='</table>'; 
		        echo json_encode($ajax_payload);
		        exit();

		    }else{
		        echo 'Error';
		        exit();
		    }
	}
	public function question_selection($id=''){
		$this->session->unset_userdata('question_cart');
		$this->session->unset_userdata('auto_question_cart');

		$exam_data=$this->Exam_model->get_exam_data($id);
		if(empty($exam_data)){
			show_404();
		}else{
			$subject_name='';
			$standard_name='';
			if($exam_data->subject_id!=''){
				$subject=$this->Exam_model->get_subject_name($exam_data->subject_id);
				$subject_name=$subject->subject_name;
			}
			if($exam_data->standard_id){
				$standard=$this->Exam_model->get_standard_name($exam_data->subject_id);
				$standard_name=$standard->standard_name;
			}
			$subject_id = $exam_data->subject_id;
			$standard_id = $exam_data->standard_id;

			//selected question
			$selected_que=$this->Exam_model->selected_que("exam_id= '".$id."' and standard_id= '".$standard_id."' and subject_id = '".$subject_id."' and delete_status!='1' and status='1'");
			$question_id=[];
			if(!empty($selected_que)){
				foreach ($selected_que as $key1 => $row) {
					$question_id[]=$row['question_id'];
				}
				$this->session->set_userdata('question_cart', $question_id);
			}

			$jsfilearray = array(
				base_url().'assets/backend/js/jquery.validate.min.js',
				base_url().'assets/backend/js/additional-methods.js',
				base_url().'assets/backend/js/question_selection.js',
				base_url().'assets/backend/js/exam_question.js'
			);
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['exam_id'] = $exam_data->id;
			$mode='';
			if($exam_data->id!=''){
				$exam_mode = $this->Exam_model->get_exam_mode("exam_id= '".$exam_data->id."'");
				if(isset($exam_mode->selection_mode)){
					$mode=$exam_mode->selection_mode;
				}
			}
			$this->viewParams['mode']=$mode;

			$selected_que_count='';
			if($exam_data->id!=''){
				$selected_count = $this->Exam_model->total_selected_question("exam_id= '".$exam_data->id."'");
				if(isset($selected_count->selected_que)){
					$selected_que_count=$selected_count->selected_que;
				}
			}
			$this->viewParams['selected_que_count']=$selected_que_count;
			
			$auto_question = $this->Exam_model->get_auto_question("que.standard_id= '".$standard_id."' and que.subject_id = '".$subject_id."' and que.delete_status!='1' and que.status='1'",$exam_data->total_que);
			$question_id=array_column($auto_question, 'id'); 
			$this->session->set_userdata('auto_question_cart', implode(',',$question_id));
			$this->viewParams['auto_question_id'] = $question_id;
			$this->viewParams['subject_id'] = $exam_data->subject_id;
			$this->viewParams['standard_id'] = $exam_data->standard_id;
			$this->viewParams['maximum_question'] = $exam_data->total_que;
			$this->viewParams['subject_name'] = $subject_name;
			$this->viewParams['standard_name'] = $standard_name;
			$this->viewParams['page_title'] = 'Question Selection';
			$this->viewParams['content'] = 'exam/selection';
	    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
	}
	// public function manual_question($standard_id,$subject_id,$exam_id)
	// {
	// 	$this->load->model('Admin/Question_model');
	// 	echo json_encode($this->Question_model->datatable_data("que.standard_id= '".$standard_id."' and que.subject_id = '".$subject_id."'",$exam_id));
	// 	exit();
	// }
	public function manual_question($standard_id,$subject_id,$exam_id)
	{
		$arr = $this->session->userdata('question_cart');
		$data=$this->Exam_model->manual_data("que.standard_id= '".$standard_id."' and que.subject_id = '".$subject_id."' and que.delete_status!='1' and que.status='1'",$exam_id);
		if($arr){
			if(!empty($data)){
				foreach ($data as $key => $value) {
					if(in_array($value['id'], $arr)) {
						$data[$key]['is_selected'] = '1';
				   	}else{
						$data[$key]['is_selected'] = '0';
				   	}
				}
			}else{
				$data = [];
			}
		}
		$total_rec = $this->Exam_model->total_manualdata("que.standard_id= '".$standard_id."' and que.subject_id = '".$subject_id."' and que.delete_status!='1' and que.status='1'");
        $pagination = $cur_page = $limit = '';
        if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
        }
		echo json_encode(
			[
				"data"=>$data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]
			);
		exit();
	}
	public function auto_question($standard_id,$subject_id,$maximum_que)
	{
		$data=$this->Exam_model->auto_questiondata($standard_id,$subject_id,$maximum_que);
		$total_rec = $this->Exam_model->total_autodata("que.standard_id= '".$standard_id."' and que.subject_id = '".$subject_id."' and que.delete_status!='1' and que.status='1'",$maximum_que);
        $pagination = $cur_page = $limit = '';
        if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
        }
		echo json_encode(["data"=>$data,"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]]);
		exit();
	}
	public function exam_question_save(){
		if($this->input->post('exam_id')){
			$this->load->helper('form');
			$this->load->library('Form_validation');
			$this->form_validation->set_rules('mode', 'Mode', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$exam_data=$this->Exam_model->get_exam_data($this->input->post('exam_id'));
				if(empty($exam_data)){
					show_404();
				}else{
					$subject_name='';
					$standard_name='';
					$subject_id = $exam_data->subject_id;
					$standard_id = $exam_data->standard_id;
					if($exam_data->subject_id!=''){
						$subject=$this->Exam_model->get_subject_name($exam_data->subject_id);
						$subject_name=$subject->subject_name;
					}
					if($exam_data->standard_id){
						$standard=$this->Exam_model->get_standard_name($exam_data->subject_id);
						$standard_name=$standard->standard_name;
					}
					$jsfilearray = array(
					base_url().'assets/backend/js/jquery.validate.min.js',
					base_url().'assets/backend/js/additional-methods.js',
					base_url().'assets/backend/js/question_selection.js',
					base_url().'assets/backend/js/exam_question.js'
					);
					$this->viewParams['jsfilearray'] = $jsfilearray;	
					$this->viewParams['exam_id'] = $exam_data->id;
					$this->viewParams['subject_id'] = $exam_data->subject_id;
					$this->viewParams['standard_id'] = $exam_data->standard_id;
					$this->viewParams['maximum_question'] = $exam_data->total_que;
					$this->viewParams['subject_name'] = $subject_name;
					$this->viewParams['standard_name'] = $standard_name;
					$this->viewParams['page_title'] = 'Question Selection';
					$this->viewParams['content'] = 'exam/selection';
					$this->load->view('admin/layout/2colmn-left',$this->viewParams);
				}
			}else{
				$exam_id=$this->input->post('exam_id');
				$subject_id=$this->input->post('subject_id');
				$standard_id=$this->input->post('standard_id');
				$mode=$this->input->post('mode');
				$exist_question = [];
				if($mode=='manual'){
					// $question_id = $this->input->post('selected[]');
					$question_id = $this->session->userdata('question_cart');
				}else{
					$q_id = explode(',',$this->session->userdata('auto_question_cart'));
					$question_id = $q_id;
					//$question_id = $this->input->post('auto_selected[]');
				}
				if(!empty($question_id)){
					foreach ($question_id as $key => $value) {
						$questionanswer_data=$this->Exam_model->get_questionanswer_data($value);
						$records = array('exam_id' 		=> $exam_id,
										'subject_id' 	=> $subject_id,
										'standard_id' 	=> $standard_id,
										'question_id' 	=> $questionanswer_data->id,
										'selection_mode'=> $mode,
										'question' 		=> $questionanswer_data->question,
										'correct_answer'=> $questionanswer_data->correct_answer,
										'answer_type' 	=> $questionanswer_data->answer_type,
										'answer1' 		=> $questionanswer_data->answer1,
										'answer2' 		=> $questionanswer_data->answer2,
										'answer3' 		=> $questionanswer_data->answer3,
										'answer4' 		=> $questionanswer_data->answer4,
										'created_at'	=> date("Y-m-d H:i:s"));
						$result=$this->Exam_model->check_dublicate_question($exam_id,$value);
						if($result!=null)
						{
							$exist_question[] = $result->id;
						}else{
							$insert_record=$this->Exam_model->insert_exam_question($records);
						}
					}
				}else{
					$this->session->set_flashdata('error', 'Please select question.');
					redirect(ADMIN_BASE.'exams/question_selection/'.$exam_id, 'refresh');
				}
				
				if(count($exist_question)>0){
					$this->session->set_flashdata('success', 'Some question were already added.');
					redirect(ADMIN_BASE.'exams/question_selection/'.$exam_id, 'refresh');
				}
				else{
					$this->session->set_flashdata('success', 'Added successfully.');
					redirect(ADMIN_BASE.'exams/list', 'refresh');
				}
			}					
		}
	}
	public function add_question(){
		//$this->session->unset_userdata('question_cart'); exit();
		$question_id=$this->input->post('id'); 
		$arr = [];
		if(!empty($this->session->userdata('question_cart'))){
			$arr = $this->session->userdata('question_cart');
			if(in_array($question_id, $arr)) {
		        $key = array_search($question_id, $arr);
		        unset($arr[$key]);
		   	}else{
		   		$arr[] = $question_id;
		   	}
			$this->session->set_userdata('question_cart', $arr);
		}
		else{
			$arr[] = $question_id;
			$this->session->set_userdata('question_cart', $arr);
		}
		$data = array('total_count' => count($arr),'question_data'=>$arr);
		echo json_encode($data);
		exit();
	}

	public function exam_result($id=''){
		$exam_data= $this->Exam_model->check_exam("id= '".$id."'");
		if(empty($exam_data)){
			show_404();
		}else{
			$this->viewParams['exam_id'] = $id;
			$this->viewParams['result_status'] = $exam_data->result_status;
			$this->viewParams['page_title'] = 'Exam result list';
			$jsfilearray = array(
				base_url('assets/backend/js/jquery.validate.min.js'),
				base_url('assets/backend/js/additional-methods.js'),
				base_url('assets/backend/js/exam_result_datatable.js')
			);
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['content'] = 'exam/result_list';
			$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
	}
	public function result_dataTables($id='')
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$total_question=$this->Exam_model->count_question('exam_id="'.$id.'"');
		$exam_result=$this->Exam_model->result_datatable_data($generalSearch,$id);
		//echo '<pre>'; var_dump($exam_result); die();
		foreach ($exam_result as $key => $value) {
			$coorect_count='';
			$question_data=json_decode($value['question_data']);
			foreach ($question_data as $key2 => $row) {
				$question_id=$row->question_id;
				$answer_id=$row->answer_id;
				if($question_id!='' && $answer_id!=''){
					$coorect=$this->Exam_model->get_result('exam_id="'.$id.'" AND delete_status!="1" AND question_id="'.$question_id.'" AND correct_answer="'.$answer_id.'"');
					if($coorect==1){
						$coorect_count++;
					}
				}
			}
			if($coorect_count!=''){
				$exam_result[$key]['result']=$coorect_count.'/'.$total_question;
			}else{
				$exam_result[$key]['result']='0'.'/'.$total_question;
			}
			
		}
		usort($exam_result, function($a, $b) {
		    return $a['result'] <= $b['result'];
		});
		$datatable_data = $exam_result;
		//echo '<pre>'; var_dump($datatable_data); die();
		$total_rec = $this->Exam_model->result_total_record("exam_result.delete_status!='1' AND exam_result.exam_id='".$id."'",$generalSearch);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}
	public function user_viewquestion($exam_id='',$user_id=''){
		$exam_data= $this->Exam_model->check_userexam("exam_id='".$exam_id."' AND user_id='".$user_id."'");
		//echo '<pre>'; var_dump($exam_data); die();
		if(empty($exam_data)){
			show_404();
		}else{
			$this->viewParams['exam_id'] = $exam_data->exam_id;
			$this->viewParams['user_id'] = $exam_data->user_id;
			$this->viewParams['page_title'] = 'User question list';
			$jsfilearray = array(
				base_url('assets/backend/js/userquestion_datatable.js')
			);
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['content'] = 'exam/userquestion_list';
			$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
	}
	public function userquestion_dataTables($exam_id='',$user_id='')
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$user_data 		= $this->Exam_model->get_user_examdata("exam_id='".$exam_id."' AND user_id='".$user_id."'");
		$question_data 	= json_decode($user_data->question_data,true);
		$question_id 	= array_column($question_data, 'question_id');
		$user_questions = array_column($question_data, 'answer_id', 'question_id');
		$user_answers 	= array_column($question_data, 'answer_val', 'question_id');
		$question_list 	= $this->Exam_model->get_exam_question_user($question_id,'que.exam_id="'.$exam_id.'" AND que.delete_status!="1"',$generalSearch);
		foreach ($question_list as $key => $value) {
			//$question_list[$key]['question_id'] = intval($question_list[$key]['question_id']);
			$qu_id = $value['question_id'];
			$question_list[$key]['user_answer']=$user_answers[$qu_id];
			if($value['correct_answer']==$user_questions[$qu_id]){
				$question_list[$key]['sign']='1';
			}
			else{
				$question_list[$key]['sign']='0';
			}
		}
		usort($question_list, function($a, $b) {
		    return $a['sign'] <= $b['sign'];
		});
		$total_rec = $this->Exam_model->total_record_user_question($question_id,'que.exam_id="'.$exam_id.'" AND que.delete_status!="1"',$generalSearch);
		//var_dump($total_rec); die();
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		$datatable_data=$question_list; 
		echo json_encode(["data"=>$datatable_data,"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]]);
		exit();
	}
	public function rank($exam_id='',$user_id=''){
		$total_users=$this->Exam_model->count_users('exam_id="'.$exam_id.'"');
		$user_rank=$this->Exam_model->get_rank('exam_id="'.$exam_id.'" AND user_id="'.$user_id.'"');
		$rank="";
		if(isset($user_rank)){
			if($user_rank!=NULL){
				$rank=$user_rank;
			}
		}
		$option_val='';
		    $ajax_payload = '';
		    if($total_users!=''){
		    	$i=1;
		    	$ajax_payload = '<div class="row"><label class="col-sm-3">Rank List</label><div class="col-sm-9"><select class="form-control" id="rank_number" name="rank_number"><option value="">Rank</option>';
       				for($i; $i<=$total_users; $i++){
       					$myselected="";
       					if($i==$rank)
       					{
       						$myselected="selected";
       					}
		    			$option_val.='<option '.$myselected.' value='.$i.'>'.$i.'</option>';
		    	 	}
		    	$ajax_payload.=$option_val;
		    	$ajax_payload.='</select></div></div>'; 
		        echo json_encode($ajax_payload);
		        exit();
		    }else{
		        echo 'Error';
		        exit();
		    }
	}
	public function rank_update($exam_id='',$user_id=''){
		$rank_number=$this->input->post('rank_number');
		if($rank_number!=''){
			$records = array(
				'winning_no'	=>	$this->input->post('rank_number'),
				'winning_no_date'  	=>  date("Y-m-d H:i:s"),
				'updated_at'  	=>  date("Y-m-d H:i:s")
			);	
			$update=$this->Exam_model->update_user_rank($records,'exam_id="'.$exam_id.'" AND user_id="'.$user_id.'"');
			if($update==true){
				echo json_encode(array('success'=>"Rank updated successfully."));
	 			exit();
			}else{
				echo json_encode(['error'=>'Error']);
				exit();
			}
		}else{
			echo json_encode(['error'=>'Error']);
				exit();
		}		
	}
	public function generate_result($exam_id=''){
		$exam_result=$this->Exam_model->get_exam_result('exam_result.delete_status!="1" AND exam_result.exam_id="'.$exam_id.'"','exam_result.id');
		//$exam_result=$this->Exam_model->get_exam_result($exam_id);
		if(!empty($exam_result)){
			foreach ($exam_result as $key => $value) {
				$coorect_count='';
				$question_data=json_decode($value['question_data']);
				foreach ($question_data as $key2 => $row) {
					$question_id=$row->question_id;
					$answer_id=$row->answer_id;
					if($question_id!='' && $answer_id!=''){
						$coorect=$this->Exam_model->get_result('exam_id="'.$exam_id.'" AND delete_status!="1" AND question_id="'.$question_id.'" AND correct_answer="'.$answer_id.'"');
						if($coorect==1){
							$coorect_count++;
						}
					}
				}
				if($coorect_count!=''){
					$exam_result[$key]['result']=$coorect_count;
				}else{
					$exam_result[$key]['result']='0';
				}
				$user_id = $exam_result[$key]['user_id'];
				//User data
				$user_data=$this->Exam_model->get_user_data('id="'.$user_id.'" AND status="1"');
				//level data
				$level_data=$this->Exam_model->get_level_data();
				if(!empty($user_data)){
					if($user_data->current_level_id===null)
						$current_level_id			=	0;
					else
						$current_level_id 		=	$user_data->current_level_id;
					$next_level_id				=	$user_data->next_level_id;
					$level_exam_completed_count	=	$user_data->level_exam_completed_count+1;
					$total_discount_amount		=	$user_data->total_discount_amount;

					if($next_level_id==0 && $user_data->current_level_id!==null){
						// $this->session->set_flashdata('error', 'Generate result unsuccessfully..');
						// redirect(ADMIN_BASE.'exams/exam_result/'.$exam_id, 'refresh');
						$level_records = array(
							'level_exam_completed_count'=>	0,
							'next_level_id'				=>	$next_level_id,
							'updated_at'  				=>  date("Y-m-d H:i:s")
						);
						$user_update=$this->Exam_model->user_level_update($level_records,'id="'.$user_id.'"');
					}elseif(($level_exam_completed_count) == $level_data[$current_level_id]['exam_count']){
						$total_discount_amount 			+= floatval($level_data[$current_level_id]['amount']);

						$n_level_id=0;
						if(isset($level_data[$current_level_id+1]['level_id'])){
							$n_level_id=$level_data[$current_level_id+1]['level_id'];
						}

						$level_records = array(
							'current_level_id'			=>	$level_data[$current_level_id]['level_id'],
							'total_discount_amount'		=>	$total_discount_amount,
							'level_exam_completed_count'=>	0,
							//'next_level_id'			=>	$level_data[$current_level_id+1]['level_id'],
							'next_level_id'				=>	$n_level_id,
							'updated_at'				=>  date("Y-m-d H:i:s")
						);
						$user_update=$this->Exam_model->user_level_update($level_records,'id="'.$user_id.'"');
						if($user_update==true){
							$master_level_data=$this->Exam_model->get_level_name('level_id="'.$level_data[$current_level_id]['level_id'].'"');
							$userlevel_records = array(
								'user_id'				=>	$user_id,
								'level_id'				=>	$level_data[$current_level_id]['level_id'],
								'amount'				=>	$total_discount_amount,
								'exam_count'			=>	$level_data[$current_level_id]['exam_count'],
								'level_name'			=>	$master_level_data->level_name,
								'created_at'			=>  date("Y-m-d H:i:s")
							);
							$insert_record=$this->Exam_model->insert_user_level($userlevel_records);
						}
						// update above 4 values in db
					}else{
						$level_records = array(
							'level_exam_completed_count'=>	$level_exam_completed_count,
							'next_level_id'				=>	$level_data[$current_level_id]['level_id'],
							'updated_at'  			=>  date("Y-m-d H:i:s")
						);
						$user_update=$this->Exam_model->user_level_update($level_records,'id="'.$user_id.'"');
						// update above 2 values in db
					}
				}
				//insert result and result date
				$records = array(
					'result'	=>	$coorect_count,
					'result_date' =>  date("Y-m-d H:i:s")
				);
				$update=$this->Exam_model->update_exam_result($records,'exam_id="'.$exam_id.'" AND user_id="'.$user_id.'"');
			}
			if($update){
				$exam_result_score=$this->Exam_model->get_exam_result_score('exam_result.delete_status!="1" AND exam_result.exam_id="'.$exam_id.'"','exam_result.result');
				if(!empty($exam_result_score)){
					$rank = 1;
					foreach ($exam_result_score as $key3 => $score_value) {
						$score=$score_value['score']; 
						$score_records = array(
							'winning_no'		=>	$rank++,
							'winning_no_date'  	=>  date("Y-m-d H:i:s"),
							'updated_at'  		=>  date("Y-m-d H:i:s")
						);
						$rank_update=$this->Exam_model->update_user_rank($score_records,'result="'.$score.'" AND exam_id="'.$exam_id.'"');
						
					}
				}
				$exam_record = array(
					'result_status'	=>	1,
					'updated_at' =>  date("Y-m-d H:i:s")
				);
				$result_status=$this->Exam_model->change_result_status($exam_record,'id="'.$exam_id.'"');
				$this->session->set_flashdata('success', 'Generate result successfully.');
				redirect(ADMIN_BASE.'exams/exam_result/'.$exam_id, 'refresh');
			}else{
				$this->session->set_flashdata('error', 'Generate result unsuccessfully..');
				redirect(ADMIN_BASE.'exams/exam_result/'.$exam_id, 'refresh');
			}
		}else{
			$this->session->set_flashdata('error', 'No data available.');
			redirect(ADMIN_BASE.'exams/exam_result/'.$exam_id, 'refresh');
		}
	}
	public function exam_book($id=''){
		$exam_data= $this->Exam_model->check_exam("id= '".$id."'");
		if(empty($exam_data)){
			show_404();
		}else{
			$this->viewParams['exam_id'] = $id;
			$this->viewParams['page_title'] = 'Exam book list';
			$jsfilearray = array(
				base_url('assets/backend/js/jquery.validate.min.js'),
				base_url('assets/backend/js/additional-methods.js'),
				base_url('assets/backend/js/exam_book_datatable.js')
			);
			$this->viewParams['jsfilearray'] = $jsfilearray;	
			$this->viewParams['content'] = 'exam/book_list';
			$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}
	}

	public function book_dataTables($id='')
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Exam_model->exambook_datatable_data($generalSearch,$id);
		$total_rec = $this->Exam_model->exambook_totalrecord("exam.delete_status!='1' AND exam_book.exam_id='".$id."'",$generalSearch);
	    $pagination = $cur_page = $limit = '';
	    $limit = 1;
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}
	public function book_changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_exam_book",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}
	public function transaction_list()
	{	
		$jsfilearray = array(
			base_url().'assets/backend/js/transaction_datatable.js'
		);
		$this->viewParams['jsfilearray'] = $jsfilearray;
		$this->viewParams['page_title'] = 'Transaction list';
		$this->viewParams['content'] = 'exam/transaction_list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function transaction_dataTables($start_date='',$end_date='')
	{
		$filter_where='';
		if($start_date!='' && $end_date!=''){
			$start_date = date('Y-m-d',strtotime($start_date));
			$end_date 	= date('Y-m-d',strtotime($end_date));
			$filter_where="( DATE(exam_book.created_at) BETWEEN '".$start_date."' AND '".$end_date."')";
		}
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->Exam_model->transaction_datatable_data($generalSearch,$filter_where);
		$total_rec = $this->Exam_model->transaction_total_record("exam.delete_status!='1'",$generalSearch,$filter_where);
	    $pagination = $cur_page = $limit = '';
	    if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
	    }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}
	public function transaction_changestatus($id,$status)
	{ 
		$update=array("status"=>$status);
		$this->db->where("id",$id);
		$this->db->update("tbl_exam_book",$update);
		echo json_encode(['responce'=>'success']);
		exit();
	}			
}