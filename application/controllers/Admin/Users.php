<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		if(!isset($this->session->userdata['admin_data']['adminid']) || $this->session->userdata['admin_data']['adminid']=='')
		{
			header('location:'.base_url().ADMIN_BASE);
			exit;
		}
		$this->load->model('Admin/User_model');
	}

	public function index()
	{	
		$jsfilearray = array(
								base_url().'assets/backend/js/user_datatable.js'
	 	                       );
		$this->viewParams['jsfilearray'] = $jsfilearray;
		// $this->viewParams['users_data'] = $this->User_model->list_users($orderby='id');
		$this->viewParams['page_title'] = 'User list';
		$this->viewParams['content'] = 'user/list';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);	
		
	}

	public function dataTables()
	{
		$generalSearch='';
		$data=$this->input->post('query');
		if(!empty($data)){
				$generalSearch=$data['generalSearch'];		
		}
		$datatable_data=$this->User_model->list_users($generalSearch);
		$total_rec = $this->User_model->total_record($generalSearch);
    	$pagination = $cur_page = $limit = '';
        if($this->input->get_post("pagination")!=''){
			$pagination = $this->input->get_post("pagination");
			$cur_page = $pagination['page'];
				if(isset($pagination['perpage']))
			 		$limit = $pagination['perpage'];
				else
					$limit = 10;
        }
		echo json_encode([
				"data"=>$datatable_data,
				"meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
			]);
		exit();
	}

    public function edit($id=0)
	{
		if($id==0){
				$this->viewParams['mode']='add';
				$this->viewParams['standard_data'] = $this->User_model->get_standard_data(array('status'),array('1'));
				//echo '<pre>'; var_dump($this->viewParams['standard_data']); die();
		}else{
			//var_dump('EDIT'); die();
			$this->viewParams['mode']='edit';
			$this->viewParams['users_data'] = array();
			if($id!=0){	
				$users_data = $this->User_model->get_user_detail(array('id'),array($id));
				$standard_data = $this->User_model->get_standard_data(array('status'),array('1'));
				if(empty($users_data)){
					show_404();
				}else{
					$this->viewParams['users_data'] = $users_data;
					$this->viewParams['standard_data'] = $standard_data;
				}
				
				//echo '<pre>'; var_dump($this->viewParams['users_data']); die();
			}
		}
		if($this->input->post('mode')){
				$this->load->helper('form');
				$this->load->library('Form_validation');
				$this->load->library('encryption');
				if($this->input->post('mode') == 'add'){
					//$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('fullname', 'Full Name', 'required');
					$this->form_validation->set_rules('email','Email','required|valid_email|callback_email_check');
					$this->form_validation->set_rules('mobile','Mobile no.','required|callback_phoneno_check');
					$this->form_validation->set_rules('password', 'Password', 'required');
				}elseif($this->input->post('mode') == 'edit'){
					//$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('fullname', 'Full Name', 'required');
					$this->form_validation->set_rules('email','Email','required|valid_email');
					$this->form_validation->set_rules('mobile','Mobile no.','required');	
				}

				if ($this->form_validation->run() === FALSE)
				{
					$errors = validation_errors();
					echo json_encode(['error'=>$errors]);
					exit();
				}else{					
					if($this->input->post('mode') == 'edit')
					{
						$birthdate='';
						if(!empty($this->input->post('birthdate'))){
							$birthdate = date('Y-m-d',strtotime($this->input->post('birthdate')));
						}
						$records = array(
							'fullname'	    			=>	$this->input->post('fullname'),
							'email'	    			=>	$this->input->post('email'),
							'mobile'	        	=>	$this->input->post('mobile'),
							'birthdate'				=>	$birthdate,
							'school'	    		=>	$this->input->post('school'),
							'standard'	   			=>	$this->input->post('standard'),
							'updated_at'  			=>  date("Y-m-d H:i:s")
							//'username'				=>  $this->input->post('username')
						);	

						$update=$this->User_model->update($records,array('id'=>$id));
						if($update==true){
							echo json_encode(array('success'=>"User updated successfully."));
				 			exit();
						}else{
							echo json_encode(['error'=>'Error']);
							exit();
						}

					}elseif($this->input->post('mode') == 'add'){
						$birthdate='';
						if(!empty($this->input->post('birthdate'))){
							$birthdate = date('Y-m-d',strtotime($this->input->post('birthdate')));
						}
						$password = $this->encryption->encrypt($this->input->post('password'));
						
						$records = array(
							'fullname'	    		=>	$this->input->post('fullname'),
							'email'	    			=>	$this->input->post('email'),
							'mobile'	        	=>	$this->input->post('mobile'),
							'birthdate'				=>	$birthdate,
							'school'	    		=>	$this->input->post('school'),
							'standard'	   			=>	$this->input->post('standard'),
							'password'				=>	$password,
							'created_at'  			=>  date("Y-m-d H:i:s")
							//'username'				=>  $this->input->post('username')
						);	
						$insert=$this->User_model->insert($records);
						if($insert==true){
							echo json_encode(array('success'=>"Userl added successfully."));
				 			exit();
						}else{
							echo json_encode(['error'=>'Error']);
							exit();
						}
					}
				}
							
			}
		if($id==0)
		{
			$this->viewParams['page_title'] = 'Add User';
			$jsfilearray = array(
									base_url().'assets/backend/js/jquery.validate.min.js',
									base_url().'assets/backend/js/additional-methods.js',
									base_url().'assets/backend/js/users.js'
		 	                       );
			$this->viewParams['jsfilearray'] = $jsfilearray;
		}
		else
		{
			$this->viewParams['page_title'] = 'Edit User';
			$jsfilearray = array(
									base_url().'assets/backend/js/jquery.validate.min.js',
									base_url().'assets/backend/js/additional-methods.js',
									base_url().'assets/backend/js/users.js'
		 	                       );
			$this->viewParams['jsfilearray'] = $jsfilearray;
		}
		$this->viewParams['content'] = 'user/add';
    	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
	}

		public function email_check($str)
		{
			$result=$this->User_model->check_email($str);
				if($result>0)
				{
					$this->form_validation->set_message('email_check', 'The email id is already exists');
	                return FALSE;
				}
				else
				{
					return TRUE;
				}
		}
		public function phoneno_check($str)
		{
			$result=$this->User_model->check_mobile($str);
				if($result>0)
				{
					$this->form_validation->set_message('phoneno_check', 'The Mobile No. is already exists');
	                return FALSE;
				}
				else
				{
					return TRUE;
				}
		}
		public function user_delete($id)
		{
			$this->User_model->delete($id);		
			return TRUE;
		}

		public function user_changestatus($id,$status)
		{ 
			$update=array("status"=>$status);
			$this->db->where("id",$id);
			$this->db->update("oauth_users",$update);
			echo json_encode(['responce'=>'success']);
			exit();
		
		}
		public function changepassword($id=0)
		{	
			if($this->input->post())
			{
				$this->load->helper('form');
				$this->load->library('Form_validation');
				$this->load->library('encryption');
				$this->form_validation->set_rules('new_password', 'New password', 'required');
				$this->form_validation->set_rules('r_password', 'Confirm new password', 'required|matches[new_password]');
				if ($this->form_validation->run() === FALSE)
				{
					$errors = validation_errors();
					echo json_encode(['error'=>$errors]);
					exit();
				}else{	
					$user_array = array('password'=>$this->encryption->encrypt($this->input->post('new_password')));
					$where_array = array('id'=>$id);
					$update=$this->User_model->update($user_array,$where_array);
					if($update==true){
						echo json_encode(array('success'=>"Password change successfully."));
			 			exit();
					}else{
						echo json_encode(['error'=>'Unsuccessfully']);
						exit();
					}
				}	
			}
			$this->viewParams['page_title'] = 'Change Password';
			$jsfilearray = array(
									base_url().'assets/backend/js/jquery.validate.min.js',
									base_url().'assets/backend/js/additional-methods.js',
									base_url().'assets/backend/js/changepassword.js'
		 	                       );
			$this->viewParams['jsfilearray'] = $jsfilearray;
			$this->viewParams['content'] = 'user/changepassword';
        	$this->load->view('admin/layout/2colmn-left',$this->viewParams);
		}

		public function view($id){
		    $this->viewParams['users_data']= $this->User_model->view_userdata('user.id="'.$id.'"');
		    $users_device_data= $this->User_model->view_user_devicedata('device.user_id="'.$id.'" AND device.delete_status="0"');
		    // $ud_id=[];
		    // $token=[];
		    // $device_name=[];
		    // $mobile_type=[];
		    // if(!empty($users_device_data)){
		    // 	foreach ($users_device_data as $key => $value) {
				  //   $ud_id[]=$value['ud_id'];
				  //   $token[]=$value['token'];
				  //   $device_name[]=$value['device_name'];
				  //   $mobile_type[]=$value['mobile_type'];
		    // 	}
		    // }
		    //echo '<pre>'; var_dump($this->viewParams['users_data']); die();
		    $ajax_payload = '';
		    if(!empty($this->viewParams['users_data'])){
		    	$full_name='';
		    	$email_id='';
		    	$phone_no='';
		    	if($this->viewParams['users_data']->fullname){
		    		$full_name=$this->viewParams['users_data']->fullname;
		    	}

		    	if($this->viewParams['users_data']->email){
		    		$email_id=$this->viewParams['users_data']->email;
		    	}
		    	
		    	if($this->viewParams['users_data']->mobile){
		    		$phone_no=$this->viewParams['users_data']->mobile;
		    	}
		    	
		    	$ajax_payload = '<table id="user_list" class="table display responsive nowrap"><tr><th>Full Name</th><td>'.$full_name.'</td></tr><tr><th>Email</th><td>'.$email_id.'</td></tr><tr><th>Mobile no.</th><td>'.$phone_no.'</td></tr>';

		    	$birthdate_val='';
		    	if($this->viewParams['users_data']->birthdate!='0000-00-00 00:00:00'){
		    		$birthdate_val = date('Y-m-d',strtotime($this->viewParams['users_data']->birthdate));
		    		$ajax_payload.='<tr><th>Birthdate.</th><td>'.$birthdate_val.'</td></tr>';
		    	}

		    	$school_name='';
		    	if($this->viewParams['users_data']->school){
		    		$school_name=$this->viewParams['users_data']->school;
		    		$ajax_payload.='<tr><th>School Name.</th><td>'.$school_name.'</td></tr>';
		    	}
		    	
		    	$standard='';
		    	if($this->viewParams['users_data']->standard_name){
		    		$standard=$this->viewParams['users_data']->standard_name;
		    		$ajax_payload.='<tr><th>Standard Name.</th><td>'.$standard.'</td></tr>';
		    	}

		    	// if(!empty($ud_id)){
		    	// 	$ud_count=1;
		    	// 	foreach ($ud_id as $key => $udid_value) {
		    	// 		$ajax_payload.='<tr>';
		    	// 		if($ud_count==1){
		    	// 			$ajax_payload.='<th>UDID</th>';
		    	// 		}else{
		    	// 			$ajax_payload.='<th></th>';	
		    	// 		}
		    	// 		$ajax_payload.='<td>'.$udid_value.'</td></tr>';
		    	// 		$ud_count++;
		    	// 	}
		    	// }
		    	// if(!empty($token)){
		    	// 	$token_count=1;
		    	// 	foreach ($token as $key => $token_value) {
		    	// 		$ajax_payload.='<tr>';
		    	// 		if($token_count==1){
		    	// 			$ajax_payload.='<th>Token</th>';
		    	// 		}else{
		    	// 			$ajax_payload.='<th></th>';	
		    	// 		}
		    	// 		$ajax_payload.='<td>'.$token_value.'</td></tr>';
		    	// 		$token_count++;
		    	// 	}
		    	// }
		    	// if(!empty($device_name)){
		    	// 	$devicename_count=1;
		    	// 	foreach ($device_name as $key => $devicename_value) {
		    	// 		$ajax_payload.='<tr>';
		    	// 		if($devicename_count==1){
		    	// 			$ajax_payload.='<th>Device Name</th>';
		    	// 		}else{
		    	// 			$ajax_payload.='<th></th>';	
		    	// 		}
		    	// 		$ajax_payload.='<td>'.$devicename_value.'</td></tr>';
		    	// 		$devicename_count++;
		    	// 	}
		    	// }
		    	// if(!empty($mobile_type)){
		    	// 	$mobiletype_count=1;
		    	// 	foreach ($mobile_type as $key => $mobiletype_value) {
		    	// 		$ajax_payload.='<tr>';
		    	// 		if($mobiletype_count==1){
		    	// 			$ajax_payload.='<th>Mobile Type</th>';
		    	// 		}else{
		    	// 			$ajax_payload.='<th></th>';	
		    	// 		}
		    	// 		$ajax_payload.='<td>'.$mobiletype_value.'</td></tr>';
		    	// 		$mobiletype_count++;
		    	// 	}
		    	// }

		    	// $ud_id='';
		    	// if($this->viewParams['users_data']->ud_id!=NULL){
		    	// 	$ud_id=$this->viewParams['users_data']->ud_id;
		    	// 	$ajax_payload.='<tr><th>UDID.</th><td>'.$ud_id.'</td></tr>';
		    	// }

		    	// $mobile_type='';
		    	// if($this->viewParams['users_data']->mobile_type!=NULL){
		    	// 	$mobile_type=$this->viewParams['users_data']->mobile_type;
		    	// 	$ajax_payload.='<tr><th>Standard Name.</th><td>'.$mobile_type.'</td></tr>';
		    	// }

		    	$current_level_id='';
		    	if($this->viewParams['users_data']->current_level_id!=NULL){
		    		$current_level_id=$this->viewParams['users_data']->current_level_id;
		    		$ajax_payload.='<tr><th>Current Level Id.</th><td>'.$current_level_id.'</td></tr>';
		    	}

		    	$total_discount_amount='';
		    	if($this->viewParams['users_data']->total_discount_amount!=NULL){
		    		$total_discount_amount=$this->viewParams['users_data']->total_discount_amount;
		    		$ajax_payload.='<tr><th>Total Discount Amount.</th><td>'.$total_discount_amount.'</td></tr>';
		    	}

		    	$level_exam_completed_count='';
		    	if($this->viewParams['users_data']->level_exam_completed_count!=NULL){
		    		$level_exam_completed_count=$this->viewParams['users_data']->level_exam_completed_count;
		    		$ajax_payload.='<tr><th>Level Exam Completed Count.</th><td>'.$level_exam_completed_count.'</td></tr>';
		    	}

		    	$next_level_id='';
		    	if($this->viewParams['users_data']->next_level_id!=NULL){
		    		$next_level_id=$this->viewParams['users_data']->next_level_id;
		    		$ajax_payload.='<tr><th>Next level Id.</th><td>'.$next_level_id.'</td></tr>';
		    	}
		    	
		    	$ajax_payload.='</table>';
				if(!empty($users_device_data)){
					$ajax_payload.='<hr/><h6>Device Data<h6/><table id="device_list" class="table display responsive" style="word-break:break-word"><thead><tr><th>UDID</th><th>Token</th><th>Device Name</th><th>Mobile Type</th></tr></thead><tbody>';
					foreach ($users_device_data as $key => $value) {
						$ud_id=isset($value['ud_id'])?$value['ud_id']:'';
						$token=isset($value['token'])?$value['token']:'';
						$device_name=isset($value['device_name'])?$value['device_name']:'';
						$mobile_type=isset($value['mobile_type'])?$value['mobile_type']:'';
						$ajax_payload.='<tr><td width="25%">'.$ud_id.'</td><td width="25%">'.$token.'</td><td width="25%">'.$device_name.'</td><td width="25%">'.$mobile_type.'</td></tr>'; 
					}
					$ajax_payload.='</tbody></table>'; 
				}
		        echo json_encode($ajax_payload);
		        exit();

		    }else{
		       	echo json_encode('ERROR');
		        exit();
		    }
		}
					
}