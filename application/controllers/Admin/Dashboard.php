<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    protected $viewParams;
    
    public function __construct(){
        parent::__construct();
        $this->viewParams['page_title'] = 'Admin';
        $this->viewParams['page_description'] = 'Admin';
        $this->load->model('Admin/Dashboard_model');
    }
    
    public function index(){
        $this->load->view('admin/layout/empty',$this->viewParams);
    }
    
    public function dashboard(){
        $jsfilearray = array(
            base_url().'assets/backend/js/dashboard_exam.js'
        );
        $this->viewParams['jsfilearray']            = $jsfilearray;
        $this->viewParams['total_user']             = $this->Dashboard_model->count_total_user();
        $this->viewParams['total_subject']          = $this->Dashboard_model->count_total_subject();
        $this->viewParams['total_standard']         = $this->Dashboard_model->count_total_standard();
        $this->viewParams['total_exam']             = $this->Dashboard_model->count_total_exam();
        $this->viewParams['total_upcoming_exam']    = $this->Dashboard_model->count_total_upcoming_exam();
        $this->viewParams['total_pending_result']   = $this->Dashboard_model->count_total_pending_result();
        $this->viewParams['content']                = 'dashboard';
        $this->load->view('admin/layout/2colmn-left',$this->viewParams);
    }
    public function exam_dataTables(){
        $generalSearch='';
        $filter_where='(exam.start_date BETWEEN curdate() AND DATE_ADD(curdate(), INTERVAL 1 WEEK))';
        $data=$this->input->post('query');
        if(!empty($data)){
                $generalSearch=$data['generalSearch'];      
        }
        $datatable_data=$this->Dashboard_model->datatable_data($generalSearch,$filter_where);
        $total_rec = $this->Dashboard_model->total_record($generalSearch,$filter_where);
        $pagination = $cur_page = $limit = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
                if(isset($pagination['perpage']))
                    $limit = $pagination['perpage'];
                else
                    $limit = 10;
        }
        echo json_encode([
                "data"=>$datatable_data,
                "meta"=>['page'=>$cur_page,'pages'=>ceil($total_rec/$limit),'perpage'=>$limit,'total'=>$total_rec]
            ]);
        exit();
    }
    
}