<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Answer extends CI_Migration {

        public function up()
        {
 			      	$this->dbforge->add_field(array(
 			      	                        'id' => array(
 			      	                                'type' => 'INT',
 			      	                                'constraint' => 11,
 			      	                                'unsigned' => TRUE,
 			      	                                'auto_increment' => TRUE
 			      	                        ),
 			      	                        'question_id' => array(
 			      	                                'type' => 'INT',
 			      	                                'constraint' => 11
 			      	                        ),
 			      	                        'answer_type' => array(
 			      	                                'type' => 'INT',
 			      	                                'constraint' => 11
 			      	                        ),
 			      	                        'answer1' => array(
 			      	                                'type' => 'TEXT'
 			      	                        ),
 			      	                        'answer2' => array(
 			      	                                'type' => 'TEXT'
 			      	                        ),
 			      	                        'answer3' => array(
 			      	                                'type' => 'TEXT'
 			      	                        ),
 			      	                        'answer4' => array(
 			      	                                'type' => 'TEXT'
 			      	                        ),
 			      	                        'created_at' => array(
 			      	                                'type' => 'DATETIME'
 			      	                        ),
 			      	                        'updated_at' => array(
 			      	                                'type' => 'DATETIME'
 			      	                        )
 			      	                ));
 			      	$status_field="status int(11) NOT NULL DEFAULT '1' COMMENT '0-Text-4,1-Text-2,2-Image-4,3-Image-2'";
 			      	$this->dbforge->add_field($status_field);
					$this->dbforge->add_key('id', TRUE);
		          	$this->dbforge->create_table('tbl_answer');     
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_answer');
        }
}