<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Notification_modify extends CI_Migration {

        public function up()
        {
        	$fields = array('notification_date' => array('type'=>'datetime','null' => TRUE));
        	$this->dbforge->modify_column('tbl_notification', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_notification');
        }
}