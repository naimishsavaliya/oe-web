<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Email_template_data  extends CI_Migration {

        public function up()
        {
            $template_data = array(
                        array(
                            'name'          =>'Forgot Password',
                            'subject'       =>'{{customer_name}} Forgot Password Token.',
                            'description'   =>'<p>Hello {{customer_name}},</p><p>{{token}}</p><p>{{customer_email}}</p>',
                            'email_type'    =>1,
                            'from_name'     =>'Online Exam',
                            'from_email'    =>'onlineexam@gmail.com',
                            'created_at'    =>'2018-07-20 06:40:11',
                            'variable'      =>'{{customer_name}},{{token}},{{customer_email}}'
                        ),
                        array(
                            'name'          =>'Welcome User message.',
                            'subject'       =>'Welcome,{{customer_name}}.',
                            'description'   =>'<p>Hello,&nbsp;{{customer_name}}</p><p>congratulations!</p><p>You have been&nbsp;successfully&nbsp;registered.</p>',
                            'email_type'    =>1,
                            'from_name'     =>'Online Exam',
                            'from_email'    =>'onlineexam@gmail.com',
                            'created_at'    =>'2018-07-20 06:40:11',
                            'variable'      =>'{{customer_name}}'
                        ),
                        array(
                            'name'          =>'Admin Registration Info',
                            'subject'       =>'User Registration Detail {{customer_name}}.',
                            'description'   =>'<p>FullName:&nbsp;{{customer_name}}</p><p>Mobile: {{customer_mobile}}</p><p>Email: {{customer_email}}</p><p>Birthdate: {{customer_birthdate}}</p><p>Profile: {{customer_profile}}</p><p>UDID: {{customer_udid}}</p><p>Mobile type: {{customer_mobile_type}}</p>',
                            'email_type'    =>2,
                            'from_name'     =>'Online Exam',
                            'from_email'    =>'naimish.empyreal@gmail.com',
                            'created_at'    =>'2018-07-20 06:40:11',
                            'variable'      =>'{{customer_name}},{{customer_mobile}},{{customer_email}},{{customer_birthdate}},{{customer_profile}},{{customer_udid}},{{customer_mobile_type}}'
                        ),
                        array(
                            'name'          =>'Exam book Info',
                            'subject'       =>'Exam book Detail {{customer_name}}.',
                            'description'   =>'<p>FullName:&nbsp;{{customer_name}}</p><p>Mobile: {{customer_mobile}}</p><p>Email: {{customer_email}}</p><p>Exam title: {{exam_title}}</p><p>Standard name: {{standard_name}}</p><p>Subject name: {{subject_name}}</p><p>Amount: {{amount}}</p><p>Transaction id: {{transaction_id}}</p>',
                            'email_type'    =>2,
                            'from_name'     =>'Online Exam',
                            'from_email'    =>'naimish.empyreal@gmail.com',
                            'created_at'    =>'2018-07-20 06:40:11',
                            'variable'      =>'{{customer_name}},{{customer_mobile}},{{customer_email}},{{exam_title}},{{standard_name}},{{subject_name}},{{amount}},{{transaction_id}}'
                        ),
                        array(
                            'name'          =>'Exam book Registration',
                            'subject'       =>'Registration Exam book {{customer_name}}.',
                            'description'   =>'<p>Hello,&nbsp;{{customer_name}}</p><p>Exam Has been booked.</p><p>FullName:&nbsp;{{customer_name}}</p><p>Mobile: {{customer_mobile}}</p><p>Email: {{customer_email}}</p><p>Exam title: {{exam_title}}</p><p>Standard name: {{standard_name}}</p><p>Subject name: {{subject_name}}</p><p>Amount: {{amount}}</p><p>Transaction id: {{transaction_id}}</p>',
                            'email_type'    =>1,
                            'from_name'     =>'Online Exam',
                            'from_email'    =>'onlineexam@gmail.com',
                            'created_at'    =>'2018-07-20 06:40:11',
                            'variable'      =>'{{customer_name}},{{customer_mobile}},{{customer_email}},{{exam_title}},{{standard_name}},{{subject_name}},{{amount}},{{transaction_id}}'
                        ),
                        array(
                            'name'          =>'Exam due date',
                            'subject'       =>'Exam due date information.',
                            'description'   =>'<p>Hello,&nbsp;{{customer_name}}</p><p>FullName:&nbsp;{{customer_name}}</p><p>Mobile: {{customer_mobile}}</p><p>Email: {{customer_email}}</p><p>Exam start date: {{exam_startdate}}</p><p>Exam title: {{exam_title}}</p><p>Standard name: {{standard_name}}</p><p>Subject name: {{subject_name}}</p><p>&nbsp;</p>',
                            'email_type'    =>1,
                            'from_name'     =>'Online Exam',
                            'from_email'    =>'onlineexam@gmail.com',
                            'created_at'    =>'2018-07-20 06:40:11',
                            'variable'      =>'{{customer_name}},{{customer_mobile}},{{customer_email}},{{exam_startdate}},{{exam_title}},{{standard_name}},{{subject_name}}'
                        )
                    );
            $this->db->insert_batch('tbl_email_template',$template_data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_email_template');
        }
}