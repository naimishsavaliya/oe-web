<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_result_status extends CI_Migration {

        public function up()
        {
            $status_field="result_status int(11) NOT NULL DEFAULT '0' COMMENT '0-pending,1-declare'";
            $this->dbforge->add_column('tbl_exam', $status_field);   	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam');
        }
}