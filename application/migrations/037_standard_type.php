<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Standard_type extends CI_Migration {

        public function up()
        {
        	$fields = array(
        	        'standard' => array('type' => 'INT','constraint' => '11')
        	);
        	$this->dbforge->modify_column('oauth_users', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}