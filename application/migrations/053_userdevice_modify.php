<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Userdevice_modify extends CI_Migration {

        public function up()
        {
        	$fields = array(
        	        'ud_id' => array('type' => 'TEXT'),
                        'token' => array('type' => 'TEXT')
        	);
        	$this->dbforge->modify_column('tbl_user_device', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_user_device');
        }
}