<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Asubject extends CI_Migration {

        public function up()
        {	
                $fields = array('user_id' => array('type' => 'INT','constraint' => '11' ),
                                'image' => array('type' => 'TEXT'));
                $this->dbforge->add_column('tbl_subject', $fields);		             
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_subject');
        }
}
