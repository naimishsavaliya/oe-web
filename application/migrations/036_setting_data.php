<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Setting_data  extends CI_Migration {

        public function up()
        {
            $data = array(
                'email_template_id'          =>1,
                'setting_name'       =>'Forgot Password Token.',
                'setting_value'   =>'Forgot Password.',
                'setting_code'    =>'forgot_password',
                'created_at'    =>'2018-07-06 00:00:00'
            );

            $this->db->insert('tbl_setting',$data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_setting');
        }
}