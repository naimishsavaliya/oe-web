<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Oauth_discount_amount extends CI_Migration {

        public function up()
        {
            
            $fields = array(
                            'total_discount_amount' => array(
                                'type' => 'FLOAT',
                                'null' => TRUE
                            ));
            $this->dbforge->modify_column('oauth_users', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}