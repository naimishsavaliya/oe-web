<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_exam extends CI_Migration {

        public function up()
        {
            $fields = array(
                'que_mark' => array('type' => 'INT','constraint' => '11' ),
                            'negative_mark' => array('type' => 'FLOAT','constraint' => '11'),'end_time' => array('type' => 'TIME'));
            $this->dbforge->add_column('tbl_exam', $fields);
              	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam');
        }
}