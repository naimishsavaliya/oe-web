<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Exam_start extends CI_Migration {

        public function up()
        {
            $this->dbforge->add_field(array(
                    'id' => array(
                            'type' => 'INT',
                            'constraint' => 11,
                            'unsigned' => TRUE,
                            'auto_increment' => TRUE
                    ),
                'user_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                ),
                'exam_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                ),
                'start_date' => array(
                        'type' => 'DATE',
                        'null' => TRUE
                ),
                'start_time' => array(
                        'type' => 'TIME',
                        'null' => TRUE
                ),
                'end_time' => array(
                        'type' => 'TIME',
                        'null' => TRUE
                ),
                'status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                ),
                'created_at' => array(
                        'type' => 'DATETIME'
                ),
                'updated_at' => array(
                        'type' => 'DATETIME'
                )
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('tbl_exam_start');    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam_start');
        }
}