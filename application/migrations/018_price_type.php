<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Price_type extends CI_Migration {

        public function up()
        {
        	$fields = array(
        	        'price' => array('type' => 'FLOAT','constraint' => '11')
        	);
        	$this->dbforge->modify_column('tbl_exam', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam');
        }
}