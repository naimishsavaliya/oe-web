<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Aoauth_User extends CI_Migration {

        public function up()
        {
            
            $fields = array('fullname' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '255'
                            ),
                            'ud_id' => array(
                                    'type' => 'VARCHAR',
                                     'constraint' => '255'
                            ),
                            'mobile_type' => array(
                                    'type' => 'VARCHAR',
                                     'constraint' => '255'
                            ));
            $this->dbforge->add_column('oauth_users', $fields);
            $this->dbforge->drop_column('oauth_users', 'first_name');
            $this->dbforge->drop_column('oauth_users', 'last_name');
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}