<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Delete_status extends CI_Migration {

        public function up()
        {
            $status_field="delete_status int(11) NOT NULL DEFAULT '0' COMMENT '0-Not Delete,1-Delete'";
            $this->dbforge->add_column('tbl_question', $status_field);  	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_question');
        }
}