<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_variable extends CI_Migration {

        public function up()
        {
            $fields = array('variable' => array('type' => 'TEXT')
                            );
            $this->dbforge->add_column('tbl_email_template', $fields);
              	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_email_template');
        }
}