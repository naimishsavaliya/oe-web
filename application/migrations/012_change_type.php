<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_type extends CI_Migration {

        public function up()
        {
        	$fields = array(
        	        'answer_type' => array('type' => 'VARCHAR','constraint' => '255')
        	);
        	$this->dbforge->modify_column('tbl_answer', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_answer');
        }
}