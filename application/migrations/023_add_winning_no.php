<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_winning_no extends CI_Migration {

        public function up()
        {
            $fields = array(
                'winning_no' => array('type' => 'INT','constraint' => '11',
                'comment' => '0-Fail' ));
            $this->dbforge->add_column('tbl_exam_result', $fields);
              	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam_result');
        }
}