<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Remove_username extends CI_Migration {

        public function up()
        {
            $this->dbforge->drop_column('oauth_users', 'username');
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}