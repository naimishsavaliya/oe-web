<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Notification  extends CI_Migration {

        public function up()
        {
            $this->dbforge->add_field(array(
                    'id' => array(
                            'type' => 'INT',
                            'constraint' => 11,
                            'unsigned' => TRUE,
                            'auto_increment' => TRUE
                    ),
                'users' => array(
                        'type' => 'TEXT'
                ),
                'message' => array(
                        'type' => 'TEXT'
                ),
                'subject' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '255'
                ),
                'notification_type' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'COMMENT'=>'1-Push notification , 2-Email'
                ),
                'notification_date' => array(
                        'type' => 'DATETIME'
                ),
                'created_at' => array(
                        'type' => 'DATETIME'
                ),
                'updated_at' => array(
                        'type' => 'DATETIME'
                ),
                'deleted_at' => array(
                        'type' => 'DATETIME'
                )
            ));
            $status_field="status int(11) NOT NULL DEFAULT '0' COMMENT '0-Pending,1-Send'";
            $this->dbforge->add_field($status_field);
            $delete_field="delete_status int(11) NOT NULL DEFAULT '0' COMMENT '0-Not Delete,1-Delete'";
            $this->dbforge->add_field($delete_field);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('tbl_notification');    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_notification');
        }
}