<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_level_name extends CI_Migration {

        public function up()
        {
            $fields = array('level_name' => array('type' => 'VARCHAR', 'constraint' => '255')
                            );
            $this->dbforge->add_column('tbl_user_level', $fields);
              	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_user_level');
        }
}