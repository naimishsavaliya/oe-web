<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Setting_admin_userdatadata  extends CI_Migration {

        public function up()
        {
            $data = array(
                'email_template_id'          =>6,
                'setting_name'       =>'Admin Registration Info',
                'setting_value'   =>'User Registration Detail ',
                'setting_code'    =>'user_info',
                'created_at'    =>'2018-07-18 00:00:00'
            );

            $this->db->insert('tbl_setting',$data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_setting');
        }
}