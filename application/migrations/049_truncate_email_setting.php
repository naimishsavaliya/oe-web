<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Truncate_email_setting extends CI_Migration {

        public function up()
        {
            $this->db->truncate('tbl_email_template');
            $this->db->truncate('tbl_setting');        	  
        }

        public function down()
        {
            $this->dbforge->drop_table('tbl_email_template');
            $this->dbforge->drop_table('tbl_setting');
        }
}