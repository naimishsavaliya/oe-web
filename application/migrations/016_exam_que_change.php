<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Exam_que_change extends CI_Migration {

        public function up()
        {
            
            $fields = array('question' => array(
                                     'type' => 'TEXT'
                            ),
                            'correct_answer' => array(
                                    'type' => 'TEXT'
                            ),
                            'answer_type' => array(
                                   'type' => 'VARCHAR','constraint' => '255'
                            ),
                            'answer1' => array(
                                    'type' => 'TEXT'
                            ),
                            'answer2' => array(
                                    'type' => 'TEXT'
                            ),
                            'answer3' => array(
                                    'type' => 'TEXT'
                            ),
                            'answer4' => array(
                                    'type' => 'TEXT'
                            ));
            $this->dbforge->add_column('tbl_exam_question', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam_question');
        }
}