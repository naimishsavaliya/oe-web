<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Admin_Usertemplate_data  extends CI_Migration {

        public function up()
        {
            $data = array(
                'name'          =>'Admin Registration Info',
                'subject'       =>'User Registration Detail {{customer_name}}.',
                'description'   =>"<p>hmmmm</p>",
                'email_type'    =>2,
                'from_name'     =>'Online Exam',
                'from_email'    =>'naimish.empyreal@gmail.com',
                'created_at'    =>'2018-07-18 06:40:11',
                'variable'      =>'{{customer_name}}'
            );

            $this->db->insert('tbl_email_template',$data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_email_template');
        }
}