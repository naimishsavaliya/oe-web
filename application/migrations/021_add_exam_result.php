<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_exam_result extends CI_Migration {

        public function up()
        {
            $fields = array(
                'result' => array('type' => 'INT','constraint' => '11' ),
                            'result_date' => array( 'type' => 'DATE'));
            $this->dbforge->add_column('tbl_exam_result', $fields);
              	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam_result');
        }
}