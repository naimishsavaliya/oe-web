<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Subject extends CI_Migration {

        public function up()
        {			
        	          	$this->dbforge->add_field(array(
        	          	                        'id' => array(
        	          	                                'type' => 'INT',
        	          	                                'constraint' => 11,
        	          	                                'unsigned' => TRUE,
        	          	                                'auto_increment' => TRUE
        	          	                        ),
        	          	                        'name' => array(
        	          	                                'type' => 'VARCHAR',
        	          	                                'constraint' => '255'
        	          	                        ),
        	          	                        'created_at' => array(
        	          	                                'type' => 'DATETIME'
        	          	                        ),
        	          	                        'updated_at' => array(
        	          	                                'type' => 'DATETIME'
        	          	                        ),
        	          	                        'deleted_at' => array(
        	          	                                'type' => 'DATETIME'
        	          	                        )
        	          	                ));
        	          	$status_field="status int(11) NOT NULL DEFAULT '1' COMMENT '0-Deactive,1-Active'";
        	          	$this->dbforge->add_field($status_field);
        				$this->dbforge->add_key('id', TRUE);
        	          	$this->dbforge->create_table('tbl_subject');             
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_subject');
        }
}