<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Setting_template_data  extends CI_Migration {

        public function up()
        {
            $setting_data = array(
                        array(
                            'email_template_id'  => 1,
                            'setting_name'       =>'Forgot Password Token.',
                            'setting_value'      =>'Forgot Password.',
                            'setting_code'       =>'forgot_password',
                            'created_at'         =>'2018-07-20 06:40:11',
                        ),
                        array(
                            'email_template_id'  => 2,
                            'setting_name'       =>'Welcome User message.',
                            'setting_value'      =>'Welcome User.',
                            'setting_code'       =>'welcome_user',
                            'created_at'         =>'2018-07-20 06:40:11',
                        ),
                        array(
                            'email_template_id'  => 3,
                            'setting_name'       =>'Admin Registration Info.',
                            'setting_value'      =>'User Registration Detail',
                            'setting_code'       =>'user_info',
                            'created_at'         =>'2018-07-20 06:40:11',
                        ),
                        array(
                            'email_template_id'  => 4,
                            'setting_name'       =>'Exam book Info.',
                            'setting_value'      =>'Exam book Detail user',
                            'setting_code'       =>'examBook_admin',
                            'created_at'         =>'2018-07-20 06:40:11',
                        ),
                        array(
                            'email_template_id'  => 5,
                            'setting_name'       =>'Registration Exam book.',
                            'setting_value'      =>'Exam book Detail user',
                            'setting_code'       =>'examBook_customer',
                            'created_at'         =>'2018-07-20 06:40:11',
                        ),
                        array(
                            'email_template_id'  => 6,
                            'setting_name'       =>'Exam due date.',
                            'setting_value'      =>'Exam due date information',
                            'setting_code'       =>'exam_duedate',
                            'created_at'         =>'2018-07-20 06:40:11',
                        )
                    );
            $this->db->insert_batch('tbl_setting',$setting_data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_setting');
        }
}