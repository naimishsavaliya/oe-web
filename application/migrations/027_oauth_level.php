<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Oauth_level extends CI_Migration {

        public function up()
        {
            
            $fields = array('current_level_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE
                            ),
                            'total_discount_amount' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE
                            ),
                            'level_exam_completed_count' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE
                            ),
                            'next_level_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE
                            ));
            $this->dbforge->add_column('oauth_users', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}