<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_forgot_token extends CI_Migration {

        public function up()
        {
            $fields = array('forgot_token' => array('type' => 'VARCHAR', 'constraint' => '50'),
                            'forgot_token_date' => array('type' => 'DATETIME')
                            );
            $this->dbforge->add_column('oauth_users', $fields);
              	  
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}