<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_User_birthdate extends CI_Migration {

        public function up()
        {
        	$fields = array(
        	        'birthdate' => array('type' => 'DATE')
        	);
        	$this->dbforge->modify_column('oauth_users', $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}