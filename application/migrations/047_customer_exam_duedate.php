<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Customer_exam_duedate  extends CI_Migration {

        public function up()
        {
            $data = array(
                'name'          =>'Exam due date',
                'subject'       =>'Exam due date information.',
                'description'   =>"<p>Hello,&nbsp;{{customer_name}}</p>",
                'email_type'    =>1,
                'from_name'     =>'Online Exam',
                'from_email'    =>'onlineexam@gmail.com',
                'created_at'    =>'2018-07-18 06:40:11',
                'variable'      =>'{{customer_name}}'
            );

            $this->db->insert('tbl_email_template',$data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_email_template');
        }
}