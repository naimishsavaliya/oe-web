<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Setting_welcomedata  extends CI_Migration {

        public function up()
        {
            $data = array(
                'email_template_id'          =>4,
                'setting_name'       =>'Welcome User message.',
                'setting_value'   =>'Welcome User.',
                'setting_code'    =>'welcome_user',
                'created_at'    =>'2018-07-18 00:00:00'
            );

            $this->db->insert('tbl_setting',$data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_setting');
        }
}