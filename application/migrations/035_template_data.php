<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Template_data  extends CI_Migration {

        public function up()
        {
            $data = array(
                'name'          =>'Forgot Password',
                'subject'       =>'{{customer_name}} Forgot Password Token.',
                'description'   =>"<p>Hello {{customer_name}},</p>
                                    <p>{{token}}</p>
                                    <p>{{customer_email}}</p>",
                'email_type'    =>1,
                'from_name'     =>'Online Exam',
                'from_email'    =>'onlineexam@gmail.com',
                'created_at'    =>'2018-07-06 00:00:00'
            );

            $this->db->insert('tbl_email_template',$data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_email_template');
        }
}