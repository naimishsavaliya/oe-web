<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Setting_admin_exambook  extends CI_Migration {

        public function up()
        {
            $data = array(
                'email_template_id'          =>8,
                'setting_name'       =>'Exam book Info',
                'setting_value'   =>'Exam book Detail user',
                'setting_code'    =>'examBook_admin',
                'created_at'    =>'2018-07-18 00:00:00'
            );

            $this->db->insert('tbl_setting',$data);    
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_setting');
        }
}