<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_winning_no_date extends CI_Migration {

        public function up()
        {
            $fields = array(
                'winning_no_date' => array('type' => 'DATETIME'));
            $this->dbforge->add_column('tbl_exam_result', $fields);

            $modify_field = array('result_date' => array('type' => 'DATETIME' ));
            $this->dbforge->modify_column('tbl_exam_result', $modify_field);
              	  
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_exam_result');
        }
}