<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Oauth_User extends CI_Migration {

        public function up()
        {
            
            $fields = array('id' => array('type' => 'INT',
                                            'constraint' => 11,
                                            'unsigned' => TRUE
                                    ),
                            'email' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '255'
                            ),
                            'mobile' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '255'
                            ),
                            'birthdate' => array(
                                    'type' => 'DATETIME'
                            ),
                            'standard' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '255'
                            ),
                            'school' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '255'
                            ),
                            'profile_pic' => array(
                                    'type' => 'TEXT'
                            ),
                            'created_at' => array(
                                    'type' => 'DATETIME'
                            ),
                            'updated_at' => array(
                                    'type' => 'DATETIME'
                            ),
                            'deleted_at' => array(
                                    'type' => 'DATETIME'
                            )
                        );
            $this->dbforge->add_column('oauth_users', $fields);
            $this->db->query('ALTER TABLE `oauth_users` ADD UNIQUE INDEX (`id`)');
            $this->db->query('ALTER TABLE `oauth_users` MODIFY COLUMN `id` INT auto_increment');
            $status_field="status int(11) NOT NULL DEFAULT '1' COMMENT '0-Deactive,1-Active'";
            $this->dbforge->add_column('oauth_users', $status_field);
            
        }

        public function down()
        {
                $this->dbforge->drop_table('oauth_users');
        }
}