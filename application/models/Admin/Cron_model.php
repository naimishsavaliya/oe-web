<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_model extends CI_Model{
    private $table_name = 'tbl_exam';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */

    function get_exam_data($day){
        $date=date('Y-m-d',$day);
        $this->db->select('id,start_date');
        $this->db->from('tbl_exam');
        $this->db->where('delete_status',0);
        $this->db->where('start_date',$date);
        $query=$this->db->get();
        return $query->result_array();
    }
    function get_ExamUser_data($exam_id){
        $this->db->select('exam_book.id as id,exam_book.user_id as user_id,exam_book.exam_id as exam_id,user.fullname as fullname,user.email as email,user.mobile as mobile,exam.subject_id as subject_id,exam.standard_id as standard_id,exam.exam_title as exam_title,exam.start_date as start_date,standard.name as standard_name,subject.name as subject_name');
        $this->db->from('tbl_exam_book as exam_book');
        $this->db->join('tbl_exam as exam','exam.id=exam_book.exam_id','Left');
        $this->db->join('oauth_users as user','user.id=exam_book.user_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->order_by('exam_book.exam_id', 'DESC');
        $this->db->where('exam_book.delete_status',0);
        $this->db->where('exam.delete_status',0);
        if($exam_id)
            $this->db->where_in('exam_book.exam_id',$exam_id);
        $query=$this->db->get();
        return $query->result_array();
    }
    public function get_template_data($where=''){
        $this->db->select('main.id as setting_id,
            main.email_template_id,
            main.setting_name,
            main.setting_value,
            main.setting_code,
            sub.name as name,
            sub.subject as subject,
            sub.description as description,
            sub.email_type as email_type,
            sub.from_name as from_name,
            sub.from_email as from_email,
            sub.variable as variable');
        $this->db->from('tbl_setting as main');
        $this->db->join('tbl_email_template as sub','sub.id=main.email_template_id','Left');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows() > 0) {
            return $query->row();
        }else{
            return false;
        }
    }
   function get_notification_data($where=''){
        $this->db->select('*');
        $this->db->from('tbl_notification');
        $this->db->order_by('id', 'DESC');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->result_array();
    } 
    function notification_users($users=''){
        $users = explode(',', $users);
        $field='id';
        $asc='DESC';
        $this->db->select('id,fullname,email,mobile');
        $this->db->from('oauth_users');
        $this->db->order_by($field, $asc);
        if($users)
            $this->db->where_in('id',$users);
        $query=$this->db->get();
        return $query->result_array();
    }
    function update($records,$notification_id){
        $this->db->where('id', $notification_id);
        $this->db->update('tbl_notification',$records);
        return true;       
    } 

}

?>