<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	private $table_name = 'oauth_users';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */


    function list_users($generalSearch=''){
        $where=' ( email LIKE "%'.$generalSearch.'%" OR fullname LIKE "%'.$generalSearch.'%" OR mobile LIKE "%'.$generalSearch.'%" OR birthdate LIKE "%'.$generalSearch.'%" OR standard LIKE "%'.$generalSearch.'%" OR school LIKE "%'.$generalSearch.'%" ) ';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $field='id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('*');
        if($generalSearch)
            $this->db->where($where);
        $this->db->order_by($field, $asc);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $raw = $this->db->get($this->table_name);
        $rs = $raw->result_array();
        //echo '<br>====>'.$this->db->last_query(); exit();
        return $rs;
    }
    function total_record($generalSearch='')
      {
        $search_where=' ( email LIKE "%'.$generalSearch.'%" OR fullname LIKE "%'.$generalSearch.'%" OR mobile LIKE "%'.$generalSearch.'%" OR birthdate LIKE "%'.$generalSearch.'%" OR standard LIKE "%'.$generalSearch.'%" OR school LIKE "%'.$generalSearch.'%" ) ';
        $this->db->select('users.id as id ');
        $this->db->from('oauth_users as users');
        if($generalSearch)
            $this->db->where($search_where);
        $query=$this->db->get();
        return $query->num_rows();
      } 
    function get_user_detail($field=array(),$val=array(),$orderby='id'){
            $this->db->select('*');
            if(!empty($field))
            {
                for($i=0;$i<count($field);$i++)
                    $this->db->where($field[$i],$val[$i]);
            }
            $rs = $this->db->get($this->table_name)->row();
            return $rs;
        }

    function update($fieldarray=array(),$wherearray=array()){
        $this->db->update($this->table_name,$fieldarray,$wherearray);           
        return true;        
    } 
    function insert($fieldarray=array()){
        $this->db->insert($this->table_name,$fieldarray);           
        return true;        
    }
    function delete($id){
        $this->db->where('id',$id)->delete($this->table_name);
        $update=array("deleted_at"=>date("Y-m-d H:i:s")); 
        $this->db->where('id',$id)->update($this->table_name,$update);
                 
        return true;        
    }
    function check_email($email)
    {
        $this->db->select('email');
        $this->db->where('email',$email);
        $query=$this->db->get($this->table_name);
        return $query->num_rows();
    }
    function check_mobile($mobile)
    {
        $this->db->select('mobile');
        $this->db->where('mobile',$mobile);
        $query=$this->db->get($this->table_name);
        return $query->num_rows();
    }
    function get_standard_data($field=array(),$val=array(),$orderby='id'){
            $this->db->select('id,name');
            if(!empty($field))
            {
                for($i=0;$i<count($field);$i++)
                    $this->db->where($field[$i],$val[$i]);
            }
            $rs = $this->db->get('tbl_standard')->result_array();
            return $rs;
    }
    function view_userdata($where='')
    {
            $this->db->select('user.id as id,
                user.fullname as fullname,
                user.email as email,
                user.mobile as mobile,
                user.birthdate as birthdate,
                user.standard as standard_id,
                user.school as school,
                user.ud_id as ud_id,
                user.mobile_type as mobile_type,
                user.current_level_id as current_level_id,
                user.total_discount_amount as total_discount_amount,
                user.level_exam_completed_count as level_exam_completed_count,
                user.next_level_id as next_level_id,
                standard.name as standard_name,
                user.created_at as created_at,
                user.updated_at as updated_at');
            $this->db->from('oauth_users as user');
            $this->db->join('tbl_standard as standard','standard.id=user.standard','Left');
            $this->db->where($where);
            $query=$this->db->get();
            return $query->row();
    }
    function view_user_devicedata($where=''){
        $this->db->select('device.id as id,
            device.user_id as user_id,
            device.ud_id as ud_id,
            device.token as token,
            device.device_name as device_name,
            device.mobile_type as mobile_type');
        $this->db->from('tbl_user_device as device');
        $this->db->where($where);
        $this->db->order_by('device.id','DESC');
        $query=$this->db->get();
        return $query->result_array();
    }

}

?>