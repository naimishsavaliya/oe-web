<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject_model extends CI_Model{
	private $table_name = 'tbl_subject';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */


    function list_subject($generalSearch=''){
        $where='( name LIKE "%'.$generalSearch.'%" OR id LIKE "%'.$generalSearch.'%" )';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $field='id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('*');
        if($generalSearch)
            $this->db->where($where);
        $this->db->order_by($field, $asc);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $raw = $this->db->get($this->table_name);
        $rs = $raw->result_array();
        return $rs;
    }
    function total_record($generalSearch='')
      {
        $search_where='( name LIKE "%'.$generalSearch.'%" OR id LIKE "%'.$generalSearch.'%" )';
        $this->db->select('subjects.id as id ');
        $this->db->from('tbl_subject as subjects');
        if($generalSearch)
            $this->db->where($search_where);
        $query=$this->db->get();
        return $query->num_rows();
      }
    function get_subject_detail($field=array(),$val=array(),$orderby='id'){
            $this->db->select('*');
            if(!empty($field))
            {
                for($i=0;$i<count($field);$i++)
                    $this->db->where($field[$i],$val[$i]);
            }
            $rs = $this->db->get($this->table_name)->row();
            return $rs;
        }

    function update($fieldarray=array(),$wherearray=array()){
        $this->db->update($this->table_name,$fieldarray,$wherearray);           
        return true;        
    } 
    function insert($fieldarray=array()){
        $this->db->insert($this->table_name,$fieldarray);           
        return true;        
    }
    function delete($id){
        $this->db->where('id',$id)->delete($this->table_name);
        $update=array("deleted_at"=>date("Y-m-d H:i:s")); 
        $this->db->where('id',$id)->update($this->table_name,$update);      
        return true;        
    } 
}

?>