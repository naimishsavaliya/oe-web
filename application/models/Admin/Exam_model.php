<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exam_model extends CI_Model{
    private $table_name = 'tbl_exam';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */

    function datatable_data($generalSearch='',$filter_where='')
      {
        $search_where='( exam.id LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR exam.discription LIKE "%'.$generalSearch.'%" OR exam.total_que LIKE "%'.$generalSearch.'%" OR exam.max_student LIKE "%'.$generalSearch.'%" OR exam.min_student LIKE "%'.$generalSearch.'%" OR exam.price LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam.created_at LIKE "%'.$generalSearch.'%" OR exam.start_date LIKE "%'.$generalSearch.'%" ) ';
        $where='exam.delete_status!="1"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
          $pagination = $this->input->get_post("pagination");
          $cur_page = $pagination['page'];
          if(isset($pagination['perpage']))
            $limit = $pagination['perpage'];
          else
            $limit = 10;
        }
        $field='exam.id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('exam.id as id,exam.standard_id as standard_id,exam.subject_id as subject_id,exam.exam_title as exam_title,exam.discription as discription,exam.total_que as total_que,
        exam.max_student as max_student,exam.min_student as min_student,exam.price as price,
        exam.start_date as start_date,exam.start_time as start_time,exam.question_time as question_time,
        exam.exam_time as exam_time,exam.que_mark as que_mark,exam.negative_mark as negative_mark,exam.end_time as end_time,exam.created_at as created_at,exam.updated_at as updated_at,
        exam.deleted_at as deleted_at,exam.status as status,exam.result_status as result_status,exam.delete_status as delete_status,standard.name as standard_name,subject.name as subject_name, IF((SELECT COUNT(id) as question_cnt FROM `tbl_exam_question` where `exam_id` = exam.id)>0,"1","0") as is_selected');
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        if($filter_where)
            $this->db->where($filter_where);
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        return $query->result_array();
    }

    // function total_record($where='',$filter_where=''){
    //     $this->db->select('exam.id as id ');
    //     $this->db->from('tbl_exam as exam');
    //     $this->db->where($where);
    //     if($filter_where)
    //         $this->db->where($filter_where);
    //     $query=$this->db->get();
    //     return $query->num_rows();
    // }

    function total_record($generalSearch='',$filter_where='')
      {
        $search_where='( exam.id LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR exam.discription LIKE "%'.$generalSearch.'%" OR exam.total_que LIKE "%'.$generalSearch.'%" OR exam.max_student LIKE "%'.$generalSearch.'%" OR exam.min_student LIKE "%'.$generalSearch.'%" OR exam.price LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam.created_at LIKE "%'.$generalSearch.'%" OR exam.start_date LIKE "%'.$generalSearch.'%" ) ';
        $where='exam.delete_status!="1"';
        $this->db->select('exam.id as id,exam.standard_id as standard_id,exam.subject_id as subject_id,exam.exam_title as exam_title,exam.discription as discription,exam.total_que as total_que,
        exam.max_student as max_student,exam.min_student as min_student,exam.price as price,
        exam.start_date as start_date,exam.start_time as start_time,exam.question_time as question_time,
        exam.exam_time as exam_time,exam.que_mark as que_mark,exam.negative_mark as negative_mark,exam.end_time as end_time,exam.created_at as created_at,exam.updated_at as updated_at,
        exam.deleted_at as deleted_at,exam.status as status,exam.result_status as result_status,exam.delete_status as delete_status,standard.name as standard_name,subject.name as subject_name, IF((SELECT COUNT(id) as question_cnt FROM `tbl_exam_question` where `exam_id` = exam.id)>0,"1","0") as is_selected');
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        if($generalSearch)
            $this->db->where($search_where);
        if($filter_where)
            $this->db->where($filter_where);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    } 

    function get_exam_data($id){
        $where='id="'.$id.'"';
        $this->db->select('*');
        $this->db->from('tbl_exam');
        $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }
    function get_subject_name($id){
        $where='id="'.$id.'"';
        $this->db->select('name as subject_name');
        $this->db->from('tbl_subject');
        $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }
    function get_standard_name($id){
        $where='id="'.$id.'"';
        $this->db->select('name as standard_name');
        $this->db->from('tbl_standard');
        $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }

    function get_standard_data($field=array(),$val=array(),$orderby='id'){
        $this->db->select('id,name');
        if(!empty($field))
        {
            for($i=0;$i<count($field);$i++)
                $this->db->where($field[$i],$val[$i]);
        }
        $rs = $this->db->get('tbl_standard')->result_array();
        return $rs;
    }
    function get_subject_data($field=array(),$val=array(),$orderby='id'){
        $this->db->select('id,name');
        if(!empty($field))
        {
            for($i=0;$i<count($field);$i++)
                $this->db->where($field[$i],$val[$i]);
        }
        $rs = $this->db->get('tbl_subject')->result_array();
        return $rs;
    }
    function insert($records){
        $this->db->insert($this->table_name,$records); 
        $insert_id = $this->db->insert_id();          
        return $insert_id;        
    }

    function update($records,$exam_id){
        $this->db->where('id', $exam_id);
        $this->db->update($this->table_name,$records);
        return true;       
    }

    function get_viewdata($id){
        $where='exam.id="'.$id.'"';
        $this->db->select('exam.id as id,exam.standard_id as standard_id,exam.subject_id as subject_id,exam.exam_title as exam_title,exam.discription as discription,exam.total_que as total_que,
        exam.max_student as max_student,exam.min_student as min_student,exam.price as price,
        exam.start_date as start_date,exam.start_time as start_time,exam.question_time as question_time,
        exam.exam_time as exam_time,exam.created_at as created_at,exam.updated_at as updated_at,
        exam.deleted_at as deleted_at,exam.que_mark as que_mark,exam.negative_mark as negative_mark,exam.end_time as end_time,exam.status as status,exam.delete_status as delete_status,standard.name as standard_name,subject.name as subject_name');
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }

    public function selected_que($where=''){
        $this->db->select('question_id');
        $this->db->from('tbl_exam_question');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->result_array();
    }

    function auto_questiondata($standard_id,$subject_id,$maximum_que){
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $question_id=$this->session->userdata('auto_question_cart');
        $where='que.subject_id="'.$subject_id.'" AND que.standard_id="'.$standard_id.'"';
        $this->db->select('que.id as id,que.standard_id as standard_id,que.subject_id as subject_id,que.question as question,
            que.correct_answer as correct_answer, que.created_at as que_created_at,que.updated_at as que_updated_at,que.deleted_at as que_deleted_at,que.status as que_status,que.delete_status,
            ans.id as ans_id,
            ans.question_id as question_id,
            ans.answer_type as answer_type,
            ans.answer1 as answer1,
            ans.answer2 as answer2 ,
            ans.answer3 as answer3,
            ans.answer4 as answer4,
            ans.created_at as ans_created_at,
            ans.updated_at as ans_updated_at,
            ans.status as ans_status,standard.name as standard_name,subject.name as subject_name');
        $this->db->from('tbl_question as que');
        $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
        $this->db->join('tbl_standard as standard','standard.id=que.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=que.subject_id','Left');
        $this->db->where($where);
        $question_id = explode(',', $question_id);
        if($question_id)
            $this->db->where_in('que.id',$question_id);
        $this->db->where('que.delete_status',0);
        $this->db->where('que.status',1);
        $this->db->order_by('que.id', 'ASC');
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        //$this->db->limit($maximum_que);
        $query=$this->db->get();
        return $query->result_array();
    }
    function total_autodata($where='',$maximum_que){
        if($where==''){
          $where='que.delete_status!="1" AND que.status="1"';
        }
        $this->db->select('que.id as id');
        $this->db->from('tbl_question as que');
        $this->db->where($where);
        $this->db->limit($maximum_que);
        $query=$this->db->get();
        return $query->num_rows();
    }

    function get_questionanswer_data($id){
         $where='que.id="'.$id.'" AND ans.question_id="'.$id.'"';
        $this->db->select('que.id as id,que.standard_id as standard_id,que.subject_id as subject_id,que.question as question,
            que.correct_answer as correct_answer,
            ans.answer_type as answer_type,
            ans.answer1 as answer1,
            ans.answer2 as answer2 ,
            ans.answer3 as answer3,
            ans.answer4 as answer4');
               $this->db->from('tbl_question as que');
               $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
               $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }

    public function check_dublicate_question($exam_id,$value){
        $where_array = '(exam_id="'.$exam_id.'" AND question_id="'.$value.'")';
        $this->db->select('id,question_id,question');
        $this->db->where($where_array);
        $query = $this->db->get('tbl_exam_question');
        return $query->row();
    }
    public function insert_exam_question($records){
        $this->db->insert('tbl_exam_question',$records);          
        return 'true';        
    }

    public function manual_data($where='',$exam_id=''){
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $is_selected = '';
        if($where==''){
          $where='que.delete_status!="1"';
        }
        if($exam_id!=''){
          $is_selected = " , IF((SELECT COUNT(*) from `tbl_exam_question` where `exam_id`='".$exam_id."' and `question_id` = que.id),'1','0') as is_selected";
        }
        $this->db->select('que.id as id,que.standard_id as standard_id,que.subject_id as subject_id,que.question as question,
            que.correct_answer as correct_answer, que.created_at as que_created_at,que.updated_at as que_updated_at,que.deleted_at as que_deleted_at,que.status as que_status,que.delete_status,
            ans.id as ans_id,
            ans.question_id as question_id,
            ans.answer_type as answer_type,
            ans.answer1 as answer1,
            ans.answer2 as answer2 ,
            ans.answer3 as answer3,
            ans.answer4 as answer4,
            ans.created_at as ans_created_at,
            ans.updated_at as ans_updated_at,
            ans.status as ans_status,standard.name as standard_name,subject.name as subject_name'.$is_selected);
        $this->db->from('tbl_question as que');
        $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
        $this->db->join('tbl_standard as standard','standard.id=que.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=que.subject_id','Left');
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        return $query->result_array();
    }
    function total_manualdata($where=''){
        if($where==''){
          $where='que.delete_status!="1"';
        }
        $this->db->select('que.id as id ');
        $this->db->from('tbl_question as que');
        $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    }

    function check_exam($where){
        $this->db->select('id,result_status');
        $this->db->where($where);
        $query=$this->db->get($this->table_name);
        return $query->row();
    }

    function result_datatable_data($generalSearch='',$id){
        $search_where=' ( exam_result.id LIKE "%'.$generalSearch.'%" OR user.fullname LIKE "%'.$generalSearch.'%" OR user.email LIKE "%'.$generalSearch.'%" OR user.mobile LIKE "%'.$generalSearch.'%") ';
        $where='exam_result.delete_status!="1" AND exam_result.exam_id="'.$id.'"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $field='exam_result.id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('exam_result.id as id,
                            exam_result.exam_id as exam_id,
                            exam_result.question_data,
                            exam_result.user_id as user_id,
                            exam_result.result_date as result_date,
                            exam_result.winning_no as rank,
                            user.fullname as fullname,
                            user.email as email,
                            user.mobile as mobile');
        $this->db->from('tbl_exam_result as exam_result');
        $this->db->join('oauth_users as user','user.id=exam_result.user_id','Left');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        // echo $this->db->last_query(); exit();
        return $query->result_array();
    }

    function result_total_record($where='',$generalSearch=''){
        $search_where=' ( exam_result.id LIKE "%'.$generalSearch.'%" OR user.fullname LIKE "%'.$generalSearch.'%" OR user.email LIKE "%'.$generalSearch.'%" OR user.mobile LIKE "%'.$generalSearch.'%") ';
        $this->db->select('exam_result.id as id,
            exam_result.exam_id as exam_id,
            exam_result.question_data,
            exam_result.user_id as user_id,
            exam_result.result_date as result_date,
            exam_result.winning_no as rank,
            user.fullname as fullname,user.email as email,user.mobile as mobile');
        $this->db->from('tbl_exam_result as exam_result');
        $this->db->join('oauth_users as user','user.id=exam_result.user_id','Left');
        $this->db->where($where);
        if($generalSearch)
            $this->db->where($search_where);
        $query=$this->db->get();
        return $query->num_rows();
    }

    function count_question($where=''){
        $this->db->select('*');
        $this->db->from('tbl_exam_question');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    } 

    function get_result($where=''){
        $this->db->select('*');
        $this->db->from('tbl_exam_question');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    }
    function check_userexam($where=''){
        $this->db->select('*');
        if($where)
            $this->db->where($where);
        $query=$this->db->get('tbl_exam_result');
        return $query->row();
    }
    function get_user_examdata($where=''){
        $this->db->select('*');
        if($where)
            $this->db->where($where);
        $query=$this->db->get('tbl_exam_result');
        return $query->row();
    }
    function get_exam_question_user($question_id='',$where='',$generalSearch=''){
        $search_where=' ( que.question_id LIKE "%'.$generalSearch.'%" OR que.question LIKE "%'.$generalSearch.'%" ) ';
        $field='que.question_id';
        $asc='DESC';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $this->db->select('que.question_id as question_id,que.question as question,que.correct_answer as correct_answer,
            que.answer_type as answer_type,
            (
                CASE 
                    WHEN `que`.`correct_answer`="answer1" THEN `que`.`answer1`
                    WHEN `que`.`correct_answer`="answer2" THEN `que`.`answer2`
                    WHEN `que`.`correct_answer`="answer3" THEN `que`.`answer3`
                    WHEN `que`.`correct_answer`="answer4" THEN `que`.`answer4`
                    ELSE NULL
                END
            ) as `c_ans`,
            que.answer1 as answer1, 
            que.answer2 as answer2,
            que.answer3 as answer3,
            que.answer4 as answer4');
        $this->db->from('tbl_exam_question as que');
        if($where)
            $this->db->where($where);
        if($generalSearch)
            $this->db->where($search_where);
        if($question_id)
            $this->db->where_in('que.question_id',$question_id);
        $this->db->order_by($field, $asc);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        return $query->result_array();
    }

    function total_record_user_question($question_id='',$where='',$generalSearch=''){
        $search_where=' ( que.question_id LIKE "%'.$generalSearch.'%" OR que.question LIKE "%'.$generalSearch.'%" ) ';
        $this->db->select('que.question_id as question_id,que.question as question,que.correct_answer as correct_answer,
            que.answer_type as answer_type,
            (
                CASE 
                    WHEN `que`.`correct_answer`="answer1" THEN `que`.`answer1`
                    WHEN `que`.`correct_answer`="answer2" THEN `que`.`answer2`
                    WHEN `que`.`correct_answer`="answer3" THEN `que`.`answer3`
                    WHEN `que`.`correct_answer`="answer4" THEN `que`.`answer4`
                    ELSE NULL
                END
            ) as `c_ans`,
            que.answer1 as answer1, 
            que.answer2 as answer2,
            que.answer3 as answer3,
            que.answer4 as answer4');
        $this->db->from('tbl_exam_question as que');
        if($where)
            $this->db->where($where);
        if($generalSearch)
            $this->db->where($search_where);
        if($question_id)
            $this->db->where_in('que.question_id',$question_id);
        $query=$this->db->get();
        return $query->num_rows();
    }
    function count_users($where=''){
        $this->db->select('*');
        $this->db->from('tbl_exam_result');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    }
    function get_rank($where=''){
        $this->db->select('winning_no');
        $this->db->from('tbl_exam_result');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row()->winning_no;
    }

    function update_user_rank($records,$where){
        $this->db->where($where);
        $this->db->update('tbl_exam_result',$records);
        return true;       
    }
    // function get_exam_result($id){
    //     $where='exam_result.delete_status!="1" AND exam_result.exam_id="'.$id.'"';
    //     $field='exam_result.id';
    //     $asc='DESC';
    //     $this->db->select('exam_result.id as id,exam_result.exam_id as exam_id,exam_result.question_data,exam_result.user_id as user_id,user.fullname as fullname,user.email as email,user.mobile as mobile');
    //     $this->db->from('tbl_exam_result as exam_result');
    //     $this->db->join('oauth_users as user','user.id=exam_result.user_id','Left');
    //     $this->db->order_by($field, $asc);
    //     $this->db->where($where);
    //     $query=$this->db->get();
    //     return $query->result_array();
    // }
    function get_exam_result($where='',$orderby=''){
        $asc='DESC';
        $this->db->select('exam_result.id as id,exam_result.exam_id as exam_id,exam_result.question_data,exam_result.user_id as user_id,user.fullname as fullname,user.email as email,user.mobile as mobile,IFNULL(exam_result.result,0) as score');
        $this->db->from('tbl_exam_result as exam_result');
        $this->db->join('oauth_users as user','user.id=exam_result.user_id','Left');
        if($orderby)
            $this->db->order_by($orderby, $asc);
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->result_array();
    }
    function get_exam_result_score($where='',$orderby=''){
        $asc='DESC';
        $this->db->select('exam_result.id as id,exam_result.exam_id as exam_id,exam_result.user_id as user_id,user.fullname as fullname,user.email as email,user.mobile as mobile,IFNULL(exam_result.result,0) as score');
        $this->db->from('tbl_exam_result as exam_result');
        $this->db->join('oauth_users as user','user.id=exam_result.user_id','Left');
        $this->db->group_by('exam_result.result'); 
        if($orderby)
            $this->db->order_by($orderby, $asc);
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->result_array();
    }
    function update_exam_result($records,$where){
        $this->db->where($where);
        $this->db->update('tbl_exam_result',$records);
        return true;       
    }
    function get_user_data($where=''){
        $this->db->select('id as user_id,fullname,current_level_id,total_discount_amount,level_exam_completed_count,next_level_id');
        $this->db->from('oauth_users');
        if($where)
            $this->db->where($where);
        $this->db->order_by('id', 'DESC');
        $query=$this->db->get();
        return $query->row();
    }
    function get_level_data(){
        $this->db->select('level_id,level_name,amount,exam_count,parent_id');
        $this->db->from('tbl_level_master');
        $this->db->where('status',1);
        $this->db->where('delete_status',0);
        $this->db->order_by('level_id', 'ASC');
        $query=$this->db->get();
        return $query->result_array();
    }
    function user_level_update($level_records,$where){
        $this->db->where($where);
        $this->db->update('oauth_users',$level_records);
        return true;       
    }
    function change_result_status($exam_record,$where){
        $this->db->where($where);
        $this->db->update($this->table_name,$exam_record);
        return true;       
    }
    function get_auto_question($where,$maximum_que){
        $this->db->select('que.id as id');
        $this->db->from('tbl_question as que');
        $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
        $this->db->join('tbl_standard as standard','standard.id=que.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=que.subject_id','Left');
        if($where)
            $this->db->where($where);
        //$this->db->order_by('que.id', 'ASC');
        $this->db->order_by('que.id', 'RANDOM');
        $this->db->limit($maximum_que);
        $query=$this->db->get();
        return $query->result_array();
    }
    function insert_user_level($userlevel_records){
        $this->db->insert('tbl_user_level',$userlevel_records); 
        $insert_id = $this->db->insert_id();          
        return $insert_id;        
    }
    function get_level_name($where=''){
        $this->db->select('level_name');
        $this->db->from('tbl_level_master');
        if($where)
            $this->db->where($where);
        $this->db->where('status',1);
        $this->db->where('delete_status',0);
        $this->db->order_by('level_id', 'ASC');
        $query=$this->db->get();
        return $query->row();
    }
    function exambook_datatable_data($generalSearch='',$id){
        $search_where=' ( exam_book.id LIKE "%'.$generalSearch.'%" OR user.fullname LIKE "%'.$generalSearch.'%" OR user.email LIKE "%'.$generalSearch.'%" OR user.mobile LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam_book.amount LIKE "%'.$generalSearch.'%" OR exam_book.transaction_id LIKE "%'.$generalSearch.'%" OR exam_book.user_id LIKE "%'.$generalSearch.'%") ';
        $where='exam.delete_status!="1" AND exam_book.exam_id="'.$id.'"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $field='exam_book.id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('exam_book.id as id,
            exam_book.user_id as user_id,
            exam_book.exam_id as exam_id,
            exam_book.amount as amount,
            exam_book.transaction_id as transaction_id,
            user.fullname as fullname,
            user.email as email,
            user.mobile as mobile,
            exam.subject_id as subject_id,
            exam.standard_id as standard_id,
            exam.exam_title as exam_title,
            standard.name as standard_name,
            subject.name as subject_name,
            exam_book.status as status,
            exam_book.created_at as created_at,
            exam_book.updated_at as updated_at');
        $this->db->from('tbl_exam_book as exam_book');
        $this->db->join('tbl_exam as exam','exam.id=exam_book.exam_id','Left');
        $this->db->join('oauth_users as user','user.id=exam_book.user_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        if(isset($cur_page) && isset($limit) && $cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        return $query->result_array();
    }
    function exambook_totalrecord($where='',$generalSearch=''){
         $search_where=' ( exam_book.id LIKE "%'.$generalSearch.'%" OR user.fullname LIKE "%'.$generalSearch.'%" OR user.email LIKE "%'.$generalSearch.'%" OR user.mobile LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam_book.amount LIKE "%'.$generalSearch.'%" OR exam_book.transaction_id LIKE "%'.$generalSearch.'%" OR exam_book.user_id LIKE "%'.$generalSearch.'%") ';
        $this->db->select('exam_book.id as id,
            exam_book.user_id as user_id,
            exam_book.exam_id as exam_id,
            exam_book.amount as amount,
            exam_book.transaction_id as transaction_id,
            user.fullname as fullname,
            user.email as email,
            user.mobile as mobile,
            exam.subject_id as subject_id,
            exam.standard_id as standard_id,
            exam.exam_title as exam_title,
            standard.name as standard_name,
            subject.name as subject_name,
            exam_book.status as status,
            exam_book.created_at as created_at,
            exam_book.updated_at as updated_at');
        $this->db->from('tbl_exam_book as exam_book');
        $this->db->join('tbl_exam as exam','exam.id=exam_book.exam_id','Left');
        $this->db->join('oauth_users as user','user.id=exam_book.user_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    }
    function transaction_datatable_data($generalSearch='',$filter_where){
        $search_where=' ( exam_book.id LIKE "%'.$generalSearch.'%" OR user.fullname LIKE "%'.$generalSearch.'%" OR user.email LIKE "%'.$generalSearch.'%" OR user.mobile LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam_book.amount LIKE "%'.$generalSearch.'%" OR exam_book.transaction_id LIKE "%'.$generalSearch.'%" OR exam_book.user_id LIKE "%'.$generalSearch.'%") ';
        $where='exam.delete_status!="1"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $field='exam_book.id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('exam_book.id as id,
            exam_book.user_id as user_id,
            exam_book.exam_id as exam_id,
            exam_book.amount as amount,
            exam_book.transaction_id as transaction_id,
            user.fullname as fullname,
            user.email as email,
            user.mobile as mobile,
            exam.subject_id as subject_id,
            exam.standard_id as standard_id,
            exam.exam_title as exam_title,
            standard.name as standard_name,
            subject.name as subject_name,
            exam_book.status as status,
            exam_book.created_at as created_at,
            exam_book.updated_at as updated_at');
        $this->db->from('tbl_exam_book as exam_book');
        $this->db->join('tbl_exam as exam','exam.id=exam_book.exam_id','Left');
        $this->db->join('oauth_users as user','user.id=exam_book.user_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        if($filter_where)
            $this->db->where($filter_where);
        $this->db->where($where);
        if(isset($cur_page) && isset($limit) && $cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        return $query->result_array();
    }
    function transaction_total_record($where='',$generalSearch='',$filter_where){
         $search_where=' ( exam_book.id LIKE "%'.$generalSearch.'%" OR user.fullname LIKE "%'.$generalSearch.'%" OR user.email LIKE "%'.$generalSearch.'%" OR user.mobile LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam_book.amount LIKE "%'.$generalSearch.'%" OR exam_book.transaction_id LIKE "%'.$generalSearch.'%" OR exam_book.user_id LIKE "%'.$generalSearch.'%") ';
        $this->db->select('exam_book.id as id,
            exam_book.user_id as user_id,
            exam_book.exam_id as exam_id,
            exam_book.amount as amount,
            exam_book.transaction_id as transaction_id,
            user.fullname as fullname,
            user.email as email,
            user.mobile as mobile,
            exam.subject_id as subject_id,
            exam.standard_id as standard_id,
            exam.exam_title as exam_title,
            standard.name as standard_name,
            subject.name as subject_name,
            exam_book.status as status,
            exam_book.created_at as created_at,
            exam_book.updated_at as updated_at');
        $this->db->from('tbl_exam_book as exam_book');
        $this->db->join('tbl_exam as exam','exam.id=exam_book.exam_id','Left');
        $this->db->join('oauth_users as user','user.id=exam_book.user_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        if($generalSearch)
            $this->db->where($search_where);
        if($filter_where)
            $this->db->where($filter_where);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    }
    function get_exam_mode($where=''){
        $this->db->select('selection_mode');
        $this->db->from('tbl_exam_question');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }
    function total_selected_question($where=''){
        $this->db->select('count(id) as selected_que');
        $this->db->from('tbl_exam_question');
        if($where)
            $this->db->where($where);
        $this->db->where('delete_status',0);
        $query=$this->db->get();
        return $query->row();
    }  

}

?>