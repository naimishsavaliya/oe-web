<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends CI_Model{
    private $table_name = 'tbl_question';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */

    function datatable_data($where='')
      {
        if($where==''){
          $where='que.delete_status!="1"';
        }
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
          $pagination = $this->input->get_post("pagination");
          $cur_page = $pagination['page'];
          if(isset($pagination['perpage']))
            $limit = $pagination['perpage'];
          else
            $limit = 10;
        }
        $field='que.id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('que.id as id,que.standard_id as standard_id,que.subject_id as subject_id,que.question as question,
            que.correct_answer as correct_answer, que.created_at as que_created_at,que.updated_at as que_updated_at,que.deleted_at as que_deleted_at,que.status as que_status,que.delete_status,
            ans.id as ans_id,
            ans.question_id as question_id,
            ans.answer_type as answer_type,
            ans.answer1 as answer1,
            ans.answer2 as answer2 ,
            ans.answer3 as answer3,
            ans.answer4 as answer4,
            ans.created_at as ans_created_at,
            ans.updated_at as ans_updated_at,
            ans.status as ans_status,standard.name as standard_name,subject.name as subject_name');
        $this->db->from('tbl_question as que');
        $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
        $this->db->join('tbl_standard as standard','standard.id=que.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=que.subject_id','Left');
        $this->db->order_by($field, $asc);
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        return $query->result_array();
      }

      function total_record($generalSearch='')
        {
          $this->db->select('que.id as id,que.standard_id as standard_id,que.subject_id as subject_id,que.question as question,
            que.correct_answer as correct_answer, que.created_at as que_created_at,que.updated_at as que_updated_at,que.deleted_at as que_deleted_at,que.status as que_status,que.delete_status,
            ans.id as ans_id,
            ans.question_id as question_id,
            ans.answer_type as answer_type,
            ans.answer1 as answer1,
            ans.answer2 as answer2 ,
            ans.answer3 as answer3,
            ans.answer4 as answer4,
            ans.created_at as ans_created_at,
            ans.updated_at as ans_updated_at,
            ans.status as ans_status,standard.name as standard_name,subject.name as subject_name');
        $this->db->from('tbl_question as que');
        $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
        $this->db->join('tbl_standard as standard','standard.id=que.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=que.subject_id','Left');
        if($generalSearch)
          $this->db->where($generalSearch);
        $this->db->where('que.delete_status',0);
        $query=$this->db->get();
        return $query->num_rows();
        } 

    function get_questionanswer_data($id)
      {
         $where='que.id="'.$id.'" AND ans.question_id="'.$id.'"';
        $this->db->select('que.id as id,que.standard_id as standard_id,que.subject_id as subject_id,que.question as question,
                   que.correct_answer as correct_answer,ans.id as ans_id,ans.question_id as question_id,ans.answer_type as answer_type,ans.answer1 as answer1,ans.answer2 as answer2,ans.answer3 as answer3,ans.answer4 as answer4');
               $this->db->from('tbl_question as que');
               $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
               $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
      }

    function get_standard_data($field=array(),$val=array(),$orderby='id'){
            $this->db->select('id,name');
            if(!empty($field))
            {
                for($i=0;$i<count($field);$i++)
                    $this->db->where($field[$i],$val[$i]);
            }
            $rs = $this->db->get('tbl_standard')->result_array();
            return $rs;
        }
    function get_subject_data($field=array(),$val=array(),$orderby='id'){
            $this->db->select('id,name');
            if(!empty($field))
            {
                for($i=0;$i<count($field);$i++)
                    $this->db->where($field[$i],$val[$i]);
            }
            $rs = $this->db->get('tbl_subject')->result_array();
            return $rs;
        }
    function insert($question_records){
        $this->db->insert($this->table_name,$question_records); 
        $insert_id = $this->db->insert_id();          
        return $insert_id;        
    }
    function answer_insert($answer_records){
        $this->db->insert('tbl_answer',$answer_records); 
        $insert_id = $this->db->insert_id();          
        return $insert_id;        
    }

    function update($que_records,$question_id){
        $this->db->where('id', $question_id);
        $this->db->update($this->table_name,$que_records);
        return true;       
    }
    function answer_update($answer_records,$question_id){
        $this->db->where('question_id', $question_id);
        $this->db->update('tbl_answer',$answer_records);
        return true;       
    }
    function get_viewdata($id)
      {
         $where='que.id="'.$id.'" AND ans.question_id="'.$id.'"';
        $this->db->select('que.id as id,que.standard_id as standard_id,que.subject_id as subject_id,que.question as question,
            que.correct_answer as correct_answer, que.created_at as que_created_at,que.updated_at as que_updated_at,que.deleted_at as que_deleted_at,que.status as que_status,
            ans.id as ans_id,
            ans.question_id as question_id,
            ans.answer_type as answer_type,
            ans.answer1 as answer1,
            ans.answer2 as answer2 ,
            ans.answer3 as answer3,
            ans.answer4 as answer4,
            ans.created_at as ans_created_at,
            ans.updated_at as ans_updated_at,
            ans.status as ans_status,standard.name as standard_name,subject.name as subject_name');
               $this->db->from('tbl_question as que');
               $this->db->join('tbl_answer as ans','ans.question_id=que.id','Left');
               $this->db->join('tbl_standard as standard','standard.id=que.standard_id','Left');
               $this->db->join('tbl_subject as subject','subject.id=que.subject_id','Left');
               $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
      }  
    

}

?>