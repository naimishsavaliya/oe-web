<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Level_model extends CI_Model{
    private $table_name = 'tbl_level_master';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */

    function datatable_data($generalSearch=''){
        $search_where='(main.level_id LIKE "%'.$generalSearch.'%" OR main.level_name LIKE "%'.$generalSearch.'%" OR main.amount LIKE "%'.$generalSearch.'%" OR main.exam_count LIKE "%'.$generalSearch.'%" OR main.created_at LIKE "%'.$generalSearch.'%" OR main.updated_at LIKE "%'.$generalSearch.'%" OR sub.level_name LIKE "%'.$generalSearch.'%")';
        $where='main.delete_status!="1"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
            $limit = 10;
        }
        $field='level_id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('main.level_id,main.level_name,main.amount,main.exam_count,sub.level_name as parent_level,main.status,main.created_at,main.updated_at');
        $this->db->from('tbl_level_master as main');
        $this->db->join('tbl_level_master as sub','sub.level_id=main.parent_id','Left');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
            if($cur_page && $limit){
                if($cur_page=='1'){
                    $start = '0';
                }else{
                    $start = (($cur_page-1) * $limit);
                }
                $this->db->limit($limit, $start);
            }
            $query=$this->db->get();
            return $query->result_array();
    }

    function total_record($where='',$generalSearch='')
    {
        $search_where='(main.level_id LIKE "%'.$generalSearch.'%" OR main.level_name LIKE "%'.$generalSearch.'%" OR main.amount LIKE "%'.$generalSearch.'%" OR main.exam_count LIKE "%'.$generalSearch.'%" OR main.created_at LIKE "%'.$generalSearch.'%" OR main.updated_at LIKE "%'.$generalSearch.'%" OR sub.level_name LIKE "%'.$generalSearch.'%")';
        $this->db->select('main.level_id,main.level_name,main.amount,main.exam_count,sub.level_name as parent_level,main.status,main.created_at,main.updated_at');
        $this->db->from('tbl_level_master as main');
        $this->db->join('tbl_level_master as sub','sub.level_id=main.parent_id','Left');
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    }
    function get_parent_data($where='')
    {
        $this->db->select('level_id,level_name');
        $this->db->from('tbl_level_master');
        $this->db->where($where);
        $query=$this->db->get();
        return $query->result_array();
    } 
    function insert($records){
        $this->db->insert($this->table_name,$records); 
        $insert_id = $this->db->insert_id();          
        return $insert_id;        
    }

    function get_level_data($where='')
    {
        $this->db->select('*');
        $this->db->from('tbl_level_master');
        $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    } 

    function update($records,$level_id){
        $this->db->where('level_id', $level_id);
        $this->db->update($this->table_name,$records);
        return true;       
    } 

}

?>