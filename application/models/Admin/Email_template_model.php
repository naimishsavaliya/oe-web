<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_template_model extends CI_Model{
    private $table_name = 'tbl_email_template';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */

    function datatable_data($generalSearch=''){
        $search_where='(id LIKE "%'.$generalSearch.'%" OR name LIKE "%'.$generalSearch.'%" OR subject LIKE "%'.$generalSearch.'%" OR description LIKE "%'.$generalSearch.'%" OR from_name LIKE "%'.$generalSearch.'%" OR from_email LIKE "%'.$generalSearch.'%")';
        $where='delete_status!="1"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
            $limit = 10;
        }
        $field='id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('*');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get($this->table_name);
        // echo '<br/>===>'; 
        // echo $this->db->last_query(); exit();
        return $query->result_array();
    }

    function total_record($where='',$generalSearch='')
    {
        $search_where='(id LIKE "%'.$generalSearch.'%" OR name LIKE "%'.$generalSearch.'%" OR subject LIKE "%'.$generalSearch.'%" OR description LIKE "%'.$generalSearch.'%" OR from_name LIKE "%'.$generalSearch.'%" OR from_email LIKE "%'.$generalSearch.'%")';
        $this->db->select('*');
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        $query=$this->db->get($this->table_name);
        return $query->num_rows();
    }

    function get_template_data($where='')
    {
        $this->db->select('*');
        $this->db->from('tbl_email_template');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }

    function insert($records){
        $this->db->insert($this->table_name,$records); 
        $insert_id = $this->db->insert_id();          
        return $insert_id;        
    } 

    function update($records,$template_id){
        $this->db->where('id', $template_id);
        $this->db->update($this->table_name,$records);
        return true;       
    }
    function fetch_all_template()
    {
        $this->db->select('id,name,subject');
        $this->db->from('tbl_email_template');
        $this->db->where('delete_status',0);
        $query=$this->db->get();
        return $query->result_array();
    }
    function setting_dataTable_data($generalSearch=''){
        $search_where='( setting.id LIKE "%'.$generalSearch.'%" OR setting.setting_name LIKE "%'.$generalSearch.'%" OR setting.setting_value LIKE "%'.$generalSearch.'%" OR setting.setting_code LIKE "%'.$generalSearch.'%" OR main.name LIKE "%'.$generalSearch.'%" ) ';
        $where='setting.delete_status!="1"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
            $limit = 10;
        }
        $field='setting.id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('setting.id as id,setting.email_template_id as email_template_id,setting.setting_name as setting_name,setting.setting_value as setting_value,setting.setting_code as setting_code,main.name as name,setting.created_at as created_at,
        setting.updated_at as updated_at,setting.status as status');
        $this->db->from('tbl_setting as setting');
        $this->db->join('tbl_email_template as main','main.id=setting.email_template_id','Left');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        // echo '<br/>===>'; 
        // echo $this->db->last_query(); exit();
        return $query->result_array();
    }

    function setting_total_record($where='',$generalSearch='')
    {
        $search_where='( setting.id LIKE "%'.$generalSearch.'%" OR setting.setting_name LIKE "%'.$generalSearch.'%" OR setting.setting_value LIKE "%'.$generalSearch.'%" OR setting.setting_code LIKE "%'.$generalSearch.'%" OR main.name LIKE "%'.$generalSearch.'%" ) ';
        $this->db->select('setting.id as id,setting.email_template_id as email_template_id,setting.setting_name as setting_name,setting.setting_value as setting_value,setting.setting_code as setting_code,main.name as name,setting.created_at as created_at,
        setting.updated_at as updated_at,setting.status as status');
        $this->db->from('tbl_setting as setting');
        $this->db->join('tbl_email_template as main','main.id=setting.email_template_id','Left');
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    }
    function get_setting_data($where='')
    {
        $this->db->select('*');
        $this->db->from('tbl_setting');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }
    function update_setting($records,$setting_id){
        $this->db->where('id', $setting_id);
        $this->db->update('tbl_setting',$records);
        return true;       
    } 

}

?>