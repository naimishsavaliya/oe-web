<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model{
	private $table_name = 'oauth_users';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */

    function count_total_user(){
        $this->db->select('count(id) as total_user');
        $this->db->from('oauth_users');
        $query=$this->db->get();
        return $query->row()->total_user;
    }
    function count_total_subject(){
        $this->db->select('count(id) as total_subject');
        $this->db->from('tbl_subject');
        $query=$this->db->get();
        return $query->row()->total_subject;
    }
    function count_total_standard(){
        $this->db->select('count(id) as total_standard');
        $this->db->from('tbl_standard');
        $query=$this->db->get();
        return $query->row()->total_standard;
    }
    function count_total_exam(){
        $this->db->select('count(id) as total_exam');
        $this->db->from('tbl_exam');
        $this->db->where('delete_status',0);
        $query=$this->db->get();
        return $query->row()->total_exam;
    }
    function count_total_upcoming_exam(){
        $date=date("Y-m-d");
        $filter_where='start_date >"'.$date.'"';
        $this->db->select('count(id) as total_upcoming_exam');
        $this->db->from('tbl_exam');
        $this->db->where($filter_where);
        $this->db->where('delete_status',0);
        $query=$this->db->get();
        return $query->row()->total_upcoming_exam;
    }
    function count_total_pending_result($where=''){
        $this->db->select('count(id) as total_pending_result');
        $this->db->from('tbl_exam');
        $this->db->where('result_status',0);
        $this->db->where('delete_status',0);
        $query=$this->db->get();
        return $query->row()->total_pending_result;
    }
    function datatable_data($generalSearch='',$filter_where=''){
        $search_where='( exam.id LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR exam.discription LIKE "%'.$generalSearch.'%" OR exam.total_que LIKE "%'.$generalSearch.'%" OR exam.max_student LIKE "%'.$generalSearch.'%" OR exam.min_student LIKE "%'.$generalSearch.'%" OR exam.price LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam.created_at LIKE "%'.$generalSearch.'%" OR exam.start_date LIKE "%'.$generalSearch.'%" ) ';
        $where='exam.delete_status!="1"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
          $pagination = $this->input->get_post("pagination");
          $cur_page = $pagination['page'];
          if(isset($pagination['perpage']))
            $limit = $pagination['perpage'];
          else
            $limit = 10;
        }
        $field='exam.id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('exam.id as id,exam.standard_id as standard_id,exam.subject_id as subject_id,exam.exam_title as exam_title,exam.discription as discription,exam.total_que as total_que,
        exam.max_student as max_student,exam.min_student as min_student,exam.price as price,
        exam.start_date as start_date,exam.start_time as start_time,exam.question_time as question_time,
        exam.exam_time as exam_time,exam.que_mark as que_mark,exam.negative_mark as negative_mark,exam.end_time as end_time,exam.created_at as created_at,exam.updated_at as updated_at,
        exam.deleted_at as deleted_at,exam.status as status,exam.result_status as result_status,exam.delete_status as delete_status,standard.name as standard_name,subject.name as subject_name, IF((SELECT COUNT(id) as question_cnt FROM `tbl_exam_question` where `exam_id` = exam.id)>0,"1","0") as is_selected');
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        if($filter_where)
            $this->db->where($filter_where);
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        // echo $this->db->last_query(); exit();
        return $query->result_array();
    }
    function total_record($generalSearch='',$filter_where=''){
        $search_where='( exam.id LIKE "%'.$generalSearch.'%" OR exam.exam_title LIKE "%'.$generalSearch.'%" OR exam.discription LIKE "%'.$generalSearch.'%" OR exam.total_que LIKE "%'.$generalSearch.'%" OR exam.max_student LIKE "%'.$generalSearch.'%" OR exam.min_student LIKE "%'.$generalSearch.'%" OR exam.price LIKE "%'.$generalSearch.'%" OR standard.name LIKE "%'.$generalSearch.'%" OR subject.name LIKE "%'.$generalSearch.'%" OR exam.created_at LIKE "%'.$generalSearch.'%" OR exam.start_date LIKE "%'.$generalSearch.'%" ) ';
        $where='exam.delete_status!="1"';
        $this->db->select('exam.id as id,exam.standard_id as standard_id,exam.subject_id as subject_id,exam.exam_title as exam_title,exam.discription as discription,exam.total_que as total_que,
        exam.max_student as max_student,exam.min_student as min_student,exam.price as price,
        exam.start_date as start_date,exam.start_time as start_time,exam.question_time as question_time,
        exam.exam_time as exam_time,exam.que_mark as que_mark,exam.negative_mark as negative_mark,exam.end_time as end_time,exam.created_at as created_at,exam.updated_at as updated_at,
        exam.deleted_at as deleted_at,exam.status as status,exam.result_status as result_status,exam.delete_status as delete_status,standard.name as standard_name,subject.name as subject_name, IF((SELECT COUNT(id) as question_cnt FROM `tbl_exam_question` where `exam_id` = exam.id)>0,"1","0") as is_selected');
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        if($generalSearch)
            $this->db->where($search_where);
        if($filter_where)
            $this->db->where($filter_where);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->num_rows();
    } 

}

?>