<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model{
    private $table_name = 'tbl_notification';
    /**
     * get_users function to fetch countries list
     * 
     * @return void
     */

    function datatable_data($generalSearch=''){
        $search_where='(id LIKE "%'.$generalSearch.'%" OR message LIKE "%'.$generalSearch.'%" OR subject LIKE "%'.$generalSearch.'%")';
        $where='delete_status!="1"';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
                $limit = 10;
        }
        $field='id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('*');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get($this->table_name);
        // echo '<br/>===>'; 
        // echo $this->db->last_query(); exit();
        return $query->result_array();
    }

    function total_record($where='',$generalSearch=''){
         $search_where='(id LIKE "%'.$generalSearch.'%" OR message LIKE "%'.$generalSearch.'%" OR subject LIKE "%'.$generalSearch.'%")';
        $this->db->select('*');
        if($generalSearch)
            $this->db->where($search_where);
        $this->db->where($where);
        $query=$this->db->get($this->table_name);
        return $query->num_rows();
    }
    function get_users_data(){
        $this->db->select('id,fullname');
        $rs = $this->db->get('oauth_users')->result_array();
        return $rs;
    }

    function insert($records){
        $this->db->insert($this->table_name,$records); 
        $insert_id = $this->db->insert_id();          
        return $insert_id;        
    }

    function get_notification_data($where=''){
        $this->db->select('*');
        $this->db->from('tbl_notification');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    } 

    function update($records,$notification_id){
        $this->db->where('id', $notification_id);
        $this->db->update($this->table_name,$records);
        return true;       
    }
    function check_notification($where){
        $this->db->select('id,users');
        if($where)
            $this->db->where($where);
        $query=$this->db->get($this->table_name);
        return $query->row();
    }
    function users_datatable_data($generalSearch='',$users=''){
        $users = explode(',', $users);
        $search_where='( id LIKE "%'.$generalSearch.'%" OR fullname LIKE "%'.$generalSearch.'%" OR email LIKE "%'.$generalSearch.'%" OR mobile LIKE "%'.$generalSearch.'%" ) ';
        $pagination = '';
        if($this->input->get_post("pagination")!=''){
            $pagination = $this->input->get_post("pagination");
            $cur_page = $pagination['page'];
            if(isset($pagination['perpage']))
                $limit = $pagination['perpage'];
            else
            $limit = 10;
        }
        $field='id';
        $asc='DESC';
        if($this->input->get_post("sort")!=''){
          $sort=$this->input->get_post("sort");
          $field=$sort['field'];
          $asc=$sort['sort'];
        }
        $this->db->select('id,fullname,email,mobile');
        $this->db->from('oauth_users');
        $this->db->order_by($field, $asc);
        if($generalSearch)
            $this->db->where($search_where);
        if($users)
            $this->db->where_in('id',$users);
        if($cur_page && $limit){
            if($cur_page=='1'){
                $start = '0';
            }
            else{
                $start = (($cur_page-1) * $limit);
            }
            $this->db->limit($limit, $start);
        }
        $query=$this->db->get();
        return $query->result_array();
    }
    function users_total_record($generalSearch='',$users=''){
        $users = explode(',', $users);
        $search_where='( id LIKE "%'.$generalSearch.'%" OR fullname LIKE "%'.$generalSearch.'%" OR email LIKE "%'.$generalSearch.'%" OR mobile LIKE "%'.$generalSearch.'%" ) ';
        $this->db->select('*');
        if($generalSearch)
            $this->db->where($search_where);
        if($users)
            $this->db->where_in('id',$users);
        $query=$this->db->get('oauth_users');
        return $query->num_rows();
    }

}

?>