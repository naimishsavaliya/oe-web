<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model
{
    CONST USER_TABLE = 'oauth_users';
    CONST SUBJECT_TABLE = 'tbl_subject';
    CONST STANDARD_TABLE = 'tbl_standard';
    CONST USER_LEVEL_TABLE = 'tbl_user_level';
    CONST LEVEL_MASTER_TABLE = 'tbl_level_master';
    CONST USER_DEVICE_TABLE = 'tbl_user_device';
    
    //public $username;
    //public $password;
    //public $fullname;
    //public $mobile;
    
    public function __construct(){
        
    }
    
    public function login($username){
    $where_array = '(email="'.$username.'" OR mobile="'.$username.'")';
    //$where_array_second = 'ud_id="'.$ud_id.'" AND mobile_type="'.$mobile_type.'"';
        $this->db->select(array('id AS user_id','fullname','mobile','email','password','profile_pic'));
        $this->db->where($where_array);
        //$this->db->where($where_array_second);
        $query = $this->db->get(self::USER_TABLE);
        //echo $this->db->last_query();
        //exit();
        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function check_duplicate($mobile,$email)
    {
        $where_array = '(email="'.$email.'" OR mobile="'.$mobile.'")';
        $this->db->select('id');
        $this->db->where($where_array);
        $query = $this->db->get(self::USER_TABLE);
        return $query->num_rows();
    }
    public function check_user($user_id){
        $this->db->select('*');
        $this->db->where('id',$user_id);
        $query = $this->db->get(self::USER_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function check_subject($subject_id){
        $this->db->select('*');
        $this->db->where('id',$subject_id);
        $query = $this->db->get(self::SUBJECT_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insert($params){
        $this->fullname     = $params['fullname'];
        $this->mobile       = $params['mobile'];
        $this->email        = $params['email'];
        $this->birthdate    = $params['birthdate'];
        $this->password     = $params['password'];
        $this->ud_id        = $params['ud_id'];
        $this->mobile_type  = $params['mobile_type'];
        $this->profile_pic  = $params['profile_pic'];
        $this->created_at   = $params['created_at'];
        $this->db->insert(self::USER_TABLE, $this);
        $insert_id = $this->db->insert_id();          
        return $insert_id; 
    }
    
    // public function update($params){
    //     //$this->username = $params['username'];
    //     $this->password = $params['password'];
    //     $this->first_name = $params['first_name'];
    //     $this->last_name = $params['last_name'];
        
    //     $this->db->insert(self::USER_TABLE, $this, array('username' => $username));
    // }
    public function list_subject($field='status'){
        $active=1;
        $src=base_url('assets/uploads/subject/');
        $this->db->select(array('id','name','CONCAT("'.$src.'", image) as image'));
        $this->db->where('status',$active);
        
        $query = $this->db->get(self::SUBJECT_TABLE);
        
        if($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function list_standard($field='status'){
        $active=1;
        $this->db->select(array('id','name'));
        $this->db->where('status',$active);
        
        $query = $this->db->get(self::STANDARD_TABLE);
        
        if($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function get_profile_data($where=''){
        $src=base_url('assets/uploads/profile/');
        $this->db->select(array('user.id as id',
            'user.fullname as fullname',
            'user.email as email',
            'IFNULL(user.mobile,0) as mobile',
            'DATE_FORMAT(user.birthdate,"%d-%m-%Y") as birthdate',
            'IFNULL(user.standard,0) as standard_id',
            'user.school as school','user.ud_id as ud_id',
            'user.mobile_type as mobile_type',
            'CONCAT("'.$src.'", user.profile_pic) as profile_pic',
            'std.name as standard_name',
            'count(result.user_id) as total_exam',
            'count(NULLIF( result.winning_no, 0 )) as total_winning',
            'IFNULL(user.current_level_id,0) as current_level_id',
            'IFNULL(user.total_discount_amount,0) as total_discount_amount',
            'IFNULL(user.level_exam_completed_count,0) as level_exam_completed_count',
            'IFNULL(user.next_level_id,0) as next_level_id',
            'IFNULL((SELECT exam_count FROM tbl_level_master as level_master WHERE level_id=IFNULL(user.next_level_id,(SELECT level_id FROM `tbl_level_master` ORDER BY `level_id` ASC limit 1))),0) as next_level_exam_count',
            'IFNULL((SELECT amount FROM tbl_level_master as level_master WHERE level_id=IFNULL(user.next_level_id,(SELECT level_id FROM `tbl_level_master` ORDER BY `level_id` ASC limit 1))),0) as next_level_exam_amount'));
        $this->db->from('oauth_users as user');
        $this->db->join('tbl_standard as std','user.standard=std.id','Left');
        $this->db->join('tbl_exam_result as result','user.id=result.user_id','Left');
        $this->db->where($where);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function update_profile($params,$user_id){
        $this->db->where('id', $user_id);
        $this->db->update(self::USER_TABLE,$params);
        return true;       
    }
    function update_pw($params,$where){
        $this->db->where($where);
        $this->db->update(self::USER_TABLE,$params);
        return true;       
    }
    public function check_email_mobile($where)
    {
        $this->db->select('id');
        $this->db->where($where);
        $query = $this->db->get(self::USER_TABLE);
        return $query->num_rows();
    }
    public function check_email($email){
        $this->db->select('*');
        $this->db->where('email',$email);
        $query = $this->db->get(self::USER_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function insert_forgot_token($params,$email){
        $this->db->where('email', $email);
        $this->db->update(self::USER_TABLE,$params);
        return true;       
    }
    public function verify_token($where=''){
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get(self::USER_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function check_level($level_id){
        $this->db->select('*');
        $this->db->where('level_id',$level_id);
        $query = $this->db->get(self::LEVEL_MASTER_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function get_user_reward($where=''){
        $this->db->select(array('id','user_id','level_id','level_name','amount','exam_count'));
        if($where)
            $this->db->where($where);
        $query = $this->db->get(self::USER_LEVEL_TABLE);
        if($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function get_level_data($where=''){
        $this->db->select(array('main.level_id','main.level_name','main.amount','main.exam_count','main.parent_id','sub.level_name as parent_level'));
        $this->db->from('tbl_level_master as main');
        $this->db->join('tbl_level_master as sub','sub.level_id=main.parent_id','Left');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows() > 0) {
            return $query->row();
        }else{
            return false;
        }
    }
    public function get_template_data($where=''){
        $this->db->select(array('main.id as setting_id','main.email_template_id','main.setting_name','main.setting_value','main.setting_code','sub.name as name','sub.subject as subject','sub.description as description','sub.email_type as email_type','sub.from_name as from_name','sub.from_email as from_email','sub.variable as variable'));
        $this->db->from('tbl_setting as main');
        $this->db->join('tbl_email_template as sub','sub.id=main.email_template_id','Left');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows() > 0) {
            return $query->row();
        }else{
            return false;
        }
    }
    public function get_user_data($where=''){
        $this->db->select(array('id AS user_id','fullname','mobile','email'));
        if($where)
            $this->db->where($where);
        $query = $this->db->get(self::USER_TABLE);
        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function insert_user_device($params){
        $this->user_id              = $params['user_id'];
        $this->ud_id                = $params['ud_id'];
        $this->token                = $params['token'];
        $this->device_name          = $params['device_name'];
        $this->mobile_type          = $params['mobile_type'];
        $this->created_at           = $params['created_at'];
        $this->db->insert(self::USER_DEVICE_TABLE, $this);
        $insert_id = $this->db->insert_id();          
        return $insert_id; 
    }
    public function check_user_device($where){
        $this->db->select('*');
        if($where)
            $this->db->where($where);
        $query = $this->db->get(self::USER_DEVICE_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function user_device_delete($where){
        $deleted_at = date("Y-m-d H:i:s");
        $delete_status = '1';
        $update=array("deleted_at"=>$deleted_at,"delete_status"=>$delete_status);
        if($where)
            $this->db->where($where);
        $this->db->update(self::USER_DEVICE_TABLE,$update);
        return true;       
    }
    public function update_device_token($params,$where){
        if($where)
            $this->db->where($where);
        $this->db->update(self::USER_DEVICE_TABLE,$params);
        return true;       
    }
    
}