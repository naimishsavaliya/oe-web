<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam_Model extends CI_Model
{
    CONST USER_TABLE = 'oauth_users';
    CONST EXAM_TABLE = 'tbl_exam';
    CONST EXAM_QUESTION_TABLE = 'tbl_exam_question';
    CONST EXAM_RESULT_TABLE = 'tbl_exam_result';
    CONST EXAM_BOOK_TABLE = ' tbl_exam_book';
    CONST EXAM_START_TABLE = ' tbl_exam_start';

    public function __construct(){
        
    }
    public function exam_list($subject_id,$standard_id,$user_id){
        $active=1;
        $delete=0;
        $date=date("Y-m-d");
        $src=base_url('assets/uploads/subject/');
        $where='exam.subject_id="'.$subject_id.'" AND exam.standard_id="'.$standard_id.'" AND exam.start_date >"'.$date.'"';
        $this->db->select(array('exam.id as id','exam.standard_id as standard_id','exam.subject_id as subject_id','exam.exam_title as exam_title','exam.discription as discription','exam.que_mark as que_mark','exam.negative_mark as negative_mark','exam.total_que as total_question','exam.max_student as max_student','exam.min_student as min_student','exam.price as price','DATE_FORMAT(exam.start_date,"%d-%m-%Y") as start_date','exam.start_time as start_time','exam.end_time as end_time','exam.question_time as question_time','exam.exam_time as exam_time','exam.result_status as result_status','standard.name as standard_name','subject.name as subject_name','CONCAT("'.$src.'", subject.image) as subject_image','exam.created_at as exam_created_at','IF((SELECT count(id) FROM tbl_exam_book where exam_id = exam.id AND user_id="'.$user_id.'")>0,"1","0") as exam_book_status'));
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->order_by('exam.start_date', 'DESC');
        $this->db->where('exam.status',$active);
        $this->db->where('exam.delete_status',$delete);
        $this->db->where($where);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $data=$query->result_array();
            foreach ($data as $key => $value) {
                $data[$key]['discription'] = strip_tags($value['discription']);
                $data[$key]['discription'] = trim(($data[$key]['discription']));
                $date = new DateTime($data[$key]['exam_created_at'], new DateTimeZone('UTC'));
                $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                $data[$key]['exam_created_at'] = $date->format('d-m-Y H:i:s');
            }
            return $data;
        } else {
            return false;
        }
    }
    public function question_list($exam_id){
        $active=1;
        $delete=0;
        $where='exam_id="'.$exam_id.'"';
        $this->db->select(array('id','exam_id','subject_id','standard_id','question_id','question','answer_type as type','answer1','answer2','answer3','answer4'));
        $this->db->where('status',$active);
        $this->db->where('delete_status',$delete);
        $this->db->where($where);
        $query = $this->db->get(self::EXAM_QUESTION_TABLE);
        if($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function check_user($user_id){
        $this->db->select('*');
        $this->db->where('id',$user_id);
        $query = $this->db->get(self::USER_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function check_exam($exam_id){
        $this->db->select('*');
        $this->db->where('exam_id',$exam_id);
        $query = $this->db->get(self::EXAM_QUESTION_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function check_question($question_id){
        $this->db->select('*');
        $this->db->where('question_id',$question_id);
        $query = $this->db->get(self::EXAM_QUESTION_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insert($params){
        $this->user_id        = $params['user_id'];
        $this->exam_id        = $params['exam_id'];
        $this->question_data  = $params['question_data'];
        $this->created_at     = $params['created_at'];
        $this->db->insert(self::EXAM_RESULT_TABLE, $this);
    }

    public function check_exam_id($exam_id){
        $this->db->select('*');
        $this->db->where('id',$exam_id);
        $query = $this->db->get(self::EXAM_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insert_exambook($params){
        $this->user_id              = $params['user_id'];
        $this->exam_id              = $params['exam_id'];
        $this->amount               = $params['amount'];
        $this->transaction_id       = $params['transaction_id'];
        $this->transaction_detail   = $params['transaction_detail'];
        $this->created_at           = $params['created_at'];
        $this->db->insert(self::EXAM_BOOK_TABLE, $this);
        $insert_id = $this->db->insert_id();          
        return $insert_id; 
    }
    // public function upcoming_exam_list($user_id){
    //     date_default_timezone_set('Asia/Kolkata');
    //     $active=1;
    //     $delete=0;
    //     $date=date("Y-m-d");
    //     $time=date("H:i:s");
    //     $day=date('Y-m-d',strtotime("+1 days"));
    //     $src=base_url('assets/uploads/subject/');
    //     $where='exam_book.user_id="'.$user_id.'" AND exam.start_date >="'.$date.'"';
    //     $this->db->select(array('exam.id as id','exam.standard_id as standard_id','exam.subject_id as subject_id','exam.exam_title as exam_title','exam.discription as discription','exam.que_mark as que_mark','exam.negative_mark as negative_mark','exam.total_que as total_question','exam.max_student as max_student','exam.min_student as min_student','exam.price as price','DATE_FORMAT(exam.start_date,"%d-%m-%Y") as start_date','exam.start_time as start_time','exam.end_time as end_time','exam.question_time as question_time','exam.exam_time as exam_time','exam.result_status as result_status','exam_book.id as book_id','exam_book.user_id as user_id','exam_book.exam_id as exam_id','exam_book.amount as amount','exam_book.transaction_id as transaction_id','exam_book.transaction_detail as transaction_detail','standard.name as standard_name','subject.name as subject_name','CONCAT("'.$src.'", subject.image) as subject_image','exam.created_at as exam_created_at','exam_book.created_at as book_date','IF(exam.start_date="'.$day.'" OR exam.start_date="'.$date.'", 1, 0) as download_status','IF(exam.start_date="'.$date.'" AND exam.start_time<="'.$time.'" AND exam.end_time>="'.$time.'", 1, 0) as exam_status','IF((SELECT count(id) FROM tbl_exam_start where exam_id = exam.id AND user_id="'.$user_id.'")>0,"1","0") as exam_start'));
    //     $this->db->from('tbl_exam as exam');
    //     $this->db->join('tbl_exam_book as exam_book','exam.id=exam_book.exam_id','Left');
    //     $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
    //     $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
    //     $this->db->order_by('exam.start_date', 'DESC');
    //     $this->db->where('exam.status',$active);
    //     $this->db->where('exam.delete_status',$delete);
    //     $this->db->where($where); 
    //     $query = $this->db->get();
    //     if($query->num_rows() > 0) {
    //         $data=$query->result_array();
    //         foreach ($data as $key => $value) {
    //             $data[$key]['discription'] = strip_tags($value['discription']);
    //             $data[$key]['discription'] = trim(($data[$key]['discription']));
    //             $date = new DateTime($data[$key]['exam_created_at'], new DateTimeZone('UTC'));
    //             $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
    //             $data[$key]['exam_created_at'] = $date->format('d-m-Y H:i:s');
    //             $date_book_date = new DateTime($data[$key]['book_date'], new DateTimeZone('UTC'));
    //             $date_book_date->setTimezone(new DateTimeZone('Asia/Kolkata'));
    //             $data[$key]['book_date'] = $date_book_date->format('d-m-Y H:i:s');
    //             $data[$key]['download_message'] = 'you can download exam question paper.';
    //         }
    //         return $data;
    //     } else {
    //         return false;
    //     }
    // }

    public function upcoming_exam_list($user_id){
        date_default_timezone_set('Asia/Kolkata');
        $active=1;
        $delete=0;
        $date=date("Y-m-d");
        $time=date("H:i:s");
        $day=date('Y-m-d',strtotime("+1 days"));
        $src=base_url('assets/uploads/subject/');
        $where='exam_book.user_id="'.$user_id.'" AND exam.start_date >="'.$date.'"';
        $startwhere='(select tbl_exam_start.exam_id from tbl_exam_start where tbl_exam_start.user_id='.$user_id.' and tbl_exam_start.exam_id=exam.id)';
        $this->db->select(array('exam.id as id','exam.standard_id as standard_id','exam.subject_id as subject_id','exam.exam_title as exam_title','exam.discription as discription','exam.que_mark as que_mark','exam.negative_mark as negative_mark','exam.total_que as total_question','exam.max_student as max_student','exam.min_student as min_student','exam.price as price','DATE_FORMAT(exam.start_date,"%d-%m-%Y") as start_date','exam.start_time as start_time','exam.end_time as end_time','exam.question_time as question_time','exam.exam_time as exam_time','exam.result_status as result_status','exam_book.id as book_id','exam_book.user_id as user_id','exam_book.exam_id as exam_id','exam_book.amount as amount','exam_book.transaction_id as transaction_id','exam_book.transaction_detail as transaction_detail','standard.name as standard_name','subject.name as subject_name','CONCAT("'.$src.'", subject.image) as subject_image','exam.created_at as exam_created_at','exam_book.created_at as book_date','IF(exam.start_date="'.$day.'" OR exam.start_date="'.$date.'", 1, 0) as download_status','IF(exam.start_date="'.$date.'" AND exam.start_time<="'.$time.'" AND exam.end_time>="'.$time.'", 1, 0) as exam_status','IF((SELECT count(id) FROM tbl_exam_start where exam_id = exam.id AND user_id="'.$user_id.'")>0,"1","0") as exam_start'));
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_exam_book as exam_book','exam.id=exam_book.exam_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->order_by('exam.start_date', 'DESC');
        $this->db->where('exam.status',$active);
        $this->db->where('exam.delete_status',$delete);
        $this->db->where($where);
        $this->db->where('exam.id not in '.$startwhere);  
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $data=$query->result_array();
            foreach ($data as $key => $value) {
                $data[$key]['discription'] = strip_tags($value['discription']);
                $data[$key]['discription'] = trim(($data[$key]['discription']));
                $date = new DateTime($data[$key]['exam_created_at'], new DateTimeZone('UTC'));
                $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                $data[$key]['exam_created_at'] = $date->format('d-m-Y H:i:s');
                $date_book_date = new DateTime($data[$key]['book_date'], new DateTimeZone('UTC'));
                $date_book_date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                $data[$key]['book_date'] = $date_book_date->format('d-m-Y H:i:s');
                $data[$key]['download_message'] = 'you can download exam question paper.';
            }
            return $data;
        } else {
            return false;
        }
    }

    // public function past_exam_list($user_id){
    //     $active=1;
    //     $delete=0;
    //     $date=date("Y-m-d");
    //     $src=base_url('assets/uploads/subject/');
    //     $where='exam_book.user_id="'.$user_id.'" AND exam.start_date < "'.$date.'"';
    //     $this->db->select(array('exam.id as id','exam.standard_id as standard_id','exam.subject_id as subject_id','exam.exam_title as exam_title','exam.discription as discription','exam.que_mark as que_mark','exam.negative_mark as negative_mark','exam.total_que as total_question','exam.max_student as max_student','exam.min_student as min_student','exam.price as price','DATE_FORMAT(exam.start_date,"%d-%m-%Y") as start_date','exam.start_time as start_time','exam.end_time as end_time','exam.question_time as question_time','exam.exam_time as exam_time','exam.result_status as result_status','exam_book.id as book_id','exam_book.user_id as user_id','exam_book.exam_id as exam_id','exam_book.amount as amount','exam_book.transaction_id as transaction_id','exam_book.transaction_detail as transaction_detail','standard.name as standard_name','subject.name as subject_name','CONCAT("'.$src.'", subject.image) as subject_image','exam.created_at as exam_created_at','exam_book.created_at as book_date','IF((SELECT count(id) FROM tbl_exam_start where exam_id = exam.id AND user_id="'.$user_id.'")>0,"1","0") as exam_start'));
    //     $this->db->from('tbl_exam as exam');
    //     $this->db->join('tbl_exam_book as exam_book','exam.id=exam_book.exam_id','Left');
    //     $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
    //     $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
    //     //$this->db->join('tbl_exam_book as exam_book','exam.id=exam_book.exam_id and user_id="'.$user_id.'"','Left');
    //     $this->db->order_by('exam.start_date', 'DESC');
    //     $this->db->where('exam.status',$active);
    //     $this->db->where('exam.delete_status',$delete);
    //     $this->db->where($where);
    //     $query = $this->db->get();
    //     if($query->num_rows() > 0) {
    //         $data=$query->result_array();
    //         foreach ($data as $key => $value) {
    //             $data[$key]['discription'] = strip_tags($value['discription']);
    //             $data[$key]['discription'] = trim(($data[$key]['discription']));
    //             $date = new DateTime($data[$key]['exam_created_at'], new DateTimeZone('UTC'));
    //             $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
    //             $data[$key]['exam_created_at'] = $date->format('d-m-Y H:i:s');
    //             $date_book_date = new DateTime($data[$key]['book_date'], new DateTimeZone('UTC'));
    //             $date_book_date->setTimezone(new DateTimeZone('Asia/Kolkata'));
    //             $data[$key]['book_date'] = $date_book_date->format('d-m-Y H:i:s');
    //         }
    //         return $data;
    //     } else {
    //         return false;
    //     }
    // }
    public function past_exam_list($user_id){
        $active=1;
        $delete=0;
        $date=date("Y-m-d");
        $src=base_url('assets/uploads/subject/');
        $where='exam_book.user_id="'.$user_id.'" AND exam.start_date < "'.$date.'"';
        $startwhere='(select tbl_exam_start.exam_id from tbl_exam_start where tbl_exam_start.user_id='.$user_id.' and tbl_exam_start.exam_id=exam.id and exam_book.user_id="'.$user_id.'")';
        $this->db->select(array('exam.id as id','exam.standard_id as standard_id','exam.subject_id as subject_id','exam.exam_title as exam_title','exam.discription as discription','exam.que_mark as que_mark','exam.negative_mark as negative_mark','exam.total_que as total_question','exam.max_student as max_student','exam.min_student as min_student','exam.price as price','DATE_FORMAT(exam.start_date,"%d-%m-%Y") as start_date','exam.start_time as start_time','exam.end_time as end_time','exam.question_time as question_time','exam.exam_time as exam_time','exam.result_status as result_status','exam_book.id as book_id','exam_book.user_id as user_id','exam_book.exam_id as exam_id','exam_book.amount as amount','exam_book.transaction_id as transaction_id','exam_book.transaction_detail as transaction_detail','standard.name as standard_name','subject.name as subject_name','CONCAT("'.$src.'", subject.image) as subject_image','exam.created_at as exam_created_at','exam_book.created_at as book_date','IF((SELECT count(id) FROM tbl_exam_start where exam_id = exam.id AND user_id="'.$user_id.'")>0,"1","0") as exam_start'));
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_exam_book as exam_book','exam.id=exam_book.exam_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        //$this->db->join('tbl_exam_book as exam_book','exam.id=exam_book.exam_id and user_id="'.$user_id.'"','Left');
        $this->db->order_by('exam.start_date', 'DESC');
        $this->db->where('exam.status',$active);
        $this->db->where('exam.delete_status',$delete);
        $this->db->where($where);
        $this->db->or_where('exam.id in '.$startwhere);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $data=$query->result_array();
            foreach ($data as $key => $value) {
                $data[$key]['discription'] = strip_tags($value['discription']);
                $data[$key]['discription'] = trim(($data[$key]['discription']));
                $date = new DateTime($data[$key]['exam_created_at'], new DateTimeZone('UTC'));
                $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                $data[$key]['exam_created_at'] = $date->format('d-m-Y H:i:s');
                $date_book_date = new DateTime($data[$key]['book_date'], new DateTimeZone('UTC'));
                $date_book_date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                $data[$key]['book_date'] = $date_book_date->format('d-m-Y H:i:s');
            }
            return $data;
        } else {
            return false;
        }
    }
    public function get_user_data($where=''){
        $this->db->select(array('id AS user_id','fullname','mobile','email'));
        if($where)
            $this->db->where($where);
        $query = $this->db->get(self::USER_TABLE);
        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function get_template_data($where=''){
        $this->db->select(array('main.id as setting_id','main.email_template_id','main.setting_name','main.setting_value','main.setting_code','sub.name as name','sub.subject as subject','sub.description as description','sub.email_type as email_type','sub.from_name as from_name','sub.from_email as from_email','sub.variable as variable'));
        $this->db->from('tbl_setting as main');
        $this->db->join('tbl_email_template as sub','sub.id=main.email_template_id','Left');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        if($query->num_rows() > 0) {
            return $query->row();
        }else{
            return false;
        }
    }
    public function get_exambook_data($where=''){
        $this->db->select(array('exam_book.id as id','exam_book.user_id as user_id','exam_book.exam_id as exam_id','exam_book.amount as amount','exam_book.transaction_id as transaction_id','user.fullname as fullname','user.email as email','user.mobile as mobile','exam.subject_id as subject_id','exam.standard_id as standard_id','exam.exam_title as exam_title','standard.name as standard_name','subject.name as subject_name','exam_book.status as status'));
        $this->db->from('tbl_exam_book as exam_book');
        $this->db->join('tbl_exam as exam','exam.id=exam_book.exam_id','Left');
        $this->db->join('oauth_users as user','user.id=exam_book.user_id','Left');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        if($where)
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }
    // public function get_exam_data($where=''){
    //     $src=base_url('assets/uploads/subject/');
    //     $this->db->select(array('exam.id as id',
    //                             'exam.standard_id as standard_id',
    //                             'exam.subject_id as subject_id',
    //                             'exam.exam_title as exam_title',
    //                             'exam.discription as discription',
    //                             'exam.total_que as total_question',
    //                             'exam.que_mark as que_mark',
    //                             'exam.negative_mark as negative_mark',
    //                             'exam.max_student as max_student',
    //                             'exam.min_student as min_student',
    //                             'exam.price as price',
    //                             'DATE_FORMAT(exam.start_date,"%d-%m-%Y") as start_date',
    //                             'exam.start_time as start_time',
    //                             'exam.end_time as end_time',
    //                             'exam.question_time as question_time',
    //                             'exam.exam_time as exam_time',
    //                             'IF(exam.result_status=0, 0, 1) as result_status',
    //                             'standard.name as standard_name',
    //                             'subject.name as subject_name',
    //                             'CONCAT("'.$src.'", subject.image) as subject_image',
    //                             'exam.created_at as exam_created_at'));
    //     $this->db->from('tbl_exam as exam');
    //     $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
    //     $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
    //     if($where)
    //         $this->db->where($where);
    //     $query = $this->db->get();
    //     if($query->num_rows() > 0) {
    //         $data=$query->row();
    //         if(isset($data->discription)){
    //             $data->discription = strip_tags($data->discription);
    //             $data->discription = trim(($data->discription));
    //         }
    //         if(isset($data->exam_created_at)){
    //             $date = new DateTime($data->exam_created_at, new DateTimeZone('UTC'));
    //             $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
    //             $data->exam_created_at = $date->format('d-m-Y H:i:s');
    //         }
    //         return $data;
    //     } else {
    //         return false;
    //     }
    // }
    public function get_exam_data($where=''){
        $src=base_url('assets/uploads/subject/');
        $this->db->select(array('exam.id as id',
                                'exam.standard_id as standard_id',
                                'exam.subject_id as subject_id',
                                'exam.exam_title as exam_title',
                                'exam.discription as discription',
                                'exam.total_que as total_question',
                                'exam.que_mark as que_mark',
                                'exam.negative_mark as negative_mark',
                                'exam.max_student as max_student',
                                'exam.min_student as min_student',
                                'exam.price as price',
                                'DATE_FORMAT(exam.start_date,"%d-%m-%Y") as start_date',
                                'exam.start_time as start_time',
                                'exam.end_time as end_time',
                                'exam.question_time as question_time',
                                'exam.exam_time as exam_time',
                                'IF(exam.result_status=0, 0, 1) as result_status',
                                'standard.name as standard_name',
                                'subject.name as subject_name',
                                'CONCAT("'.$src.'", subject.image) as subject_image',
                                'exam_book.id as book_id',
                                'exam_book.user_id as user_id',
                                'exam_book.amount as amount',
                                'exam_book.transaction_id as transaction_id',
                                'exam_book.transaction_detail as transaction_detail',
                                'exam_book.created_at as book_date',
                                'exam.created_at as exam_created_at'));
        $this->db->from('tbl_exam as exam');
        $this->db->join('tbl_standard as standard','standard.id=exam.standard_id','Left');
        $this->db->join('tbl_subject as subject','subject.id=exam.subject_id','Left');
        $this->db->join('tbl_exam_book as exam_book','exam.id=exam_book.exam_id','Left');
        if($where)
            $this->db->where($where);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $data=$query->row();
            if(isset($data->discription)){
                $data->discription = strip_tags($data->discription);
                $data->discription = trim(($data->discription));
            }
            if(isset($data->exam_created_at)){
                $date = new DateTime($data->exam_created_at, new DateTimeZone('UTC'));
                $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                $data->exam_created_at = $date->format('d-m-Y H:i:s');
                $date_book_date = new DateTime($data->book_date, new DateTimeZone('UTC'));
                $date_book_date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                $data->book_date = $date_book_date->format('d-m-Y H:i:s');
            }
            return $data;
        } else {
            return false;
        }
    }
    public function get_exam_userResultdata($where=''){
        $src=base_url('assets/uploads/profile/');
        $this->db->select(array('exam_result.user_id as user_id','user.fullname as fullname','user.email as email','IFNULL(user.mobile,0) as mobile','CONCAT("'.$src.'", user.profile_pic) as profile_pic','IFNULL(exam_result.winning_no,0) as rank','IFNULL(exam_result.result,0) as result','exam.total_que as total_que','exam.que_mark as que_mark','(SELECT COUNT(id) FROM tbl_exam_question WHERE exam_id=exam.id) as question_cnt'));
        $this->db->from('tbl_exam_result as exam_result');
        $this->db->join('tbl_exam as exam','exam.id=exam_result.exam_id','Left');
        $this->db->join('oauth_users as user','user.id=exam_result.user_id','Left');
        $this->db->order_by('exam_result.winning_no', 'ASC');
        if($where)
            $this->db->where($where);
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        if($query->num_rows() > 0) {
            $data=$query->result_array();
            foreach ($data as $key => $value) {
                $total_question_cnt = $value['question_cnt'];
                $que_mark = $value['que_mark'];
                $total_score=($value['question_cnt'])*($value['que_mark']);
                $result = $value['result'];
                $user_total_score = ($value['result'])*($value['que_mark']);
                $total=$user_total_score.'/'.$total_score;
                unset($data[$key]['result']);
                unset($data[$key]['total_que']);
                unset($data[$key]['que_mark']);
                unset($data[$key]['question_cnt']);
                $data[$key]['total_score'] = $total;
            }
            return $data;
        } else {
            return false;
        }
    }
    public function check_user_exam_book($where=''){
        $this->db->select('*');
        if($where)
            $this->db->where($where);
        $query = $this->db->get(self::EXAM_BOOK_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function check_user_exam_submit($where=''){
        $this->db->select('*');
        if($where)
            $this->db->where($where);
        $query = $this->db->get(self::EXAM_RESULT_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insert_exam_start($params){
        $this->user_id              = $params['user_id'];
        $this->exam_id              = $params['exam_id'];
        $this->start_date           = $params['start_date'];
        $this->start_time           = $params['start_time'];
        $this->status               = $params['status'];
        $this->created_at           = $params['created_at'];
        $this->db->insert(self::EXAM_START_TABLE, $this);
        $insert_id = $this->db->insert_id();          
        return $insert_id; 
    }
    public function check_user_exam_start($where=''){
        $this->db->select('*');
        if($where)
            $this->db->where($where);
        $query = $this->db->get(self::EXAM_START_TABLE);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function update_exam_start($params,$where){
        if($where)
            $this->db->where($where);
        $this->db->update(self::EXAM_START_TABLE,$params);
        return true;       
    }
    
}