<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apibase 
{
    protected $apiserver;
    protected $db;
    
    public function __construct($params)
    {
        $this->db = $params['dbobj'];
        $this->init();
    }
    
    public function init(){
        
        if(!$this->apiserver){
            
            $db = $this->db;
            $dbhostname = $db->hostname;
            $dbname = $db->database;
            $dbuser = $db->username;
            $dbpass = $db->password;
            
            $dsn      = 'mysql:dbname='.$dbname.';host='.$dbhostname;
            
            // error reporting (this is a demo, after all!)
            //ini_set('display_errors',1);error_reporting(E_ALL);
            
            // Autoloading (composer is preferred, but for this example let's just do this)
            //require_once('oauth2-server-php/src/OAuth2/Autoloader.php');
            OAuth2\Autoloader::register();
            
            // $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
            $storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $dbuser, 'password' => $dbpass));
            
            // Pass a storage object or array of storage objects to the OAuth2 server class
            $this->apiserver = new OAuth2\Server($storage);
            
            // Add the "Client Credentials" grant type (it is the simplest of the grant types)
            $this->apiserver->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));
            
            // Add the "Authorization Code" grant type (this is where the oauth magic happens)
            $this->apiserver->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
        }
        
        return $this->apiserver;
    }
    
    public function generateToken(){

        return $this->apiserver->handleTokenRequest(OAuth2\Request::createFromGlobals());
    }
    
    public function verifyToken(){
        // Handle a request to a resource and authenticate the access token
        if (!$this->apiserver->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
            return false;
        }
        
        return true;
    }
}