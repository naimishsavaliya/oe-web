var NotificationUser_Datatable= {
    init:function() {
        var t;
        var notification_users_data = $("#users_datatableurl").val();

        t=$(".notification_users").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:notification_users_data, map:function(t) {
                            var e=t;
                            return void 0!==t.data&&(e=t.data), e
                        }
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "id", title: "#", sortable: !1, width: 30, selector: !1, textAlign: "center"
            }
            , {
                field: "fullname", title: "Fullname", width: 80
            }
             , {
                field: "email", title: "Email", width: 80
            }
            , {
                field: "mobile", title: "Mobile No.", width: 80
            }
            ]
        }
        )
    }
}

;
jQuery(document).ready(function() {
    NotificationUser_Datatable.init()
});



