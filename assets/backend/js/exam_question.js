var total_que;
var question_list = [];

$(document).ready(function(){
question_list = que_list;
  var max_que= $("#maximum_question").val();
  var actual_selected = 0;
  jQuery.validator.addMethod(
    "required_questions", 
    function(value, element) {
        console.log(question_list.length);
      if(question_list.length!='undefined')
        actual_selected = question_list.length;
      if(max_que == actual_selected){
        return true;
      }
      else
        return false;
      // return this.optional(element) || ;
    },
    "Please select exact "+max_que+" questions."
  );

  $("#question_selection").validate(
    {
        onclick:false,
        rules: 
        {
            mode:
            {
              required:true
            },
            "selected[]": {
              required:true,
              required_questions:true
             }

       },
      messages: 
       {    
            mode:
            {
              required:"Please select mode."
            },
            "selected[]":
            {
              required:"Please select question."
            }
        },
      errorPlacement: function(error, element) 
          {
            if ( element.is(":radio") ) 
            {
                error.appendTo( element.parents('div.m-radio-inline') );
            }
            else if( element.is(":checkbox") ){
              error.appendTo($('.m-radio-inline'));
            }
            else 
            { // This is the default behavior 
                error.insertAfter( element );
            }
          }
    });
    var chk_mode = $('#chk_mode').val();
    if(chk_mode){
        $('#'+chk_mode).trigger('click');
    }
  
});
var data_table;
var exam_question_data;
var data_columns;

$('#manual').on('click',function(){
    var mode = $('#manual').val();
    $('#manual_ajaxdata').show(); 
    $('#auto_ajaxdata').hide();
    exam_question_data = $("#manual_questions_url").val();
    data_table = $('#manual_ajaxdata');
    data_columns = [ 
            {
                field: "id", title: "#", sortable: !1, width: 40, selector: !1, textAlign: "center",template:function(s) {
                    if(s.is_selected=='1')
                        return '<input type="checkbox" name="selected[]" onclick="add_question(this,'+s.id+');" value="'+s.id+'" checked>'
                    else
                        return '<input type="checkbox" name="selected[]" onclick="add_question(this,'+s.id+');" value="'+s.id+'" >'
                }
            }
            , {
                field:"question", title:"Question", width: 100, template:function(q) {
                    var str=q.question;
                    if(str.length >= 30){
                        str = unescape(str);
                        return str.replace(/(<([^>]+)>)/ig,"").substring(0,30);
                    }else{
                        return str
                    } 
                }
            }
            , {
                field: "answer1", title: "Answer 1", width: 80,template:function(q) {
                    if(q.correct_answer=='answer1'){
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer1+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer1+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer1==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer1+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer1;
                        }
                    }
                }
            }
            , {
                field: "answer2", title: "Answer 2", width: 80,template:function(q) {
                    if(q.correct_answer=='answer2'){
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer2+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer2+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer2==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer2+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer2;
                        }
                    }
                }
            }
            , {
                field: "answer3", title: "Answer 3", width: 80,template:function(q) {
                    if(q.correct_answer=='answer3'){
                       if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer3+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer3+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer3==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer3+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer3;
                        }
                    }
                }
            }
            , {
                field: "answer4", title: "Answer 4", width: 80,template:function(q) {
                    if(q.correct_answer=='answer4'){
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer4+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer4+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer4==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer4+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer4;
                        }
                    }
                }
            }
            , {
                field: "answer_type", title: "Type", width: 60
            }
            ];
    exam_question.init();
});
$('#auto').on('click',function(){
    $('label[for="selected[]"]').hide();
    var mode = $('#auto').val();
    $('#manual_ajaxdata').hide(); 
    $('#auto_ajaxdata').show();
    exam_question_data = $("#auto_questions_url").val();
    data_table = $('#auto_ajaxdata');
    data_columns = [
            {
                field: "id", title: "#", sortable: !1, width: 60, selector: !1, textAlign: "center",template:function(s) {
                        return '<input type="hidden" name="auto_selected[]" value="'+s.id+'">'+s.id;        
                }

            } 
            , {
                field:"question", title:"Question", width: 100, template:function(q) {
                    var str=q.question;
                    if(str.length >= 30){
                        str = unescape(str);
                        return str.replace(/(<([^>]+)>)/ig,"").substring(0,30);
                    }else{
                        return str
                    } 
                }
            }
            , {
                field: "answer1", title: "Answer 1", width: 80,template:function(q) {
                    if(q.correct_answer=='answer1'){
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer1+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer1+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer1==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer1+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer1;
                        }
                    }
                }
            }
            , {
                field: "answer2", title: "Answer 2", width: 80,template:function(q) {
                    if(q.correct_answer=='answer2'){
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer2+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer2+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer2==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer2+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer2;
                        }
                    }
                }
            }
            , {
                field: "answer3", title: "Answer 3", width: 80,template:function(q) {
                    if(q.correct_answer=='answer3'){
                       if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer3+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer3+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer3==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer3+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer3;
                        }
                    }
                }
            }
            , {
                field: "answer4", title: "Answer 4", width: 80,template:function(q) {
                    if(q.correct_answer=='answer4'){
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer4+'" style="width:50px;height:50px;">\t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer4+' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                    }
                    else if(q.answer4==''){
                        return '-';
                    }
                    else {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.id+'/'+q.answer4+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.answer4;
                        }
                    }
                }
            }
            , {
                field: "answer_type", title: "Type", width: 60
            }
            ];
    exam_question.init();     
});
var exam_question= {
    init:function() {
        var t;
        t=data_table.mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:exam_question_data, map:function(t) {
                            var e=t;
                            return void 0!==t.data&&(e=t.data), e
                        }
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , columns: data_columns
        }
        )
    }
};
function add_question(element,id){
        $.ajax({
            cache: true,
            url: BASE_URL+"Admin/exams/add_question",
            type: 'POST',
            data: {'id':id},
            dataType: 'json',
            success: function(data) {
                total_que=data.total_count;
                // question_list=data.question_data;
                question_list = $.map(data.question_data, function(value, index) {
                    return [value];
                });
                $(element).valid();
            }
        });
    }

