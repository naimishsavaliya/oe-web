var level_Datatable= {
    init:function() {
        var t;
        var level_data = $("#level_datatableurl").val();
        var edit_url = $("#edit_level_url").val();
        var delete_url = $("#delete_level_url").val();
        t=$(".level_table").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:level_data, map:function(t) {
                            var e=t;
                            return void 0!==t.data&&(e=t.data), e
                        }
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "level_id", title: "#", sortable: !1, width: 40, selector: !1, textAlign: "center"
            }
            , {
                field: "level_name", title: "Level Name", width: 60
            }
            , {
                field: "amount", title: "Amount", width: 80
            }
            , {
                field:"exam_count", title:"Exam count", width: 100
            }
            , {
                field: "parent_level", title: "Parent level", width: 80,template:function(u) {
                    if(u.parent_level==null){
                        return '-'
                    }else{
                        return u.parent_level
                    } 
                }
            }
            , {
                field: "created_at", title: "Created Date", width: 100
            }
            , {
                field:"updated_at", title:"Updated Date", width: 80, template:function(u) {
                    if(u.updated_at=="0000-00-00 00:00:00"){
                        return '-'
                    }else{
                        return u.updated_at
                    } 
                }
            }
            , {
                field:"status", title:"Status", template:function(t) {
                    var e= {
                        0: {
                            title: "Deactive", class: "m-badge--danger"
                        }
                        , 1: {
                            title: "Active", class: " m-badge--success"
                        }
                    }
                    ;
                    if(t.status==1){
                        return'<span onclick="changestatus(this,'+t.level_id+',0);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
                    }else{
                        return'<span onclick="changestatus(this,'+t.level_id+',1);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
                    } 
                }
            }
            , {
                field:"Actions", width:110, title:"Actions", sortable:!1, overflow:"visible", template:function(t, e, a) {
                    return'\t\t\t\t\t\t<a href="'+edit_url+t.level_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\t\t\t\t\t\t\t<i class="la la-edit"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="javascript:void(0);" data-toggle="modal" data-target="#delModal" data-id="'+t.level_id+'" onclick="request_delete_record(this);" id="level_id_'+t.level_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t'
                }
            }
            ]
        }
        )
    }
}

;
jQuery(document).ready(function() {
    level_Datatable.init()
});

function request_delete_record(e)
    {
      $("#del_id").val($(e).data('id'));
    }

function delete_record()
    {
        var level_id = $("#del_id").val();
        $.ajax({
              url: BASE_URL+"Admin/levels/delete/"+level_id,
              type: 'POST',
              data: { },
              success: function (data){
                      $('#delModal').modal('toggle');
                      $('#level_id_'+level_id).closest('tr').remove();
                  }
        });
    }

function changestatus(record,level_id,status)
    {
        $.ajax({
            url: BASE_URL+"Admin/levels/changestatus/"+level_id+"/"+status,
            type: 'POST',
            data: {},
            success: function (data){ 
                if(status=='0'){
                    $(record).attr('onclick','changestatus(this,'+level_id+',1);');
                    $(record).attr('class','m-badge m-badge--danger m-badge--wide');
                    $(record).html('Deactive');
                }
                else{
                    $(record).attr('onclick','changestatus(this,'+level_id+',0);');
                    $(record).attr('class','m-badge  m-badge--success m-badge--wide');
                    $(record).html('Active');
                }
            }
        });
    }
