$(document).ready(function(){
  $("#template_formadd").validate(
    {
      ignore: [],
      debug: false,
        rules: 
        {
            name:
            {
              required:true
            },
            subject:
            {
              required:true
            },
            description:{
                required: function(){
                  var str_count= CKEDITOR.instances.description.getData().length;
                  CKEDITOR.instances.description.on('change', function() {    
                      if(CKEDITOR.instances.description.getData().length >  0) {
                        $('label[for="description"]').hide();
                      }
                  });
                  CKEDITOR.instances.description.updateElement();
                },
                minlength:10
            },
            email_type:
            {
              required:true
            },
            from_name:
            {
              required:true
            },
            from_email:
            {
              required:true,
              email: true
            },
            total_use_count:
            {
              required:true
            }
       },
      messages: 
       {    
            name:
            {
              required:"Please enter name."
            },
            subject:
            {
              required:"Please enter subject."
            },
            description:{
                required:"Please enter description",
                minlength:"Please enter 10 characters"
            },
            email_type:
            {
              required:"Please select email type."
            },
            from_name:
            {
              required:"Please enter from name."
            },
            from_email:
            {
              required:"Please enter from email."
            },
            total_use_count:
            {
              required:"Please enter total use count."
            }
        },
        errorPlacement: function(error, element){
              if(element.attr("name") == "description"){
                  error.appendTo( element.parent("div"));
              }else{
                  error.insertAfter(element);
              }
          }
    });
    $("#editform_template").validate(
      {
        ignore: [],
        debug: false,
          rules: 
          {
              name:
              {
                required:true
              },
              subject:
              {
                required:true
              },
              description:{
                  required: function(){
                    var str_count= CKEDITOR.instances.description.getData().length;
                    CKEDITOR.instances.description.on('change', function() {    
                        if(CKEDITOR.instances.description.getData().length >  0) {
                          $('label[for="description"]').hide();
                        }
                    });
                    CKEDITOR.instances.description.updateElement();
                  },
                  minlength:10
              },
              email_type:
              {
                required:true
              },
              from_name:
              {
                required:true
              },
              from_email:
              {
                required:true,
                email: true
              },
              total_use_count:
              {
                required:true
              }
         },
        messages: 
         {    
              name:
              {
                required:"Please enter name."
              },
              subject:
              {
                required:"Please enter subject."
              },
              description:{
                  required:"Please enter description",
                  minlength:"Please enter 10 characters"
              },
              email_type:
              {
                required:"Please select email type."
              },
              from_name:
              {
                required:"Please enter from name."
              },
              from_email:
              {
                required:"Please enter from email."
              },
              total_use_count:
              {
                required:"Please enter total use count."
              }
          },
          errorPlacement: function(error, element){
                if(element.attr("name") == "description"){
                    error.appendTo( element.parent("div"));
                }else{
                    error.insertAfter(element);
                }
            }
      });
});

CKEDITOR.replace( 'description', {
    customConfig: BASE_URL+'assets/backend/css/ckeditor/config_full.js'
});