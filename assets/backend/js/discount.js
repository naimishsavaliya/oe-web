$(document).ready(function(){
  $("#discount_formadd").validate(
    {
        rules: 
        {
            title:
            {
              required:true
            },
            discount_code:
            {
              required:true
            },
            discount_type:
            {
              required:true
            },
            amount:
            {
              required:true
            },
            per_user_use_count:
            {
              required:true
            },
            total_use_count:
            {
              required:true
            }
       },
      messages: 
       {    
            title:
            {
              required:"Please enter title name."
            },
            discount_code:
            {
              required:"Please enter discount code."
            },
            discount_type:
            {
              required:"Please select discount type."
            },
            amount:
            {
              required:"Please enter amount."
            },
            per_user_use_count:
            {
              required:"Please enter per user use count."
            },
            total_use_count:
            {
              required:"Please enter total use count."
            }
        }
    });
  $("#editform_discount").validate(
    {
        rules: 
        {
            title:
            {
              required:true
            },
            discount_code:
            {
              required:true
            },
            discount_type:
            {
              required:true
            },
            amount:
            {
              required:true
            },
            per_user_use_count:
            {
              required:true
            },
            total_use_count:
            {
              required:true
            }
       },
      messages: 
       {    
            title:
            {
              required:"Please enter title name."
            },
            discount_code:
            {
              required:"Please enter discount code."
            },
            discount_type:
            {
              required:"Please select discount type."
            },
            amount:
            {
              required:"Please enter amount."
            },
            per_user_use_count:
            {
              required:"Please enter per user use count."
            },
            total_use_count:
            {
              required:"Please enter total use count."
            }
        }
    });
});


$(function() {
    $("#amount").keydown(function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        }else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });
});