$(document).ready(function(){
  $("#standardformadd").validate(
    {
        rules: 
        {
            standard_name:
            {
              required:true
            }
       },
      messages: 
       {
            standard_name:
            {
              required:"Standard Name is required."
            }
        },
        submitHandler: function(form) {
            var url_path=$("#standardformadd").attr( 'action' );
            $.ajax({
                url: url_path, 
                type: "POST",             
                data: $("#standardformadd").serialize(),
                cache: false,             
                processData: false, 
                dataType:"json",
                success: function(data){
                  //console.log(data);
                    if($.isEmptyObject(data.error)){
                           window.location.href = $("#standardlist_url").val();       
                    }else{
                      setTimeout(function(){  
                        $("#standard-register-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                        $("#standard-register-error-msg").css('display','block');
                        $("#standard-register-error-msg").html(data.error); 
                              }, 3000);       
                    }
                            
                }
          });
          return false;
        }
    });
    $("#standardformedit").validate(
    {
        rules: 
        {
            standard_name:
            {
              required:true
            }
       },
      messages: 
       {
            standard_name:
            {
              required:"Standard Name is required."
            }
        },
        submitHandler: function(form) {
            var url_path=$("#standardformedit").attr( 'action' );
            $.ajax({
                url: url_path, 
                type: "POST",             
                data: $("#standardformedit").serialize(),
                cache: false,             
                processData: false, 
                dataType:"json",
                success: function(data){
                  //console.log(data);
                  if($.isEmptyObject(data.error)){
                        window.location.href = $("#standardlist_url").val();       
                  }else{
                        setTimeout(function(){  
                          $("#standard-register-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                          $("#standard-register-error-msg").css('display','block');
                          $("#standard-register-error-msg").html(data.error); 
                         }, 3000);       
                  }
                            
                }
          });
          return false;
        }
    });
});

