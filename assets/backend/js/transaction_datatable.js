var transaction_data = $("#transaction_datatableurl").val();
var actual_url = $("#transaction_datatableurl").val();
var option ={
            data: {
                type:"remote", source: {
                    read: {
                        url:transaction_data, map:function(t) {
                            var e=t;
                            return void 0!==t.data&&(e=t.data), e
                        }
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "id", title: "#", sortable: !1, width: 60, selector: !1, textAlign: "center"
            }
            , {
                field: "exam_id", title: "Exam Id", width: 60
            }
            , {
                field: "user_id", title: "User Id", width: 60
            }
             , {
                field: "exam_title", title: "Exam Title", width: 60
            }
             , {
                field: "standard_name", title: "Standard Name", width: 60
            }
             , {
                field: "subject_name", title: "Subject Name", width: 60
            }
            , {
                field: "fullname", title: "Fullname", width: 60
            }
             , {
                field: "email", title: "Email", width: 60
            }
            , {
                field: "mobile", title: "Mobile No.", width: 60
            }
            , {
                field: "amount", title: "Amount", width: 80
            }
            , {
                field:"transaction_id", title:"Transaction Id", width: 100
            }
            , {
                field: "created_at", title: "Created Date", width: 100
            }
            , {
                field:"updated_at", title:"Updated Date", width: 80, template:function(u) {
                    if(u.updated_at=="0000-00-00 00:00:00"){
                        return '-'
                    }else{
                        return u.updated_at
                    } 
                }
            }
            , {
                field:"status", title:"Status", template:function(t) {
                    var e= {
                        0: {
                            title: "Deactive", class: "m-badge--danger"
                        }
                        , 1: {
                            title: "Active", class: " m-badge--success"
                        }
                    }
                    ;
                    if(t.status==1){
                        return'<span onclick="changestatus(this,'+t.id+',0);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
                    }else{
                        return'<span onclick="changestatus(this,'+t.id+',1);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
                    } 
                }
            }
            ]
        };
$('#search').click(function(){
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    t.destroy();
    if(start_date != '' && end_date !=''){
        option.data.source.read.url = actual_url+"/"+start_date+"/"+end_date;
    }else{
        option.data.source.read.url = actual_url;
    }
    t=$(".transaction_table").mDatatable(option);
});
t=$(".transaction_table").mDatatable(option);
function changestatus(record,id,status)
    {
        $.ajax({
            url: BASE_URL+"Admin/exams/transaction_changestatus/"+id+"/"+status,
            type: 'POST',
            data: {},
            success: function (data){ 
                if(status=='0'){
                    $(record).attr('onclick','changestatus(this,'+id+',1);');
                    $(record).attr('class','m-badge m-badge--danger m-badge--wide');
                    $(record).html('Deactive');
                }
                else{
                    $(record).attr('onclick','changestatus(this,'+id+',0);');
                    $(record).attr('class','m-badge  m-badge--success m-badge--wide');
                    $(record).html('Active');
                }
            }
        });
    }

