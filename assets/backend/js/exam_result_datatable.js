var exam_result_data = $("#list_exam_result").val();
var view_question = $("#user_viewquestion").val();
var option ={
            data: {
                type:"remote", source: {
                    read: {
                        url:exam_result_data, map:function(t) {
                            var e=t;
                            return void 0!==t.data&&(e=t.data), e
                        }
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                    field: "id", title: "#", sortable: !1, width: 60, selector: !1, textAlign: "center"
                }
                , {
                    field: "fullname", title: "Full name", width: 70
                }
                ,{
                   field: "email", title: "Email", width: 70
                }
                ,{
                    field: "mobile", title: "Mobile no.", width: 70
                }
                ,{
                    field: "result", title: "Result", width: 70
                }
                , {
                    field:"rank", title:"Rank", width: 30, template:function(u) {
                        if(u.rank==null){
                            return '-'
                        }else{
                            return u.rank
                        } 
                    }
                }
                , {
                    field:"result_date", title:"Result Date", width: 80, template:function(u) {
                        if(u.result_date=="0000-00-00 00:00:00" || u.result_date==null){
                            return '-'
                        }else{
                            return u.result_date
                        } 
                    }
                }
                , {
                    field:"Actions", width:110, title:"Actions", sortable:!1, overflow:"visible", template:function(t, e, a) {
                        return'\t\t\t\t\t\t<a href="'+view_question+t.exam_id+'/'+t.user_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View question">\t\t\t\t\t\t\t<i class="la la-eye"></i>\t\t\t\t\t\t</a>\t\t\t\t\t<a href="javascript:void(0);" data-toggle="modal" data-target="#rankModal" onclick="rank_form(this);" data-user_id="'+t.user_id+'" data-exam_id="'+t.exam_id+'" id="user_id_'+t.user_id+'" class="mm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Add Rank">\t\t\t\t\t\t\t<i class="la la-plus"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t'
                    }
                }
            ]
        };
t=$(".exam_result_table").mDatatable(option);
var rank_user_id, rank_exam_id;
function rank_form(e){
    rank_user_id = $(e).data('user_id');
    rank_exam_id = $(e).data('exam_id');
    var url_path=BASE_URL+"Admin/exams/rank/"+rank_exam_id+"/"+rank_user_id;
    $.ajax({
            url: url_path,  
            type: 'POST',
            data: {},
            success: function (result){
                    var result = JSON.parse(result);
                    $("#Wrapper-notify").html(result); 
              }
    });
}

function closeRankPopup()
{
    $("#RankFrm").validate().resetForm();
    $("#RankFrm").trigger("reset");
    $('#rankModal').modal('hide');
}

$(document).ready(function(){
  $("#RankFrm").validate(
    {
        rules: 
        {
            rank_number:
            {
              required:true
            }
       },
      messages: 
       {
            rank_number:
            {
              required:"Please select rank."
            }
        },
        submitHandler: function(form) {
          var url_path=BASE_URL+"Admin/exams/rank_update/"+rank_exam_id+"/"+rank_user_id;
          $.ajax({
            url: url_path, 
            type: "POST",             
            data: $("#RankFrm").serialize(),
            cache: false,             
            processData: false, 
            dataType:"json",
            success: function(data){
                if($.isEmptyObject(data.error)){
                    t.destroy();
                    t=$(".exam_result_table").mDatatable(option);
                    $('#rankModal').modal('hide');
                    $("#exam-result-error-msg").addClass('m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn');
                    $("#exam-result-error-msg").css('display','block');
                    $("#exam-result-error-msg").html(data.success); 
                    setTimeout(function(){ $("#exam-result-error-msg").hide(); }, 3000); 
                }else{
                    $('#rankModal').modal('hide');
                    $("#exam-result-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                    $("#exam-result-error-msg").css('display','block');
                    $("#exam-result-error-msg").html(data.error); 
                    setTimeout(function(){ $("#exam-result-error-msg").hide(); }, 3000);    
                }           
            }
          });
          return false;  
        }
    });
});


 