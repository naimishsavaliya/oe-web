var exam_data = $("#exam_datatableurl").val();
var actual_url = $("#exam_datatableurl").val();
var edit_url = $("#edit_exam_url").val();
var questionselection_url = $("#questionselection_url").val();
var exam_result_url = $("#exam_result_url").val();
var exam_book_url = $("#exam_book_url").val();
var delete_url = $("#delete_exam_url").val();
var option ={
    data: {
        type:"remote", source: {
            read: {
                url:exam_data, map:function(t) {
                    var e=t;
                    return void 0!==t.data&&(e=t.data), e
                }
            }
        }
        , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
        saveState: {
            cookie: false,
            webstorage: false
        }
    }
    , layout: {
        scroll: !1, footer: !1
    }
    , sortable:!0, pagination:!0, toolbar: {
        items: {
            pagination: {
                pageSizeSelect: [10, 20, 30, 50, 100]
            }
        }
    }
    , search: {
        input: $("#generalSearch")
    }
    , columns: [ {
        field: "id", title: "#", sortable: !1, width: 60, selector: !1, textAlign: "center"
    }
    , {
        field: "subject_name", title: "Subject Name", width: 60
    }
    , {
        field: "standard_name", title: "Standard Name", width: 80
    }
    , {
        field:"exam_title", title:"Exam Title", width: 100, template:function(q) {
            var str=q.exam_title;
            if(str.length >= 30){
                str = unescape(str);
                return str.replace(/(<([^>]+)>)/ig,"").substring(0,30);
            }else{
                return str
            } 
        }
    }
    , {
        field:"start_date", title:"Start Date", width: 80, template:function(u) {
            if(u.start_date=="0000-00-00"){
                return '-'
            }else{
                return u.start_date
            } 
        }
    }
    , {
        field:"start_time", title:"Start time & End time", width: 80, template:function(u) {
            if(u.start_time=="00:00:00" && u.end_time=="00:00:00"){
                return '-'
            }else{
                return u.start_time+' To '+u.end_time
            } 
        }
    }
    , {
        field: "created_at", title: "Created Date", width: 100
    }
    , {
        field:"updated_at", title:"Updated Date", width: 80, template:function(u) {
            if(u.updated_at=="0000-00-00 00:00:00"){
                return '-'
            }else{
                return u.updated_at
            } 
        }
    }
    , {
        field:"status", title:"Status", template:function(t) {
            var e= {
                0: {
                    title: "Deactive", class: "m-badge--danger"
                }
                , 1: {
                    title: "Active", class: " m-badge--success"
                }
            }
            ;
            if(t.status==1){
                return'<span onclick="changestatus(this,'+t.id+',0);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
            }else{
                return'<span onclick="changestatus(this,'+t.id+',1);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
            } 
        }
    }
    , {
        field:"result_status", title:"Result status", template:function(t) {
            var e= {
                0: {
                    title: "Pending", class: "m-badge--danger"
                }
                , 1: {
                    title: "Declare", class: " m-badge--success"
                }
            }
            ;
            if(t.result_status==1){
                return'<span class="m-badge '+e[t.result_status].class+' m-badge--wide">'+e[t.result_status].title+"</span>"
            }else{
                return'<span class="m-badge '+e[t.result_status].class+' m-badge--wide">'+e[t.result_status].title+"</span>"
            } 
        }
    }
    , {
        field:"Actions", width:110, title:"Actions", sortable:!1, overflow:"visible", template:function(t, e, a) {
            var show_question_selection = '';
            if(t.is_selected=='0')
                var show_question_selection = '<a href="'+questionselection_url+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Question selection">\t\t\t\t\t\t\t<i class="la la-check-circle-o"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t';
            return'\t\t\t\t\t<a href="javascript:void(0);" data-toggle="modal" data-target="#dataModal" data-id="'+t.id+'" onclick="get_record(this);" id="exam_id_'+t.id+'" class="mm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View details">\t\t\t\t\t\t\t<i class="la la-eye"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="'+edit_url+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\t\t\t\t\t\t\t<i class="la la-edit"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="javascript:void(0);" data-toggle="modal" data-target="#delModal" data-id="'+t.id+'" onclick="request_delete_record(this);" id="exam_id_'+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="'+questionselection_url+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Question selection">\t\t\t\t\t\t\t<i class="la la-check-circle-o"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="'+exam_result_url+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Exam result">\t\t\t\t\t\t\t<i class="la la-square-o"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="'+exam_book_url+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Exam book">\t\t\t\t\t\t\t<i class="la la-circle-o"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t'
        }
    }]
};
$("#examType").change(function(){
    var type_val=$("#examType").val();
    t.destroy();
    if(type_val=='1' || type_val=='2'){
        option.data.source.read.url = actual_url+"/"+type_val;
    }else{
        option.data.source.read.url = actual_url;
    }
    t=$(".exam_table").mDatatable(option);
});

t=$(".exam_table").mDatatable(option);

function request_delete_record(e)
{
  $("#del_id").val($(e).data('id'));
}

function delete_record()
{
    var id = $("#del_id").val();
    $.ajax({
          url: BASE_URL+"Admin/exams/delete/"+id,
          type: 'POST',
          data: { },
          success: function (data){
                  $('#delModal').modal('toggle');
                  $('#exam_id_'+id).closest('tr').remove();
              }
    });
}

function changestatus(record,id,status)
{
    // console.log($(record).attr('onclick'));
    $.ajax({
        url: BASE_URL+"Admin/exams/changestatus/"+id+"/"+status,
        type: 'POST',
        data: {},
        success: function (data){ 
            if(status=='0'){
                $(record).attr('onclick','changestatus(this,'+id+',1);');
                $(record).attr('class','m-badge m-badge--danger m-badge--wide');
                $(record).html('Deactive');
            }
            else{
                $(record).attr('onclick','changestatus(this,'+id+',0);');
                $(record).attr('class','m-badge  m-badge--success m-badge--wide');
                $(record).html('Active');
            }
        }
    });
}

function get_record(e){
    var id = $(e).data('id');
    var url_path=BASE_URL+"Admin/exams/view/"+id;
    $.ajax({
            url: url_path,  
            type: 'POST',
            data: {},
            success: function (result){
                    var result = JSON.parse(result);
                    $("#Wrapper-notify").html(result); 
              }
    });
}   