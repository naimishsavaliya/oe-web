$(document).ready(function(){

  // jQuery.validator.addMethod("alphanumeric", function(value, element) {
  //     return this.optional(element) || /^[\w.]+$/i.test(value);
  // }, "Letters, numbers, and underscores only please");
  jQuery.validator.addMethod("alphanumeric", function(value, element) {
      return this.optional(element) || /^[a-zA-Z0-9_]+$/i.test(value);
  }
  , "Letters, numbers or underscores only please"); 

  $("#setting_form").validate(
    {
        rules: 
        {
            email_template_id:
            {
              required:true
            },
            setting_name:
            {
              required:true
            },
            setting_value:
            {
              required:true
            },
            setting_code:
            {
              required:true,
              alphanumeric: true
            }
       },
      messages: 
       {    
            email_template_id:
            {
              required:"Please select name."
            },
            setting_name:
            {
              required:"Please enter setting name."
            },
            setting_value:
            {
              required:"Please select setting value."
            },
            setting_code:
            {
              required:"Please enter setting code."
            }
        }
    });
});
