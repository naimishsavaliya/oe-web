$(document).ready(function(){
  $("#image").on("change", function(e) {
    $(this).valid();
  });
  $("#subjectformadd").validate(
    {
        rules: 
        {
            subject_name:
            {
              required:true
            },
            image:
            {
              required: true,
              accept: "image/*"
            }
       },
      messages: 
       {
            subject_name:
            {
              required:"Subject Name is required."
            },
            image:
            {
              required:"image is required.",
              accept: "Please enter valid image extension"
            }
        },
        submitHandler: function(form) {
            var formData = new FormData(form);
            var url_path=$("#subjectformadd").attr( 'action' );
            $.ajax({
                url: url_path, 
                type: "POST",             
                data: formData,
                cache: false,             
                processData: false, 
                dataType:"json",
                mimeType: "multipart/form-data",
                contentType: false,
                success: function(data){
                  //console.log(data);
                    if($.isEmptyObject(data.error)){
                           window.location.href = $("#subjectlist_url").val();       
                    }else{
                      setTimeout(function(){  
                        $("#subject-register-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                        $("#subject-register-error-msg").css('display','block');
                        $("#subject-register-error-msg").html(data.error); 
                                            }, 3000);       
                    }
                            
                }
          });
          return false;
        }
    });
    $("#subjectformedit").validate(
    {
        rules: 
        {
            subject_name:
            {
              required:true
            },
            image:
            {
              accept: "image/*"
            }
       },
      messages: 
       {
            subject_name:
            {
              required:"Subject Name is required."
            },
            image:
            {
              accept: "Please enter valid image extension"
            }
        },
        submitHandler: function(form) {
            var formData = new FormData(form);
            var url_path=$("#subjectformedit").attr( 'action' );
            $.ajax({
                url: url_path, 
                type: "POST",             
                data: formData,
                cache: false,             
                processData: false, 
                dataType:"json",
                mimeType: "multipart/form-data",
                contentType: false,
                success: function(data){
                  //console.log(data);
                  if($.isEmptyObject(data.error)){
                        window.location.href = $("#subjectlist_url").val();       
                  }else{
                        setTimeout(function(){  
                          $("#subject-register-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                          $("#subject-register-error-msg").css('display','block');
                          $("#subject-register-error-msg").html(data.error); 
                         }, 3000);       
                  }
                            
                }
          });
          return false;
        }
    });
});

