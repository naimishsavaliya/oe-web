$(document).ready(function(){
  $("#questionformedit").validate(
    {
      ignore: [],
      debug: false,
      rules: 
      {
        subject_id:
        {
          required:true
        },
        standard_id:
        {
          required:true
        },
        question:{
            required: function() 
            {
              //CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });
                CKEDITOR.instances.question.updateElement();
                CKEDITOR.instances.question.on('change', function() {    
                  if(CKEDITOR.instances.question.getData().length >  0) {
                    $('label[for="question"]').hide();
                  }
              });
            },
            minlength:10
        }
      },
      messages: 
      {
        subject_id:
        {
          required:"Please select subject name."
        },
        standard_id:
        {
          required:"Please select standard name."
        },
        question:{
            required:"Please enter question",
            minlength:"Please enter 10 characters"
        }
      },
      errorPlacement: function(error, element) 
          {
              if(element.attr("name") == "question") 
              {
                  error.appendTo( element.parent("div"));
              }
             else 
             {
                  error.insertAfter(element);
             }
          }
  });
});

