$(document).ready(function(){
  $("#exam_formadd").validate(
    {
        ignore: [],
        debug: false,
        rules: 
        {
            subject_id:
            {
              required:true
            },
            standard_id:
            {
              required:true
            },
            exam_title:
            {
              required:true
            },
            description:{
              required: function() 
              {
                var str_count= CKEDITOR.instances.description.getData().length;
                CKEDITOR.instances.description.on('change', function() {    
                  if(CKEDITOR.instances.description.getData().length >  0) {
                      $('label[for="description"]').hide();
                    }
                  });
                CKEDITOR.instances.description.updateElement();
                },
                minlength:10
            },
            que_mark:
            {
              required:true,
               maxlength: 4
            },
            negative_mark:
            {
              required:true,
               maxlength: 4
            },
            total_que:
            {
              required:true,
               maxlength: 4
            },
            max_student:
            {
              required:true
            },
            min_student:
            {
              required:true
            },
            price:
            {
              required:true
            },
            start_date:
            {
              required:true
            },
            start_time:
            {
              required:true
            },
            end_time:
            {
              required:true
            },
            question_time:
            {
              required:true,
              maxlength: 3
            },
            exam_time:
            {
              required:true,
              maxlength: 3
            }
       },
      messages: 
       {    
            subject_id:
            {
              required:"Please select subject name."
            },
            standard_id:
            {
              required:"Please select standard name."
            },
            exam_title:
            {
              required:"Exam title is required."
            },
            description:{
                required:"Please enter description"
            },
            que_mark:
            {
              required:"Question mark is required."
            },
            negative_mark:
            {
              required:"Negative mark is required."
            },
            total_que:
            {
              required:"Total question is required."
            },
            max_student:
            {
              required:"Max student is required."
            },
            min_student:
            {
              required:"Min student is required."
            },
            price:
            {
              required:"Price is required."
            },
            start_date:
            {
              required:"Start date is required."
            },
            start_time:
            {
              required:"Start time is required."
            },
            end_time:
            {
              required:"End time is required."
            },
            question_time:
            {
              required:"Question time is required."
            },
            exam_time:
            {
              required:"Exam time is required."
            }
        },
      errorPlacement: function(error, element) 
          {
              if(element.attr("name") == "description") 
              {
                  error.appendTo( element.parent("div"));
              }
             else 
             {
                  error.insertAfter(element);
             }
          }
    });
});

// $("#amount").on("keyup", function(){
//     var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
//         val = this.value;
    
//     if(!valid){
//         console.log("Invalid input!");
//         this.value = val.substring(0, val.length - 1);
//     }
// });

// function isNumberKey(evt)
// {
//   var charCode = (evt.which) ? evt.which : event.keyCode
//     if (charCode > 31 && (charCode < 48 || charCode > 57))
//     return false;
//     return true;
// }

$(function() {
    $("#price").keydown(function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        }else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });
});

$(function() {
    $("#negative_mark").keydown(function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        }else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });
});