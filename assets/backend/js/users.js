$(document).ready(function(){
  $("#userformadd").validate(
    {
        rules: 
        {
            fullname:
            {
              required:true
            },
            email:
            {
              required:true,
              email: true
            },
            mobile:
            {
              required:true,
               minlength: 10,
               maxlength:15
            },
            password:
            {
              required:true
            }
       },
      messages: 
       {
            fullname:
            {
              required:"Full Name is required."
            },
            email:
            {
              required:"Email is required."
            },
            mobile:
            {
              required:"Mobile is required."
            },
            password:
            {
              required:"Password is required."
            }
        },
        submitHandler: function(form) {
            var url_path=$("#userformadd").attr( 'action' );
            $.ajax({
                url: url_path, 
                type: "POST",             
                data: $("#userformadd").serialize(),
                cache: false,             
                processData: false, 
                dataType:"json",
                success: function(data){
                  //console.log(data);
                    if($.isEmptyObject(data.error)){
                           window.location.href = $("#userlist_url").val();       
                    }else{
                      setTimeout(function(){  $("#user-register-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                                              $("#user-register-error-msg").css('display','block');
                                              $("#user-register-error-msg").html(data.error); 
                                            }, 3000);       
                    }
                            
                }
          });
          return false;
        }
    });
    $("#userformedit").validate(
    {
        rules: 
        {
            fullname:
            {
              required:true
            },
            email:
            {
              required:true,
              email: true
            },
            mobile:
            {
              required:true,
               minlength: 10,
              maxlength:15
            }
       },
      messages: 
       {
            fullname:
            {
              required:"Full Name is required."
            },
            email:
            {
              required:"Email is required."
            },
            mobile:
            {
              required:"Mobile is required."
            }
        },
        submitHandler: function(form) {
            var url_path=$("#userformedit").attr( 'action' );
            $.ajax({
                url: url_path, 
                type: "POST",             
                data: $("#userformedit").serialize(),
                cache: false,             
                processData: false, 
                dataType:"json",
                success: function(data){
                  //console.log(data);
                  if($.isEmptyObject(data.error)){
                        window.location.href = $("#userlist_url").val();       
                  }else{
                        setTimeout(function(){  
                          $("#user-register-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                          $("#user-register-error-msg").css('display','block');
                          $("#user-register-error-msg").html(data.error); 
                         }, 3000);       
                  }
                            
                }
          });
          return false;
        }
    });
});

// function isNumberKey(evt)
// {
//   var charCode = (evt.which) ? evt.which : event.keyCode
//     if (charCode > 31 && (charCode < 48 || charCode > 57))
//     return false;
//     return true;
// }