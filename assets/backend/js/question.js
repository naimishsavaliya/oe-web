$('#answer_type').change(function(){
    if($("#answer_type").val()=='text2'){
        $(".text2-required").attr('required','');
        $(".text4-required").removeAttr('required');
        $(".image2-required").removeAttr('required');
        $(".image4-required").removeAttr('required');
        $('#text2').show(); 
        $('#text4').hide();
        $('#image2').hide();
        $('#image4').hide();        
    }else if($("#answer_type").val()=='text4'){
        $(".text4-required").attr('required','');
        $(".text2-required").removeAttr('required');
        $(".image2-required").removeAttr('required');
        $(".image4-required").removeAttr('required');
        $('#text2').hide(); 
        $('#text4').show();
        $('#image2').hide();
        $('#image4').hide();
    }else if($("#answer_type").val()=='image2'){
        $(".image2-required").attr('required','');
        $(".image4-required").removeAttr('required');
        $(".text2-required").removeAttr('required');
        $(".text4-required").removeAttr('required');
        $('#text2').hide(); 
        $('#text4').hide();
        $('#image2').show();
        $('#image4').hide();
    }else if($("#answer_type").val()=='image4'){
        $(".image4-required").attr('required','');
        $(".image2-required").removeAttr('required');
        $(".text2-required").removeAttr('required');
        $(".text4-required").removeAttr('required');
        $('#text2').hide(); 
        $('#text4').hide();
        $('#image2').hide();
        $('#image4').show();
    }else{
        $('#text2').hide(); 
        $('#text4').hide();
        $('#image2').hide();
        $('#image4').hide();
    }
});

$(document).ready(function(){
  $("#questionformadd").validate(
    {
      ignore: [],
      debug: false,
      rules: 
      {
        subject_id:
        {
          required:true
        },
        standard_id:
        {
          required:true
        },
        question:{
            required: function() 
            {
              //CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });
              var str_count= CKEDITOR.instances.question.getData().length;
              CKEDITOR.instances.question.on('change', function() {    
                  if(CKEDITOR.instances.question.getData().length >  0) {
                    $('label[for="question"]').hide();
                  }
              });
              CKEDITOR.instances.question.updateElement();
            },
            minlength:10
        },
        answer_type:
        {
          required:true
        }
      },
      messages: 
      {
        subject_id:
        {
          required:"Please select subject name."
        },
        standard_id:
        {
          required:"Please select standard name."
        },
        answer_type:
        {
          required:"Please select answer type."
        },
        question:{
            required:"Please enter question",
            minlength:"Please enter 10 characters"
        }
      },
      errorPlacement: function(error, element) 
          {
              if(element.attr("name") == "question") 
              {
                  error.appendTo( element.parent("div"));
              }
             else 
             {
                  error.insertAfter(element);
             }
          }
  });
});