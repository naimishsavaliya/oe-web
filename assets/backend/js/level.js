$(document).ready(function(){
  $("#level_formadd").validate(
    {
        rules: 
        {
            level_name:
            {
              required:true
            },
            amount:
            {
              required:true
            },
            exam_count:
            {
              required:true
            }
       },
      messages: 
       {    
            level_name:
            {
              required:"Please enter level name."
            },
            amount:
            {
              required:"Please enter amount."
            },
            exam_count:
            {
              required:"Please enter exam count."
            }
        }
    });
  $("#editform_level").validate(
    {
        rules: 
        {
            level_name:
            {
              required:true
            },
            amount:
            {
              required:true
            },
            exam_count:
            {
              required:true
            }
       },
      messages: 
       {    
            level_name:
            {
              required:"Please enter level name."
            },
            amount:
            {
              required:"Please enter amount."
            },
            exam_count:
            {
              required:"Please enter exam count."
            }
        }
    });
});


$(function() {
    $("#amount").keydown(function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        }else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });
});