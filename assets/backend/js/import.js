$(document).ready(function(){
  $("#zip_file").on("change", function(e) {
    $(this).valid();
  });
  $("#import_file").validate(
    {
        rules: 
        {
            subject_id:
            {
              required:true
            },
            standard_id:
            {
              required:true
            },
            zip_file:
            {
              required: true,
              extension: "zip"
            }
       },
      messages: 
       {
            subject_id:
            {
              required:"Please select subject name."
            },
            standard_id:
            {
              required:"Please select standard name."
            },
            zip_file:
            {
              required:"zip file is required.",
              extension: "Please enter valid extension"
            }
        }
    });
});

