var UserQuestion_Datatable= {
    init:function() {
        var t;
        var list_question = $("#list_question").val();
        t=$(".user_question_table").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:list_question, map:function(t) {
                            var e=t;
                            return void 0!==t.data&&(e=t.data), e
                        }
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                    field: "question_id", title: "#", sortable: !1, width: 40, selector: !1, textAlign: "center"
                }
                , {
                    field:"question", title:"Question", width: 150, template:function(q) {
                        var str=q.question;
                        if(str.length >= 30){
                            str = unescape(str);
                            return str.replace(/(<([^>]+)>)/ig,"").substring(0,30);
                        }else{
                            return str
                        } 
                    }
                }
                , {
                    field: "user_answer", title: "User Answer", width: 100
                }
                , {
                    field: "c_ans", title: "Correct Answer", width: 80,template:function(q) {
                        if(q.answer_type=='image2' || q.answer_type=='image4'){
                            return '<img src="'+BASE_URL+'assets/uploads/question/'+q.question_id+'/'+q.c_ans+'" style="width:50px;height:50px;">';
                        }else if(q.answer_type=='text2' || q.answer_type=='text4'){
                            return q.c_ans;
                        }
                    }
                }
                , {
                    field: "sign", title: "Result", width: 80,template:function(q) {
                        if(q.sign=='1'){
                                return ' \t\t\t\t\t\t<i class="fa fa-check-square" style="color:green;"></i>';
                        }
                        else {
                            return ' \t\t\t\t\t\t<i class="fa fa-close" style="color:red;"></i>';
                        }
                    }
                }
            ]
        }
        )
    }
}

;
jQuery(document).ready(function() {
    UserQuestion_Datatable.init()
});

 