$('#send_now').on('click',function(){
  $('#n_date').hide();
  $(".n_date-required").removeAttr('required');
});
$('#schedule').on('click',function(){
  $('#n_date').show();
  $(".n_date-required").attr('required','');
});
$(document).ready(function(){
  $("#notification_formadd").validate({
        rules:{
            "mul_user[]":
            {
              required:true
            },
            message:
            {
              required:true
            },
            notification_type:
            {
              required:true
            }
       },
      messages: 
       {    
            "mul_user[]":
            {
              required:"Please select user."
            },
            message:
            {
              required:"Please enter message."
            },
            notification_type:
            {
              required:"Please select type."
            }         
        },
        errorPlacement: function(error, element){
              if(element.attr("name") == "message"){
                  error.appendTo( element.parent("div"));
              }else{
                  error.insertAfter(element);
              }
          }
    });
  $("#notification_formedit").validate({
        rules:{
            "mul_user[]":
            {
              required:true
            },
            message:
            {
              required:true
            },
            notification_type:
            {
              required:true
            }
       },
      messages: 
       {    
            "mul_user[]":
            {
              required:"Please select user."
            },
            message:
            {
              required:"Please enter message."
            },
            notification_type:
            {
              required:"Please select type."
            }        
        },
        errorPlacement: function(error, element){
              if(element.attr("name") == "message"){
                  error.appendTo( element.parent("div"));
              }else{
                  error.insertAfter(element);
              }
          }
    });
  var radioValue = $("input[name='notification_mode']:checked").val();
  if(radioValue=='schedule'){
      $("input:radio[name=notification_mode]").click();
  }
});
