var Notification_Datatable= {
    init:function() {
        var t;
        var notification_data = $("#notification_datatableurl").val();
        var edit_url = $("#edit_notification_url").val();
        var delete_url = $("#delete_notification_url").val();
        var notification_user_url = $("#notification_user_url").val();
        t=$(".notification_table").mDatatable( {
            data: {
                type:"remote", source: {
                    read: {
                        url:notification_data, map:function(t) {
                            var e=t;
                            return void 0!==t.data&&(e=t.data), e
                        }
                    }
                }
                , pageSize:10, serverPaging:!0, serverFiltering:!0, serverSorting:!0,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
            }
            , layout: {
                scroll: !1, footer: !1
            }
            , sortable:!0, pagination:!0, toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            }
            , search: {
                input: $("#generalSearch")
            }
            , columns:[ {
                field: "id", title: "#", sortable: !1, width: 40, selector: !1, textAlign: "center"
            }
            , {
                field: "subject", title: "Subject", width: 60
            }
            , {
                field:"message", title:"Message", width: 200, template:function(q) {
                    var str=q.message;
                    if(str.length >= 30){
                        str = unescape(str);
                        return str.replace(/(<([^>]+)>)/ig,"").substring(0,30);
                    }else{
                        return str
                    } 
                }
            }
            , {
                field:"notification_type", title:"Type", template:function(t) {
                    var e= {
                        1: {
                            title: "Push", class: "m-badge--info"
                        }
                        , 2: {
                            title: "Email", class: " m-badge--primary"
                        }
                    }
                    ;
                    if(t.notification_type==1){
                        return'<span class="m-badge '+e[t.notification_type].class+' m-badge--wide">'+e[t.notification_type].title+"</span>"
                    }else{
                        return'<span class="m-badge '+e[t.notification_type].class+' m-badge--wide">'+e[t.notification_type].title+"</span>"
                    } 
                }
            }
            , {
                field:"notification_date", title:"Date", width: 80, template:function(u) {
                    if(u.notification_date=="0000-00-00 00:00:00" || u.notification_date==null){
                        return '-'
                    }else{
                        return u.notification_date
                    } 
                }
            }
            , {
                field: "created_at", title: "Created Date", width: 80
            }
            , {
                field:"updated_at", title:"Updated Date", width: 80, template:function(u) {
                    if(u.updated_at=="0000-00-00 00:00:00"){
                        return '-'
                    }else{
                        return u.updated_at
                    } 
                }
            }
            , {
                field:"status", title:"Status", template:function(t) {
                    var e= {
                        0: {
                            title: "Pending", class: "m-badge--danger"
                        }
                        , 1: {
                            title: "Send", class: " m-badge--success"
                        }
                    }
                    ;
                    if(t.status==1){
                        return'<span onclick="changestatus(this,'+t.id+',0);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
                    }else{
                        return'<span onclick="changestatus(this,'+t.id+',1);" class="m-badge '+e[t.status].class+' m-badge--wide">'+e[t.status].title+"</span>"
                    } 
                }
            }
            , {
                field:"Actions", width:110, title:"Actions", sortable:!1, overflow:"visible", template:function(t, e, a) {
                    return'\t\t\t\t\t\t<a href="'+edit_url+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\t\t\t\t\t\t\t<i class="la la-edit"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="javascript:void(0);" data-toggle="modal" data-target="#delModal" data-id="'+t.id+'" onclick="request_delete_record(this);" id="notification_id_'+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="'+notification_user_url+t.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Users">\t\t\t\t\t\t\t<i class="la la-circle-o"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t'
                }
            }
            ]
        }
        )
    }
}

;
jQuery(document).ready(function() {
    Notification_Datatable.init()
});

function request_delete_record(e)
    {
      $("#del_id").val($(e).data('id'));
    }

function delete_record()
    {
        var id = $("#del_id").val();
        $.ajax({
              url: BASE_URL+"Admin/notification/delete/"+id,
              type: 'POST',
              data: { },
              success: function (data){
                      $('#delModal').modal('toggle');
                      $('#notification_id_'+id).closest('tr').remove();
                  }
        });
    }

function changestatus(record,id,status)
    {
        $.ajax({
            url: BASE_URL+"Admin/notification/changestatus/"+id+"/"+status,
            type: 'POST',
            data: {},
            success: function (data){ 
                if(status=='0'){
                    $(record).attr('onclick','changestatus(this,'+id+',1);');
                    $(record).attr('class','m-badge m-badge--danger m-badge--wide');
                    $(record).html('Pending');
                }
                else{
                    $(record).attr('onclick','changestatus(this,'+id+',0);');
                    $(record).attr('class','m-badge  m-badge--success m-badge--wide');
                    $(record).html('Send');
                }
            }
        });
    }
