$(document).ready(function(){
    $("#changepasswordform").validate(
    {
        rules: 
        {
            new_password:
            {
              required:true,
              minlength: 5
            },
            r_password: {
              minlength: 5,
              equalTo: "#new_password"            
            } 
       },
      messages: 
       {
            new_password: {
              required: 'New password is required.',  
              minlength: 'New password should be atleast 5 characters.'        
            },  
            r_password: {
              minlength: 'Confirm new password should be atleast 5 characters.'                
            }
        },
        submitHandler: function(form) {
            var url_path=$("#changepasswordform").attr( 'action' );
            $.ajax({
                url: url_path, 
                type: "POST",             
                data: $("#changepasswordform").serialize(),
                cache: false,             
                processData: false, 
                dataType:"json",
                success: function(data){
                  //console.log(data);
                  if($.isEmptyObject(data.error)){
                        window.location.href = $("#userlist_url").val();
                        //console.log(data.success);      
                  }else{
                        setTimeout(function(){  
                          $("#password-register-error-msg").addClass('m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn');
                          $("#password-register-error-msg").css('display','block');
                          $("#password-register-error-msg").html(data.error); 
                         }, 3000);       
                  }
                            
                }
          });
          return false;
        }
    });
});

